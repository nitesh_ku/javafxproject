package com.onesoft.courier.invoice.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class InvoiceBillJavaFXBean
{

	private SimpleIntegerProperty invoiceserialno;
	private SimpleStringProperty invoiceNumber;
	private SimpleStringProperty invoiceDate;
	private SimpleStringProperty from_date;
	private SimpleStringProperty to_date;
	private SimpleStringProperty invoiceClientCode;
	private SimpleStringProperty invoiceClientName;
	private SimpleStringProperty invoiceSubClient;
	private SimpleStringProperty invoiceRemarks;
	private SimpleStringProperty discount_additional;
	private SimpleStringProperty type_flat2percent;
	private SimpleDoubleProperty rateof_percent;
	private SimpleDoubleProperty discount_Additional_amount;
	
	private SimpleDoubleProperty invoiceGrandTotal;
	private SimpleDoubleProperty invoiceServiceTax;
	
	private SimpleStringProperty invoiceBranch;
	private SimpleStringProperty status;
	
	
	public InvoiceBillJavaFXBean(int invoiceserialno, String invoicenumber, String invoicedate, String invoiceBranch, String from_date, String to_date, String invoiceClientCode,String clientname,String invoiceSubClient,double servicetax,double grandtotal,String remarks,String status)
	{
	
		super();
		this.invoiceserialno=new SimpleIntegerProperty(invoiceserialno);
		
		this.invoiceNumber=new SimpleStringProperty(invoicenumber);
		this.invoiceBranch=new SimpleStringProperty(invoiceBranch);
		this.invoiceDate=new SimpleStringProperty(invoicedate);
		this.from_date=new SimpleStringProperty(from_date);
		this.to_date=new SimpleStringProperty(to_date);
		this.invoiceClientCode=new SimpleStringProperty(invoiceClientCode);
		this.invoiceSubClient=new SimpleStringProperty(invoiceSubClient);
		this.invoiceClientName=new SimpleStringProperty(clientname);
		this.invoiceGrandTotal=new SimpleDoubleProperty(grandtotal);
		this.invoiceServiceTax=new SimpleDoubleProperty(servicetax);
		this.invoiceRemarks=new SimpleStringProperty(remarks);
		this.setStatus(new SimpleStringProperty(status));
		
	}
	
	
	public int getInvoiceserialno() {
		return invoiceserialno.get();
	}
	public void setInvoiceserialno(SimpleIntegerProperty invoiceserialno) {
		this.invoiceserialno = invoiceserialno;
	}
	public String getInvoiceNumber() {
		return invoiceNumber.get();
	}
	public void setInvoiceNumber(SimpleStringProperty invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getInvoiceDate() {
		return invoiceDate.get();
	}
	public void setInvoiceDate(SimpleStringProperty invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getFrom_date() {
		return from_date.get();
	}
	public void setFrom_date(SimpleStringProperty from_date) {
		this.from_date = from_date;
	}
	public String getTo_date() {
		return to_date.get();
	}
	public void setTo_date(SimpleStringProperty to_date) {
		this.to_date = to_date;
	}
	public String getInvoiceClientCode() {
		return invoiceClientCode.get();
	}
	public void setInvoiceClientCode(SimpleStringProperty invoiceClientCode) {
		this.invoiceClientCode = invoiceClientCode;
	}
	public String getInvoiceClientName() {
		return invoiceClientName.get();
	}
	public void setInvoiceClientName(SimpleStringProperty invoiceClientName) {
		this.invoiceClientName = invoiceClientName;
	}
	public String getInvoiceSubClient() {
		return invoiceSubClient.get();
	}
	public void setInvoiceSubClient(SimpleStringProperty invoiceSubClient) {
		this.invoiceSubClient = invoiceSubClient;
	}
	public String getInvoiceRemarks() {
		return invoiceRemarks.get();
	}
	public void setInvoiceRemarks(SimpleStringProperty invoiceRemarks) {
		this.invoiceRemarks = invoiceRemarks;
	}
	public double getInvoiceGrandTotal() {
		return invoiceGrandTotal.get();
	}
	public void setInvoiceGrandTotal(SimpleDoubleProperty invoiceGrandTotal) {
		this.invoiceGrandTotal = invoiceGrandTotal;
	}
	public double getInvoiceServiceTax() {
		return invoiceServiceTax.get();
	}
	public void setInvoiceServiceTax(SimpleDoubleProperty invoiceServiceTax) {
		this.invoiceServiceTax = invoiceServiceTax;
	}
	public String getInvoiceBranch() {
		return invoiceBranch.get();
	}
	public void setInvoiceBranch(SimpleStringProperty invoiceBranch) {
		this.invoiceBranch = invoiceBranch;
	}	
	
	public String getDiscount_additional() {
		return discount_additional.get();
	}


	public void setDiscount_additional(SimpleStringProperty discount_additional) {
		this.discount_additional = discount_additional;
	}


	public String getType_flat2percent() {
		return type_flat2percent.get();
	}


	public void setType_flat2percent(SimpleStringProperty type_flat2percent) {
		this.type_flat2percent = type_flat2percent;
	}


	public double getRateof_percent() {
		return rateof_percent.get();
	}


	public void setRateof_percent(SimpleDoubleProperty rateof_percent) {
		this.rateof_percent = rateof_percent;
	}


	public double getDiscount_Additional_amount() {
		return discount_Additional_amount.get();
	}


	public void setDiscount_Additional_amount(SimpleDoubleProperty discount_Additional_amount) {
		this.discount_Additional_amount = discount_Additional_amount;
	}


	public String getStatus() {
		return status.get();
	}


	public void setStatus(SimpleStringProperty status) {
		this.status = status;
	}
	
}
