package com.onesoft.courier.invoice.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

public class InvoiceBillBean {
	
	private int invoiceserialno;
	private String invoiceNumber;
	private String invoiceDate;
	private String from_date;
	private String to_date;
	private String invoiceClientCode;
	private String invoiceClientName;
	private String invoiceSubClient;
	private String invoiceRemarks;
	private double invoiceGrandTotal;
	private double invoiceServiceTax;
	private String invoiceBranch;
	
	private String discount_additional;
	private String type_flat2percent;
	private Double rateof_percent;
	private Double discount_Additional_amount;
	
	
	public int getInvoiceserialno() {
		return invoiceserialno;
	}
	public String getDiscount_additional() {
		return discount_additional;
	}
	public void setDiscount_additional(String discount_additional) {
		this.discount_additional = discount_additional;
	}
	public String getType_flat2percent() {
		return type_flat2percent;
	}
	public void setType_flat2percent(String type_flat2percent) {
		this.type_flat2percent = type_flat2percent;
	}
	public Double getRateof_percent() {
		return rateof_percent;
	}
	public void setRateof_percent(Double rateof_percent) {
		this.rateof_percent = rateof_percent;
	}
	public Double getDiscount_Additional_amount() {
		return discount_Additional_amount;
	}
	public void setDiscount_Additional_amount(Double discount_Additional_amount) {
		this.discount_Additional_amount = discount_Additional_amount;
	}
	public void setInvoiceserialno(int invoiceserialno) {
		this.invoiceserialno = invoiceserialno;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getFrom_date() {
		return from_date;
	}
	public void setFrom_date(String from_date) {
		this.from_date = from_date;
	}
	public String getTo_date() {
		return to_date;
	}
	public void setTo_date(String to_date) {
		this.to_date = to_date;
	}
	public String getInvoiceClientCode() {
		return invoiceClientCode;
	}
	public void setInvoiceClientCode(String invoiceClientCode) {
		this.invoiceClientCode = invoiceClientCode;
	}
	public String getInvoiceClientName() {
		return invoiceClientName;
	}
	public void setInvoiceClientName(String invoiceClientName) {
		this.invoiceClientName = invoiceClientName;
	}
	public String getInvoiceSubClient() {
		return invoiceSubClient;
	}
	public void setInvoiceSubClient(String invoiceSubClient) {
		this.invoiceSubClient = invoiceSubClient;
	}
	public String getInvoiceRemarks() {
		return invoiceRemarks;
	}
	public void setInvoiceRemarks(String invoiceRemarks) {
		this.invoiceRemarks = invoiceRemarks;
	}
	public double getInvoiceGrandTotal() {
		return invoiceGrandTotal;
	}
	public void setInvoiceGrandTotal(double invoiceGrandTotal) {
		this.invoiceGrandTotal = invoiceGrandTotal;
	}
	public double getInvoiceServiceTax() {
		return invoiceServiceTax;
	}
	public void setInvoiceServiceTax(double invoiceServiceTax) {
		this.invoiceServiceTax = invoiceServiceTax;
	}
	public String getInvoiceBranch() {
		return invoiceBranch;
	}
	public void setInvoiceBranch(String invoiceBranch) {
		this.invoiceBranch = invoiceBranch;
	}

}
