package com.onesoft.courier.invoice.bean;



public class InvoiceBean {
	

	
	
	
	
	private String invoice_start;
	private long invoice_end;
	private int serieano;
	
	private String abw_no;
	private String booking_date;
	private String invoice_number;
	private String invoice_date;
	private String invoice2branchcode;
	
	private String clientcode;
	private String clientname;
	private String sub_clientcode;
	private String reason;
	private String remarks;

	private Double serviceTaxPercentage;
	
	private Double amount;
	private Double fwd_weight;
	private Double billing_weight;
	private Double insurance_amount;
	private Double docket_charge;
	private Double cod_amount;
	private Double fod_amount;
	private Double other_amount;
	private Double discount;
	private Double additional_charge;
	private Double fuel;
	private Double servicetax;
	private Double cess1;
	private Double cess2;
	private Double subtotal;
	private Double total;
	private Double grandtotal;
	private Double amountAfterDis_Add;
	
	private String discount_additional;
	private String type_flat2percent;
	private Double rateof_percent;
	private Double discount_Additional_amount;
	private String status;
	private String invoice_status;
	
	
	
	
	private int ahc;
	private int mtd;
	

	public String getInvoice_number()
	{
		return invoice_number;
	}
	public void setInvoice_number(String invoice_number)
	{
		this.invoice_number = invoice_number;
	}
	
	
	public String getInvoice_date()
	{
		return invoice_date;
	}
	public void setInvoice_date(String invoice_date)
	{
		this.invoice_date = invoice_date;
	}
	
	
	public String getInvoice2branchcode()
	{
		return invoice2branchcode;
	}
	public void setInvoice2branchcode(String invoice2branchcode)
	{
		this.invoice2branchcode = invoice2branchcode;
	}
	
	
	
	
	
	public String getClientcode()
	{
		return clientcode;
	}
	public void setClientcode(String clientcode)
	{
		this.clientcode = clientcode;
	}
	
	
	public String getSub_clientcode()
	{
		return sub_clientcode;
	}
	public void setSub_clientcode(String sub_clientcode)
	{
		this.sub_clientcode = sub_clientcode;
	}
	
	
	public String getType_flat2percent()
	{
		return type_flat2percent;
	}
	public void setType_flat2percent(String type_flat2percent)
	{
		this.type_flat2percent = type_flat2percent;
	}
	
	
	public String getReason()
	{
		return reason;
	}
	public void setReason(String reason)
	{
		this.reason = reason;
	}
	
	
	public String getDiscount_additional() {
		return discount_additional;
	}
	public void setDiscount_additional(String discount_additional) {
		this.discount_additional = discount_additional;
	}
	public Double getRateof_percent() {
		return rateof_percent;
	}
	public void setRateof_percent(Double rateof_percent) {
		this.rateof_percent = rateof_percent;
	}
	public Double getDiscount_Additional_amount() {
		return discount_Additional_amount;
	}
	public void setDiscount_Additional_amount(Double discount_Additional_amount) {
		this.discount_Additional_amount = discount_Additional_amount;
	}
	public String getRemarks()
	{
		return remarks;
	}
	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}
	
	
	public Double getAmount()
	{
		return amount;
	}
	public void setAmount(Double amount)
	{
		this.amount = amount;
	}
	
	
	public Double getInsurance_amount()
	{
		return insurance_amount;
	}
	public void setInsurance_amount(Double insurance_amount)
	{
		this.insurance_amount = insurance_amount;
	}
	
	
	public Double getDocket_charge()
	{
		return docket_charge;
	}
	public void setDocket_charge(Double docket_charge)
	{
		this.docket_charge = docket_charge;
	}
	
	
	public Double getCod_amount()
	{
		return cod_amount;
	}
	public void setCod_amount(Double cod_amount)
	{
		this.cod_amount = cod_amount;
	}
	
	
	public Double getFod_amount()
	{
		return fod_amount;
	}
	public void setFod_amount(Double fod_amount)
	{
		this.fod_amount = fod_amount;
	}
	
	
	public Double getDiscount()
	{
		return discount;
	}
	public void setDiscount(Double discount)
	{
		this.discount = discount;
	}
	
	
	public Double getAdditional_charge()
	{
		return additional_charge;
	}
	public void setAdditional_charge(Double additional_charge)
	{
		this.additional_charge = additional_charge;
	}
	
	
	public Double getFuel()
	{
		return fuel;
	}
	public void setFuel(Double fuel)
	{
		this.fuel = fuel;
	}
	
	
	public Double getServicetax()
	{
		return servicetax;
	}
	public void setServicetax(Double servicetax)
	{
		this.servicetax = servicetax;
	}
	

	public Double getCess1()
	{
		return cess1;
	}
	public void setCess1(Double cess1)
	{
		this.cess1 = cess1;
	}
	
	
	public Double getCess2()
	{
		return cess2;
	}
	public void setCess2(Double cess2)
	{
		this.cess2 = cess2;
	}
	
	
	public Double getSubtotal()
	{
		return subtotal;
	}
	public void setSubtotal(Double subtotal)
	{
		this.subtotal = subtotal;
	}
	
	
	public Double getGrandtotal()
	{
		return grandtotal;
	}
	public void setGrandtotal(Double grandtotal)
	{
		this.grandtotal = grandtotal;
	}
	
	
	public int getAhc()
	{
		return ahc;
	}
	public void setAhc(int ahc)
	{
		this.ahc = ahc;
	}
	
	
	public int getMtd()
	{
		return mtd;
	}
	public void setMtd(int mtd)
	{
		this.mtd = mtd;
	}
	
	
	public Double getOther_amount() {
		return other_amount;
	}
	public void setOther_amount(Double other_amount) {
		this.other_amount = other_amount;
	}
	
	
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	
	
	public Double getAmountAfterDis_Add() {
		return amountAfterDis_Add;
	}
	public void setAmountAfterDis_Add(Double amountAfterDis_Add) {
		this.amountAfterDis_Add = amountAfterDis_Add;
	}
	
	
	public Double getServiceTaxPercentage() {
		return serviceTaxPercentage;
	}
	public void setServiceTaxPercentage(Double serviceTaxPercentage) {
		this.serviceTaxPercentage = serviceTaxPercentage;
	}
	
	
	public String getInvoice_start() {
		return invoice_start;
	}
	public void setInvoice_start(String invoice_start) {
		this.invoice_start = invoice_start;
	}
	
	
	public long getInvoice_end() {
		return invoice_end;
	}
	public void setInvoice_end(long l) {
		this.invoice_end = l;
	}
	public String getAbw_no() {
		return abw_no;
	}
	public void setAbw_no(String abw_no) {
		this.abw_no = abw_no;
	}
	public String getBooking_date() {
		return booking_date;
	}
	public void setBooking_date(String booking_date) {
		this.booking_date = booking_date;
	}
	public Double getBilling_weight() {
		return billing_weight;
	}
	public void setBilling_weight(Double billing_weight) {
		this.billing_weight = billing_weight;
	}
	public int getSerieano() {
		return serieano;
	}
	public void setSerieano(int serieano) {
		this.serieano = serieano;
	}
	public String getClientname() {
		return clientname;
	}
	public void setClientname(String clientname) {
		this.clientname = clientname;
	}
	public Double getFwd_weight() {
		return fwd_weight;
	}
	public void setFwd_weight(Double fwd_weight) {
		this.fwd_weight = fwd_weight;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getInvoice_status() {
		return invoice_status;
	}
	public void setInvoice_status(String invoice_status) {
		this.invoice_status = invoice_status;
	}
		
	
}
