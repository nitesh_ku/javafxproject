package com.onesoft.courier.invoice.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.poi.hssf.util.HSSFColor.AQUA;

import com.DB.DBconnection;
import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.graphs.utils.FilterUtils;
import com.onesoft.courier.invoice.bean.InvoiceBean;
import com.onesoft.courier.invoice.bean.InvoiceBillBean;
import com.onesoft.courier.invoice.bean.InvoiceBillJavaFXBean;
import com.onesoft.courier.invoice.bean.InvoiceTableBean;
import com.onesoft.courier.invoice.utils.InvoiceUtils;
import com.onesoft.courier.main.Main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class InvoiceController implements Initializable{

	private ObservableList<InvoiceTableBean> detailedtabledata=FXCollections.observableArrayList();
	private ObservableList<InvoiceBillJavaFXBean> invoicetabledata=FXCollections.observableArrayList();
	
	private ObservableList<String> comboBoxBranchItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxClientItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxSubClientItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxDiscountAddtitionalItems=FXCollections.observableArrayList(InvoiceUtils.INVOICDEFAULT,InvoiceUtils.INVOICEDISCOUNT,InvoiceUtils.INVOICEADDITIONAL);
	//private ObservableList<String> comboBoxTypeAdditionalItems=FXCollections.observableArrayList(InvoiceUtils.INVOICDEFAULT,InvoiceUtils.a);
	private ObservableList<String> comboBoxTypeFlatPercentItems=FXCollections.observableArrayList(InvoiceUtils.INVOICDEFAULT,InvoiceUtils.TYPE1,InvoiceUtils.TYPE2);
	
	InvoiceBean invcBean=new InvoiceBean();
	
	List<String> awbNoToInoviceGeneratedList=new ArrayList<>();
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	
	
	Set<String> awb=new HashSet<String>();
	
	
	@FXML
	private ImageView imgSearchIcon;
	
	@FXML
	private Label labSummaryNetwork;
	
	@FXML
	private Label labSummaryService;
	
	@FXML
	private Label labSummaryDoxNonDox;
	
	@FXML
	private Button btnGenerate;
	
	@FXML
	private Button btnPrint;
	
	@FXML
	private TextField txtInvoiceNo;
	
	@FXML
	private TextField txtamount;
	
	@FXML
	private TextField txtCOD;
	
	@FXML
	private TextField txtSub_Client;
	
	@FXML
	private TextField txtFOD;
	
	@FXML
	private TextField txtDocket;
	
	@FXML
	private TextField txtFuel;
	
	@FXML
	private TextField txtOther;
	
	@FXML
	private TextField txtInsurance;
	
	@FXML
	private TextField txtSubTotal;
	
	@FXML
	private TextField txtReason;
	
	@FXML
	private TextField txtTypeAmount;
	
	@FXML
	private TextField txtDiscountAddtitionalAmt;
	
	@FXML
	private TextField txtServiceTax;
	
	@FXML
	private TextField txtGrandTotal;
	
	@FXML
	private TextField txtremarks;
	
	@FXML
	private TextField txtFrom;
	
	@FXML
	private TextField txtTo;
	
	@FXML
	private TextField txtNew;
	
	@FXML
	private TextField txtdiscountLessAmt;
	
	@FXML
	private CheckBox chkFilter;
	
	@FXML
	private CheckBox chkShowDue;
	
	@FXML
	private CheckBox chkBankDetails;
	
	@FXML
	private CheckBox chkPCS;
	
	@FXML
	private CheckBox chkD_N;
	
	@FXML
	private CheckBox chkLogo;
	
	@FXML
	private CheckBox chkStatus;
	
	@FXML
	private CheckBox chkBill;
	
	@FXML
	private ComboBox<String> comboBoxBranchCode;
	
	@FXML
	private ComboBox<String> comboBoxClientCode;
	
	@FXML
	private ComboBox<String> comboBoxSubClient;
	
	@FXML
	private ComboBox<String> comboBoxDisAdd;
	
	@FXML
	private ComboBox<String> comboBoxTypeFlatPercent;
	
	@FXML
	private DatePicker dpkInvoiceDate;
	
	@FXML
	private DatePicker dpkFromDate;
	
	@FXML
	private DatePicker dpkToDate;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private Button btnExit;
	

//================================================================

	@FXML
	private TableView<InvoiceBillJavaFXBean> invoiceTable;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Integer> invoiceserialno;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> invoicebranch;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> invoicenumber;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> invoicedate;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> fromdate;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> todate;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> invoiceclientcode;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> invoicesub_clientcode;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> clientname;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> invoiceservicetax;
	
	@FXML
	private TableColumn<InvoiceBillJavaFXBean, Double> grandtotal;

	@FXML
	private TableColumn<InvoiceBillJavaFXBean, String> remarks;
	
	
//================================================================	
	
	@FXML
	private TableView<InvoiceTableBean> detailedtable;
	
	@FXML
	private TableColumn<InvoiceTableBean, Integer> serialno;
	
	@FXML
	private TableColumn<InvoiceTableBean, String> awb_no;
	
	@FXML
	private TableColumn<InvoiceTableBean, String> bookingdate;
	
	@FXML
	private TableColumn<InvoiceTableBean, String> branch;
	
	@FXML
	private TableColumn<InvoiceTableBean, String> clientcode;
	
	@FXML
	private TableColumn<InvoiceTableBean, String> sub_clientcode;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> fwdWeight;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> billingweight;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> amount;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> insuracneamount;
	 
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> cod;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> fod;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> docketcharge;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> otherVas;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> fuel;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> servicetax;
	
	@FXML
	private TableColumn<InvoiceTableBean, Double> total;
	
	public String updateinvoice=null;
	
	
// ====================================================================================================================
	
	Main m=new Main();
	
	
// ====== Check invoice replaced by getInvoiceByBranch and getLatestInvoiceNo() from GenerateInvoiceByBranch class =============	
	
	/*public void checkInvoice() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		InvoiceBean inbean=new InvoiceBean();
		
		
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select * from invoiceseries order by invoice_end_no DESC limit 1";
			rs=st.executeQuery(sql);
	
			//System.out.println("row count: "+rs.getRow());
			
			if(!rs.next())
			{
				//System.out.println("No value: ");
				txtInvoiceNo.setEditable(true);
				
			}
			else
			{
				inbean.setInvoice_start(rs.getString("invoice_start_no"));
				inbean.setInvoice_end(rs.getLong("invoice_end_no"));
				inbean.setInvoice_number(inbean.getInvoice_start()+(inbean.getInvoice_end()+1));
				invcBean.setInvoice_number(inbean.getInvoice_number());
				//txtInvoiceNo.setText(inbean.getInvoice_number());
				//txtInvoiceNo.setEditable(false);
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}*/
	
	
// ====================================================================================================================	

	public void generateInvoiceSeries() throws IOException, SQLException, NumberFormatException
	{
	
		InvoiceBean inbean=new InvoiceBean();
	
		inbean.setInvoice_start(txtInvoiceNo.getText().replaceAll("[^A-Za-z]+", ""));
		inbean.setInvoice_end(Long.parseLong(txtInvoiceNo.getText().replaceAll("\\D+","")));
		
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();

		//ResultSet rs=null;
		PreparedStatement preparedStmt=null;
		
		try
		{	
			String query = "insert into invoiceseries(invoice_start_no, invoice_end_no) values(?,?)";
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setString(1,inbean.getInvoice_start());
			preparedStmt.setLong(2, inbean.getInvoice_end());
			preparedStmt.executeUpdate();
			
			System.out.println("table size: --------- "+detailedtabledata.size());
			
			
		
			//invoiceGeneratedAwbNo();
			saveInvoiceDate();
			loadInvoiceDate();
			for(String invoicedAwbno: awbNoToInoviceGeneratedList)
			{
				updateDBT_Invoice(invoicedAwbno);
			}
			reset();
			detailedtabledata.clear();
			labSummaryNetwork.setText("Networks:-");
			labSummaryService.setText("Services:-");
			labSummaryDoxNonDox.setText("D/N:-");
			
			
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Confirmation");
			alert.setHeaderText("Send mail request");
			alert.setContentText("Do you want to copy of invoice in your mail?");
			
			ButtonType buttonTypeYes = new ButtonType("Yes");
			ButtonType buttonTypeNo = new ButtonType("No", ButtonData.CANCEL_CLOSE);
			//ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
			alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);
			
			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == buttonTypeYes)
			{
			
			sendMail();
				}
			else if(result.get() == buttonTypeNo)
				{
				//System.out.println("No Mail");
				}
			
			
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}

	
// ====================================================================================================================	

	String clientname_Sendmail=null;
	
	public void sendMail()
	{	
		LocalDate indate=dpkInvoiceDate.getValue();
		LocalDate from=dpkFromDate.getValue();
		LocalDate to=dpkToDate.getValue();
		Date invoicedate=Date.valueOf(indate);
		Date fromdate=Date.valueOf(from);
		Date todate=Date.valueOf(to);
		
		String msg= "Invoice Details:"
				+ "<table border=2 bordercolor=black><tr><td><b>Invoice No.</b></td><td><b>Invoice Date</b></td><td><b>Billing Period</b></td>"
				+ "<td><b>Amount</b></td><td><b>Fuel</b></td><td><b>Service Tax</b></td><td><b>Grand Total</b></td></tr>"
				+ "<tr><td>"+txtInvoiceNo.getText()+"</td><td>"+date.format(invoicedate)+"</td><td>"+date.format(fromdate)+" to "+date.format(todate)+"</td>"
						+ "<td>"+txtamount.getText()+"</td><td>"+txtFuel.getText()+"</td><td>"+txtServiceTax.getText()+"</td><td>"+txtGrandTotal.getText()+"</td></tr></table>";
		
		
		Properties props = new Properties(); 
		props.put("mail.smtp.host","mail.onesoft.in"); 
		props.put("mail.smtp.auth","true");
		props.put("mail.smtp.port","587");
					
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
		protected PasswordAuthentication getPasswordAuthentication() {  
		   return new PasswordAuthentication("nitesh@onesoft.in","nitesh@321");  
				      }  
				  });  
						   
		try
		{  
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress("nitesh@onesoft.in"));
			message.addRecipient(Message.RecipientType.TO,new InternetAddress("nitesh@onesoft.in"));
			message.setSubject("Invoice Details");
			
			
			message.setContent(msg,"text/html");
			Transport.send(message);
						    	
			System.out.println("Email send Successfully");
		}
		catch (MessagingException e)
		{
			e.printStackTrace();
		} 
		
		 //System.out.println("Authentication method is closed");
		 //System.out.println("\n");
		
	}
	
// ====================================================================================================================

	public void updateDBT_Invoice(String awbno) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		
		try
		{	
		
			System.out.println("From update DBT: "+ txtInvoiceNo.getText() +" "+awbno);
		
			
			st = con.createStatement();
			String query = "update dailybookingtransaction as dbt set invoice_number='"+txtInvoiceNo.getText()+"' where air_way_bill_number='"+awbno+"'";
			System.out.println("SQL quer from update: "+query);
			st.executeUpdate(query);
            System.out.println("Invoice Updated...");
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(null, st, null, con);
		}
	}
	
	
// ====================================================================================================================		

	public void saveInvoiceDate() throws SQLException
	{
		LocalDate indate=dpkInvoiceDate.getValue();
		LocalDate from=dpkFromDate.getValue();
		LocalDate to=dpkToDate.getValue();
		Date invoicedate=Date.valueOf(indate);
		Date fromdate=Date.valueOf(from);
		Date todate=Date.valueOf(to);
		
		InvoiceBean inbean=new InvoiceBean();
		
		String[] clientcode=comboBoxClientCode.getValue().replaceAll("\\s+","").split("\\|");
		clientname_Sendmail=clientcode[1];
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();

		
		PreparedStatement preparedStmt=null;
		
		try
		{	
			inbean.setInvoice_number(txtInvoiceNo.getText());
			inbean.setInvoice2branchcode(comboBoxBranchCode.getValue());
			inbean.setClientcode(clientcode[0]);
			inbean.setClientname(clientcode[1]);
			inbean.setSub_clientcode(comboBoxSubClient.getValue());
			
			inbean.setAmountAfterDis_Add(Double.parseDouble(txtDiscountAddtitionalAmt.getText()));
			inbean.setCod_amount(Double.parseDouble(txtCOD.getText()));
			inbean.setFod_amount(Double.parseDouble(txtFOD.getText()));
			inbean.setDocket_charge(Double.parseDouble(txtDocket.getText()));
			
			if(comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICEDISCOUNT))
			{
				inbean.setDiscount_additional(comboBoxDisAdd.getValue());
			}
			else if(comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICEADDITIONAL))
			{
				inbean.setDiscount_additional(comboBoxDisAdd.getValue());
			}
			
			//System.out.println("Discount additional: "+inbean.getDiscount_additional());
			
			if(!txtReason.getText().equals(null) && !txtReason.getText().isEmpty())
			{
				inbean.setReason(txtReason.getText());
			}
			else
			{
				
			}
			
			//System.out.println("Reason: "+inbean.getReason());
			
			if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE1))
			{
				inbean.setType_flat2percent("P");
			}
			else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2))
			{
				inbean.setType_flat2percent("F");
			}
			
			
			//System.out.println("Flat or percent: "+inbean.getType_flat2percent());
			
			if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2) && !txtTypeAmount.getText().equals(null) && !txtTypeAmount.getText().isEmpty())
			{
				inbean.setRateof_percent(0.0);
			}
			else if(!txtTypeAmount.getText().equals(null) && !txtTypeAmount.getText().isEmpty()) 
			{
				inbean.setRateof_percent(Double.parseDouble(txtTypeAmount.getText()));
			}
			
			else
			{
				inbean.setRateof_percent(0.0);
			}
			//System.out.println("rate of percent: "+inbean.getRateof_percent());
			
			if(!txtdiscountLessAmt.getText().equals(null) && !txtdiscountLessAmt.getText().isEmpty())
			{
				inbean.setDiscount_Additional_amount(Double.parseDouble(txtdiscountLessAmt.getText()));
			}
			else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2))
			{
				inbean.setDiscount_Additional_amount(Double.parseDouble(txtTypeAmount.getText()));
			}
			else
			{
				inbean.setDiscount_Additional_amount(0.0);
			}
			//System.out.println("discount less amount: "+inbean.getDiscount_Additional_amount());
			
			inbean.setFuel(Double.parseDouble(txtFuel.getText()));
			
			//inbean.setOther_amount(Double.parseDouble(txtOther.getText()));
			inbean.setInsurance_amount(Double.parseDouble(txtInsurance.getText()));
			
			
			
			inbean.setServicetax(Double.parseDouble(txtServiceTax.getText()));
			inbean.setGrandtotal(Double.parseDouble(txtGrandTotal.getText()));
			inbean.setRemarks(txtremarks.getText());
			inbean.setStatus("UN");
			
			
			String query = "insert into invoice(invoice_number, invoice_date,invoice2branchcode,clientcode,clientname,sub_clientcode,amount,insurance_amount,docket_charge,cod_amount,fod_amount,discount_additional,reason,type_flat_percent,rateof_percent,discount_additional_amount,fuel,from_date,to_date,"
							+ "servicetax,grandtotal,remarks,status) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			
			preparedStmt = con.prepareStatement(query);
			
			preparedStmt.setString(1,inbean.getInvoice_number());
			preparedStmt.setDate(2,invoicedate);
			preparedStmt.setString(3, inbean.getInvoice2branchcode());
			preparedStmt.setString(4, inbean.getClientcode());
			preparedStmt.setString(5, inbean.getClientname());
			preparedStmt.setString(6, inbean.getSub_clientcode());
			preparedStmt.setDouble(7, inbean.getAmountAfterDis_Add());
			preparedStmt.setDouble(8, inbean.getInsurance_amount());
			preparedStmt.setDouble(9, inbean.getDocket_charge());
			preparedStmt.setDouble(10, inbean.getCod_amount());
			preparedStmt.setDouble(11, inbean.getFod_amount());
			preparedStmt.setString(12, inbean.getDiscount_additional());
			preparedStmt.setString(13, inbean.getReason());
			preparedStmt.setString(14, inbean.getType_flat2percent());
			preparedStmt.setDouble(15, inbean.getRateof_percent());
			preparedStmt.setDouble(16, inbean.getDiscount_Additional_amount());
			preparedStmt.setDouble(17, inbean.getFuel());
			preparedStmt.setDate(18, fromdate);
			preparedStmt.setDate(19, todate);
			preparedStmt.setDouble(20, inbean.getServicetax());
			preparedStmt.setDouble(21, inbean.getGrandtotal());
			preparedStmt.setString(22, inbean.getRemarks());
			preparedStmt.setString(23, inbean.getStatus());
			
			preparedStmt.executeUpdate();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
	}
	

// ====================================================================================================================
	
	public void invoiceGeneratedAwbNo() throws SQLException
	{
		InvoiceBean inbean=new InvoiceBean();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();

		inbean.setInvoice_status("Invoice Generated");
		
		
		PreparedStatement preparedStmt=null;
		
		try
		{	
			for(String awbno: awbNoToInoviceGeneratedList){
			
			String query = "update dailybookingtransaction set invoice_status=? where air_way_bill_number=?";
			System.out.println("from ============= "+awbno+" "+inbean.getInvoice_status());	
			//System.out.println(inbean.);
			preparedStmt = con.prepareStatement(query);
			
			preparedStmt.setString(1,inbean.getInvoice_status());
			preparedStmt.setString(2,awbno);
			
			preparedStmt.executeUpdate();
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
	}
	
// ====================================================================================================================	

	public void rangeFilter() throws SQLException
	{
		if(chkFilter.isSelected()==true)
		{
			txtFrom.setDisable(false);
			txtTo.setDisable(false);
			txtNew.setDisable(false);
		}
		else
		{
			txtFrom.clear();
			txtTo.clear();
			txtNew.clear();
			
			txtFrom.setDisable(true);
			txtTo.setDisable(true);
			txtNew.setDisable(true);
			showWithoutInvoiceTable(updateinvoice);
		}
	}
	
	
// ====================================================================================================================

	public void loadBranch() throws SQLException
	{	
		
		new	LoadBranch().loadBranch();
		
		System.out.println("Network Compare Contorller >> value from LoadBranch Class: >>>>>>>> ");
		
		//System.out.println("Network Contorller >> Load from Global set object");
		for(String s:LoadBranch.SETLOADBRANCHFORALL)
		{
			comboBoxBranchItems.add(s);
		}
		comboBoxBranchCode.setItems(comboBoxBranchItems);
		
		
		for(String firstBranch:LoadBranch.SETLOADBRANCHFORALL)
		{
			comboBoxBranchCode.setValue(firstBranch);
			GenerateInvoiceByBranch branchInvoice= new GenerateInvoiceByBranch();
			branchInvoice.getInvoiceByBranch(comboBoxBranchCode.getValue());
			txtInvoiceNo.setText(branchInvoice.getLatestInvoiceNo());
			//txtInvoiceNo.setText("");
			//txtInvoiceNo.clear();
			System.out.println("Text box invoice number from load branch : =-=================== "+txtInvoiceNo.getText());
			if(txtInvoiceNo.getText()!=null)
			{
				txtInvoiceNo.setEditable(false);
			}else
			{
				txtInvoiceNo.setEditable(true);
			}
			//txtInvoiceNo.setText(invcBean.getInvoice_number());
			//checkInvoice();
			break;
		}
		
		/*DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
	
		Set<String> set = new HashSet<String>();
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select code from brndetail";
			rs=st.executeQuery(sql);
	
			while(rs.next())
			{
				set.add(rs.getString(1));
			}
		
			for(String s:set)
			{
				comboBoxBranchItems.add(s);
			}
			comboBoxBranchCode.setItems(comboBoxBranchItems);
			
			for(String firstBranch:set)
			{
				comboBoxBranchCode.setValue(firstBranch);
				GenerateInvoiceByBranch branchInvoice= new GenerateInvoiceByBranch();
				branchInvoice.getInvoiceByBranch(comboBoxBranchCode.getValue());
				txtInvoiceNo.setText(branchInvoice.getLatestInvoiceNo());
				//txtInvoiceNo.setText("");
				//txtInvoiceNo.clear();
				System.out.println("Text box invoice number from load branch : =-=================== "+txtInvoiceNo.getText());
				if(txtInvoiceNo.getText()!=null)
				{
					txtInvoiceNo.setEditable(false);
				}else
				{
					txtInvoiceNo.setEditable(true);
				}
				//txtInvoiceNo.setText(invcBean.getInvoice_number());
				//checkInvoice();
				break;
			}
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}*/
	}
	
	
// ====================================================================================================================

	public void loadClients() throws SQLException
	{
		
		GenerateInvoiceByBranch branchInvoice= new GenerateInvoiceByBranch();
		branchInvoice.getInvoiceByBranch(comboBoxBranchCode.getValue());
		txtInvoiceNo.setText(branchInvoice.getLatestInvoiceNo());
		System.out.println("Text box invoice number : =-=================== "+txtInvoiceNo.getText());
		if(txtInvoiceNo.getText()!=null)
		{
			txtInvoiceNo.setEditable(false);
		}else
		{
			txtInvoiceNo.setEditable(true);
		}
		loadInvoiceDate();
		
		LocalDate from=dpkFromDate.getValue();
		LocalDate to=dpkToDate.getValue();
		Date stdate=Date.valueOf(from);
		Date edate=Date.valueOf(to);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		String sql=null;
		
		Set<String> setclient = new HashSet<String>();
		
		try
		{	
			
			 if(!comboBoxBranchCode.getValue().equals(InvoiceUtils.ALLBRANCH))
			 {
				 comboBoxClientCode.getItems().clear();
				 comboBoxSubClient.getItems().clear();
				/*sql="select dbt.dailybookingtransaction2client from dailybookingtransaction as dbt, clientmaster as cm where dbt.dailybookingtransaction2client=cm.code "
							+ "and dbt.dailybookingtransaction2Branch='"+comboBoxBranchCode.getValue()+"' and booking_date between '"+stdate+"' and '"+edate+"'";*/
				
				sql="select dbt.dailybookingtransaction2client,cm.name from dailybookingtransaction as dbt, clientmaster as cm where dbt.dailybookingtransaction2client=cm.code "
						+ "and dbt.dailybookingtransaction2Branch='"+comboBoxBranchCode.getValue()+"' and booking_date between '"+stdate+"' and '"+edate+"'";
			 }
			else
			{
				comboBoxSubClient.setDisable(true);
				sql="select cm.code,cm.name from dailybookingtransaction as dbt, clientmaster as cm where dbt.dailybookingtransaction2client=cm.code  and booking_date between '"+stdate+"' and '"+edate+"'";
			}
			
			setclient.clear();
			 
			st=con.createStatement();
			rs=st.executeQuery(sql);
			
			if(!rs.next())
			{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Database Alert");
				alert.setHeaderText(null);
				alert.setContentText("No match found");
				alert.showAndWait();
				
				comboBoxBranchCode.requestFocus();
				comboBoxClientCode.getItems().clear();
				comboBoxSubClient.setDisable(true);
			
			}
			else
			{
				comboBoxClientCode.setPromptText("select client");
			do
			{
				setclient.add(rs.getString(1)+" | "+ rs.getString(2));
			}while(rs.next());
			
			comboBoxClientCode.getItems().clear();
			for(String s:setclient)
			{
				comboBoxClientItems.add(s);
			}
			comboBoxSubClient.setDisable(false);
			comboBoxClientCode.setItems(comboBoxClientItems);
			//loadSubClients();
			
			}
			
		
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		
		}
		
		
	}
	

	
// ====================================================================================================================
	
	public void loadSubClients() throws SQLException
	{
		String[] clientcode=comboBoxClientCode.getValue().replaceAll("\\s+","").split("\\|");
	
		LocalDate from=dpkFromDate.getValue();
		LocalDate to=dpkToDate.getValue();
		Date stdate=Date.valueOf(from);
		Date edate=Date.valueOf(to);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		String sql=null;
	
		Set<String> setsubclient = new HashSet<String>();
	
		try
		{	
			
			 if(!comboBoxBranchCode.getValue().equals(null))
			 {
				sql="select sub_client_code from dailybookingtransaction where  dailybookingtransaction2client='"+clientcode[0]+"' and booking_date between '"+stdate+"' and '"+edate+"' group by sub_client_code";
							
			 }
			else if(!comboBoxBranchCode.getValue().equals(InvoiceUtils.ALLBRANCH) && !comboBoxBranchCode.getValue().equals(null))
			{
				sql="select sub_client_code from dailybookingtransaction where dailybookingtransaction2Branch='"+comboBoxBranchCode.getValue()+"' and dailybookingtransaction2client='"+clientcode[0]+"' and booking_date between '2017-03-27' and '2017-04-10' group by sub_client_code";
			}
			
			setsubclient.clear();
			 
			//System.out.println("sql from client data: "+sql);
			
			st=con.createStatement();
			
			rs=st.executeQuery(sql);
			
			if(!rs.next())
			{
				/*Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Database Alert");
				alert.setHeaderText(null);
				alert.setContentText("No match found");
				alert.showAndWait();*/
				
				//comboBoxBranchCode.requestFocus();
				//comboBoxClientCode.getItems().clear();
			
			}
			else
			{
				comboBoxSubClient.setPromptText("select sub client");
			do
			{
				setsubclient.add(rs.getString(1));
			}while(rs.next());
			
			comboBoxSubClient.getItems().clear();
			
			comboBoxSubClientItems.add(InvoiceUtils.SUBCLIENT2);
			
			for(String s:setsubclient)
			{
				if(!s.equals(""))
				{
					comboBoxSubClientItems.add(s);
				}
				else
				{
					comboBoxSubClientItems.add("Default");
				}
			}
			
			comboBoxSubClient.setItems(comboBoxSubClientItems);
			}
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		
		}
	
	}
	
// ====================================================================================================================	
	
	public void showDetails() throws SQLException
	{
		
		String[] clientcode=comboBoxClientCode.getValue().replaceAll("\\s+","").split("\\|");
		
		LocalDate from=dpkFromDate.getValue();
		LocalDate to=dpkToDate.getValue();
		Date stdate=Date.valueOf(from);
		Date edate=Date.valueOf(to);
		//String joinsql="";
		String sql="";
		String branchValue="";
		String clientSubClient="";
			
		try
		{
			if(!comboBoxBranchCode.getValue().equals(InvoiceUtils.ALLBRANCH))
			{
				branchValue=" dbt.dailybookingtransaction2branch='"+comboBoxBranchCode.getValue()+"' and ";
				//System.out.println("Ist if : ===== >>> "+comboBoxBranchCode.getValue());
			}
			
			if(!comboBoxClientCode.getValue().equals(null) && comboBoxSubClient.getValue().equals(InvoiceUtils.SUBCLIENT2))
			{
				clientSubClient="dbt.dailybookingtransaction2client='"+clientcode[0]+"' and ";
			}
			else if(!comboBoxClientCode.getValue().equals(null) && comboBoxSubClient.getValue().equals(InvoiceUtils.SUBCLIENT1))
			{
				clientSubClient="dbt.dailybookingtransaction2client='"+clientcode[0]+"' and dbt.sub_client_code='' and ";
			}
			else
			{
				clientSubClient="dbt.dailybookingtransaction2client='"+clientcode[0]+"' and dbt.sub_client_code='"+comboBoxSubClient.getValue()+"' and ";
			}
		
			sql=branchValue+clientSubClient+" dbt.invoice_number='' and dbt.booking_date between '"+stdate+"' and '"+edate+"'";
			
			updateinvoice=sql;
			getData(sql);
		
			
			if(!comboBoxSubClient.getValue().equals(null))
			{
				chkBill.setDisable(false);
			}
			else
			{
				chkBill.setDisable(true);
			}
			
			showWithoutInvoiceTable(sql);
		}	
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
				
	}


// ====================================================================================================================	

	public void getData(String sql) throws SQLException
	{
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
			
		ResultSet rs=null;
		Statement st=null;
		String finalsql=null;
		
		InvoiceBean inbean=new InvoiceBean();
		
		try
		{
			st=con.createStatement();
			finalsql="select sum(dbt.amount) as amount,sum(dbt.insurance_amount) as insurance,sum(dbt.docket_charge)as docket,sum(dbt.cod_amount) as cod,sum(dbt.fod_amount) as fod,"
					+ "sum(dbt.vas_amount) as vas,sum(dbt.fuel) as fuel,sum(dbt.service_tax+dbt.cess1+dbt.cess2) as tax,sum(dbt.total) as total from dailybookingtransaction as dbt where "+sql;
			//System.out.println(finalsql);
			rs=st.executeQuery(finalsql);
			
			//System.out.println(rs.getRow());
			
			if(!rs.next())
			{
				
			}
			else
			{
				do
				{
					
					inbean.setAmount(Double.parseDouble(df.format(rs.getDouble("amount"))));
					inbean.setInsurance_amount(Double.parseDouble(df.format(rs.getDouble("insurance"))));
					inbean.setDocket_charge(Double.parseDouble(df.format(rs.getDouble("docket"))));
					inbean.setCod_amount(Double.parseDouble(df.format(rs.getDouble("cod"))));
					inbean.setFod_amount(Double.parseDouble(df.format(rs.getDouble("fod"))));
					inbean.setOther_amount(Double.parseDouble(df.format(rs.getDouble("vas"))));
					inbean.setFuel(Double.parseDouble(df.format(rs.getDouble("fuel"))));
					inbean.setServicetax(Double.parseDouble(df.format(rs.getDouble("tax"))));
					inbean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
						
				}while(rs.next());
			}
			
			
			txtamount.setText(String.valueOf(inbean.getAmount()));
			txtCOD.setText(String.valueOf(inbean.getCod_amount()));
			txtFOD.setText(String.valueOf(inbean.getFod_amount()));
			txtDocket.setText(String.valueOf(inbean.getDocket_charge()));
			txtFuel.setText(String.valueOf(inbean.getFuel()));
			txtOther.setText(String.valueOf(inbean.getOther_amount()));
			txtInsurance.setText(String.valueOf(inbean.getInsurance_amount()));
			txtSubTotal.setText(df.format(inbean.getAmount()+inbean.getCod_amount()+inbean.getFod_amount()+inbean.getDocket_charge()+inbean.getFuel()+inbean.getOther_amount()+inbean.getInsurance_amount()));
			txtDiscountAddtitionalAmt.setText(df.format(Double.parseDouble(txtSubTotal.getText())));
			txtServiceTax.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*invcBean.getServiceTaxPercentage()/100));
			txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtServiceTax.getText())));
			
			if(!txtSubTotal.getText().equals(null))
			{
				comboBoxDisAdd.setDisable(false);
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		finally
		{	
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
// ====================================================================================================================
	
	public void loadInvoiceDate() throws SQLException
	{
		invoicetabledata.clear();
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
			
		ResultSet rs=null;
		Statement st=null;
		String finalsql=null;
		
		InvoiceBillBean inbean=new InvoiceBillBean();
		int serialno=1;
		try
		{
			st=con.createStatement();
			finalsql="select invoice_number,invoice_date,invoice2branchcode,from_date,to_date,clientcode,clientname,sub_clientcode,servicetax,grandtotal,remarks from invoice where invoice2branchcode='"+comboBoxBranchCode.getValue()+"' order by invoice_number";
			//System.out.println(finalsql);
			rs=st.executeQuery(finalsql);
		
			if(!rs.next())
			{
				
			}
			else
			{
				do
				{
					
					inbean.setInvoiceserialno(serialno);
					inbean.setInvoiceNumber(rs.getString("invoice_number"));
					inbean.setInvoiceDate(date.format(rs.getDate("invoice_date")));
					inbean.setInvoiceBranch(rs.getString("invoice2branchcode"));
					inbean.setFrom_date(date.format(rs.getDate("from_date")));
					inbean.setTo_date(date.format(rs.getDate("to_date")));
					inbean.setInvoiceClientCode(rs.getString("clientcode"));
					inbean.setInvoiceClientName(rs.getString("clientname"));
					inbean.setInvoiceSubClient(rs.getString("sub_clientcode"));
					inbean.setInvoiceServiceTax(Double.parseDouble(df.format(rs.getDouble("servicetax"))));
					inbean.setInvoiceGrandTotal(Double.parseDouble(df.format(rs.getDouble("grandtotal"))));
					inbean.setInvoiceRemarks(rs.getString("remarks"));
										
					invoicetabledata.add(new InvoiceBillJavaFXBean(inbean.getInvoiceserialno(),inbean.getInvoiceNumber(),inbean.getInvoiceDate(),inbean.getInvoiceBranch(),
							inbean.getFrom_date(),inbean.getTo_date(),inbean.getInvoiceClientCode(),inbean.getInvoiceClientName(),inbean.getInvoiceSubClient(),inbean.getInvoiceServiceTax(),
							inbean.getInvoiceGrandTotal(),inbean.getInvoiceRemarks(),null));
					
					serialno++;
					
				}while(rs.next());
			}
			
			invoiceTable.setItems(invoicetabledata);
		
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{	
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
	
// ====================================================================================================================	

	
	public void showWithoutInvoiceTable(String sql) throws SQLException
	{
		detailedtabledata.clear();
		awbNoToInoviceGeneratedList.clear();
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
			
		ResultSet rs=null;
		Statement st=null;
		String finalsql=null;
		
		Set<String> networkset = new HashSet<String>();
		Set<String> serviceset = new HashSet<String>();
		Set<String> dnset = new HashSet<String>();
		
		
		
		InvoiceBean inbean=new InvoiceBean();
		int serialno=1;
		try
		{
			st=con.createStatement();
			finalsql="select dbt.air_way_bill_number,dbt.booking_date,dbt.dailybookingtransaction2branch,dbt.dailybookingtransaction2network,dbt.dailybookingtransaction2service,dbt.dox_nondox,dbt.dailybookingtransaction2client,dbt.sub_client_code,(bcdi.billing_weight) as fwd_weight,(dbt.billing_weight) as weight,"
					+ "sum(dbt.amount) as amount,sum(dbt.insurance_amount) as insurance,sum(dbt.docket_charge)as docket,sum(dbt.cod_amount) as cod,sum(dbt.fod_amount) as fod,"
					+ "sum(dbt.vas_amount) as vas,sum(dbt.fuel) as fuel,sum(dbt.service_tax+dbt.cess1+dbt.cess2) as tax,sum(dbt.total) as total from dailybookingtransaction as dbt,bookingconsignmentdetailimport as bcdi "
					+ "where dbt.air_way_bill_number=bcdi.air_way_bill_number and dbt.invoice_number='' and "+sql+" group by dbt.booking_date,dbt.air_way_bill_number,dbt.dailybookingtransaction2branch,dbt.dailybookingtransaction2network,dbt.dailybookingtransaction2service,dbt.dox_nondox,"
							+ "dbt.dailybookingtransaction2client,dbt.sub_client_code,bcdi.billing_weight,dbt.billing_weight";
			System.out.println("From detailed table: "+finalsql);
			rs=st.executeQuery(finalsql);
			
			System.out.println("resultset size is : ======================  "+rs.getRow());
			
			if(!rs.next())
			{
			
				detailedtabledata.clear();
				labSummaryNetwork.setText("Networks:-");
				labSummaryService.setText("Services:-");
				labSummaryDoxNonDox.setText("D/N:-");
				
				chkBill.setDisable(true);
				txtamount.clear();
				txtCOD.clear();
				txtFOD.clear();
				txtInsurance.clear();
				txtDocket.clear();
				txtFuel.clear();
				txtOther.clear();
				txtSubTotal.clear();
				txtDiscountAddtitionalAmt.clear();
				txtServiceTax.clear();
				txtGrandTotal.clear();
				
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Data not found");
				alert.setHeaderText(null);
				alert.setContentText("Invoice already Generated / Data not found for the Given detail...!");
				alert.showAndWait();
				
			}
			else
			{
				do
				{
					
					networkset.add(rs.getString("dailybookingtransaction2network"));
					serviceset.add(rs.getString("dailybookingtransaction2service"));
					dnset.add(rs.getString("dox_nondox"));
					
					awbNoToInoviceGeneratedList.add(rs.getString("air_way_bill_number"));
					
					inbean.setSerieano(serialno);
					inbean.setAbw_no(rs.getString("air_way_bill_number"));
					inbean.setBooking_date(date.format(rs.getDate("booking_date")));
					inbean.setInvoice2branchcode(rs.getString("dailybookingtransaction2branch"));
					inbean.setClientcode(rs.getString("dailybookingtransaction2client"));
					inbean.setSub_clientcode(rs.getString("sub_client_code"));
					inbean.setFwd_weight(Double.parseDouble(df.format(rs.getDouble("fwd_weight"))));
					inbean.setBilling_weight(Double.parseDouble(df.format(rs.getDouble("weight"))));
					inbean.setAmount(Double.parseDouble(df.format(rs.getDouble("amount"))));
					inbean.setInsurance_amount(Double.parseDouble(df.format(rs.getDouble("insurance"))));
					inbean.setDocket_charge(Double.parseDouble(df.format(rs.getDouble("docket"))));
					inbean.setCod_amount(Double.parseDouble(df.format(rs.getDouble("cod"))));
					inbean.setFod_amount(Double.parseDouble(df.format(rs.getDouble("fod"))));
					inbean.setOther_amount(Double.parseDouble(df.format(rs.getDouble("vas"))));
					inbean.setFuel(Double.parseDouble(df.format(rs.getDouble("fuel"))));
					inbean.setServicetax(Double.parseDouble(df.format(rs.getDouble("tax"))));
					inbean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
					
					detailedtabledata.add(new InvoiceTableBean(inbean.getSerieano(), inbean.getAbw_no(), inbean.getBooking_date(),
											inbean.getInvoice2branchcode(),inbean.getClientcode(), inbean.getSub_clientcode(), inbean.getFwd_weight(), inbean.getBilling_weight(),
											inbean.getAmount(),inbean.getInsurance_amount() , inbean.getDocket_charge(), inbean.getCod_amount(),
											inbean.getFod_amount(), inbean.getOther_amount(), inbean.getFuel(), inbean.getServicetax(), inbean.getTotal()));
						
					
					serialno++;
					
				}while(rs.next());
				
				detailedtable.setItems(detailedtabledata);
				showSummary(sql, networkset, serviceset, dnset);
			}
			
			
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{	
			dbcon.disconnect(null, st, rs, con);
		}
		
	}
	
	
	
	// ====================================================================================================================	

	public void showDetaildTableData() throws SQLException
	{
			
		if(txtTo.getText().equals("."))
		{
		}
		else if(txtTo.getText().equals(""))
		{
		//System.out.println("blank textfield");
		}
		else
		{
			detailedtabledata.clear();
			awb.clear();
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
				
			ResultSet rs=null;
			Statement st=null;
			String finalsql=null;
			
			Set<String> networkset = new HashSet<String>();
			Set<String> serviceset = new HashSet<String>();
			Set<String> dnset = new HashSet<String>();
			
			
			InvoiceBean inbean=new InvoiceBean();
			int serialno=1;
			try
			{
				st=con.createStatement();
				finalsql="select dbt.air_way_bill_number,dbt.booking_date,dbt.dailybookingtransaction2branch,dbt.dailybookingtransaction2network,dbt.dailybookingtransaction2service,dbt.dox_nondox,dbt.dailybookingtransaction2client,dbt.sub_client_code,(bcdi.billing_weight) as fwd_weight,(dbt.billing_weight) as weight,"
						+ "sum(dbt.amount) as amount,sum(dbt.insurance_amount) as insurance,sum(dbt.docket_charge)as docket,sum(dbt.cod_amount) as cod,sum(dbt.fod_amount) as fod,"
						+ "sum(dbt.vas_amount) as vas,sum(dbt.fuel) as fuel,sum(dbt.service_tax+dbt.cess1+dbt.cess2) as tax,sum(dbt.total) as total from dailybookingtransaction as dbt,bookingconsignmentdetailimport as bcdi "
						+ "where dbt.air_way_bill_number=bcdi.air_way_bill_number and bcdi.billing_weight between '"+txtFrom.getText()+"' and '"+txtTo.getText()+"' and "+updateinvoice+" group by dbt.booking_date,dbt.air_way_bill_number,dbt.dailybookingtransaction2branch,dbt.dailybookingtransaction2network,dbt.dailybookingtransaction2service,dbt.dox_nondox,"
								+ "dbt.dailybookingtransaction2client,dbt.sub_client_code,bcdi.billing_weight,dbt.billing_weight";
				//System.out.println("From detailed table: "+finalsql);
				rs=st.executeQuery(finalsql);
				
				if(!rs.next())
				{
				
					detailedtabledata.clear();
					labSummaryNetwork.setText("Networks:-");
					labSummaryService.setText("Services:-");
					labSummaryDoxNonDox.setText("D/N:-");
					
					chkBill.setDisable(true);
					txtamount.clear();
					txtCOD.clear();
					txtFOD.clear();
					txtInsurance.clear();
					txtDocket.clear();
					txtFuel.clear();
					txtOther.clear();
					txtSubTotal.clear();
					txtDiscountAddtitionalAmt.clear();
					txtServiceTax.clear();
					txtGrandTotal.clear();
					
					/*Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Data not found");
					alert.setHeaderText(null);
					alert.setContentText("Invoice already Generated for the Given detail...!");
					alert.showAndWait();*/
				
					
					
				}
				else
				{
					do
					{
					
						awb.add(rs.getString("air_way_bill_number"));
						
						networkset.add(rs.getString("dailybookingtransaction2network"));
						serviceset.add(rs.getString("dailybookingtransaction2service"));
						dnset.add(rs.getString("dox_nondox"));
						
						
						inbean.setSerieano(serialno);
						inbean.setAbw_no(rs.getString("air_way_bill_number"));
						inbean.setBooking_date(date.format(rs.getDate("booking_date")));
						inbean.setInvoice2branchcode(rs.getString("dailybookingtransaction2branch"));
						inbean.setClientcode(rs.getString("dailybookingtransaction2client"));
						inbean.setSub_clientcode(rs.getString("sub_client_code"));
						inbean.setFwd_weight(Double.parseDouble(df.format(rs.getDouble("fwd_weight"))));
						inbean.setBilling_weight(Double.parseDouble(df.format(rs.getDouble("weight"))));
						inbean.setAmount(Double.parseDouble(df.format(rs.getDouble("amount"))));
						inbean.setInsurance_amount(Double.parseDouble(df.format(rs.getDouble("insurance"))));
						inbean.setDocket_charge(Double.parseDouble(df.format(rs.getDouble("docket"))));
						inbean.setCod_amount(Double.parseDouble(df.format(rs.getDouble("cod"))));
						inbean.setFod_amount(Double.parseDouble(df.format(rs.getDouble("fod"))));
						inbean.setOther_amount(Double.parseDouble(df.format(rs.getDouble("vas"))));
						inbean.setFuel(Double.parseDouble(df.format(rs.getDouble("fuel"))));
						inbean.setServicetax(Double.parseDouble(df.format(rs.getDouble("tax"))));
						inbean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
						
						detailedtabledata.add(new InvoiceTableBean(inbean.getSerieano(), inbean.getAbw_no(), inbean.getBooking_date(),
												inbean.getInvoice2branchcode(),inbean.getClientcode(), inbean.getSub_clientcode(), inbean.getFwd_weight(), inbean.getBilling_weight(),
												inbean.getAmount(),inbean.getInsurance_amount() , inbean.getDocket_charge(), inbean.getCod_amount(),
												inbean.getFod_amount(), inbean.getOther_amount(), inbean.getFuel(), inbean.getServicetax(), inbean.getTotal()));
							
						
						serialno++;
						
					}while(rs.next());
					
					
					
					detailedtable.setItems(detailedtabledata);
					printAWB();
					showSummary(updateinvoice, networkset, serviceset, dnset);
				}
				
				
				
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}
			finally
			{	
				dbcon.disconnect(null, st, rs, con);
				System.out.println("Detailed table size: ========= "+detailedtabledata.size());
			}
			}
		}	
	
	
// ====================================================================================================================

	public void printAWB()
	{
		for(String a:awb)
		{
			System.out.println("awb: "+a);
		}
	}

// ====================================================================================================================
		
		
		
	public void updateWeightFilter() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
	/*	if(txtFrom.getText().equals(null) || txtFrom.getText().isEmpty())
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Please fill the textbox");
			alert.setHeaderText(null);
			alert.setContentText("From Textfield is blank...!!");
			alert.showAndWait();
			txtFrom.clear();
			txtFrom.requestFocus();
		}
		else if (!txtFrom.getText().matches("[0-9]*"))
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Please fill the textbox");
			alert.setHeaderText(null);
			alert.setContentText("From Textfield: please enter only numeric values!!");
			alert.showAndWait();
			txtFrom.clear();
			txtFrom.requestFocus();
		}
		else if(txtTo.getText().equals(null) || txtTo.getText().isEmpty())
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Please fill the textbox");
			alert.setHeaderText(null);
			alert.setContentText("To Textfield is blank...!!");
			alert.showAndWait();
			txtTo.clear();
			txtTo.requestFocus();
		}
		else if (!txtTo.getText().matches("[0-9]*"))
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Please fill the textbox");
			alert.setHeaderText(null);
			alert.setContentText("To Textfield: please enter only numeric values!!");
			alert.showAndWait();
			txtTo.clear();
			txtTo.requestFocus();
		}
		else if(txtNew.getText().equals(null) || txtNew.getText().isEmpty())
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Please fill the textbox");
			alert.setHeaderText(null);
			alert.setContentText("New Textfield is blank...!!");
			alert.showAndWait();
			txtNew.clear();
			txtNew.requestFocus();
		}
		else if (!txtNew.getText().matches("[0-9]*"))
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Incorrect value");
			alert.setHeaderText(null);
			alert.setContentText("New TextField: please enter only numeric values!!");
			alert.showAndWait();
			txtNew.clear();
			txtNew.requestFocus();
		}
		else
		{*/
		
		st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
		
			try
			{	
				for(String awb:awb)
				{
				
					String query = "update dailybookingtransaction set billing_weight="+Double.parseDouble(txtNew.getText())+" where air_way_bill_number='"+awb+"'";
					//System.out.println(query);
					st.executeUpdate(query);
					//System.out.println("Invoice Updated...");
	            
					showWithoutInvoiceTable(updateinvoice);
				}
				
				showDetaildTableData();
				
				
				
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{	
				awb.clear();
				dbcon.disconnect(null, st, null, con);
			}
		//}
		
	}
	
// ====================================================================================================================

	public void showSummary(String sql,Set<String> networkset,Set<String> serviceset,Set<String> dnset) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
			
		ResultSet rs=null;
		Statement st=null;
		String finalsql=null;
		
		int records = 0;
		
		try
		{
			String count=null;
			
			String networkList=" ";
			String serviceList=" ";
			String dnList=" ";
			
			st=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			
			// --------------------- Network Summary ---------------------------------
			
			for(String net:networkset)
			{
			
				finalsql = "select dbt.dailybookingtransaction2network from dailybookingtransaction as dbt where dailybookingtransaction2network='"+net+"' and "+sql;
				rs=st.executeQuery(finalsql);
				
				if(!rs.next())
				{
					//System.out.println("no data available");
				}
				else
				{
					do
					{
						records++;
					}
					while (rs.next());
				}
				
				if(net.equals(net) && records>0)
				{
					count=net+":"+records+" ";
				}
				
				records=0;
				
				if(!count.equals(null))
				{
					networkList=networkList.concat(count);
				}
			}
			rs.last();
			labSummaryNetwork.setText(networkList);
			count=null;
			//networkList="Network:- ";
			
			
			// --------------------- Service Summary ---------------------------------
			
			for(String service:serviceset)
			{	
				finalsql = "select dbt.dailybookingtransaction2service from dailybookingtransaction as dbt where dbt.dailybookingtransaction2service='"+service+"' and "+sql;
				rs=st.executeQuery(finalsql);
				
				if(!rs.next())
				{
					
				}
				else
				{
					do
					{
						records++;
					}
					while (rs.next());
				}
				
				
				if(service.equals(service) && records>0)
				{
					count=service+":"+records+" ";
				}
				
				records=0;
			
				if(!count.equals(null))
				{
				serviceList=serviceList.concat(count);
				}
			}
			rs.last();
			labSummaryService.setText(serviceList);
			count=null;
			//serviceList="Service:- ";
			
			
			// --------------------- Dox Non-Dox Summary ---------------------------------
			
			for(String dn:dnset)
			{
				finalsql = "select dbt.dox_nondox from dailybookingtransaction as dbt where dbt.dox_nondox='"+dn+"' and "+sql;
				rs=st.executeQuery(finalsql);
					
				if(!rs.next())
				{
				
				}
				else
				{
					do
					{
						records++;
					}
					while (rs.next());
				}
						
				if(dn.equals(dn) && records>0)
				{
					count=dn+":"+records+" ";
				}
						
				records=0;
					
				if(!count.equals(null))
				{
				dnList=dnList.concat(count);
				}
			}
			rs.last();		
			labSummaryDoxNonDox.setText(dnList);
			count=null;
			//dnList="D/N:- ";
			
			
		}	
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{	
			serviceset.clear();
			dbcon.disconnect(null, st, rs, con);
		}
	}	

	
// ====================================================================================================================	
	
	public void setDiscountAdd()
	{
		if(comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICDEFAULT))
		{
			txtTypeAmount.clear();
			txtTypeAmount.setDisable(true);
			txtReason.setDisable(true);
			comboBoxTypeFlatPercent.setDisable(true);
			comboBoxTypeFlatPercent.setValue(InvoiceUtils.TYPEDEFAULT);
		}
		else
		{
			txtTypeAmount.clear();
			txtReason.setDisable(false);
			comboBoxTypeFlatPercent.setDisable(false);
		}
	
		txtDiscountAddtitionalAmt.setText(df.format(Double.parseDouble(txtSubTotal.getText())));
		txtServiceTax.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*invcBean.getServiceTaxPercentage()/100));
		txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtServiceTax.getText())));
		
	}
	
	
	public void type()
	{
		if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPEDEFAULT))
		{
			txtTypeAmount.clear();
			txtdiscountLessAmt.clear();
			txtTypeAmount.setDisable(true);
			txtdiscountLessAmt.setDisable(true);
		}
		
		else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE1))
		{
			txtdiscountLessAmt.clear();
			txtTypeAmount.setDisable(false);
			txtdiscountLessAmt.setDisable(false);
		}
		else
		{
			txtdiscountLessAmt.clear();
			txtTypeAmount.clear();
			txtTypeAmount.setDisable(false);
			txtdiscountLessAmt.setDisable(true);
		}
		
		txtDiscountAddtitionalAmt.setText(df.format(Double.parseDouble(txtSubTotal.getText())));
		txtServiceTax.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*invcBean.getServiceTaxPercentage()/100));
		txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtServiceTax.getText())));
	}

// ====================================================================================================================
	
	public void setPercentFlat()
	{
		double discount=0.0;
		InvoiceBean inbean=new InvoiceBean();
		
		
		if(comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICEDISCOUNT))
		{	
			if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE1))
			{
				discount=Double.parseDouble(txtSubTotal.getText())*Integer.parseInt(txtTypeAmount.getText())/100;
				txtdiscountLessAmt.setText(String.valueOf(discount));
				inbean.setAmountAfterDis_Add(Double.parseDouble(txtSubTotal.getText())-discount);
				
			}
			else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2))
			{
				inbean.setAmountAfterDis_Add(Double.parseDouble(txtSubTotal.getText())-Integer.parseInt(txtTypeAmount.getText()));
			}
		}
		else if(comboBoxDisAdd.getValue().equals(InvoiceUtils.INVOICEADDITIONAL))
		{
			if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE1))
			{
				discount=Double.parseDouble(txtSubTotal.getText())*Integer.parseInt(txtTypeAmount.getText())/100;
				txtdiscountLessAmt.setText(String.valueOf(discount));
				inbean.setAmountAfterDis_Add(Double.parseDouble(txtSubTotal.getText())+discount);
			}
			else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2))
			{
				inbean.setAmountAfterDis_Add(Double.parseDouble(txtSubTotal.getText())+Integer.parseInt(txtTypeAmount.getText()));
			}
		}
		
		
		txtDiscountAddtitionalAmt.setText(df.format(inbean.getAmountAfterDis_Add()));
		grandtotal();
		txtServiceTax.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())*invcBean.getServiceTaxPercentage()/100));
	}

	
// ====================================================================================================================

	public void grandtotal()
	{
		
		if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE1))
		{
			txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtServiceTax.getText())));
			
		}
		else if(comboBoxTypeFlatPercent.getValue().equals(InvoiceUtils.TYPE2))
		{
			txtGrandTotal.setText(df.format(Double.parseDouble(txtDiscountAddtitionalAmt.getText())+Double.parseDouble(txtServiceTax.getText())));
		}
	}
	
	
// ====================================================================================================================
	
	public void showGenerateButton()
	{
		if(chkBill.isSelected()==true)
		{
			btnGenerate.setDisable(false);
		}
		else
		{
			btnGenerate.setDisable(true);
		}
	}
	
// ====================================================================================================================

	public void getServiceTaxPercentage() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		try
		{	
			st=con.createStatement();
			String sql="select effective_date ,sum(tax+tax1+tax2) as servicetax from expensestaxtype group by effective_date order by effective_date DESC limit 1";
			rs=st.executeQuery(sql);
	
			while(rs.next())
			{
			invcBean.setServiceTaxPercentage(rs.getDouble("servicetax"));
			}
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
// ====================================================================================================================	

	public void reset() throws IOException
	{
		m.showInvoice();	
	}
		
// ====================================================================================================================	

	public void exit() throws SQLException
	{
		try {
			m.homePage();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	

// ==================================================================================================
	
	@FXML
	public void datePickerEnterTab(KeyEvent e) throws SQLException
	{
		loadClients();
		System.out.println("this method: ===>>>"+e);
		
		if(e.getCode().equals(KeyCode.ENTER))
		{
			System.out.println("this method running ===================== ");
			
			dpkToDate.requestFocus();
		}
		
	}
	
	
	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException, ParseException
	{

	Node n=(Node) e.getSource();
		
		
	if(n.getId().equals("txtInvoiceNo"))
	{
		if(e.getCode().equals(KeyCode.ENTER))
		{
			//loadClients();
			dpkInvoiceDate.requestFocus();
		}
	}
	
	else if(n.getId().equals("dpkInvoiceDate"))
	{
		if(e.getCode().equals(KeyCode.ENTER))
		{
			dpkFromDate.requestFocus();
		}
	}
	
	else if(n.getId().equals("dpkFromDate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				//loadClients();
				dpkToDate.requestFocus();
			}
		}
		
		else if(n.getId().equals("dpkToDate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxBranchCode.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxBranchCode"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxClientCode.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxClientCode"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxSubClient.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxSubClient"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxDisAdd.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxDisAdd"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(txtReason.isDisable()==true)
				{
					txtremarks.requestFocus();
				}
				else
				{
					txtReason.requestFocus();	
				}
			}
		}
		
		else if(n.getId().equals("txtReason"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxTypeFlatPercent.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxTypeFlatPercent"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(txtTypeAmount.isDisable()==true)
				{
					txtremarks.requestFocus();
				}
				else
				{
					txtTypeAmount.requestFocus();
				}
			}
		}
		
		else if(n.getId().equals("txtTypeAmount"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(txtTypeAmount.getText().equals("") || txtTypeAmount.getText().isEmpty())
				{
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Empty field");
					alert.setHeaderText(null);
					alert.setContentText("Please enter the discount or additional amount...");
					alert.showAndWait();
					txtTypeAmount.requestFocus();
				}
				else
				{
					if(txtdiscountLessAmt.isDisable()==true)
					{
						txtremarks.requestFocus();
					}
					else
					{
						txtdiscountLessAmt.requestFocus();
					}
				}
			}
		}

		else if(n.getId().equals("txtdiscountLessAmt"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtremarks.requestFocus();
			}
		}
	
	
	}	
	
	
// ====================================================================================================================
	
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		
		imgSearchIcon.setImage(new Image("/com/onesoft/courier/icon/searchicon_blue.png"));
		
		dpkFromDate.requestFocus();
		
		comboBoxDisAdd.setValue(InvoiceUtils.INVOICDEFAULT);
		comboBoxTypeFlatPercent.setValue(InvoiceUtils.TYPEDEFAULT);
		
		
		comboBoxDisAdd.setItems(comboBoxDiscountAddtitionalItems);
		comboBoxTypeFlatPercent.setItems(comboBoxTypeFlatPercentItems);
		
		txtReason.setDisable(true);
		txtTypeAmount.setDisable(true);
		txtFrom.setDisable(true);
		txtTo.setDisable(true);
		txtNew.setDisable(true);
		comboBoxSubClient.setDisable(true);
		comboBoxTypeFlatPercent.setDisable(true);
		comboBoxDisAdd.setDisable(true);
		chkBill.setDisable(true);
		btnGenerate.setDisable(true);
		txtdiscountLessAmt.setDisable(true);
		
		dpkFromDate.setValue(LocalDate.now());
		dpkToDate.setValue(LocalDate.now());
		dpkInvoiceDate.setValue(LocalDate.now());
		
		try
		{
			//checkInvoice();
			loadBranch();
			getServiceTaxPercentage();
			loadInvoiceDate();
			//loadClients();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		
		invoiceserialno.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Integer>("invoiceserialno"));
		invoicenumber.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceNumber"));
		invoicebranch.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceBranch"));
		invoicedate.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceDate"));
		fromdate.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("from_date"));
		todate.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("to_date"));
		invoiceclientcode.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceClientCode"));
		invoicesub_clientcode.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceSubClient"));
		clientname.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceClientName"));
		grandtotal.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("invoiceGrandTotal"));
		remarks.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,String>("invoiceRemarks"));
		invoiceservicetax.setCellValueFactory(new PropertyValueFactory<InvoiceBillJavaFXBean,Double>("invoiceServiceTax"));
		
		serialno.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Integer>("serialno"));
		awb_no.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,String>("awb_no"));
		bookingdate.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,String>("booking_date"));
		branch.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,String>("invoice2branchcode"));
		clientcode.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,String>("clientcode"));
		sub_clientcode.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,String>("sub_clientcode"));
		fwdWeight.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("fwb_weight"));
		billingweight.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("billingweight"));
		insuracneamount.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("insurance_amount"));
		cod.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("cod_amount"));
		fod.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("fod_amount"));
		docketcharge.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("docket_charge"));
		otherVas.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("other_amount"));
		amount.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("amount"));
		fuel.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("fuel"));
		servicetax.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("servicetax"));
		total.setCellValueFactory(new PropertyValueFactory<InvoiceTableBean,Double>("total"));
		
		
		
	}
	
	

}
