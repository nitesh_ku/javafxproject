package com.onesoft.courier.alert;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

public class AlertBoxTableBean {
	
	private SimpleStringProperty invoiceno;
	private SimpleStringProperty clientname;
	private SimpleDoubleProperty totalamount;
	private SimpleDoubleProperty receivedamount;
	private SimpleDoubleProperty dueamount;
	private SimpleStringProperty invoicedate;
	
	
	public AlertBoxTableBean(String invoiceno, String clientname, double totalamount, double receivedamount,double dueamount,String invoicedate)
	{
		super();
		this.invoiceno=new SimpleStringProperty(invoiceno);
		this.clientname=new SimpleStringProperty(clientname);
		this.totalamount=new SimpleDoubleProperty(totalamount);
		this.receivedamount=new SimpleDoubleProperty(receivedamount);
		this.dueamount=new SimpleDoubleProperty(dueamount);
		this.invoicedate=new SimpleStringProperty(invoicedate);
		
	}
	

	public String getInvoiceno() {
		return invoiceno.get();
	}
	public void setInvoiceno(SimpleStringProperty invoiceno) {
		this.invoiceno = invoiceno;
	}
	public String getClientname() {
		return clientname.get();
	}
	public void setClientname(SimpleStringProperty clientname) {
		this.clientname = clientname;
	}
	public double getTotalamount() {
		return totalamount.get();
	}
	public void setTotalamount(SimpleDoubleProperty totalamount) {
		this.totalamount = totalamount;
	}
	public double getReceivedamount() {
		return receivedamount.get();
	}
	public void setReceivedamount(SimpleDoubleProperty receivedamount) {
		this.receivedamount = receivedamount;
	}
	public double getDueamount() {
		return dueamount.get();
	}
	public void setDueamount(SimpleDoubleProperty dueamount) {
		this.dueamount = dueamount;
	}
	public String getInvoicedate() {
		return invoicedate.get();
	}
	public void setInvoicedate(SimpleStringProperty invoicedate) {
		this.invoicedate = invoicedate;
	}

}
