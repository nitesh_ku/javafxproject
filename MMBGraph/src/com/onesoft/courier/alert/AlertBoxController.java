package com.onesoft.courier.alert;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

import com.DB.DBconnection;
import com.onesoft.courier.main.Main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;


public class AlertBoxController implements Initializable{
	
	public static boolean checkAlert=true;
	
	private ObservableList<AlertBoxTableBean> alerttabledata=FXCollections.observableArrayList();
	ObservableList<String> ComboBoxSnoozeTimer = FXCollections.observableArrayList("2 sec","5 sec","10 sec","15 sec","20 sec","30 sec","35 sec","40 sec","45 sec","50 sec","55 sec");
	
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	DecimalFormat df=new DecimalFormat(".##");
	
	@FXML
	private ComboBox<String> comboBoxSnooze;
	
	@FXML
	private TableView<AlertBoxTableBean> alertTable;
	
	@FXML
	private TableColumn<AlertBoxTableBean, String> invoiceNumber;
	
	@FXML
	private TableColumn<AlertBoxTableBean, String> invoiceDate;
	
	@FXML
	private TableColumn<AlertBoxTableBean, String> clientName;

	@FXML
	private TableColumn<AlertBoxTableBean, Double> dueAmount;
	
	@FXML
	private TableColumn<AlertBoxTableBean, Double> totalAmount;
	
	@FXML
	private TableColumn<AlertBoxTableBean, Double> receivedAmount;
	
	@FXML
	private Button btnSnooze;
	
	
	public void loadUnpaidInvoiceDate() throws SQLException
	{
		alerttabledata.clear();
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
			
		ResultSet rs=null;
		Statement st=null;
		String finalsql=null;
		
		AlertBoxBean alrtBean=new AlertBoxBean();
		
		int serialno=1;
		try
		{
			st=con.createStatement();
			finalsql="select invoice_number,invoice_date,clientname,grandtotal,due,amtreceived from invoice where status !='P' order by invoice_number";
			//System.out.println(finalsql);
			rs=st.executeQuery(finalsql);
		
			if(!rs.next())
			{
				
			}
			else
			{
				do
				{
					alrtBean.setInvoiceno(rs.getString("invoice_number"));
					alrtBean.setClientname(rs.getString("clientname"));
					alrtBean.setTotalamount(Double.parseDouble(df.format(rs.getDouble("grandtotal"))));
				
					alrtBean.setReceivedamount(Double.parseDouble(df.format(rs.getDouble("amtreceived"))));
					alrtBean.setDueamount(Double.parseDouble(df.format(rs.getDouble("due"))));
					alrtBean.setInvoicedate(date.format(rs.getDate("invoice_date")));
					
					if(alrtBean.getReceivedamount()==0.0)
					{
						alrtBean.setDueamount(alrtBean.getTotalamount());	
					}
										
					alerttabledata.add(new AlertBoxTableBean(alrtBean.getInvoiceno(), alrtBean.getClientname(), alrtBean.getTotalamount(), alrtBean.getReceivedamount(), alrtBean.getDueamount(), alrtBean.getInvoicedate()));
					
				}while(rs.next());
			}
			
			alertTable.setItems(alerttabledata);
			//invoiceTable.setItems(alerttabledata);
		
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{	
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
	
	
	@FXML
	public void snooze(ActionEvent actionEvent) throws InterruptedException, IOException, SQLException{
		String val= comboBoxSnooze.getValue();
		//System.out.println(val);
		if (val!=null) {
			System.out.println("selected time interval is: " +val);
			 com.onesoft.courier.main.Main m = new com.onesoft.courier.main.Main();
			 Node  source = (Node)  actionEvent.getSource();
			Stage stage  = (Stage) source.getScene().getWindow();
			 //m.alertMessage();
		stage.close();
		    int szooneTime = Integer.valueOf(val.replaceAll("\\D+",""));
		   
		    Task<Void> sleeper = new Task<Void>() {
	            @Override
	            protected Void call() throws Exception {
	                try {
	                	checkAlert=false;
	                    Thread.sleep(szooneTime*1000);
	                } catch (InterruptedException e) {
	                }
	                return null;
	            }
	        };
	        
	        sleeper.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
	            @Override
	            public void handle(WorkerStateEvent event) {
	             //   label.setText("Hello World");
	            	 try {
						m.alertbox();
						checkAlert=true;
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            }
	        });
	      
	      new Thread(sleeper).start();
			 
		}
		else{
			
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Snooze Alert");
			//alert.setHeaderText("Snooze Alert");
			alert.setContentText("Please select the Snooze time interval...!!");

			alert.showAndWait();
			//System.out.println("please select the time interval");
		}
		//sendmail();
	}
	
	

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
		
		comboBoxSnooze.setItems(ComboBoxSnoozeTimer);
		
		try {
			loadUnpaidInvoiceDate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		invoiceNumber.setCellValueFactory(new PropertyValueFactory<AlertBoxTableBean,String>("invoiceno"));
		clientName.setCellValueFactory(new PropertyValueFactory<AlertBoxTableBean,String>("clientname"));
		invoiceDate.setCellValueFactory(new PropertyValueFactory<AlertBoxTableBean,String>("invoicedate"));
		totalAmount.setCellValueFactory(new PropertyValueFactory<AlertBoxTableBean,Double>("totalamount"));
		receivedAmount.setCellValueFactory(new PropertyValueFactory<AlertBoxTableBean,Double>("receivedamount"));
		dueAmount.setCellValueFactory(new PropertyValueFactory<AlertBoxTableBean,Double>("dueamount"));
		
	}

}
