package com.onesoft.courier.alert;

public class AlertBoxBean {
	
	private String invoiceno;
	private String clientname;
	private Double totalamount;
	private Double receivedamount;
	private Double dueamount;
	private String invoicedate;
	
	
	public String getInvoiceno() {
		return invoiceno;
	}
	public void setInvoiceno(String invoiceno) {
		this.invoiceno = invoiceno;
	}
	public String getClientname() {
		return clientname;
	}
	public void setClientname(String clientname) {
		this.clientname = clientname;
	}
	public Double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}
	public Double getReceivedamount() {
		return receivedamount;
	}
	public void setReceivedamount(Double receivedamount) {
		this.receivedamount = receivedamount;
	}
	public Double getDueamount() {
		return dueamount;
	}
	public void setDueamount(Double dueamount) {
		this.dueamount = dueamount;
	}
	public String getInvoicedate() {
		return invoicedate;
	}
	public void setInvoicedate(String invoicedate) {
		this.invoicedate = invoicedate;
	}
	

}
