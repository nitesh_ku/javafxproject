package com.onesoft.courier.complaint.bean;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public class ComplaintPendingTableBean {
	
	private SimpleIntegerProperty serialno;
	private SimpleStringProperty complaintno;
	private SimpleStringProperty complaintname;
	private SimpleStringProperty emailID;
	private SimpleStringProperty indate;
	private SimpleStringProperty awbno;
	private SimpleStringProperty status;
	private SimpleStringProperty remarks;
	
	private SimpleLongProperty phone;
	
	
	public ComplaintPendingTableBean(int serialno, String complaintno, String name, String email, String indate, String awbno, String status, String remarks, long phone)
	{
		super();
		this.serialno=new SimpleIntegerProperty(serialno);
		this.complaintno=new SimpleStringProperty(complaintno);
		this.complaintname=new SimpleStringProperty(name);
		this.emailID=new SimpleStringProperty(email);
		this.indate=new SimpleStringProperty(indate);
		this.awbno=new SimpleStringProperty(awbno);
		this.status=new SimpleStringProperty(status);
		this.remarks=new SimpleStringProperty(remarks);
		this.phone=new SimpleLongProperty(phone); 
				
	}
	

	public Integer getSerialno() {
		return serialno.get();
	}

	public void setSerialno(SimpleIntegerProperty serialno) {
		this.serialno = serialno;
	}

	public String getComplaintno() {
		return complaintno.get();
	}

	public void setComplaintno(SimpleStringProperty complaintno) {
		this.complaintno = complaintno;
	}

	public String getIndate() {
		return indate.get();
	}

	public void setIndate(SimpleStringProperty indate) {
		this.indate = indate;
	}

	public String getAwbno() {
		return awbno.get();
	}

	public void setAwbno(SimpleStringProperty awbno) {
		this.awbno = awbno;
	}

	public String getStatus() {
		return status.get();
	}

	public void setStatus(SimpleStringProperty status) {
		this.status = status;
	}

	public String getRemarks() {
		return remarks.get();
	}

	public void setRemarks(SimpleStringProperty remarks) {
		this.remarks = remarks;
	}

	public Long getPhone() {
		return phone.get();
	}

	public void setPhone(SimpleLongProperty phone) {
		this.phone = phone;
	}


	public String getComplaintname() {
		return complaintname.get();
	}


	public void setComplaintname(SimpleStringProperty complaintname) {
		this.complaintname = complaintname;
	}


	public String getEmailID() {
		return emailID.get();
	}


	public void setEmailID(SimpleStringProperty emailID) {
		this.emailID = emailID;
	}

}
