package com.onesoft.courier.graphs.utils;

public class FilterUtils {

	
	public static final String ALLBRANCH = "All Branches";
	
	
	
	public static final String TYPE1 = "Both";
	public static final String TYPE2 = "Cash";
	public static final String TYPE3 = "Credit";
	
	public static final String CASH = "CASH";
	public static final String CREDIT = "CREDIT";
	
	public static final String MODE_AIR = "Air";
	public static final String MODE_SURFACE = "Surface";
	
	public static final String CLIENT="All Clients";
	
	public static final String ALLCITY="All Cities";
	
	public static final String TOP5CITY="Top 5 Cities";
	public static final String TOP10CITY="Top 10 Cities";
	public static final String TOP5LIMIT = "DESC LIMIT 5";
	public static final String TOP10LIMIT = "DESC LIMIT 10";
	
	public static final String NETWORK1 = "All Networks";
	
	public static final String ALLSERVICEGROUP = "All Service Groups";
	
	public static final String DND="All Dox/Non-Dox";
	
	public static final String ALLZONE="All Zones";

	public static final String YEAR1 = "2017-2018";
	public static final String YEAR2 = "2016-2017";
	public static final String YEAR3 = "2015-2016";
	public static final String YEAR4 = "2014-2015";
	public static final String YEAR5 = "2013-2014";
	public static final String YEAR6 = "2012-2013";
	
	public static final String EXPENSEBRANCHNAME="ONE";
	
	public static final String CATEGORY1SDATE = "-04-01";					// Date DD.MM format
	public static final String CATEGORY1EDATE = "-03-31";
	


	
	
	
}
