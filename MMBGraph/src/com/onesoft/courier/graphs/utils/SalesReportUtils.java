package com.onesoft.courier.graphs.utils;

import javax.xml.soap.Text;

public class SalesReportUtils {
	
	
	public static final String BRANCH1 = "All Branches";

	
	// ----------- ComboBox Items for Categories -------------
	
	public static final String CATEGORY1 = "Yearly";
	public static final String CATEGORY1SDATE = "01.04";					// Date DD.MM format
	public static final String CATEGORY1EDATE = "31.03";					// Date DD.MM format
	
	public static final String CATEGORY2 = "Half-Yearly";
	public static final String CATEGORY3 = "Quarterly";
	
	
	// ----------- ComboBox Items for Years -------------
	
	public static final String YEAR1 = "2017-2018";
	public static final String YEAR2 = "2016-2017";
	public static final String YEAR3 = "2015-2016";
	public static final String YEAR4 = "2014-2015";
	public static final String YEAR5 = "2013-2014";
	public static final String YEAR6 = "2012-2013";
	
	
	// ----------- ComboBox Items for Semi-yearly -------------

	public static final String SEMI1 = "Apr-Sep";
	public static final String SEMI1SDATE = "01.04";					// Date DD.MM format
	public static final String SEMI1EDATE = "30.09";					// Date DD.MM format
	
	public static final String SEMI2 = "Oct-Mar";
	public static final String SEMI2SDATE = "01.10";					// Date DD.MM format
	public static final String SEMI2EDATE = "31.03";					// Date DD.MM format
	
	// ----------- ComboBox Items for Quarterly -------------
	
				// Date DD.MM format	
	
	public static final String QUARTER1 = "Apr-Jun";
	public static final String QUARTER1SDATE = "01.04";					// Date DD.MM format
	public static final String QUARTER1EDATE = "30.06";					// Date DD.MM format
	
	public static final String QUARTER2 = "July-Sep";
	public static final String QUARTER2SDATE = "01.07";					// Date DD.MM format
	public static final String QUARTER2EDATE = "30.09";					// Date DD.MM format
	
	public static final String QUARTER3 = "Oct-Dec";
	public static final String QUARTER3SDATE = "01.10";					// Date DD.MM format
	public static final String QUARTER3EDATE = "31.12";	
	
	public static final String QUARTER4 = "Jan-Mar";
	public static final String QUARTER4SDATE = "01.01";					// Date DD.MM format
	public static final String QUARTER4EDATE = "31.03";					// Date DD.MM format
	
	// ----------- ComboBox Items for Type -------------
	
	public static final String AlERTBOXTITLE = "Validation Alert Message";
	public static final String AlERTBOXMESSAGE = "Please fill all the textfields...!";

}
