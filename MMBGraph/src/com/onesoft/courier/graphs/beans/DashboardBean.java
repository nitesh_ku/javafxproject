package com.onesoft.courier.graphs.beans;

public class DashboardBean {
	
	private double cash;
	private double credit;
	private String client;
	private double totalamount;
	
	private int dox;
	private int nondox;
	private int totalpcs;
	private String booking_date;
	
	private int salesDays;
	private int clientDays;
	private int zoneDays;
	private int cityDays;
	private int networkDays;
	private int srvcDays;
	
	
	
	
	
	public int getSalesDays() {
		return salesDays;
	}
	public void setSalesDays(int salesDays) {
		this.salesDays = salesDays;
	}
	public int getClientDays() {
		return clientDays;
	}
	public void setClientDays(int clientDays) {
		this.clientDays = clientDays;
	}
	public int getZoneDays() {
		return zoneDays;
	}
	public void setZoneDays(int zoneDays) {
		this.zoneDays = zoneDays;
	}
	public int getCityDays() {
		return cityDays;
	}
	public void setCityDays(int cityDays) {
		this.cityDays = cityDays;
	}
	public int getNetworkDays() {
		return networkDays;
	}
	public void setNetworkDays(int networkDays) {
		this.networkDays = networkDays;
	}
	public int getSrvcDays() {
		return srvcDays;
	}
	public void setSrvcDays(int srvcDays) {
		this.srvcDays = srvcDays;
	}
	public int getDox() {
		return dox;
	}
	public void setDox(int dox) {
		this.dox = dox;
	}
	public int getNondox() {
		return nondox;
	}
	public void setNondox(int nondox) {
		this.nondox = nondox;
	}
	public int getTotalpcs() {
		return totalpcs;
	}
	public void setTotalpcs(int totalpcs) {
		this.totalpcs = totalpcs;
	}
	public String getBooking_date() {
		return booking_date;
	}
	public void setBooking_date(String booking_date) {
		this.booking_date = booking_date;
	}
	public double getCash() {
		return cash;
	}
	public void setCash(double cash) {
		this.cash = cash;
	}
	public double getCredit() {
		return credit;
	}
	public void setCredit(double credit) {
		this.credit = credit;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(double totalamount) {
		this.totalamount = totalamount;
	}
	

}
