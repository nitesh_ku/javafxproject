package com.onesoft.courier.graphs.beans;

public class ServiceGroupBean {
	
	//-------------------- Normal Java Bean class  -----------------
	
	private Integer serialno;
	private String awb_no;
	private String dbtClient;
	private String bookingdate;
	private String branch;
	private String clientcode;
	private String city;
	private String Network;
	private String service;
	private String doxnonDox;
	private String profitlosspercentage;
	private String profitloss;
	
	private Integer packets;
	
	private Double billingweight;
	private Double insuracneamount;
	private Double shipercost;
	private Double cod;
	private Double fod;
	private Double docketcharge;
	private Double otherVas;
	private Double amount;
	private Double fuel;
	private Double servicetax;
	private Double total;
	
	
	
	
	public String getAwb_no() {
		return awb_no;
	}
	public void setAwb_no(String awb_no) {
		this.awb_no = awb_no;
	}
	
	
	public String getBookingdate() {
		return bookingdate;
	}
	public void setBookingdate(String bookingdate) {
		this.bookingdate = bookingdate;
	}
	
	
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	
	public String getClientcode() {
		return clientcode;
	}
	public void setClientcode(String clientcode) {
		this.clientcode = clientcode;
	}
	
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	
	public String getNetwork() {
		return Network;
	}
	public void setNetwork(String network) {
		Network = network;
	}
	
	
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	
	
	public String getDoxnonDox() {
		return doxnonDox;
	}
	public void setDoxnonDox(String doxnonDox) {
		this.doxnonDox = doxnonDox;
	}
	
	
	public Integer getPackets() {
		return packets;
	}
	public void setPackets(Integer packets) {
		this.packets = packets;
	}
	
	
	public Double getBillingweight() {
		return billingweight;
	}
	public void setBillingweight(Double billingweight) {
		this.billingweight = billingweight;
	}
	
	
	public Double getInsuracneamount() {
		return insuracneamount;
	}
	public void setInsuracneamount(Double insuracneamount) {
		this.insuracneamount = insuracneamount;
	}
	
	
	public Double getShipercost() {
		return shipercost;
	}
	public void setShipercost(Double shipercost) {
		this.shipercost = shipercost;
	}
	
	
	public Double getCod() {
		return cod;
	}
	public void setCod(Double cod) {
		this.cod = cod;
	}
	
	
	public Double getFod() {
		return fod;
	}
	public void setFod(Double fod) {
		this.fod = fod;
	}
	
	
	public Double getDocketcharge() {
		return docketcharge;
	}
	public void setDocketcharge(Double docketcharge) {
		this.docketcharge = docketcharge;
	}
	
	
	public Double getOtherVas() {
		return otherVas;
	}
	public void setOtherVas(Double otherVas) {
		this.otherVas = otherVas;
	}
	
	
	public Double getFuel() {
		return fuel;
	}
	public void setFuel(Double fuel) {
		this.fuel = fuel;
	}
	
	
	public Double getServicetax() {
		return servicetax;
	}
	public void setServicetax(Double servicetax) {
		this.servicetax = servicetax;
	}
	
	
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	
	
	public Integer getSerialno() {
		return serialno;
	}
	public void setSerialno(Integer serialno) {
		this.serialno = serialno;
	}
	
	
	public String getDbtClient() {
		return dbtClient;
	}
	public void setDbtClient(String dbtClient) {
		this.dbtClient = dbtClient;
	}
	
	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public String getProfitlosspercentage() {
		return profitlosspercentage;
	}
	public void setProfitlosspercentage(String profitlosspercentage) {
		this.profitlosspercentage = profitlosspercentage;
	}
	
	
	public String getProfitloss() {
		return profitloss;
	}
	public void setProfitloss(String profitloss) {
		this.profitloss = profitloss;
	}
	
	
}
