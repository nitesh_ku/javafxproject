package com.onesoft.courier.graphs.beans;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class SalesJavaFXBean {


	//-------------------- JavaFX Bean class for populate the Tableview and other JavaFX components -----------------
	
	private SimpleIntegerProperty serialno;
	private SimpleStringProperty awb_no;
	private SimpleStringProperty dbtclient;
	private SimpleStringProperty bookingdate;
	private SimpleStringProperty branch;
	private SimpleStringProperty clientcode;
	private SimpleStringProperty city;
	private SimpleStringProperty network;
	private SimpleStringProperty service;
	private SimpleStringProperty doxnonDox;
	private SimpleStringProperty percentage;
	private SimpleStringProperty profitlosspercentage;
	private SimpleStringProperty profitloss;
	
	private SimpleIntegerProperty packets;
	
	private SimpleDoubleProperty billingweight;
	private SimpleDoubleProperty insuracneamount;
	private SimpleDoubleProperty shipercost;
	private SimpleDoubleProperty cod;
	private SimpleDoubleProperty fod;
	private SimpleDoubleProperty docketcharge;
	private SimpleDoubleProperty otherVas;
	private SimpleDoubleProperty amount;
	private SimpleDoubleProperty fuel;
	private SimpleDoubleProperty servicetax;
	private SimpleDoubleProperty total;
	
	
	public SalesJavaFXBean(int serialno,String awb_no, String dbtclient,String bookingdate, String branch, String clientcode, String city, String network, String service,
								String doxnonDox, int packets, double billingweight, double insuracneamount, double shipercost, double cod,
									double fod, double docketcharge, double otherVas, double amount, double fuel, double servicetax, double total,String percent, String profitloss,String profitlosspercentage)
	{
		super();
		this.serialno=new SimpleIntegerProperty(serialno);
		this.awb_no= new SimpleStringProperty(awb_no);		
		this.dbtclient=new SimpleStringProperty(dbtclient);
		this.bookingdate=new SimpleStringProperty(bookingdate);
		this.branch=new SimpleStringProperty(branch);
		this.clientcode=new SimpleStringProperty(clientcode);
		this.city=new SimpleStringProperty(city);
		this.network=new SimpleStringProperty(network);
		this.service=new SimpleStringProperty(service);
		this.doxnonDox=new SimpleStringProperty(doxnonDox);
		this.packets=new SimpleIntegerProperty(packets);
		this.billingweight=new SimpleDoubleProperty(billingweight);
		this.insuracneamount=new SimpleDoubleProperty(insuracneamount);
		this.shipercost=new SimpleDoubleProperty(shipercost);
		this.cod=new SimpleDoubleProperty(cod);
		this.fod=new SimpleDoubleProperty(fod);
		this.docketcharge=new SimpleDoubleProperty(docketcharge);
		this.otherVas=new SimpleDoubleProperty(otherVas);
		this.amount=new SimpleDoubleProperty(amount);
		this.fuel=new SimpleDoubleProperty(fuel);
		this.servicetax=new SimpleDoubleProperty(servicetax);
		this.total=new SimpleDoubleProperty(total);
		this.percentage=new SimpleStringProperty(percent);
		this.profitloss=new SimpleStringProperty(profitloss);
		this.profitlosspercentage=new SimpleStringProperty(profitlosspercentage);
		
	}
	
	public String getAwb_no() {
		return awb_no.get();
	}
	public void setAwb_no(SimpleStringProperty awb_no) {
		this.awb_no = awb_no;
	}
	
	
	public String getBookingdate() {
		return bookingdate.get();
	}
	public void setBookingdate(SimpleStringProperty bookingdate) {
		this.bookingdate = bookingdate;
	}
	
	
	public String getBranch() {
		return branch.get();
	}
	public void setBranch(SimpleStringProperty branch) {
		this.branch = branch;
	}
	
	
	public String getClientcode() {
		return clientcode.get();
	}
	public void setClientcode(SimpleStringProperty clientcode) {
		this.clientcode = clientcode;
	}
	
	
	public String getCity() {
		return city.get();
	}
	public void setCity(SimpleStringProperty city) {
		this.city = city;
	}
	
	
	public String getNetwork() {
		return network.get();
	}
	public void setNetwork(SimpleStringProperty network) {
		this.network = network;
	}
	
	
	public String getService() {
		return service.get();
	}
	public void setService(SimpleStringProperty service) {
		this.service = service;
	}
	
	
	public String getDoxnonDox() {
		return doxnonDox.get();
	}
	public void setDoxnonDox(SimpleStringProperty doxnonDox) {
		this.doxnonDox = doxnonDox;
	}
	
	
	public int getPackets() {
		return packets.get();
	}
	public void setPackets(SimpleIntegerProperty packets) {
		this.packets = packets;
	}
	
	
	public double getBillingweight() {
		return billingweight.get();
	}
	public void setBillingweight(SimpleDoubleProperty billingweight) {
		this.billingweight = billingweight;
	}
	
	
	public double getInsuracneamount() {
		return insuracneamount.get();
	}
	public void setInsuracneamount(SimpleDoubleProperty insuracneamount) {
		this.insuracneamount = insuracneamount;
	}
	
	
	public double getShipercost() {
		return shipercost.get();
	}
	public void setShipercost(SimpleDoubleProperty shipercost) {
		this.shipercost = shipercost;
	}
	
	
	public double getCod() {
		return cod.get();
	}
	public void setCod(SimpleDoubleProperty cod) {
		this.cod = cod;
	}
	
	
	public double getFod() {
		return fod.get();
	}
	public void setFod(SimpleDoubleProperty fod) {
		this.fod = fod;
	}
	
	
	public double getDocketcharge() {
		return docketcharge.get();
	}
	public void setDocketcharge(SimpleDoubleProperty docketcharge) {
		this.docketcharge = docketcharge;
	}
	
	
	public double getOtherVas() {
		return otherVas.get();
	}
	public void setOtherVas(SimpleDoubleProperty otherVas) {
		this.otherVas = otherVas;
	}
	
	
	public double getFuel() {
		return fuel.get();
	}
	public void setFuel(SimpleDoubleProperty fuel) {
		this.fuel = fuel;
	}
	
	
	public double getServicetax() {
		return servicetax.get();
	}
	public void setServicetax(SimpleDoubleProperty servicetax) {
		this.servicetax = servicetax;
	}
	
	
	public double getTotal() {
		return total.get();
	}
	public void setTotal(SimpleDoubleProperty total) {
		this.total = total;
	}

	public Integer getSerialno() {
		return serialno.get();
	}
	public void setSerialno(SimpleIntegerProperty serialno) {
		this.serialno = serialno;
	}

	public String getDbtclient() {
		return dbtclient.get();
	}
	public void setDbtclient(SimpleStringProperty dbtclient) {
		this.dbtclient = dbtclient;
	}

	public Double getAmount() {
		return amount.get();
	}

	public void setAmount(SimpleDoubleProperty amount) {
		this.amount = amount;
	}
	
	public String getPercentage() {
		return percentage.get();
	}

	public void setPercentage(SimpleStringProperty percentage) {
		this.percentage = percentage;
	}
	
	public String getProfitlosspercentage() {
		return profitlosspercentage.get();
	}
	public void setProfitlosspercentage(SimpleStringProperty profitlosspercentage) {
		this.profitlosspercentage = profitlosspercentage;
	}


	public String getProfitloss() {
		return profitloss.get();
	}
	public void setProfitloss(SimpleStringProperty profitloss) {
		this.profitloss = profitloss;
	}

	
	
}
