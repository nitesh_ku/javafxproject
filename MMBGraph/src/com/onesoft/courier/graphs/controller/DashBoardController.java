package com.onesoft.courier.graphs.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.scene.image.Image;
import com.DB.DBconnection;
import com.onesoft.courier.graphs.beans.DashboardBean;
import com.onesoft.courier.main.Main;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.control.Alert.AlertType;

public class DashBoardController implements Initializable{
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	
	public static String configfilePath = "typeproperties.properties";
	 Properties prop = new Properties();
	 InputStream in = this.getClass().getClassLoader().getResourceAsStream(configfilePath);

	
	 @FXML
	 private Button btnsales;
	 @FXML
	 private Button btncity;
	 @FXML
	 private Button btnclient;
	 @FXML
	 private Button btnnetwork;
	 @FXML
	 private Button btnzone;
	 @FXML
	 private Button btnservicegroup;
	
	 @FXML
	 private TextField txtSales;
	 @FXML
	 private TextField txtClient;
	 @FXML
	 private TextField txtZone;
	 @FXML
	 private TextField txtCity;
	 @FXML
	 private TextField txtNetwork;
	 @FXML
	 private TextField txtServicegroup;
	 
	 
//======== Sales details components =============== 
	
	@FXML
	private Hyperlink linksale;
	@FXML
	private NumberAxis yAxis;
	@FXML
	private CategoryAxis xAxis;
	@FXML
	private Label labCash;
	@FXML
	private Label labCredit;
	
	
	@FXML
	private Label labSalesDox1;
	@FXML
	private Label labSalesDox2;
	
	@FXML
	private Label labSalesNonDox1;
	@FXML
	private Label labSalesNonDox2;
	
	@FXML
	private Label labSalesDNDtotal1;
	@FXML
	private Label labSalesDNDtotal2;
	
	
	@FXML
	private Label labSalesTotalDox;
	@FXML
	private Label labSalesTotalNonDox;
	@FXML
	private Label labSalesFinalTotal;
	@FXML
	private Label labSalesTotalAmt;
	@FXML
	private LineChart<?, ?> salesLineChart;
	
	
//======== Client details components =============== 
	
	
	
	@FXML
	private Hyperlink linkClient;
	@FXML
	private NumberAxis yAxisClient;
	@FXML
	private CategoryAxis xAxisClient;
	
	@FXML
	private Label labClient1;
	@FXML
	private Label labClient2;
	@FXML
	private Label labClient3;
	@FXML
	private Label labClient4;
	@FXML
	private Label labClient5;
	
	@FXML
	private Label labClient1Value;
	@FXML
	private Label labClient2Value;
	@FXML
	private Label labClient3Value;
	@FXML
	private Label labClient4Value;
	@FXML
	private Label labClient5Value;
	
	@FXML
	private Label labClientDox1;
	@FXML
	private Label labClientDox2;
	@FXML
	private Label labClientDox3;
	@FXML
	private Label labClientDox4;
	@FXML
	private Label labClientDox5;
	
	
	@FXML
	private Label labClientNonDox1;
	@FXML
	private Label labClientNonDox2;
	@FXML
	private Label labClientNonDox3;
	@FXML
	private Label labClientNonDox4;
	@FXML
	private Label labClientNonDox5;
	
	
	@FXML
	private Label labClientDNDtotal1;
	@FXML
	private Label labClientDNDtotal2;
	@FXML
	private Label labClientDNDtotal3;
	@FXML
	private Label labClientDNDtotal4;
	@FXML
	private Label labClientDNDtotal5;
	

	@FXML
	private Label labClientTotalDox;
	@FXML
	private Label labClientTotalNonDox;
	@FXML
	private Label labClientFinalTotal;
	@FXML
	private Label labClientTotalAmt;
	@FXML
	private LineChart<?, ?> clientlinechart;
	
	
//======== Zone details components =============== 
	
	@FXML
	private Hyperlink linkZone;
	@FXML
	private NumberAxis yAxisZone;
	@FXML
	private CategoryAxis xAxisZone;
	
	@FXML
	private Label labZone1;
	@FXML
	private Label labZone2;
	@FXML
	private Label labZone3;
	@FXML
	private Label labZone4;
	@FXML
	private Label labZone5;
	
	@FXML
	private Label labZone1Value;
	@FXML
	private Label labZone2Value;
	@FXML
	private Label labZone3Value;
	@FXML
	private Label labZone4Value;
	@FXML
	private Label labZone5Value;
	
	@FXML
	private Label labZoneDox1;
	@FXML
	private Label labZoneDox2;
	@FXML
	private Label labZoneDox3;
	@FXML
	private Label labZoneDox4;
	@FXML
	private Label labZoneDox5;
	
	@FXML
	private Label labZoneNonDox1;
	@FXML
	private Label labZoneNonDox2;
	@FXML
	private Label labZoneNonDox3;
	@FXML
	private Label labZoneNonDox4;
	@FXML
	private Label labZoneNonDox5;
	
	@FXML
	private Label labZoneDNDtotal1;
	@FXML
	private Label labZoneDNDtotal2;
	@FXML
	private Label labZoneDNDtotal3;
	@FXML
	private Label labZoneDNDtotal4;
	@FXML
	private Label labZoneDNDtotal5;
	
	@FXML
	private Label labZoneTotalAmt;
	@FXML
	private Label labZoneTotalDox;
	@FXML
	private Label labZoneTotalNonDoxAmt;
	@FXML
	private Label labZoneFinalTotal;
	@FXML
	private LineChart<?, ?> zonelinechart;
	
	
	
//-------------------- City detail ------------------
	
	@FXML
	private Label labCity1;
	@FXML
	private Label labCity2;
	@FXML
	private Label labCity3;
	@FXML
	private Label labCity4;
	@FXML
	private Label labCity5;
	
	
	@FXML
	private Label labCity1Amt;
	@FXML
	private Label labCity2Amt;
	@FXML
	private Label labCity3Amt;
	@FXML
	private Label labCity4Amt;
	@FXML
	private Label labCity5Amt;
	
	
	@FXML
	private Label labCityDox1;
	@FXML
	private Label labCityDox2;
	@FXML
	private Label labCityDox3;
	@FXML
	private Label labCityDox4;
	@FXML
	private Label labCityDox5;
	
	
	@FXML
	private Label labCityNonDox1;
	@FXML
	private Label labCityNonDox2;
	@FXML
	private Label labCityNonDox3;
	@FXML
	private Label labCityNonDox4;
	@FXML
	private Label labCityNonDox5;
	
	
	@FXML
	private Label labCityDNDtotal1;
	@FXML
	private Label labCityDNDtotal2;
	@FXML
	private Label labCityDNDtotal3;
	@FXML
	private Label labCityDNDtotal4;
	@FXML
	private Label labCityDNDtotal5;
	
	
	@FXML
	private Label labCityTotalAmt;
	@FXML
	private Label labCityTotalDox;
	@FXML
	private Label labCityTotalNonDoxAmt;
	@FXML
	private Label labCityFinalTotal;
	
	
	@FXML
	private NumberAxis yAxisCity;
	@FXML
	private CategoryAxis xAxisCity;
	@FXML
	private LineChart<?, ?> cityLineChart;
	@FXML
	private Hyperlink linkCity;
	
	
//-------------------- Network detail ------------------
	
	@FXML
	private Label labNet1;
	@FXML
	private Label labNet2;
	@FXML
	private Label labNet3;
	@FXML
	private Label labNet4;
	@FXML
	private Label labNet5;
	
	
	@FXML
	private Label labNet1Amt;
	@FXML
	private Label labNet2Amt;
	@FXML
	private Label labNet3Amt;
	@FXML
	private Label labNet4Amt;
	@FXML
	private Label labNet5Amt;
	
	
	@FXML
	private Label labNetDox1;
	@FXML
	private Label labNetDox2;
	@FXML
	private Label labNetDox3;
	@FXML
	private Label labNetDox4;
	@FXML
	private Label labNetDox5;
	
	
	@FXML
	private Label labNetNonDox1;
	@FXML
	private Label labNetNonDox2;
	@FXML
	private Label labNetNonDox3;
	@FXML
	private Label labNetNonDox4;
	@FXML
	private Label labNetNonDox5;
	
	
	@FXML
	private Label labNetDNDtotal1;
	@FXML
	private Label labNetDNDtotal2;
	@FXML
	private Label labNetDNDtotal3;
	@FXML
	private Label labNetDNDtotal4;
	@FXML
	private Label labNetDNDtotal5;
	
	@FXML
	private Label labNetTotalAmt;
	@FXML
	private Label labNetTotalDox;
	@FXML
	private Label labNetTotalNonDoxAmt;
	@FXML
	private Label labNetFinalTotal;
	
	
	@FXML
	private NumberAxis yAxisNet;
	@FXML
	private CategoryAxis xAxisNet;
	@FXML
	private LineChart<?, ?> NetLineChart;
	
	@FXML
	private Hyperlink linkNetwork;
	
	
//-------------------- Service Group detail ------------------
	
	@FXML
	private Label labSrcGrp1;
	@FXML
	private Label labSrcGrp2;
	@FXML
	private Label labSrcGrp3;
	@FXML
	private Label labSrcGrp4;
	@FXML
	private Label labSrcGrp5;
		
		
	@FXML
	private Label labSrcGrp1Amt;
	@FXML
	private Label labSrcGrp2Amt;
	@FXML
	private Label labSrcGrp3Amt;
	@FXML
	private Label labSrcGrp4Amt;
	@FXML
	private Label labSrcGrp5Amt;
		
		
	@FXML
	private Label labSrcGrpDox1;
	@FXML
	private Label labSrcGrpDox2;
	@FXML
	private Label labSrcGrpDox3;
	@FXML
	private Label labSrcGrpDox4;
	@FXML
	private Label labSrcGrpDox5;
		
		
	@FXML
	private Label labSrcGrpNonDox1;
	@FXML
	private Label labSrcGrpNonDox2;
	@FXML
	private Label labSrcGrpNonDox3;
	@FXML
	private Label labSrcGrpNonDox4;
	@FXML
	private Label labSrcGrpNonDox5;
		
		
	@FXML
	private Label labSrcGrpDNDtotal1;
	@FXML
	private Label labSrcGrpDNDtotal2;
	@FXML
	private Label labSrcGrpDNDtotal3;
	@FXML
	private Label labSrcGrpDNDtotal4;
	@FXML
	private Label labSrcGrpDNDtotal5;
		
	@FXML
	private Label labSrcGrpTotalAmt;
	@FXML
	private Label labSrcGrpTotalDox;
	@FXML
	private Label labSrcGrpTotalNonDoxAmt;
	@FXML
	private Label labSrcGrpFinalTotal;
	
		
	@FXML
	private NumberAxis yAxisSrcGrp;
	@FXML
	private CategoryAxis xAxisSrcGrp;
	@FXML
	private LineChart<?, ?> SrcGrpLineChart;
	
	@FXML
	private Hyperlink linkServiceGrp;

	Set<String> salesSet = new HashSet<String>();
	List<String> cityList=new ArrayList<>();
	List<String> clientList=new ArrayList<String>();
	List<String> zoneList=new ArrayList<String>();
	List<String> networkList=new ArrayList<String>();
	List<String> serviceGroupList=new ArrayList<String>();
	
	
	Main m=new Main();
	
	
	
	@FXML
	private void goMoreSalesInfo() throws IOException
	{
		m.showSalesReport();
	}
	
	@FXML
	private void goMoreClientInfo() throws IOException
	{
		m.showSalesReport();
	}
	
	@FXML
	private void goMoreZoneInfo() throws IOException
	{
		m.showZoneReport();
	}
	
	@FXML
	private void goMoreCityInfo() throws IOException
	{
		m.showTopCityReport();
	}
	
	@FXML
	private void goMoreNetworkInfo() throws IOException
	{
		m.showNetworkReport();
	}
	
	@FXML
	private void goMoreServiceGroupInfo() throws IOException
	{
		m.showServiceGroupReport();
	}
	
	
//------------------------------------------------------------------------------------------------------------------------------
	
	public void setDaysLimitforALL() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		DashboardBean dbbean=new DashboardBean();
		HashMap<String, Integer> hmap = new HashMap<String, Integer>();
		
		try
		{	
			st=con.createStatement();
			String sql="select reportname,days from dashboard";
			rs=st.executeQuery(sql);

			while(rs.next())
			{
				hmap.put(rs.getString(1), rs.getInt(2));
			}
			
			dbbean.setSalesDays(hmap.get("sales"));
			dbbean.setClientDays(hmap.get("client"));
			dbbean.setZoneDays(hmap.get("zone"));
			dbbean.setCityDays(hmap.get("city"));
			dbbean.setNetworkDays(hmap.get("network"));
			dbbean.setSrvcDays(hmap.get("servicegroup"));
			  
			
			txtSales.setText(String.valueOf(dbbean.getSalesDays()));
			txtClient.setText(String.valueOf(dbbean.getClientDays()));
			txtZone.setText(String.valueOf(dbbean.getZoneDays()));
			txtCity.setText(String.valueOf(dbbean.getCityDays()));
			txtNetwork.setText(String.valueOf(dbbean.getNetworkDays()));
			txtServicegroup.setText(String.valueOf(dbbean.getSrvcDays()));
			
			loadSalesDashData(dbbean.getSalesDays());
			loadLineChart(dbbean.getSalesDays());
			
				
			loadTopClientData(dbbean.getClientDays());
			loadClientLineChart(dbbean.getClientDays());
			
			loadTopZoneData(dbbean.getZoneDays());
			loadZoneLineChart(dbbean.getZoneDays());
			  
			loadTopCityData(dbbean.getCityDays());
			loadCityLineChart(dbbean.getCityDays());
		
			
			loadTopNetworkData(dbbean.getNetworkDays());
			loadNetworkLineChart(dbbean.getNetworkDays());
			  
			loadTopServiceGroupData(dbbean.getSrvcDays());
			loadServiceGroupLineChart(dbbean.getSrvcDays());
			  
			  
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st,rs, con);
		}
	}

	//------------------------------------------------------------------------------------------------------------------------------
	 @FXML
	public void setSalesDays() throws SQLException
	{
		if(txtSales.getText().equals(null) || txtSales.getText().isEmpty() || !txtSales.getText().matches("[0-9]*"))
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Please fill the textbox");
			alert.setHeaderText(null);
			alert.setContentText("Textfield blank or incorrect value...!!");
			alert.showAndWait();
			txtSales.clear();
			txtSales.requestFocus();
		}
		else if(Integer.parseInt(txtSales.getText())>15)
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("No. of Days Validation");
			alert.setHeaderText(null);
			alert.setContentText("Number of days should be less than 15");
			alert.showAndWait();
			txtSales.clear();
			txtSales.requestFocus();
		}
		else
		{
			salesLineChart.getData().clear();
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
	
			PreparedStatement preparedStmt=null;
			
			try
			{	
				String query = "update dashboard set days = ? where reportname = ?";
				preparedStmt = con.prepareStatement(query);
				preparedStmt.setInt(1,Integer.parseInt(txtSales.getText()));
				preparedStmt.setString(2, "sales");
				preparedStmt.executeUpdate();
				
				loadSalesDashData(Integer.parseInt(txtSales.getText()));
				loadLineChart(Integer.parseInt(txtSales.getText()));
				
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{
				dbcon.disconnect(preparedStmt,null,null, con);
			}
		}
	}
	
	@FXML
	public void setClientDays() throws SQLException
	{
		if(txtClient.getText().equals(null) || txtClient.getText().isEmpty() || !txtClient.getText().matches("[0-9]*"))
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Please fill the textbox");
			alert.setHeaderText(null);
			alert.setContentText("Textfield blank or incorrect value...!!");
			alert.showAndWait();
			txtClient.clear();
			txtClient.requestFocus();
		}
		else if(Integer.parseInt(txtClient.getText())>15)
		{

			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("No. of Days Validation");
			alert.setHeaderText(null);
			alert.setContentText("Number of days should be less than 15");
			alert.showAndWait();
			txtClient.clear();
			txtClient.requestFocus();
		}
		else
		{
			clientlinechart.getData().clear();
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
		
			PreparedStatement preparedStmt=null;
			
			try
			{	
				String query = "update dashboard set days = ? where reportname = ?";
				preparedStmt = con.prepareStatement(query);
				preparedStmt.setInt(1,Integer.parseInt(txtClient.getText()));
				preparedStmt.setString(2, "client");
				System.out.println(query);
				preparedStmt.executeUpdate();
				
				loadTopClientData(Integer.parseInt(txtClient.getText()));
				loadClientLineChart(Integer.parseInt(txtClient.getText()));
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{
				dbcon.disconnect(preparedStmt, null,null, con);
			}
		}
	}
	
	
	@FXML
	public void setCityDays() throws SQLException
	{
		
		if(txtCity.getText().equals(null) || txtCity.getText().isEmpty() || !txtCity.getText().matches("[0-9]*"))
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Empty Textfield Validation");
			alert.setHeaderText(null);
			alert.setContentText("Textfield blank or incorrect value...!!");
			alert.showAndWait();
			txtCity.clear();
			txtCity.requestFocus();
		}
		else if(Integer.parseInt(txtCity.getText())>15)
		{

			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Please fill the textbox");
			alert.setHeaderText(null);
			alert.setContentText("Number of days should be less than 15");
			alert.showAndWait();
			txtCity.clear();
			txtCity.requestFocus();
		}
		else
		{
			cityLineChart.getData().clear();
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			
			PreparedStatement preparedStmt=null;
			
			try
			{	
				String query = "update dashboard set days = ? where reportname = ?";
				preparedStmt = con.prepareStatement(query);
				preparedStmt.setInt(1,Integer.parseInt(txtCity.getText()));
				preparedStmt.setString(2, "city");
				preparedStmt.executeUpdate();
				     
				loadTopCityData(Integer.parseInt(txtCity.getText()));
				loadCityLineChart(Integer.parseInt(txtCity.getText()));
				
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{
				dbcon.disconnect(preparedStmt, null,null, con);
			}
		}
	}
	
	
	@FXML
	public void setZoneDays() throws SQLException
	{
		if(txtZone.getText().equals(null) || txtZone.getText().isEmpty() || !txtZone.getText().matches("[0-9]*"))
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Please fill the textbox");
			alert.setHeaderText(null);
			alert.setContentText("Textfield blank or incorrect value...!!");
			alert.showAndWait();
			txtZone.clear();
			txtZone.requestFocus();
		}
		else if(Integer.parseInt(txtZone.getText())>15)
		{

			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("No. of Days Validation");
			alert.setHeaderText(null);
			alert.setContentText("Number of days should be less than 15");
			alert.showAndWait();
			txtZone.clear();
			txtZone.requestFocus();
		}
		else
		{
			zonelinechart.getData().clear();
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
		
			PreparedStatement preparedStmt=null;
			
			try
			{	
				String query = "update dashboard set days = ? where reportname = ?";
				preparedStmt = con.prepareStatement(query);
			    preparedStmt.setInt(1,Integer.parseInt(txtZone.getText()));
			    preparedStmt.setString(2, "zone");
			    preparedStmt.executeUpdate();
			     
				loadTopZoneData(Integer.parseInt(txtZone.getText()));
				loadZoneLineChart(Integer.parseInt(txtZone.getText()));
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{
				dbcon.disconnect(preparedStmt, null,null, con);
			}
		}
	}
	
	
	@FXML
	public void setNetworkDays() throws SQLException
	{
		if(txtNetwork.getText().equals(null) || txtNetwork.getText().isEmpty() || !txtNetwork.getText().matches("[0-9]*"))
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Please fill the textbox");
			alert.setHeaderText(null);
			alert.setContentText("Textfield blank or incorrect value...!!");
			alert.showAndWait();
			txtNetwork.clear();
			txtNetwork.requestFocus();
		}
		else if(Integer.parseInt(txtNetwork.getText())>15)
		{

			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("No. of Days Validation");
			alert.setHeaderText(null);
			alert.setContentText("Number of days should be less than 15");
			alert.showAndWait();
			txtNetwork.clear();
			txtNetwork.requestFocus();
		}
		else
		{
			NetLineChart.getData().clear();
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
	
			PreparedStatement preparedStmt=null;
			
			try
			{	
				String query = "update dashboard set days = ? where reportname = ?";
				preparedStmt = con.prepareStatement(query);
				preparedStmt.setInt(1,Integer.parseInt(txtNetwork.getText()));
				preparedStmt.setString(2, "network");
				preparedStmt.executeUpdate();
				     
				loadTopNetworkData(Integer.parseInt(txtNetwork.getText()));
				loadNetworkLineChart(Integer.parseInt(txtNetwork.getText()));
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{
				dbcon.disconnect(preparedStmt, null,null, con);
			}
		}
	}
	
	
	
	@FXML
	public void setServiceGroupDays() throws SQLException
	{
		if(txtServicegroup.getText().equals(null) || txtServicegroup.getText().isEmpty() || !txtServicegroup.getText().matches("[0-9]*"))
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Please fill the textbox");
			alert.setHeaderText(null);
			alert.setContentText("Textfield blank or incorrect value...!!");
			alert.showAndWait();
			txtServicegroup.clear();
			txtServicegroup.requestFocus();
		}
		else if(Integer.parseInt(txtServicegroup.getText())>15)
		{

			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("No. of Days Validation");
			alert.setHeaderText(null);
			alert.setContentText("Number of days should be less than 15");
			alert.showAndWait();
			txtServicegroup.clear();
			txtServicegroup.requestFocus();
		}
		else
		{
			SrcGrpLineChart.getData().clear();
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
		
			PreparedStatement preparedStmt=null;
			
			try
			{	
				String query = "update dashboard set days = ? where reportname = ?";
				preparedStmt = con.prepareStatement(query);
				preparedStmt.setInt(1,Integer.parseInt(txtServicegroup.getText()));
				preparedStmt.setString(2, "client");
				preparedStmt.executeUpdate();
				
				loadTopServiceGroupData(Integer.parseInt(txtServicegroup.getText()));
				loadServiceGroupLineChart(Integer.parseInt(txtServicegroup.getText()));
			}
			catch(Exception e)
			{
				 System.out.println(e);
				 e.printStackTrace();
			}	
			finally
			{
				dbcon.disconnect(preparedStmt, null,null, con);
			}
		}
	}
	
	
//------------------------------------------------------------------------------------------------------------------------------
	public void loadSalesDashData(int daysvalue) throws SQLException, IOException
	{
		prop.load(in);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		double totalsum=0.0;
		Statement st=null;
		ResultSet rs=null;
		
		double t_total=0.0;
		int d_pcs=0;
		double f_total=0.0;
		int n_pcs=0;
		 		 
		try
		{	
			st=con.createStatement();
			//String sql="SELECT cm.client_Type,sum(dbt.total) as total, sum(dbt.packets) as pcs,dbt.dox_nondox FROM dailybookingtransaction as dbt,clientmaster as cm WHERE dbt.dailybookingtransaction2client=cm.code and booking_date > current_date - interval '"+daysvalue+"' day Group by cm.client_type,dbt.dox_nondox order by cm.client_type";
			String sql="SELECT cm.client_Type,sum(dbt.total) as total, sum(dbt.packets) as pcs,dbt.dox_nondox FROM dailybookingtransaction as dbt,clientmaster as cm WHERE dbt.dailybookingtransaction2client=cm.code and booking_date between '2017-03-25' and  '2017-04-03' Group by cm.client_type,dbt.dox_nondox order by cm.client_type";
			
			rs=st.executeQuery(sql);
		
	
			while(rs.next())
			{
				salesSet.add(rs.getString(1));
				
				if(rs.getString(1).equals(prop.getProperty("Cash")))
				{
					t_total=t_total+rs.getDouble(2);
					d_pcs=d_pcs+rs.getInt(3);
				}
				else
				{
					f_total=f_total+rs.getDouble(2);
					n_pcs=n_pcs+rs.getInt(3);
				}
				
				
				if(rs.getString(1).equals(prop.getProperty("Cash")) && rs.getString(4).equals(prop.getProperty("dox")))
				{
					labSalesDox1.setText(String.valueOf(rs.getInt(3)));
				}
				
				else if(rs.getString(1).equals(prop.getProperty("Cash")) && rs.getString(4).equals(prop.getProperty("nondox")))
				{
					labSalesNonDox1.setText(String.valueOf(rs.getInt(3)));
				}
				
				else if(rs.getString(1).equals(prop.getProperty("Credit")) && rs.getString(4).equals(prop.getProperty("dox")))
				{
					labSalesDox2.setText(String.valueOf(rs.getInt(3)));
				}
				else if(rs.getString(1).equals(prop.getProperty("Credit")) && rs.getString(4).equals(prop.getProperty("nondox")))
				{
					labSalesNonDox2.setText(String.valueOf(rs.getInt(3)));
				}
				
				totalsum=totalsum+rs.getDouble(2);
			}
			
			for(String type:salesSet)
			{
				System.out.println(type);
			}
			
			labCash.setText(String.valueOf(df.format(t_total)));
			labCredit.setText(String.valueOf(df.format(f_total)));
			labSalesTotalDox.setText(String.valueOf(d_pcs));
			labSalesTotalNonDox.setText(String.valueOf(n_pcs));
			labSalesFinalTotal.setText(String.valueOf(d_pcs+n_pcs));
			
			labSalesTotalAmt.setText(String.valueOf(df.format(totalsum)));
			labSalesDNDtotal1.setText(String.valueOf(Integer.parseInt(labSalesDox1.getText())+Integer.parseInt(labSalesNonDox1.getText())));
			labSalesDNDtotal2.setText(String.valueOf(Integer.parseInt(labSalesDox2.getText())+Integer.parseInt(labSalesNonDox2.getText())));
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st,rs, con);
		}
	}
	

//================================== Sales Line Chart Method  =======================================	
	
	public void loadLineChart(int daysvalue) throws SQLException, IOException
	{
		prop.load(in);
	
	
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
			
		Statement st=null;
		ResultSet rs=null;
		
		DashboardBean dashbean=new DashboardBean();
		int days=daysvalue-1;
		
		try
		{	
			st=con.createStatement();
			
			for(String clienttype:salesSet)
			{
				XYChart.Series series = new XYChart.Series();
			//String sql="SELECT date, coalesce(total,0) AS total,coalesce(client_type,'0') as client_type FROM generate_series(current_date - interval '"+days+" day' , current_date , interval '1 day') AS date LEFT OUTER JOIN (SELECT date_trunc('day', dbt.booking_date) as day, sum(dbt.total) as total,cm.client_type FROM dailybookingtransaction as dbt,clientmaster as cm WHERE dbt.dailybookingtransaction2client=cm.code and cm.client_type='"+clienttype+"' GROUP BY day, cm.client_type) results ON (date = results.day) order by date ASC";
			String sql="SELECT date, coalesce(total,0) AS total,coalesce(client_type,'0') as client_type FROM generate_series('2017-03-25' , '2017-04-03', - interval '-1 day') AS date LEFT OUTER JOIN (SELECT date_trunc('day', dbt.booking_date) as day, sum(dbt.total) as total,cm.client_type FROM dailybookingtransaction as dbt,clientmaster as cm WHERE dbt.dailybookingtransaction2client=cm.code and cm.client_type='"+clienttype+"' GROUP BY day, cm.client_type) results ON (date = results.day) order by date ASC";
			rs=st.executeQuery(sql);
			
			while(rs.next())
			{
				dashbean.setBooking_date(rs.getString(1));
				dashbean.setCash(rs.getDouble(2));
				if(clienttype.equals(prop.getProperty("Cash")))
				{
				series.setName("CA");
				}
				else
				{
					series.setName("CR");
				}
				series.getData().add(new XYChart.Data(dashbean.getBooking_date().substring(8, 10),dashbean.getCash()));
				
			}
			salesLineChart.getData().addAll(series);
		
			}
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			
			dbcon.disconnect(null, st,rs, con);
		}
		
	}
	
	
	public void loadTopClientData(int daysvalue) throws SQLException, IOException
	{
		prop.load(in);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		double totalsum=0.0;
		Statement st=null;
		ResultSet rs=null;
	
		List<Double> clientAmountList=new ArrayList<Double>();
		
		try
		{	
			st=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			//String sql="select dailybookingtransaction2client, Sum(total) as total from dailybookingtransaction WHERE booking_date > current_date - interval '"+daysvalue+"' day  group by dailybookingtransaction2client order by total DESC limit 5";
			String sql="select dailybookingtransaction2client, Sum(total) as total from dailybookingtransaction WHERE booking_date between '2017-03-25' and  '2017-04-03'  group by dailybookingtransaction2client order by total DESC limit 5";
			System.out.println(sql);
			rs=st.executeQuery(sql);
			
			while(rs.next())
			{
				clientList.add(rs.getString(1));
				clientAmountList.add(rs.getDouble(2));
				totalsum=totalsum+rs.getDouble(2);
			}
			rs.last();
			
			for(String clientName:clientList)
			{
				//String sql1="select dox_nondox,sum(packets) as pcs from dailybookingtransaction WHERE dailybookingtransaction2client='"+clientName+"' and booking_date > current_date - interval '"+daysvalue+"' day group by dox_nondox";
				String sql1="select dox_nondox,sum(packets) as pcs from dailybookingtransaction WHERE dailybookingtransaction2client='"+clientName+"' and booking_date between '2017-03-25' and  '2017-04-03' group by dox_nondox";
				
				rs=st.executeQuery(sql1);
				while(rs.next())
				{
					
					if(clientList.get(0).equals(clientName))
					{
						if(rs.getString(1).equals(prop.getProperty("dox")))
						{
							labClientDox1.setText(String.valueOf(rs.getInt(2)));
						}
						else
						{
							labClientNonDox1.setText(String.valueOf(rs.getInt(2)));
						}
					}
					
					else if(clientList.get(1).equals(clientName))
					{
						if(rs.getString(1).equals(prop.getProperty("dox")))
						{
							labClientDox2.setText(String.valueOf(rs.getInt(2)));
						}
						else
						{
							labClientNonDox2.setText(String.valueOf(rs.getInt(2)));
						}
					}
					
					else if(clientList.get(2).equals(clientName))
					{
						if(rs.getString(1).equals(prop.getProperty("dox")))
						{
							labClientDox3.setText(String.valueOf(rs.getInt(2)));
						}
						else
						{
							labClientNonDox3.setText(String.valueOf(rs.getInt(2)));
						}
					}
					
					else if(clientList.get(3).equals(clientName))
					{
						if(rs.getString(1).equals(prop.getProperty("dox")))
						{
							labClientDox4.setText(String.valueOf(rs.getInt(2)));
						}
						else
						{
							labClientNonDox4.setText(String.valueOf(rs.getInt(2)));
						}
					}
					
					else
					{
						if(rs.getString(1).equals(prop.getProperty("dox")))
						{
							labClientDox5.setText(String.valueOf(rs.getInt(2)));
						}
						else
						{
							labClientNonDox5.setText(String.valueOf(rs.getInt(2)));
						}
					}
				}
			
				labClient1.setText(clientList.get(0));
				labClient2.setText(clientList.get(1));
				labClient3.setText(clientList.get(2));
				labClient4.setText(clientList.get(3));
				labClient5.setText(clientList.get(4));
				
				labClient1Value.setText(String.valueOf(df.format(clientAmountList.get(0))));
				labClient2Value.setText(String.valueOf(df.format(clientAmountList.get(1))));
				labClient3Value.setText(String.valueOf(df.format(clientAmountList.get(2))));
				labClient4Value.setText(String.valueOf(df.format(clientAmountList.get(3))));
				labClient5Value.setText(String.valueOf(df.format(clientAmountList.get(4))));
								
				labClientDNDtotal1.setText(String.valueOf(Integer.parseInt(labClientDox1.getText())+Integer.parseInt(labClientNonDox1.getText())));
				labClientDNDtotal2.setText(String.valueOf(Integer.parseInt(labClientDox2.getText())+Integer.parseInt(labClientNonDox2.getText())));
				labClientDNDtotal3.setText(String.valueOf(Integer.parseInt(labClientDox3.getText())+Integer.parseInt(labClientNonDox3.getText())));
				labClientDNDtotal4.setText(String.valueOf(Integer.parseInt(labClientDox4.getText())+Integer.parseInt(labClientNonDox4.getText())));
				labClientDNDtotal5.setText(String.valueOf(Integer.parseInt(labClientDox5.getText())+Integer.parseInt(labClientNonDox5.getText())));
				

				labClientTotalAmt.setText(String.valueOf(df.format(totalsum)));
				
				labClientTotalDox.setText(String.valueOf(Integer.parseInt(labClientDox1.getText())+Integer.parseInt(labClientDox2.getText())+Integer.parseInt(labClientDox3.getText())
													+Integer.parseInt(labClientDox4.getText())+Integer.parseInt(labClientDox5.getText())));
				
				labClientTotalNonDox.setText(String.valueOf(Integer.parseInt(labClientNonDox1.getText())+Integer.parseInt(labClientNonDox2.getText())+Integer.parseInt(labClientNonDox3.getText())
													+Integer.parseInt(labClientNonDox4.getText())+Integer.parseInt(labClientNonDox5.getText())));

				labClientFinalTotal.setText(String.valueOf(Integer.parseInt(labClientTotalDox.getText())+Integer.parseInt(labClientTotalNonDox.getText())));
				
			}
			rs.last();
		}
			
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			
			dbcon.disconnect(null, st,rs, con);
		}
	}
	
//================================== Client Line Chart Method  =======================================
	
	@SuppressWarnings({ "unchecked", "resource", "rawtypes" })
	public void loadClientLineChart(int daysvalue) throws SQLException
	{
	
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		int days=daysvalue-1;
		
		
		
		Statement st=null;
		ResultSet rs=null;
		
		try
		{	
			st = con.createStatement();
			clientlinechart.getData().clear();
			for(String clientName:clientList)
			{
				XYChart.Series series = new XYChart.Series();
				
				// Query for Current date to back date
				//String sql1="SELECT date, coalesce(total,0) AS total FROM generate_series(current_date - interval '"+days+" day' , current_date , interval '1 day') AS date LEFT OUTER JOIN (SELECT date_trunc('day', dailybookingtransaction.booking_date) as day, sum(dailybookingtransaction.total) as total FROM dailybookingtransaction WHERE dailybookingtransaction2client='"+clientName+"' GROUP BY day) results ON (date = results.day) order by date ASC";
				
				String sql1="SELECT date, coalesce(total,0) AS total FROM generate_series('2017-03-25' , '2017-04-03', - interval '-1 day') AS date LEFT OUTER JOIN (SELECT date_trunc('day', dailybookingtransaction.booking_date) as day, sum(dailybookingtransaction.total) as total FROM dailybookingtransaction WHERE dailybookingtransaction2client='"+clientName+"' GROUP BY day) results ON (date = results.day) order by date ASC";
				
				System.out.println(sql1);
				rs=st.executeQuery(sql1);
				
				while(	rs.next())
				{
					series.setName(clientName.substring(0,3));
					series.getData().add(new XYChart.Data(rs.getString(1).substring(8,10),Double.parseDouble(df.format(rs.getDouble(2)))));	
				}
		
				clientlinechart.getData().addAll(series);
			}
		}
		
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			clientList.clear();
			dbcon.disconnect(null, st,rs, con);
		}
	}
	
	
	
	public void loadTopZoneData(int daysvalue) throws SQLException, IOException
	{
		prop.load(in);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		double totalsum=0.0;
		Statement st=null;
		ResultSet rs=null;
		
		List<Double> zoneAmountList=new ArrayList<Double>();

			
			try
			{	
				st=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
				//String sql="select zdt.zonedetail2zonetype,SUM(dbt.total) as total from dailybookingtransaction as dbt, zonedetail as zdt where dbt.dailybookingtransaction2city=zdt.zone2city and booking_date > current_date - interval '"+daysvalue+"' day group by zdt.zonedetail2zonetype order by sum(total) DESC limit 5";
				String sql="select zdt.zonedetail2zonetype,SUM(dbt.total) as total from dailybookingtransaction as dbt, zonedetail as zdt where dbt.dailybookingtransaction2city=zdt.zone2city and booking_date between '2017-03-25' and  '2017-04-03' group by zdt.zonedetail2zonetype order by sum(total) DESC limit 5";
				rs=st.executeQuery(sql);
			
				while(rs.next())
				{
					
					zoneList.add(rs.getString(1));
					zoneAmountList.add(rs.getDouble(2));
					totalsum=totalsum+rs.getDouble(2);
				}
				rs.last();
			
				int i=1;
				for(String zoneName:zoneList)
				{
				
					//String sql1="select dox_nondox, sum(packets) as pcs from dailybookingtransaction as dbt, zonedetail as zdt where dbt.dailybookingtransaction2city=zdt.zone2city and zdt.zonedetail2zonetype='"+zoneName+"' and booking_date > current_date - interval '"+daysvalue+"' day group by dox_nondox order by dox_nondox";
					String sql1="select dox_nondox, sum(packets) as pcs from dailybookingtransaction as dbt, zonedetail as zdt where dbt.dailybookingtransaction2city=zdt.zone2city and zdt.zonedetail2zonetype='"+zoneName+"' and booking_date between '2017-03-25' and  '2017-04-03' group by dox_nondox order by dox_nondox";
					rs=st.executeQuery(sql1);
					while(rs.next())
					{
						if(zoneList.get(0).equals(zoneName))
						{
							if(rs.getString(1).equals(prop.getProperty("dox")))
							{
								labZoneDox1.setText(String.valueOf(rs.getInt(2)));
							}
							else
							{
								labZoneNonDox1.setText(String.valueOf(rs.getInt(2)));
							}
						}
						
						else if(zoneList.get(1).equals(zoneName))
						{
							if(rs.getString(1).equals(prop.getProperty("dox")))
							{
								labZoneDox2.setText(String.valueOf(rs.getInt(2)));
							}
							else
							{
								labZoneNonDox2.setText(String.valueOf(rs.getInt(2)));
							}
						}
						
						else if(zoneList.get(2).equals(zoneName))
						{
							if(rs.getString(1).equals(prop.getProperty("dox")))
							{
								labZoneDox3.setText(String.valueOf(rs.getInt(2)));
							}
							else
							{
								labZoneNonDox3.setText(String.valueOf(rs.getInt(2)));
							}
						}
						
						else if(zoneList.get(3).equals(zoneName))
						{
							if(rs.getString(1).equals(prop.getProperty("dox")))
							{
								labZoneDox4.setText(String.valueOf(rs.getInt(2)));
							}
							else
							{
								labZoneNonDox4.setText(String.valueOf(rs.getInt(2)));
							}
						}
						
						else
						{
							if(rs.getString(1).equals(prop.getProperty("dox")))
							{
								labZoneDox5.setText(String.valueOf(rs.getInt(2)));
							}
							else
							{
								labZoneNonDox5.setText(String.valueOf(rs.getInt(2)));
							}
						}
				
				}
				
				labZone1.setText(zoneList.get(0));
				labZone2.setText(zoneList.get(1));
				labZone3.setText(zoneList.get(2));
				labZone4.setText(zoneList.get(3));
				labZone5.setText(zoneList.get(4));
				
				labZone1Value.setText(String.valueOf(df.format(zoneAmountList.get(0))));
				labZone2Value.setText(String.valueOf(df.format(zoneAmountList.get(1))));
				labZone3Value.setText(String.valueOf(df.format(zoneAmountList.get(2))));
				labZone4Value.setText(String.valueOf(df.format(zoneAmountList.get(3))));
				labZone5Value.setText(String.valueOf(df.format(zoneAmountList.get(4))));
							
				labZoneDNDtotal1.setText(String.valueOf(Integer.parseInt(labZoneDox1.getText())+Integer.parseInt(labZoneNonDox1.getText())));
				labZoneDNDtotal2.setText(String.valueOf(Integer.parseInt(labZoneDox2.getText())+Integer.parseInt(labZoneNonDox2.getText())));
				labZoneDNDtotal3.setText(String.valueOf(Integer.parseInt(labZoneDox3.getText())+Integer.parseInt(labZoneNonDox3.getText())));
				labZoneDNDtotal4.setText(String.valueOf(Integer.parseInt(labZoneDox4.getText())+Integer.parseInt(labZoneNonDox4.getText())));
				labZoneDNDtotal5.setText(String.valueOf(Integer.parseInt(labZoneDox5.getText())+Integer.parseInt(labZoneNonDox5.getText())));

				labZoneTotalAmt.setText(String.valueOf(df.format(totalsum)));
				
				labZoneTotalDox.setText(String.valueOf(Integer.parseInt(labZoneDox1.getText())+Integer.parseInt(labZoneDox2.getText())+Integer.parseInt(labZoneDox3.getText())
													+Integer.parseInt(labZoneDox4.getText())+Integer.parseInt(labZoneDox5.getText())));
				
				labZoneTotalNonDoxAmt.setText(String.valueOf(Integer.parseInt(labZoneNonDox1.getText())+Integer.parseInt(labZoneNonDox2.getText())+Integer.parseInt(labZoneNonDox3.getText())
													+Integer.parseInt(labZoneNonDox4.getText())+Integer.parseInt(labZoneNonDox5.getText())));
	
				labZoneFinalTotal.setText(String.valueOf(Integer.parseInt(labZoneTotalDox.getText())+Integer.parseInt(labZoneTotalNonDoxAmt.getText())));
			
				}
			}
		
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			
			dbcon.disconnect(null, st,rs, con);
		}
	}
	
//================================== Zone Line Chart Method  =======================================
	
	@SuppressWarnings({ "unchecked", "resource", "rawtypes" })
	public void loadZoneLineChart(int daysvalue) throws SQLException
	{
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		int days=daysvalue-1;
		
        Statement st=null;
		ResultSet rs=null;
		
		try
		{	
			st = con.createStatement();
		
			for(String zoneName:zoneList)
			{
				XYChart.Series series = new XYChart.Series();
				//String sql1="SELECT date, coalesce(total,0) AS total FROM generate_series(current_date - interval '"+days+" day' , current_date , interval '1 day') AS date LEFT OUTER JOIN (SELECT date_trunc('day', dbt.booking_date) as day, sum(dbt.total) as total FROM dailybookingtransaction as dbt, zonedetail as zdt where dbt.dailybookingtransaction2city=zdt.zone2city and zdt.zonedetail2zonetype='"+zoneName+"' GROUP BY day) results ON (date = results.day) order by date ASC";
				String sql1="SELECT date, coalesce(total,0) AS total FROM generate_series('2017-03-25' , '2017-04-03', - interval '-1 day') AS date LEFT OUTER JOIN (SELECT date_trunc('day', dbt.booking_date) as day, sum(dbt.total) as total FROM dailybookingtransaction as dbt, zonedetail as zdt where dbt.dailybookingtransaction2city=zdt.zone2city and zdt.zonedetail2zonetype='"+zoneName+"' GROUP BY day) results ON (date = results.day) order by date ASC";
				
				rs=st.executeQuery(sql1);
				while(	rs.next())
				{
					series.setName(zoneName.substring(0,3));
					series.getData().add(new XYChart.Data(rs.getString(1).substring(8, 10),Double.parseDouble(df.format(rs.getDouble(2)))));
					
				}
			zonelinechart.getData().addAll(series);
			}
				
		}
		
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			zoneList.clear();
			dbcon.disconnect(null, st,rs, con);
		}
	}
	

	
	//=======================================================================================================
	
	public void loadTopCityData(int daysvalue) throws SQLException, IOException
	{
		prop.load(in);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		double totalsum=0.0;
		Statement st=null;
		ResultSet rs=null;

		List<Double> cityAmountList=new ArrayList<Double>();
		
		try
		{	
			st=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			//String sql="select dailybookingtransaction2city,SUM(total) as total from dailybookingtransaction WHERE booking_date > current_date - interval '"+daysvalue+"' day group by dailybookingtransaction2city order by total DESC limit 5";
			String sql="select dailybookingtransaction2city,SUM(total) as total from dailybookingtransaction WHERE booking_date between '2017-03-25' and  '2017-04-03' group by dailybookingtransaction2city order by total DESC limit 5";
			
			rs=st.executeQuery(sql);
		
	
			while(rs.next())
			{
				
				cityList.add(rs.getString(1));
				cityAmountList.add(rs.getDouble(2));
				totalsum=totalsum+rs.getDouble(2);
			}
			rs.last();
		
			
			for(String cityName:cityList)
			{
				//String sql1="select dox_nondox,sum(packets) as pcs from dailybookingtransaction WHERE dailybookingtransaction2city='"+cityName+"' and booking_date > current_date - interval '"+daysvalue+"' day group by dox_nondox order by dox_nondox";
				String sql1="select dox_nondox,sum(packets) as pcs from dailybookingtransaction WHERE dailybookingtransaction2city='"+cityName+"' and booking_date between '2017-03-25' and  '2017-04-03' group by dox_nondox order by dox_nondox";
				rs=st.executeQuery(sql1);
				while(rs.next())
				{	
					if(cityList.get(0).equals(cityName))
					{
						if(rs.getString(1).equals(prop.getProperty("dox")))
						{
							labCityDox1.setText(String.valueOf(rs.getInt(2)));
						}
						else
						{
							labCityNonDox1.setText(String.valueOf(rs.getInt(2)));
						}
					}
					
					else if(cityList.get(1).equals(cityName))
					{
						if(rs.getString(1).equals(prop.getProperty("dox")))
						{
							labCityDox2.setText(String.valueOf(rs.getInt(2)));
						}
						else
						{
							labCityNonDox2.setText(String.valueOf(rs.getInt(2)));
						}
					}
					
					else if(cityList.get(2).equals(cityName))
					{
						if(rs.getString(1).equals(prop.getProperty("dox")))
						{
							labCityDox3.setText(String.valueOf(rs.getInt(2)));
						}
						else
						{
							labCityNonDox3.setText(String.valueOf(rs.getInt(2)));
						}
					}
					
					else if(cityList.get(3).equals(cityName))
					{
						if(rs.getString(1).equals(prop.getProperty("dox")))
						{
							labCityDox4.setText(String.valueOf(rs.getInt(2)));
						}
						else
						{
							labCityNonDox4.setText(String.valueOf(rs.getInt(2)));
						}
					}
					
					else
					{
						if(rs.getString(1).equals(prop.getProperty("dox")))
						{
							labCityDox5.setText(String.valueOf(rs.getInt(2)));
						}
						else
						{
							labCityNonDox5.setText(String.valueOf(rs.getInt(2)));
						}
					}
			
			}
			
			labCity1.setText(cityList.get(0));
			labCity2.setText(cityList.get(1));
			labCity3.setText(cityList.get(2));
			labCity4.setText(cityList.get(3));
			labCity5.setText(cityList.get(4));
			
			labCity1Amt.setText(String.valueOf(df.format(cityAmountList.get(0))));
			labCity2Amt.setText(String.valueOf(df.format(cityAmountList.get(1))));
			labCity3Amt.setText(String.valueOf(df.format(cityAmountList.get(2))));
			labCity4Amt.setText(String.valueOf(df.format(cityAmountList.get(3))));
			labCity5Amt.setText(String.valueOf(df.format(cityAmountList.get(4))));
			
					
			labCityDNDtotal1.setText(String.valueOf(Integer.parseInt(labCityDox1.getText())+Integer.parseInt(labCityNonDox1.getText())));
			labCityDNDtotal2.setText(String.valueOf(Integer.parseInt(labCityDox2.getText())+Integer.parseInt(labCityNonDox2.getText())));
			labCityDNDtotal3.setText(String.valueOf(Integer.parseInt(labCityDox3.getText())+Integer.parseInt(labCityNonDox3.getText())));
			labCityDNDtotal4.setText(String.valueOf(Integer.parseInt(labCityDox4.getText())+Integer.parseInt(labCityNonDox4.getText())));
			labCityDNDtotal5.setText(String.valueOf(Integer.parseInt(labCityDox5.getText())+Integer.parseInt(labCityNonDox5.getText())));
			
			labCityTotalAmt.setText(String.valueOf(df.format(totalsum)));
			
			labCityTotalDox.setText(String.valueOf(Integer.parseInt(labCityDox1.getText())+Integer.parseInt(labCityDox2.getText())+Integer.parseInt(labCityDox3.getText())
												+Integer.parseInt(labCityDox4.getText())+Integer.parseInt(labCityDox5.getText())));
			
			labCityTotalNonDoxAmt.setText(String.valueOf(Integer.parseInt(labCityNonDox1.getText())+Integer.parseInt(labCityNonDox2.getText())+Integer.parseInt(labCityNonDox3.getText())
												+Integer.parseInt(labCityNonDox4.getText())+Integer.parseInt(labCityNonDox5.getText())));
	
			labCityFinalTotal.setText(String.valueOf(Integer.parseInt(labCityTotalDox.getText())+Integer.parseInt(labCityTotalNonDoxAmt.getText())));
		
			}
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			
			dbcon.disconnect(null, st,rs, con);
		}
	}

	
	
//============================= City Line Chart Method ========================================
	
	@SuppressWarnings({ "unchecked", "resource", "rawtypes" })
	public void loadCityLineChart(int daysvalue) throws SQLException
	{
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		int days=daysvalue-1;
        
		Statement st=null;
		ResultSet rs=null;
		
		try
		{	
			st = con.createStatement();
			
			for(String cityName:cityList)
			{
				XYChart.Series series = new XYChart.Series();
				//String sql1="SELECT date, coalesce(total,0) AS total FROM generate_series(current_date - interval '"+days+" day' , current_date , interval '1 day') AS date LEFT OUTER JOIN (SELECT date_trunc('day', dailybookingtransaction.booking_date) as day, sum(dailybookingtransaction.total) as total FROM dailybookingtransaction WHERE dailybookingtransaction2city='"+cityName+"' GROUP BY day) results ON (date = results.day) order by date ASC";
				String sql1="SELECT date, coalesce(total,0) AS total FROM generate_series('2017-03-25' , '2017-04-03', - interval '-1 day') AS date LEFT OUTER JOIN (SELECT date_trunc('day', dailybookingtransaction.booking_date) as day, sum(dailybookingtransaction.total) as total FROM dailybookingtransaction WHERE dailybookingtransaction2city='"+cityName+"' GROUP BY day) results ON (date = results.day) order by date ASC";
				rs=st.executeQuery(sql1);
				while(rs.next())
				{
					series.setName(cityName.substring(0,3));
					series.getData().add(new XYChart.Data(rs.getString(1).substring(8, 10),Double.parseDouble(df.format(rs.getDouble(2)))));
				}
				cityLineChart.getData().addAll(series);
			}
		}
		
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			cityList.clear();
			dbcon.disconnect(null, st,rs, con);
		}
	}	
	
	
	
	
//=======================================================================================================
	
	public void loadTopNetworkData(int daysvalue) throws SQLException, IOException
	{
		prop.load(in);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		double totalsum=0.0;
		Statement st=null;
		ResultSet rs=null;

		List<Double> networkAmountList=new ArrayList<Double>();
		
		try
		{	
			st=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
	//		String sql="select dbt.dailybookingtransaction2network, Sum(dbt.total) as total from dailybookingtransaction as dbt, vendormaster as vnd where dbt.dailybookingtransaction2network=vnd.code and vnd.vendor_type='N' and booking_date > current_date - interval '"+daysvalue+"' day  group by dbt.dailybookingtransaction2network order by Sum(dbt.total) DESC limit 5";
			String sql="select dbt.dailybookingtransaction2network, Sum(dbt.total) as total from dailybookingtransaction as dbt, vendormaster as vnd where dbt.dailybookingtransaction2network=vnd.code and vnd.vendor_type='N' and booking_date between '2017-03-25' and  '2017-04-03' group by dbt.dailybookingtransaction2network order by Sum(dbt.total) DESC limit 5";
			
			rs=st.executeQuery(sql);
		
			while(rs.next())
			{
				
				networkList.add(rs.getString(1));
				networkAmountList.add(rs.getDouble(2));
				totalsum=totalsum+rs.getDouble(2);
			}
			rs.last();
		
			for(String networkName:networkList)
			{
				//String sql1="select dox_nondox,sum(packets) as pcs from dailybookingtransaction WHERE dailybookingtransaction2network='"+networkName+"' and booking_date > current_date - interval '"+daysvalue+"' day group by dox_nondox order by dox_nondox";
				String sql1="select dox_nondox,sum(packets) as pcs from dailybookingtransaction WHERE dailybookingtransaction2network='"+networkName+"' and booking_date between '2017-03-25' and  '2017-04-03' group by dox_nondox order by dox_nondox";
				rs=st.executeQuery(sql1);
				while(rs.next())
				{
					if(networkList.get(0).equals(networkName))
					{
						if(rs.getString(1).equals(prop.getProperty("dox")))
						{
							labNetDox1.setText(String.valueOf(rs.getInt(2)));
						}
						else
						{
							labNetNonDox1.setText(String.valueOf(rs.getInt(2)));
						}
					}
					
					else if(networkList.get(1).equals(networkName))
					{
						if(rs.getString(1).equals(prop.getProperty("dox")))
						{
							labNetDox2.setText(String.valueOf(rs.getInt(2)));
						}
						else
						{
							labNetNonDox2.setText(String.valueOf(rs.getInt(2)));
						}
					}
					
					else if(networkList.get(2).equals(networkName))
					{
						if(rs.getString(1).equals(prop.getProperty("dox")))
						{
							labNetDox3.setText(String.valueOf(rs.getInt(2)));
						}
						else
						{
							labNetNonDox3.setText(String.valueOf(rs.getInt(2)));
						}
					}
					
					else if(networkList.get(3).equals(networkName))
					{
						if(rs.getString(1).equals(prop.getProperty("dox")))
						{
							labNetDox4.setText(String.valueOf(rs.getInt(2)));
						}
						else
						{
							labNetNonDox4.setText(String.valueOf(rs.getInt(2)));
						}
					}
					
					else
					{
						if(rs.getString(1).equals(prop.getProperty("dox")))
						{
							labNetDox5.setText(String.valueOf(rs.getInt(2)));
						}
						else
						{
							labNetNonDox5.setText(String.valueOf(rs.getInt(2)));
						}
					}
			
			}
			
			labNet1.setText(networkList.get(0));
			labNet2.setText(networkList.get(1));
			labNet3.setText(networkList.get(2));
			labNet4.setText(networkList.get(3));
			labNet5.setText(networkList.get(4));
			
			labNet1Amt.setText(String.valueOf(df.format(networkAmountList.get(0))));
			labNet2Amt.setText(String.valueOf(df.format(networkAmountList.get(1))));
			labNet3Amt.setText(String.valueOf(df.format(networkAmountList.get(2))));
			labNet4Amt.setText(String.valueOf(df.format(networkAmountList.get(3))));
			labNet5Amt.setText(String.valueOf(df.format(networkAmountList.get(4))));
			
			labNetDNDtotal1.setText(String.valueOf(Integer.parseInt(labNetDox1.getText())+Integer.parseInt(labNetNonDox1.getText())));
			labNetDNDtotal2.setText(String.valueOf(Integer.parseInt(labNetDox2.getText())+Integer.parseInt(labNetNonDox2.getText())));
			labNetDNDtotal3.setText(String.valueOf(Integer.parseInt(labNetDox3.getText())+Integer.parseInt(labNetNonDox3.getText())));
			labNetDNDtotal4.setText(String.valueOf(Integer.parseInt(labNetDox4.getText())+Integer.parseInt(labNetNonDox4.getText())));
			labNetDNDtotal5.setText(String.valueOf(Integer.parseInt(labNetDox5.getText())+Integer.parseInt(labNetNonDox5.getText())));
			
			labNetTotalAmt.setText(String.valueOf(df.format(totalsum)));
			
			labNetTotalDox.setText(String.valueOf(Integer.parseInt(labNetDox1.getText())+Integer.parseInt(labNetDox2.getText())+Integer.parseInt(labNetDox3.getText())
												+Integer.parseInt(labNetDox4.getText())+Integer.parseInt(labNetDox5.getText())));
			
			labNetTotalNonDoxAmt.setText(String.valueOf(Integer.parseInt(labNetNonDox1.getText())+Integer.parseInt(labNetNonDox2.getText())+Integer.parseInt(labNetNonDox3.getText())
												+Integer.parseInt(labNetNonDox4.getText())+Integer.parseInt(labNetNonDox5.getText())));
			
			labNetFinalTotal.setText(String.valueOf(Integer.parseInt(labNetTotalDox.getText())+Integer.parseInt(labNetTotalNonDoxAmt.getText())));
		
			}
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			
			dbcon.disconnect(null, st,rs, con);
		}
	}

	
	
//============================= Network Line Chart Method ========================================
	
	@SuppressWarnings({ "unchecked", "resource", "rawtypes" })
	public void loadNetworkLineChart(int daysvalue) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();

		int days=daysvalue-1;
        
		Statement st=null;
		ResultSet rs=null;
	
		try
		{	
			st = con.createStatement();
			
			for(String networkName:networkList)
			{
				XYChart.Series series = new XYChart.Series();
				//String sql1="SELECT date, coalesce(total,0) AS total FROM generate_series(current_date - interval '"+days+" day' , current_date , interval '1 day') AS date LEFT OUTER JOIN (SELECT date_trunc('day', dailybookingtransaction.booking_date) as day, sum(dailybookingtransaction.total) as total FROM dailybookingtransaction WHERE dailybookingtransaction2network='"+networkName+"' GROUP BY day) results ON (date = results.day) order by date ASC";
				String sql1="SELECT date, coalesce(total,0) AS total FROM generate_series('2017-03-25' , '2017-04-03', - interval '-1 day') AS date LEFT OUTER JOIN (SELECT date_trunc('day', dailybookingtransaction.booking_date) as day, sum(dailybookingtransaction.total) as total FROM dailybookingtransaction WHERE dailybookingtransaction2network='"+networkName+"' GROUP BY day) results ON (date = results.day) order by date ASC";
				//System.out.println(sql1);
				rs=st.executeQuery(sql1);
				while(rs.next())
				{	
					series.setName(networkName.substring(0,3));
					series.getData().add(new XYChart.Data(rs.getString(1).substring(8, 10),Double.parseDouble(df.format(rs.getDouble(2)))));
				}
				NetLineChart.getData().addAll(series);
			}
		}
		
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			networkList.clear();
			dbcon.disconnect(null, st,rs, con);
		}
	}	
	
	
	//=======================================================================================================
	
		public void loadTopServiceGroupData(int daysvalue) throws SQLException, IOException
		{
			prop.load(in);
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			
			double totalsum=0.0;
			Statement st=null;
			ResultSet rs=null;

			List<Double> serviceGroupAmountList=new ArrayList<Double>();
			
			try
			{	
				st=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
				//String sql="select stp.service2group, Sum(dbt.total) as total from dailybookingtransaction as dbt, servicetype as stp where dbt.dailybookingtransaction2service=stp.code and booking_date > current_date - interval '"+daysvalue+"' day  group by stp.service2group order by Sum(dbt.total) DESC limit 5";
				String sql="select stp.service2group, Sum(dbt.total) as total from dailybookingtransaction as dbt, servicetype as stp where dbt.dailybookingtransaction2service=stp.code and booking_date between '2017-03-25' and  '2017-04-03'  group by stp.service2group order by Sum(dbt.total) DESC limit 5";
				
				rs=st.executeQuery(sql);
			
				while(rs.next())
				{
					
					serviceGroupList.add(rs.getString(1));
					serviceGroupAmountList.add(rs.getDouble(2));
					totalsum=totalsum+rs.getDouble(2);
				}
				rs.last();
				
				for(String serviceGroupName:serviceGroupList)
				{
					//String sql1="select dbt.dox_nondox,sum(dbt.packets) as pcs from dailybookingtransaction as dbt, servicetype as stp where dbt.dailybookingtransaction2service=stp.code and stp.service2group='"+serviceGroupName+"' and booking_date > current_date - interval '"+daysvalue+"' day group by dox_nondox order by dox_nondox";
					String sql1="select dbt.dox_nondox,sum(dbt.packets) as pcs from dailybookingtransaction as dbt, servicetype as stp where dbt.dailybookingtransaction2service=stp.code and stp.service2group='"+serviceGroupName+"' and booking_date between '2017-03-25' and  '2017-04-03' group by dox_nondox order by dox_nondox";
					rs=st.executeQuery(sql1);
					while(rs.next())
					{
						
						if(serviceGroupList.get(0).equals(serviceGroupName))
						{
							if(rs.getString(1).equals(prop.getProperty("dox")))
							{
								labSrcGrpDox1.setText(String.valueOf(rs.getInt(2)));
							}
							else
							{
								labSrcGrpNonDox1.setText(String.valueOf(rs.getInt(2)));
							}
						}
						
						else if(serviceGroupList.get(1).equals(serviceGroupName))
						{
							if(rs.getString(1).equals(prop.getProperty("dox")))
							{
								labSrcGrpDox2.setText(String.valueOf(rs.getInt(2)));
							}
							else
							{
								labSrcGrpNonDox2.setText(String.valueOf(rs.getInt(2)));
							}
						}
						
						else if(serviceGroupList.get(2).equals(serviceGroupName))
						{
							if(rs.getString(1).equals(prop.getProperty("dox")))
							{
								labSrcGrpDox3.setText(String.valueOf(rs.getInt(2)));
							}
							else
							{
								labSrcGrpNonDox3.setText(String.valueOf(rs.getInt(2)));
							}
						}
						
						else if(serviceGroupList.get(3).equals(serviceGroupName))
						{
							
							if(rs.getString(1).equals(prop.getProperty("dox")))
							{
								labSrcGrpDox4.setText(String.valueOf(rs.getInt(2)));
							}
							else
							{
								labSrcGrpNonDox4.setText(String.valueOf(rs.getInt(2)));
							}
						}
						
						
						else
						{
							
							if(rs.getString(1).equals(prop.getProperty("dox")))
							{
								labSrcGrpDox5.setText(String.valueOf(rs.getInt(2)));
							}
							else
							{
								labSrcGrpNonDox5.setText(String.valueOf(rs.getInt(2)));
							}
						}
				}
					
				labSrcGrp1.setText(serviceGroupList.get(0));
				labSrcGrp2.setText(serviceGroupList.get(1));
				labSrcGrp3.setText(serviceGroupList.get(2));
				labSrcGrp4.setText(serviceGroupList.get(3));
				
				if (serviceGroupList.size()<5)
				{
					labSrcGrp5.setText("--");
					labSrcGrp5Amt.setText("0.0");
				}
				else
				{
					labSrcGrp5.setText(serviceGroupList.get(4));
					labSrcGrp5Amt.setText(String.valueOf(df.format(serviceGroupAmountList.get(4))));
					
				}
				
				labSrcGrp1Amt.setText(String.valueOf(df.format(serviceGroupAmountList.get(0))));
				labSrcGrp2Amt.setText(String.valueOf(df.format(serviceGroupAmountList.get(1))));
				labSrcGrp3Amt.setText(String.valueOf(df.format(serviceGroupAmountList.get(2))));
				labSrcGrp4Amt.setText(String.valueOf(df.format(serviceGroupAmountList.get(3))));
							
				labSrcGrpDNDtotal1.setText(String.valueOf(Integer.parseInt(labSrcGrpDox1.getText())+Integer.parseInt(labSrcGrpNonDox1.getText())));
				labSrcGrpDNDtotal2.setText(String.valueOf(Integer.parseInt(labSrcGrpDox2.getText())+Integer.parseInt(labSrcGrpNonDox2.getText())));
				labSrcGrpDNDtotal3.setText(String.valueOf(Integer.parseInt(labSrcGrpDox3.getText())+Integer.parseInt(labSrcGrpNonDox3.getText())));
				labSrcGrpDNDtotal4.setText(String.valueOf(Integer.parseInt(labSrcGrpDox4.getText())+Integer.parseInt(labSrcGrpNonDox4.getText())));
				labSrcGrpDNDtotal5.setText(String.valueOf(Integer.parseInt(labSrcGrpDox5.getText())+Integer.parseInt(labSrcGrpNonDox5.getText())));

				labSrcGrpTotalAmt.setText(String.valueOf(df.format(totalsum)));
				
				labSrcGrpTotalDox.setText(String.valueOf(Integer.parseInt(labSrcGrpDox1.getText())+Integer.parseInt(labSrcGrpDox2.getText())+Integer.parseInt(labSrcGrpDox3.getText())
													+Integer.parseInt(labSrcGrpDox4.getText())+Integer.parseInt(labSrcGrpDox5.getText())));
				
				labSrcGrpTotalNonDoxAmt.setText(String.valueOf(Integer.parseInt(labSrcGrpNonDox1.getText())+Integer.parseInt(labSrcGrpNonDox2.getText())+Integer.parseInt(labSrcGrpNonDox3.getText())
													+Integer.parseInt(labSrcGrpNonDox4.getText())+Integer.parseInt(labSrcGrpNonDox5.getText())));
	
				labSrcGrpFinalTotal.setText(String.valueOf(Integer.parseInt(labSrcGrpTotalDox.getText())+Integer.parseInt(labSrcGrpTotalNonDoxAmt.getText())));
			
				}
			}
			
			catch(Exception e)
			{
				 System.out.println(e);
				 e.printStackTrace();
			}
			finally
			{
				dbcon.disconnect(null, st,rs, con);
			}
		}

		

//============================= ServiceGroup Line Chart Method ========================================
		
		@SuppressWarnings({ "unchecked", "resource", "rawtypes" })
		public void loadServiceGroupLineChart(int daysvalue) throws SQLException
		{
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
	
			int days=daysvalue-1;
			
			Statement st=null;
			ResultSet rs=null;
			
			try
			{	
				st = con.createStatement();
				
				for(String serviceGroupName:serviceGroupList)
				{
					XYChart.Series series = new XYChart.Series();
					//String sql1="SELECT date, coalesce(total,0) AS total FROM generate_series(current_date - interval '"+days+" day' , current_date , interval '1 day') AS date LEFT OUTER JOIN (SELECT date_trunc('day', dbt.booking_date) as day, sum(dbt.total) as total from dailybookingtransaction as dbt, servicetype as stp where dbt.dailybookingtransaction2service=stp.code and stp.service2group='"+serviceGroupName+"' GROUP BY day) results ON (date = results.day) order by date ASC";
					String sql1="SELECT date, coalesce(total,0) AS total FROM generate_series('2017-03-25' , '2017-04-03', - interval '-1 day') AS date LEFT OUTER JOIN (SELECT date_trunc('day', dbt.booking_date) as day, sum(dbt.total) as total from dailybookingtransaction as dbt, servicetype as stp where dbt.dailybookingtransaction2service=stp.code and stp.service2group='"+serviceGroupName+"' GROUP BY day) results ON (date = results.day) order by date ASC";
					rs=st.executeQuery(sql1);
					
					while(rs.next())
					{
						series.setName(serviceGroupName.substring(0,3));
						series.getData().add(new XYChart.Data(rs.getString(1).substring(8, 10),Double.parseDouble(df.format(rs.getDouble(2)))));
					}
					SrcGrpLineChart.getData().addAll(series);
				}
			}
			
			catch(Exception e)
			{
				 System.out.println(e);
				 e.printStackTrace();
			}	
			finally
			{
				serviceGroupList.clear();
				dbcon.disconnect(null, st,rs, con);
			}
		}	
		
//=========================================================================================================================	

		public void setRefreshButton()
		{
			File file = new File("src/com/onesoft/courier/icon/icon.png");
			Image image = new Image(file.toURI().toString());
			
			Tooltip refreshtooltip=new Tooltip();
			Tooltip textboxtooltip=new Tooltip();
			Tooltip Doxtooltip=new Tooltip();
			Tooltip NonDoxtooltip=new Tooltip();
			Tooltip PCStooltip=new Tooltip();
			
			refreshtooltip.setText("Refresh");
			textboxtooltip.setText("No. of days");
			Doxtooltip.setText("Total Dox");
			NonDoxtooltip.setText("Total Non-Dox");
			PCStooltip.setText("Total PCS");
			
			btnsales.setGraphic(new ImageView(image));
			btnsales.setTooltip(refreshtooltip);
			txtSales.setTooltip(textboxtooltip);
			linksale.setTooltip(new Tooltip("Click here for Sales Report"));
			labSalesTotalDox.setTooltip(Doxtooltip);
			labSalesTotalNonDox.setTooltip(NonDoxtooltip);
			labSalesFinalTotal.setTooltip(PCStooltip);
			
			
			btnclient.setGraphic(new ImageView(image));
			btnclient.setTooltip(refreshtooltip);
			txtClient.setTooltip(textboxtooltip);
			linkClient.setTooltip(new Tooltip("Click here for Client Report"));
			labClientTotalDox.setTooltip(Doxtooltip);
			labClientTotalNonDox.setTooltip(NonDoxtooltip);
			labClientFinalTotal.setTooltip(PCStooltip);
			
			
			btnzone.setGraphic(new ImageView(image));
			btnzone.setTooltip(refreshtooltip);
			txtZone.setTooltip(textboxtooltip);
			linkZone.setTooltip(new Tooltip("Click here for Zone Report"));
			labZoneTotalDox.setTooltip(Doxtooltip);
			labZoneTotalNonDoxAmt.setTooltip(NonDoxtooltip);
			labZoneFinalTotal.setTooltip(PCStooltip);
			
			
			btncity.setGraphic(new ImageView(image));
			btncity.setTooltip(refreshtooltip);
			txtCity.setTooltip(textboxtooltip);
			linkCity.setTooltip(new Tooltip("Click here for City Report"));
			labCityTotalDox.setTooltip(Doxtooltip);
			labCityTotalNonDoxAmt.setTooltip(NonDoxtooltip);
			labCityFinalTotal.setTooltip(PCStooltip);
			
			btnnetwork.setGraphic(new ImageView(image));
			btnnetwork.setTooltip(refreshtooltip);
			txtNetwork.setTooltip(textboxtooltip);
			linkNetwork.setTooltip(new Tooltip("Click here for Network Report"));
			labNetTotalDox.setTooltip(Doxtooltip);
			labNetTotalNonDoxAmt.setTooltip(NonDoxtooltip);
			labNetFinalTotal.setTooltip(PCStooltip);
			
			
			btnservicegroup.setGraphic(new ImageView(image));
			btnservicegroup.setTooltip(refreshtooltip);
			txtServicegroup.setTooltip(textboxtooltip);
			linkServiceGrp.setTooltip(new Tooltip("Click here for ServiceGroup Report"));
			labSrcGrpTotalDox.setTooltip(Doxtooltip);
			labSrcGrpTotalNonDoxAmt.setTooltip(NonDoxtooltip);
			labSrcGrpFinalTotal.setTooltip(PCStooltip);
			
			/*Tooltip tooltip=new Tooltip();
			
			tooltip.setText(data.getYValue().toString());
            Tooltip.install(data.getNode(), tooltip);*/
		}
	
//======================================================================================================================

	File file = new File("src/com/onesoft/courier/icon/Hover.png");
	Image hoverImage = new Image(file.toURI().toString());
	
	public void salesHoverRefreshIcon()
	{
		btnsales.setGraphic(new ImageView(hoverImage));
	}
	
	public void clientHoverRefreshIcon()
	{
		btnclient.setGraphic(new ImageView(hoverImage));
	}
		
	public void zoneHoverRefreshIcon()
	{
		btnzone.setGraphic(new ImageView(hoverImage));
	}
	
	public void cityHoverRefreshIcon()
	{
		btncity.setGraphic(new ImageView(hoverImage));
	}
	
	public void networkHoverRefreshIcon()
	{
		btnnetwork.setGraphic(new ImageView(hoverImage));
	}
	
	public void serviceGrpHoverRefreshIcon()
	{
		btnservicegroup.setGraphic(new ImageView(hoverImage));
	}
	
	
//=========================================================================================================================
		
	@Override
	public void initialize(URL location, ResourceBundle resources) {
	
		try
		{
			setRefreshButton();
			setDaysLimitforALL();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
