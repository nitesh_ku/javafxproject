package com.onesoft.courier.graphs.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.DB.DBconnection;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.common.LoadClientBean;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadNetworks;
import com.onesoft.courier.graphs.beans.ZoneBean;
import com.onesoft.courier.graphs.beans.NetworkJavaFXBean;
import com.onesoft.courier.graphs.beans.ServiceGroupJavaFXBean;
import com.onesoft.courier.graphs.beans.NetworkBean;
import com.onesoft.courier.graphs.utils.FilterUtils;
import com.onesoft.courier.main.Main;

import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Duration;


public class NetworkController implements Initializable{
	
	
	private ObservableList<NetworkJavaFXBean> tabledata=FXCollections.observableArrayList();
	
	private ObservableList<PieChart.Data> pcdata=FXCollections.observableArrayList();
	
	ObservableList<String> comboBoxBranchItems=FXCollections.observableArrayList(FilterUtils.ALLBRANCH);
	ObservableList<String> comboBoxNetworkItems=FXCollections.observableArrayList(FilterUtils.NETWORK1);
	ObservableList<String> comboBoxClientItems=FXCollections.observableArrayList();
	ObservableList<String> comboBoxDNDItems=FXCollections.observableArrayList(FilterUtils.DND);
	ObservableList<String> comboBoxTypeItems=FXCollections.observableArrayList(FilterUtils.TYPE1,FilterUtils.CASH,FilterUtils.CREDIT);
	
	
	public static String configfilePath = "typeproperties.properties";
	Properties prop = new Properties();
	InputStream in = this.getClass().getClassLoader().getResourceAsStream(configfilePath);
	 
	
	/*PropertyReader reader = new PropertyReader();
		Configuration config = reader.readProperty(configfilePath);*/
		
	DecimalFormat df=new DecimalFormat(".##");
	DecimalFormat dfWeight=new DecimalFormat(".###");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	
	double pievalue=0.0;
	double totalpievalue=0.0;
		
	@FXML
	private Button btnUpdate;
	
	@FXML
	private NumberAxis yAxis;
	
	@FXML
	private CategoryAxis xAxis;
	
	@FXML
	private BarChart<?,?> bc;
	
	@FXML
	private PieChart pc;
	
	@FXML
	private Button btnGenerate;
	
	@FXML
	private VBox vbox;
	
	@FXML
	private Button btnsave;
	
	@FXML
	private Button btnexcel;
	
	@FXML
	private Button btnExportPDF;
	
	@FXML
	private Button btnreset;
	
	@FXML
	private Button btnClose;
	
	@FXML
	private Pagination pagination;
	
	@FXML
	private CheckBox chkShowGraphData;
	
	@FXML
	private ComboBox<String> comboBoxBranch;
	
	@FXML
	private ComboBox<String> comboBoxType;
	
	@FXML
	private ComboBox<String> comboBoxClient;
	
	@FXML
	private ComboBox<String> comboBoxDND;
	
	@FXML
	private ComboBox<String> comboBoxNetwork;
	
	@FXML
	private DatePicker dpFrom;
	
	@FXML
	private DatePicker dpTo;
	
	/*@FXML
	private ComboBox<String> comboBoxQuarters;*/
	
	
	@FXML
	private TableView<NetworkJavaFXBean> table;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, Integer> serialno;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, String> awb_no;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, String> dbtclient;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, String> bookingdate;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, String> branch;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, String> clientcode;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, String> city;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, String> network;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, String> service;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, String> doxnonDox;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, String> percentage;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, Integer> packets;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, Double> billingweight;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, Double> insuracneamount;
	 
	@FXML
	private TableColumn<NetworkJavaFXBean, Double> shipercost;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, Double> cod;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, Double> fod;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, Double> docketcharge;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, Double> otherVas;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, Double> amount;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, Double> fuel;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, Double> servicetax;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, Double> total;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, String> profitlosspercentage;
	
	@FXML
	private TableColumn<NetworkJavaFXBean, String> profitloss;
	
	
	@FXML
	public void loadData() throws SQLException, FileNotFoundException
	{	
		pievalue=0.0;
		totalpievalue=0.0;
		
		//comboBoxClient.setValue(comboBoxClient.getValue());
		try
		{
			showNetworkWiseData();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	

	
	public void loadBranch() throws SQLException
	{
		
		//LoadBranch lb=new LoadBranch();
		
		System.out.println("Check value is: "+LoadBranch.check);
		//Set<String> set=lb.loadBranch()
		new LoadBranch().loadBranch();
		System.out.println("Check value after the method calling: "+LoadBranch.check);
	/*LoadBranch lb=new LoadBranch();
	lb.loadBranch();*/
	
	//System.out.println(lb.setBranch.size());
	
		System.out.println("Network Contorller ============= LoadBranch from LoadBranch Class: =========== ");
		
		//System.out.println("Network Contorller >> Load from Global set object");
		for(String s:LoadBranch.SETLOADBRANCHFORALL)
		{
			
			comboBoxBranchItems.add(s);
		}
		comboBoxBranch.setItems(comboBoxBranchItems);
		
		
		//LoadBranch.SETLOADBRANCHFORALL.add("Hello");
		//LoadBranch.SETLOADBRANCHFORALL.add("Hello1");
		/*System.out.println("New value is:  ^^^^^^^^^^ "+ LoadBranch.SETLOADBRANCHFORALL.size());
		
		for(String s:LoadBranch.SETLOADBRANCHFORALL)
		{
			System.out.println("New Values: "+s);
		}*/
		
		
		
		/*DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		//ZonePOJO netbean=new ZonePOJO();
		
		
		Set<String> set = new HashSet<String>();
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select code from brndetail";
			rs=st.executeQuery(sql);
	
			while(rs.next())
			{
				set.add(rs.getString(1));
			}
		
			for(String s:set)
			{
				comboBoxBranchItems.add(s);
			}
			comboBoxBranch.setItems(comboBoxBranchItems);
					
		}
		catch(Exception e)
		{
			 System.out.println(e +"From load network");
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}*/
	}
	
	
	public void loadDND() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		
		
		Set<String> set = new HashSet<String>();
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select dox_nondox from dailybookingtransaction";
			rs=st.executeQuery(sql);
	
			while(rs.next())
			{
				set.add(rs.getString(1));
			}
		
			for(String s:set)
			{
				comboBoxDNDItems.add(s);
			}
			comboBoxDND.setItems(comboBoxDNDItems);
					
			
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
	
	public void loadAllNetworks() throws SQLException
	{
		
		new	LoadNetworks().loadAllNetworks();
		
		System.out.println("Network Contorller ============= LoadNetworks from LoadNetwork Class: =========== ");
		
		//System.out.println("Network Contorller >> Load from Global set object");
		for(String s:LoadNetworks.SET_LOAD_NETWORK_FOR_ALL)
		{
			comboBoxNetworkItems.add(s);
			
		}
		comboBoxNetwork.setItems(comboBoxNetworkItems);
		
	/*	DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		Set<String> set = new HashSet<String>();
	
		try
		{	
			st=con.createStatement();
			String sql="select code from vendormaster where vendor_type='N'";
			rs=st.executeQuery(sql);
			comboBoxNetwork.setValue(FilterUtils.NETWORK1);
			//comboBoxNetworkItems.add(NetworkReportUtils.NETWORK1);
			while(rs.next())
			{
				set.add(rs.getString(1));
			}
	
			for(String s:set)
			{
				comboBoxNetworkItems.add(s);
			}
		
			comboBoxNetwork.setItems(comboBoxNetworkItems);
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}*/
	}
	
	
	@FXML
	public void clientvalue()
	{
		System.out.println(comboBoxClient.getValue()+"  ^^^^^^^^^^^^^^^^^^^^^^^^^^");
	}
	
	
	@FXML
	public void updateBranchByClick() throws SQLException
	{
		new LoadBranch().updateBranch();
	}
	
	
	@FXML
	public void setFilterClients() throws SQLException
	{
		comboBoxClient.getItems().clear();
		comboBoxClient.setItems(comboBoxClientItems);
		
		comboBoxClient.setValue(FilterUtils.CLIENT);
		comboBoxClientItems.add(FilterUtils.CLIENT);
			
		new	LoadClients().loadClient();
			
		Set<String> setClient =new HashSet<>();
				
			if(!comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH) && comboBoxType.getValue().equals(FilterUtils.TYPE1))
			{
				for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
				{	
					if(comboBoxBranch.getValue().equals(s.getBranchcode()))
					{
						setClient.add(s.getClientCode());
					}					 
				}
			}
				 
			else if(comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH) && !comboBoxType.getValue().equals(FilterUtils.TYPE1))
			{
				if(comboBoxType.getValue().equals(FilterUtils.CASH))
				{ 
					for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
					{
						if(comboBoxType.getValue().equals(s.getClientCode()))
						{
							setClient.add(s.getClientCode());
						}
					}
				}
				else
				{
					for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
					{
					 	if(!FilterUtils.CASH.equals(s.getClientCode()))
						{
							setClient.add(s.getClientCode());
						}
					}
				}
			}
				 
			else if(!comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH) && !comboBoxType.getValue().equals(FilterUtils.TYPE1))
			{
				if(comboBoxType.getValue().equals(FilterUtils.CASH))
				{
					for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
					{
						if(comboBoxBranch.getValue().equals(s.getBranchcode()) && comboBoxType.getValue().equals(s.getClientCode()))
						{
							setClient.add(s.getClientCode());
						}
					}
				}
				else
				{
					for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
					{
						if(comboBoxBranch.getValue().equals(s.getBranchcode()) && !FilterUtils.CASH.equals(s.getClientCode()))
						{
							setClient.add(s.getClientCode());
						}
					}
				}
			}
				 
			else
			{
				for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
				{
					setClient.add(s.getClientCode());
				}
			}
			 
			for(String set:setClient)
			{
				comboBoxClientItems.add(set);
			}
			
			comboBoxClient.setItems(comboBoxClientItems);
	}
	
	
	
	
	@FXML
	public void setClinetData() throws SQLException, FileNotFoundException
	{
		
		comboBoxClient.getItems().clear();
		comboBoxClient.setItems(comboBoxClientItems);
		
		 comboBoxClient.setValue(FilterUtils.CLIENT);
			comboBoxClientItems.add(FilterUtils.CLIENT);
		  new	LoadClients().loadClient();
		
		System.out.println("Network Contorller ========== LoadClient from LoadCient Class: ========= ");
		
		Set<String> setClient =new HashSet<>();
		
		//System.out.println("Network Contorller >> Load from Global set object");
		for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
		{
			//System.out.println("From Load Clinet N/s: "+s.getBranchcode()+" "+s.getClientCode());
			setClient.add(s.getClientCode());
			//comboBoxBranchItems.add(s.getClientCode());
		}
		//comboBoxBranch.setItems(comboBoxBranchItems);
		
		for(String set:setClient)
		{
			//System.out.println("From Load Clinet Zone SET: "+set);
			comboBoxClientItems.add(set);
		}
		
		comboBoxClient.setItems(comboBoxClientItems);
		/*comboBoxClient.getItems().clear();
		comboBoxClient.setItems(comboBoxClientItems);
		 
		//System.out.println(comboBoxClient.getValue()+" ----==============>>>>>>>>>>>>>>>>>");
		
	
		 
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		String sql=null;
		
	
		 Set<String> setclient = new HashSet<String>();
		
		
		
		try
		{	
			 
			prop.load(in);
			
			 if(!comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH) && comboBoxType.getValue().equals(FilterUtils.TYPE1))
			 {
				sql="select dbt.dailybookingtransaction2client from dailybookingtransaction as dbt, clientmaster as cm where dbt.dailybookingtransaction2client=cm.code "
							+ "and dbt.dailybookingtransaction2Branch='"+comboBoxBranch.getValue()+"'";
					System.out.println("from if: =======>"+comboBoxBranch.getValue()+" "+comboBoxType.getValue());
			 }
			 
			 else if(comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH) && !comboBoxType.getValue().equals(FilterUtils.TYPE1))
			 {
				 sql="select dbt.dailybookingtransaction2client from dailybookingtransaction as dbt, clientmaster as cm where dbt.dailybookingtransaction2client=cm.code "
							+ "and cm.client_type='"+prop.getProperty(comboBoxType.getValue())+"'";
					System.out.println("from Ist else if: =======>"+comboBoxBranch.getValue()+" "+prop.getProperty(comboBoxType.getValue()));
			 }
			 
			 else if(!comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH) && !comboBoxType.getValue().equals(FilterUtils.TYPE1))
			{
				sql="select dbt.dailybookingtransaction2client from dailybookingtransaction as dbt, clientmaster as cm where dbt.dailybookingtransaction2client=cm.code "
						+ "and cm.client_type='"+prop.getProperty(comboBoxType.getValue())+"' and dbt.dailybookingtransaction2Branch='"+comboBoxBranch.getValue()+"'";
				System.out.println("from IInd else if: =======>"+comboBoxBranch.getValue()+" "+prop.getProperty(comboBoxType.getValue()));
			}
			 
			else
			{
				sql="select code from clientmaster";
				//System.out.println("from else: =======>"+comboBoxBranch.getValue()+" "+comboBoxType.getValue());
			}
			
			setclient.clear();
			 
			//System.out.println("sql from client data: "+sql);
			
			
			
			st=con.createStatement();
			
			rs=st.executeQuery(sql);
			comboBoxClient.setValue(FilterUtils.CLIENT);
			comboBoxClientItems.add(FilterUtils.CLIENT);
			
			if(!rs.next())
			{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Database Alert");
				alert.setHeaderText(null);
				alert.setContentText("No match found");
				alert.showAndWait();
				
				comboBoxBranch.requestFocus();
				comboBoxClient.getItems().clear();
				comboBoxClient.setValue("No match found");
				comboBoxClientItems.add("No match found");
				
				
			}
			else
			{
			do
			{
				setclient.add(rs.getString(1));
			}while(rs.next());
			}
			for(String s:setclient)
			{
				comboBoxClientItems.add(s);
			}
			
			comboBoxClient.setItems(comboBoxClientItems);
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		
		}*/
		
	}
	
//--------------------------------------------------------

	
	public void showNetworkWiseData() throws SQLException, IOException
	{
		if(dpFrom.getValue()==null)
	
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Empty Field Validation");
		alert.setHeaderText(null);
		alert.setContentText("From Date field is Empty!");
		alert.showAndWait();
		dpFrom.requestFocus();
	}
	else if( dpTo.getValue()==null)
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Empty Field Validation");
		alert.setHeaderText(null);
		alert.setContentText("To Date field is Empty!");
		alert.showAndWait();
		dpTo.requestFocus();
	}
	
	else
		{
		
		LocalDate from=dpFrom.getValue();
		LocalDate to=dpTo.getValue();
		Date stdate=Date.valueOf(from);
		Date edate=Date.valueOf(to);
		if(chkShowGraphData.isSelected()==false)
				{
			
			tabledata.clear();
			
	    setFilterForTable(stdate, edate);
			
			//System.out.println("Check box is selected");
				}
		else
		{
			
			
			tabledata.clear();
    		awb_no.setVisible(true);
			clientcode.setVisible(true);
			dbtclient.setVisible(true);
			branch.setVisible(true);
			bookingdate.setVisible(true);
			shipercost.setVisible(true);
			city.setVisible(true);
			network.setVisible(true);
			service.setVisible(true);
			doxnonDox.setVisible(true);
			docketcharge.setVisible(true);
			
			amount.setVisible(false);
			percentage.setVisible(false);
				
			
				setFilterForTable(stdate,edate);
				
		    	//showZoneWiseData(stdate, edate);
	    	}
		}
	}
	
	
	
	
	//------------------------------------------------------------------------------------------------------------------
	
	
	public void simpleTable(String sql) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		NetworkBean netbean=new NetworkBean();
		// Normal java bean class
		
		ResultSet rs=null;
		Statement st=null;
		
		try{
			awb_no.setVisible(false);
			bookingdate.setVisible(false);
			clientcode.setVisible(false);
			dbtclient.setVisible(false);
			branch.setVisible(false);
			shipercost.setVisible(false);
			city.setVisible(false);
			service.setVisible(false);
			doxnonDox.setVisible(false);
			docketcharge.setVisible(false);
			pagination.setVisible(false);
			
			amount.setVisible(true);
			percentage.setVisible(true);
			network.setVisible(true);
			
			//bookingdate.setText("Networks");
			
			String finalsql="select vnd.code,sum(dbt.packets) as pcs,sum(dbt.billing_weight) as weight,sum(dbt.insurance_amount) as insuracne,sum(dbt.cod_amount) as cod,"
					+ "sum(dbt.fod_amount) as fod ,sum(dbt.vas_amount) as vas, SUM(dbt.amount) as amount,sum(dbt.fuel) as fuel,sum(dbt.tax_amount1+dbt.tax_amount2+dbt.tax_amount3) as tax,"
					+ "sum(dbt.total) as total from dailybookingtransaction as dbt, vendormaster as vnd, clientmaster as cm where dbt.dailybookingtransaction2network=vnd.code and dbt.dailybookingtransaction2client=cm.code and vnd.vendor_type='N' "+sql+" Group by vnd.code";
			
			/*finalsql="select pm.city_name,sum(dbt.packets) as pcs,sum(dbt.billing_weight) as weight,sum(dbt.insurance_amount) as insuracne,"
					+ "sum(dbt.cod_amount) as cod,sum(dbt.fod_amount) as fod ,sum(dbt.vas_amount) as vas, SUM(dbt.amount) as amount,sum(dbt.fuel) as fuel,"
					+ "sum(dbt.tax_amount1+dbt.tax_amount2+dbt.tax_amount3) as tax,sum(dbt.total) as total from dailybookingtransaction as dbt, pincode_master as pm, "
					+ "clientmaster as cm where dbt.total is not null and dbt.dailybookingtransaction2city=pm.city_name and dbt.dailybookingtransaction2client=cm.code and dbt.booking_date between '2016-12-01' AND '2017-12-18' Group by pm.city_name order by sum(dbt.total) DESC LIMIT 5";*/
			
			System.out.println("From simple table: ===>> "+finalsql);
			st=con.createStatement();
			rs=st.executeQuery(finalsql);
			int serialno=1;
		
			while (rs.next())
			{
				
				netbean.setSerialno(serialno);
				//netbean.setBookingdate(rs.getString(1));
				netbean.setNetwork(rs.getString(1));
				netbean.setPackets(rs.getInt(2));
				netbean.setBillingweight(Double.parseDouble(df.format(rs.getDouble(3))));
				netbean.setInsuracneamount(Double.parseDouble(df.format(rs.getDouble(4))));
				netbean.setCod(Double.parseDouble(df.format(rs.getDouble(5))));
				netbean.setFod(Double.parseDouble(df.format(rs.getDouble(6))));
				netbean.setOtherVas(Double.parseDouble(df.format(rs.getDouble(7))));
				netbean.setAmount(Double.parseDouble(df.format(rs.getDouble(8))));
				netbean.setFuel(Double.parseDouble(df.format(rs.getDouble(9))));
				netbean.setServicetax(Double.parseDouble(df.format(rs.getDouble(10))));
				netbean.setTotal(Double.parseDouble(df.format(rs.getDouble(11))));
				
				//System.out.println(netbean.getNetwork());
				
				
				double per=Double.parseDouble(df.format((netbean.getTotal()/totalpievalue)*100));
				
				tabledata.add(new NetworkJavaFXBean(netbean.getSerialno(),null,null,null,null,null,null,netbean.getNetwork(),null,null,
						netbean.getPackets(),netbean.getBillingweight(), netbean.getInsuracneamount(), 0, netbean.getCod(),netbean.getFod(),0,
						netbean.getOtherVas(),netbean.getAmount(),netbean.getFuel(), netbean.getServicetax(), netbean.getTotal(),per+"%",null,null));
				
				serialno++;
			}
			table.setItems(tabledata);
			
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{
			dbcon.disconnect(null, st, rs, con);
			
			totalpievalue=0.0;
		}
	}
	
	
// ------------------------------------------------ Method for Bar Chart data ---------------------------------------
	
	public void showGraph(String sql) throws SQLException
	{
		XYChart.Series series1 = new XYChart.Series();
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();

		Statement st=null;
		ResultSet rs=null;
		ZoneBean netbean=new ZoneBean();

		try
		{
			//bc.setTitle("Networks report between" +stdate+" to "+edate);
			st=con.createStatement();
			String finalsql="select vnd.code,sum(dbt.total) as total from dailybookingtransaction as dbt, vendormaster as vnd, clientmaster as cm where dbt.dailybookingtransaction2network=vnd.code and dbt.dailybookingtransaction2client=cm.code "
					+ "and vnd.vendor_type='N' "+sql+" Group by vnd.code";	


			System.out.println(finalsql);
			rs=st.executeQuery(finalsql);

			if(!rs.next())
			{
				bc.getData().clear();
				pcdata.clear();
				tabledata.clear();

				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Database Alert");
				alert.setHeaderText(null);
				alert.setContentText("Data not found");
				alert.showAndWait();

			}
			else
			{	
				do
				{	
					netbean.setBookingdate(rs.getString(1));
					netbean.setTotal(Double.parseDouble(df.format(rs.getDouble(2))));
					series1.getData().add(new XYChart.Data(netbean.getBookingdate(),netbean.getTotal()));
					series1.setName("Networks");
				}
				while(rs.next());

				bc.getData().clear();
				bc.getData().add(series1);
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
		
		
		//------------------------------------------------ Method for Pie Chart data ---------------------------------------
		
	public void showPie(String sql) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		ArrayList<NetworkBean> list=new ArrayList<NetworkBean>();
			try
			{
				pcdata.clear();
				st=con.createStatement();
				String finalsql="select vnd.code,sum(dbt.total) as total from dailybookingtransaction as dbt, vendormaster as vnd, clientmaster as cm where dbt.dailybookingtransaction2network=vnd.code and dbt.dailybookingtransaction2client=cm.code "
						+ "and vnd.vendor_type='N' "+sql+" Group by vnd.code";		
				
				System.out.println("from pie chart: "+finalsql);
				rs=st.executeQuery(finalsql);
				if(!rs.next())
				{
					btnexcel.setVisible(false);
					btnExportPDF.setVisible(false);
					tabledata.clear();
				}
				else
				{
					btnexcel.setVisible(true);
					btnExportPDF.setVisible(true);
					do
					{
						NetworkBean netbean=new NetworkBean();
						
						netbean.setBookingdate(rs.getString(1));
						netbean.setTotal(Double.parseDouble(df.format(rs.getDouble(2))));
						
						pievalue=pievalue+netbean.getTotal();
						totalpievalue=totalpievalue+netbean.getTotal();
					
					list.add(netbean);
						
						//pcdata.add(new PieChart.Data(netbean.getBookingdate(),netbean.getTotal()));
					}
					while(rs.next());
					
					for(NetworkBean bean: list)
					{	
						pcdata.add(new PieChart.Data(bean.getBookingdate()+" ("+String.valueOf(df.format((bean.getTotal()/pievalue)*100) + "%)"), bean.getTotal()));
						
					}
					
					pc.setData(pcdata);
					
					 pcdata.stream().forEach(pieData -> {
						    pieData.getNode().addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
						        Bounds b1 = pieData.getNode().getBoundsInLocal();
						        double newX = (b1.getWidth()) / 2 + b1.getMinX();
						        double newY = (b1.getHeight()) / 2 + b1.getMinY();
						       // Make sure pie wedge location is reset
						        pieData.getNode().setTranslateX(0);
						        pieData.getNode().setTranslateY(0);
						        TranslateTransition tt = new TranslateTransition(
						                Duration.millis(1500), pieData.getNode());
						        tt.setByX(newX);
						        tt.setByY(newY);
						        tt.setAutoReverse(true);
						        tt.setCycleCount(2);
						        tt.play();
						    	
						    	System.out.println("Node name >> "+pieData.getName()+" | Value: "+pieData.getPieValue());
						    	
						    });
						});
					
					
				}
				
			}
			catch(Exception e)
			{
				 System.out.println(e);
				 e.printStackTrace();
			}	
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
	}
	
	//-------------------------------------------------------------------------
	
	public static void showSaveDialog() {
		
         FileChooser fc = new FileChooser();

         //Set extension filter
         FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
         fc.getExtensionFilters().addAll(
 				new ExtensionFilter("Excel Files","*.xls"),
					new ExtensionFilter("PDF Files", "*.pdf"));
         
         //Show save file dialog
         File file = fc.showSaveDialog(null);
         
         file.getAbsoluteFile();
         System.out.println("file chooser: "+file.getAbsoluteFile());
         
     }

	
	
	
	
	//--------------------------------------------------------------------------
	
	
	@FXML
	public void exportToPDF() throws FileNotFoundException, InterruptedException, DocumentException
	{

		Document my_pdf_report = new Document();
		
		//String pdffile="E:/NetwrokReport.pdf";
		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("PDF Files", "*.pdf"));
		File file = fc.showSaveDialog(null);
		
		PdfWriter.getInstance(my_pdf_report, new FileOutputStream(file.getAbsolutePath()));
        my_pdf_report.open();            
        
        PdfPTable my_report_table = new PdfPTable(14);
        
        my_report_table.setWidthPercentage(100);
        my_report_table.setHorizontalAlignment(Element.ALIGN_LEFT);
        
        Font headingfont = new Font(Font.FontFamily.UNDEFINED, 6, Font.BOLD);
        Font tfont = new Font(Font.FontFamily.UNDEFINED, 6, Font.NORMAL);
      
      //create a cell object
       
        PdfPCell table_cell = null;
            
       	table_cell=new PdfPCell(new Phrase("Sr_No.",headingfont));
       	table_cell.setBorder(Rectangle.NO_BORDER);
       	my_report_table.addCell(table_cell);
     
    	table_cell=new PdfPCell(new Phrase("Booking Date",headingfont));
       	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
     
      	table_cell=new PdfPCell(new Phrase("Network",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
     
      	table_cell=new PdfPCell(new Phrase("D/ND",headingfont));
       	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
      	
      	table_cell=new PdfPCell(new Phrase("PCS",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
     
      	table_cell=new PdfPCell(new Phrase("Weight",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
     
      	table_cell=new PdfPCell(new Phrase("Ins Amt",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
      
      	table_cell=new PdfPCell(new Phrase("COD",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
     
      	table_cell=new PdfPCell(new Phrase("FOD",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
    
      	table_cell=new PdfPCell(new Phrase("VAS",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
    
      	table_cell=new PdfPCell(new Phrase("Amount",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
     
      	table_cell=new PdfPCell(new Phrase("Fuel",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
     
        table_cell=new PdfPCell(new Phrase("Service Tax",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
     
      	table_cell=new PdfPCell(new Phrase("Total",headingfont));
      	table_cell.setBorder(Rectangle.NO_BORDER);
      	my_report_table.addCell(table_cell);
     
      	/*my_report_table.addCell(new PdfPCell(new Phrase("Serial No",tfont)));
        my_report_table.addCell(new Phrase("Booking Date",tfont));
        my_report_table.addCell(new Phrase("Srvc Grp",tfont));
      	 */
      
        for(NetworkJavaFXBean fxBean: tabledata)
		{
        	  
			/* my_report_table.addCell(new PdfPCell(new Phrase(String.valueOf(fxBean.getSerialno()),tfont)));
        	 my_report_table.addCell(new PdfPCell(new Phrase(fxBean.getBookingdate(),tfont)));
        	 my_report_table.addCell(new PdfPCell(new Phrase(String.valueOf(fxBean.getPackets()),tfont)));
        	 my_report_table.addCell(new PdfPCell(new Phrase(String.valueOf(fxBean.getBillingweight()),tfont)));
        	*/

        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getSerialno()),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
             
            table_cell=new PdfPCell(new Phrase(fxBean.getBookingdate(),tfont));
            table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
            
        	table_cell=new PdfPCell(new Phrase(fxBean.getNetwork(),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
            
            table_cell=new PdfPCell(new Phrase(fxBean.getDoxnonDox(),tfont));
            table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
            
        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getPackets()),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
            
        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getBillingweight()),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
            
        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getInsuracneamount()),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
            
        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getCod()),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
            
        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getFod()),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
            
        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getOtherVas()),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
            
            table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getAmount()),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
            
         	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getFuel()),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
         	
             table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getServicetax()),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
         	
            table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getTotal()),tfont));
        	table_cell.setBorder(Rectangle.NO_BORDER);
            my_report_table.addCell(table_cell);
        	 
            table_cell.setBorder(0);
		
		}
     
        my_pdf_report.add(my_report_table);
        System.out.println("file chooser: "+file.getAbsoluteFile());
           
        System.out.println("File "+file.getName()+" successfully saved");
         my_pdf_report.close();
			
	}
	
	//--------------------------------------------------------------------------
		
	@FXML
	public void exportToExcel() throws SQLException, IOException
	{
			
		HSSFWorkbook workbook=new HSSFWorkbook();
		HSSFSheet sheet=workbook.createSheet("Sales Data");
		HSSFRow rowhead=sheet.createRow(0);
		
		rowhead.createCell(0).setCellValue("Serial No");
		rowhead.createCell(1).setCellValue("AWB_No");
		rowhead.createCell(2).setCellValue("Client Name");
		rowhead.createCell(3).setCellValue("Booking Date");
		
		rowhead.createCell(4).setCellValue("Branch");
		rowhead.createCell(5).setCellValue("Client Code");
		rowhead.createCell(6).setCellValue("City");
		rowhead.createCell(7).setCellValue("Network");
		
		rowhead.createCell(8).setCellValue("Service");
		rowhead.createCell(9).setCellValue("Dox Non-Dox");
		rowhead.createCell(10).setCellValue("Packets");
		rowhead.createCell(11).setCellValue("Billing Weight");
		
		rowhead.createCell(12).setCellValue("Insurance Amount");
		rowhead.createCell(13).setCellValue("Shiper Cost");
		rowhead.createCell(14).setCellValue("COD Amount");
		rowhead.createCell(15).setCellValue("FOD Amount");
		
		rowhead.createCell(16).setCellValue("Docket Charge");
		rowhead.createCell(17).setCellValue("VAS");
		rowhead.createCell(18).setCellValue("Fuel");
		rowhead.createCell(19).setCellValue("Amount");
		
		rowhead.createCell(20).setCellValue("Service Tax");
		rowhead.createCell(21).setCellValue("Total");
		rowhead.createCell(22).setCellValue("Percentage");
		
		
			int i=2;
			
			HSSFRow row;
			
			for(NetworkJavaFXBean fxBean: tabledata)
			{
				row = sheet.createRow((short) i);
			
				row.createCell(0).setCellValue(fxBean.getSerialno());
				row.createCell(1).setCellValue(fxBean.getAwb_no());
				row.createCell(2).setCellValue(fxBean.getDbtclient());
				
				row.createCell(3).setCellValue(fxBean.getBookingdate());
				row.createCell(4).setCellValue(fxBean.getBranch());
				row.createCell(5).setCellValue(fxBean.getClientcode());
				
				row.createCell(6).setCellValue(fxBean.getCity());
				row.createCell(7).setCellValue(fxBean.getNetwork());
				row.createCell(8).setCellValue(fxBean.getService());
				
				row.createCell(9).setCellValue(fxBean.getDoxnonDox());
				row.createCell(10).setCellValue(fxBean.getPackets());
				row.createCell(11).setCellValue(fxBean.getBillingweight());
				
				row.createCell(12).setCellValue(fxBean.getInsuracneamount());
				row.createCell(13).setCellValue(fxBean.getShipercost());
				row.createCell(14).setCellValue(fxBean.getCod());
			
				row.createCell(15).setCellValue(fxBean.getFod());
				row.createCell(16).setCellValue(fxBean.getDocketcharge());
				row.createCell(17).setCellValue(fxBean.getOtherVas());
			
				row.createCell(18).setCellValue(fxBean.getFuel());
				row.createCell(19).setCellValue(fxBean.getAmount());
				row.createCell(20).setCellValue(fxBean.getServicetax());
				row.createCell(21).setCellValue(fxBean.getTotal());
				row.createCell(22).setCellValue(fxBean.getPercentage());
				
				i++;
			}
			
			
		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files","*.xls"));
		File file = fc.showSaveDialog(null);
		FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
		System.out.println("file chooser: "+file.getAbsoluteFile());
		
		workbook.write(fileOut);
		fileOut.close();
			
		System.out.println("File "+file.getName()+" successfully saved");
	      
	}	
		
		
	
	
	
	public void setFilterForTable(Date stdate,Date edate) throws SQLException, IOException
	{
		prop.load(in);
		
		String sql="";
		String branchsql="";
		String typesql="";
		String clientsql="";
		String dndsql="";
		String networksql="";

		String dateRange="and dbt.booking_date between '"+ stdate+"' AND '"+edate+"'";
		
		
		if(!comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH))
		{
			branchsql="and dbt.dailybookingtransaction2branch='"+comboBoxBranch.getValue()+"' ";
		}
	
		
		if(!comboBoxType.getValue().equals(FilterUtils.TYPE1))
		{
			typesql="and cm.client_type='"+prop.getProperty(comboBoxType.getValue())+"' ";
		}
		
		
		if(!comboBoxClient.getValue().equals(FilterUtils.CLIENT))
		{
			clientsql="and dbt.dailybookingtransaction2client='"+comboBoxClient.getValue()+"' ";
		}
		
		
		if(!comboBoxDND.getValue().equals(FilterUtils.DND))
		{
			dndsql="and dbt.dox_nondox='"+comboBoxDND.getValue()+"' ";
		}
	
		
		if(!comboBoxNetwork.getValue().equals(FilterUtils.NETWORK1))
		{
			networksql="and dbt.dailybookingtransaction2network='"+comboBoxNetwork.getValue()+"' ";
		}
			
		
		sql=branchsql+""+typesql+""+clientsql+""+dndsql+""+networksql+""+dateRange;
		
		System.out.println(" Filter sql: ====>> "+sql);
		
		
		if(chkShowGraphData.isSelected()==true)
		{
			
			showDetailTable(sql);
			showGraph(sql);
			showPie(sql);
		}
		else
		{
			showPie(sql);
			showGraph(sql);
			simpleTable(sql);
		}
		
	
	}
	

	//------------------------------------------------ Method for Show Table data ---------------------------------------
		
	public void showDetailTable(String sql) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		NetworkBean netbean=new NetworkBean(); // Normal java bean class
		
		ResultSet rs=null;
		Statement st=null;
		
	/*	awb_no.setVisible(true);
		clientcode.setVisible(true);
		dbtclient.setVisible(true);
		//branch.setVisible(true);
		city.setVisible(true);
		shipercost.setVisible(true);
		network.setVisible(true);
		service.setVisible(true);
		doxnonDox.setVisible(true);
		docketcharge.setVisible(true);
		amount.setVisible(false);*/
		
	//	bookingdate.setText("Booking Date");
	
		try{
			tabledata.clear();
			st=con.createStatement();
			/*String finalsql="select dbt.booking_date,dbt.air_way_bill_number,dbt.dailybookingtransaction2client,dbt.dailybookingtransaction2branch,dbt.sub_client_code,ct.city_name,dbt.dailybookingtransaction2city,"
					+ "zdtl.zonedetail2zonetype, dbt.dailybookingtransaction2network,dbt.dailybookingtransaction2service,dbt.dox_nondox,sum(dbt.packets) as pcs,"
					+ "sum(dbt.billing_weight) as weight,sum(dbt.insurance_amount) as insurance_amount,sum(dbt.shiper_cost) as shiper_cost,sum(dbt.cod_amount) as cod_amount,"
					+ "sum(dbt.fod_amount) as fod_amount,sum(dbt.docket_charge) as docket_charge,sum(dbt.vas_amount) as vas_amount,sum(dbt.fuel) as fuel,sum(dbt.service_tax+dbt.cess1+dbt.cess2),"
					+ "sum(dbt.total) as total from dailybookingtransaction as dbt,zonedetail as zdtl, city as ct ,clientmaster as cm where dbt.dailybookingtransaction2city=zdtl.zone2city and dbt.dailybookingtransaction2client=cm.code and  dbt.dailybookingtransaction2city=ct.code "+sql+" GROUP by "
					+ "dbt.dailybookingtransaction2client,dbt.booking_date,dbt.air_way_bill_number,dbt.dailybookingtransaction2branch,dbt.sub_client_code,dbt.dailybookingtransaction2city,"
					+ "dbt.dailybookingtransaction2network,dbt.dailybookingtransaction2service,dbt.dox_nondox,zdtl.zonedetail2zonetype,ct.city_name order by dbt.booking_date";*/
			
			String finalsql="select dbt.booking_date,dbt.air_way_bill_number,dbt.dailybookingtransaction2client,dbt.dailybookingtransaction2branch,dbt.sub_client_code,pm.city_name,"
					+ "dbt.dailybookingtransaction2city, dbt.dailybookingtransaction2network,dbt.dailybookingtransaction2service,dbt.dox_nondox,"
					+ "sum(dbt.packets) as pcs,sum(dbt.billing_weight) as weight,sum(dbt.insurance_amount) as insurance_amount,sum(dbt.shiper_cost) as shiper_cost,"
					+ "sum(dbt.cod_amount) as cod_amount,sum(dbt.fod_amount) as fod_amount,sum(dbt.docket_charge) as docket_charge,sum(dbt.vas_amount) as vas_amount,"
					+ "sum(dbt.fuel) as fuel,sum(dbt.tax_amount1+dbt.tax_amount2+dbt.tax_amount3),sum(dbt.total) as total from dailybookingtransaction as dbt,"
					+ "pincode_master as pm ,clientmaster as cm where dbt.dailybookingtransaction2client=cm.code and  dbt.dailybookingtransaction2city=pm.city_name "+sql
					+" GROUP by dbt.dailybookingtransaction2client,dbt.booking_date,dbt.air_way_bill_number,dbt.dailybookingtransaction2branch,dbt.sub_client_code,"
					+ "dbt.dailybookingtransaction2city,dbt.dailybookingtransaction2network,dbt.dailybookingtransaction2service,dbt.dox_nondox,pm.city_name order by dbt.booking_date";
			
			
			System.out.println("Final sql after filters: "+finalsql);
		 
			rs=st.executeQuery(finalsql);
			int serialno=1;
		
			if(!rs.next())
			{
				//bc.getData().clear();
				//pcdata.clear();
				
				btnexcel.setVisible(false);
				btnExportPDF.setVisible(false);
				pagination.setVisible(false);
				
				pc.getData().clear();
				bc.getData().clear();
				pcdata.clear();
				table.getItems().clear();
				//tabledata.clear();
			
				
			/*	Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Database Alert");
		 		alert.setHeaderText(null);
				alert.setContentText("Data not found");
				alert.showAndWait();*/
				
			}
			else
			{
			
				btnexcel.setVisible(true);
				btnExportPDF.setVisible(true);
			do
			{
				
				
				
				netbean.setSerialno(serialno);
				netbean.setAwb_no(rs.getString("air_way_bill_number"));
				netbean.setDbtClient(rs.getString("dailybookingtransaction2client"));
				netbean.setBookingdate(date.format(rs.getDate("booking_date")));
				netbean.setBranch(rs.getString("dailybookingtransaction2branch"));
				netbean.setClientcode(rs.getString("sub_client_code"));
				netbean.setCity(rs.getString("city_name"));
				netbean.setNetwork(rs.getString("dailybookingtransaction2network"));
				netbean.setService(rs.getString("dailybookingtransaction2service"));
				netbean.setDoxnonDox(rs.getString("dox_nondox"));
				netbean.setPackets(rs.getInt("pcs"));
				netbean.setBillingweight(Double.parseDouble(dfWeight.format(rs.getDouble("weight"))));
				netbean.setInsuracneamount(Double.parseDouble(df.format(rs.getDouble("insurance_amount"))));
				netbean.setShipercost(Double.parseDouble(df.format(rs.getDouble("shiper_cost"))));
				netbean.setCod(Double.parseDouble(df.format(rs.getDouble("cod_amount"))));
				netbean.setFod(Double.parseDouble(df.format(rs.getDouble("fod_amount"))));
				netbean.setDocketcharge(Double.parseDouble(df.format(rs.getDouble("docket_charge"))));
				netbean.setOtherVas(Double.parseDouble(df.format(rs.getDouble("vas_amount"))));
				netbean.setFuel(Double.parseDouble(df.format(rs.getDouble("fuel"))));
				netbean.setServicetax(Double.parseDouble(df.format(rs.getDouble(19))));  // here, service tax contain the value of "sum(dbt.service_tax+dbt.cess1+dbt.cess2)"
				netbean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
				
				//System.err.println("Awb No :: "+ netbean.getAwb_no()+" | Clients :: "+netbean.getDbtClient());
				
				tabledata.add(new NetworkJavaFXBean(netbean.getSerialno(),netbean.getAwb_no(),netbean.getDbtClient(),netbean.getBookingdate(),netbean.getBranch(),netbean.getClientcode(),
								netbean.getCity(),netbean.getNetwork(),netbean.getService(),netbean.getDoxnonDox(),netbean.getPackets(),
									netbean.getBillingweight(), netbean.getInsuracneamount(), netbean.getShipercost(), netbean.getCod(),netbean.getFod(),
										netbean.getDocketcharge(), netbean.getOtherVas(),0,netbean.getFuel(), netbean.getServicetax(), netbean.getTotal(),null,null,null));
				serialno++;
			}
			while (rs.next());
			
			table.setItems(tabledata);
			
			for(NetworkJavaFXBean ntBean:tabledata)
			{
				System.err.println("Awb No :: "+ ntBean.getAwb_no()+" | Clients :: "+ntBean.getDbtclient());
			}
		
			pagination.setVisible(true);
			pagination.setPageCount((tabledata.size() / rowsPerPage() + 1));
			pagination.setCurrentPageIndex(0);
			pagination.setPageFactory((Integer pageIndex) -> createPage(pageIndex));
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
		
	}
	
	//--------------------------------- Code for pagination ----------------------------------------
	
	public int itemsPerPage()
	{
		return 1;
	}

	public int rowsPerPage() 
	{
		return 12;
	}

	public GridPane createPage(int pageIndex)
	{
		int lastIndex = 0;
	
		GridPane pane=new GridPane();
		int displace = tabledata.size() % rowsPerPage();
            
			if (displace >= 0)
			{
				lastIndex = tabledata.size() / rowsPerPage();
			}
			/*else
			{
				lastIndex = tabledata.size() / rowsPerPage() -1;
				System.out.println("Pagination lastindex from else condition: "+lastIndex);
			}*/
	  
	        
			int page = pageIndex * itemsPerPage();
	        for (int i = page; i < page + itemsPerPage(); i++)
	        {
	            if (lastIndex == pageIndex)
	            {
	                table.setItems(FXCollections.observableArrayList(tabledata.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
	            }
	            else
	            {
	                table.setItems(FXCollections.observableArrayList(tabledata.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
	            }
	        }
	       return pane;
	      
	    }
	
	
	@FXML
	public void resetData() throws IOException
	{
		Main m=new Main();
		m.showNetworkReport();
			
	}
	
	
	@FXML
	public void close() throws IOException, SQLException
	{
		Main m=new Main();
		m.homePage();
	}
	
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		comboBoxBranch.setValue(FilterUtils.ALLBRANCH);
		comboBoxType.setValue(FilterUtils.TYPE1);
		comboBoxDND.setValue(FilterUtils.DND);
		
	try {
		loadBranch();
		loadAllNetworks();
		//loadAllType();
		loadDND();
		setClinetData();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		System.out.println(e);
		e.printStackTrace();
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	
		comboBoxType.setItems(comboBoxTypeItems);
		comboBoxClient.setItems(comboBoxClientItems);
		
		comboBoxClient.setValue(FilterUtils.CLIENT);
		comboBoxNetwork.setValue(FilterUtils.NETWORK1);
		
		btnexcel.setVisible(false);
		btnExportPDF.setVisible(false);
		
		//comboBoxClient.setDisable(true);

		//branch.setVisible(false);
		
		serialno.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,Integer>("serialno"));
		awb_no.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,String>("awb_no"));
		dbtclient.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,String>("dbtclient"));
		bookingdate.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,String>("bookingdate"));
		branch.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,String>("branch"));
		clientcode.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,String>("clientcode"));
		city.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,String>("city"));
		network.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,String>("network"));
		service.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,String>("service"));
		doxnonDox.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,String>("doxnonDox"));
		percentage.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,String>("percentage"));
	
		
		packets.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,Integer>("packets"));
		
		billingweight.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,Double>("billingweight"));
		insuracneamount.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,Double>("insuracneamount"));
		shipercost.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,Double>("shipercost"));
		cod.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,Double>("cod"));
		fod.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,Double>("fod"));
		docketcharge.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,Double>("docketcharge"));
		otherVas.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,Double>("otherVas"));
		amount.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,Double>("amount"));
		fuel.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,Double>("fuel"));
		servicetax.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,Double>("servicetax"));
		total.setCellValueFactory(new PropertyValueFactory<NetworkJavaFXBean,Double>("total"));

		//btnGenerate.setDisable(true);
		pagination.setVisible(false);
		
	}

}
