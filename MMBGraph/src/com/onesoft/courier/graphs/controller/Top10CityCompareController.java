package com.onesoft.courier.graphs.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.DB.DBconnection;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.common.LoadCity;
import com.onesoft.courier.common.LoadClientBean;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.graphs.beans.Top10CityJavaFXBean;
import com.onesoft.courier.graphs.beans.Top10CityBean;
import com.onesoft.courier.graphs.utils.FilterUtils;
import com.onesoft.courier.graphs.utils.SalesReportUtils;
import com.onesoft.courier.graphs.utils.ZoneReportUtils;
import com.onesoft.courier.main.Main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class Top10CityCompareController implements Initializable{
	
	
private ObservableList<Top10CityJavaFXBean> tabledata=FXCollections.observableArrayList();
	
	private ObservableList<PieChart.Data> pcdata=FXCollections.observableArrayList();
	
	 ObservableList<String> comboBoxBranchItems=FXCollections.observableArrayList(FilterUtils.ALLBRANCH);
	 ObservableList<String> comboBoxCityItems=FXCollections.observableArrayList(FilterUtils.ALLCITY);
	 ObservableList<String> comboBoxClientItems=FXCollections.observableArrayList();
	 ObservableList<String> comboBoxDNDItems=FXCollections.observableArrayList(FilterUtils.DND);
	 ObservableList<String> comboBoxTypeItems=FXCollections.observableArrayList(FilterUtils.TYPE1,FilterUtils.CASH,FilterUtils.CREDIT);
	
	 private ObservableList<String> comboBoxFromItems=FXCollections.observableArrayList(FilterUtils.YEAR5,FilterUtils.YEAR4,
																							FilterUtils.YEAR3,FilterUtils.YEAR2,FilterUtils.YEAR1);

	private ObservableList<String> comboBoxToItems=FXCollections.observableArrayList(FilterUtils.YEAR1,FilterUtils.YEAR2,
																						FilterUtils.YEAR3,FilterUtils.YEAR4,FilterUtils.YEAR5);

	
	 public static String configfilePath = "typeproperties.properties";
	 Properties prop = new Properties();
	 InputStream in = this.getClass().getClassLoader().getResourceAsStream(configfilePath);
	
	
	
	DecimalFormat df=new DecimalFormat(".##");
	DecimalFormat dfWeight=new DecimalFormat(".###");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	
	double pievalue=0.0;
	double totalpievalue=0.0;
	
	@FXML
	private NumberAxis yAxis;
	
	@FXML
	private CategoryAxis xAxis;
	
	@FXML
	private BarChart<?,?> bc;
	
	@FXML
	private PieChart pc;
	
	@FXML
	private Button btnGenerate;
	
	@FXML
	private VBox vbox;
	
	@FXML
	private Button btnexcel;
	
	@FXML
	private Button btnExportPDF;
	
	@FXML
	private Button btnreset;
	
	@FXML
	private Button btnClose;
	
	@FXML
	private Pagination pagination;
	
	@FXML
	private CheckBox chkShowGraphData;
	
	@FXML
	private ComboBox<String> comboBoxBranch;
	
	@FXML
	private ComboBox<String> comboBoxType;
	
	@FXML
	private ComboBox<String> comboBoxClient;
	
	@FXML
	private ComboBox<String> comboBoxDND;
	
	@FXML
	private ComboBox<String> comboBoxCity;
	
	@FXML
	private ComboBox<String> comboBoxFromYear;
	
	@FXML
	private ComboBox<String> comboBoxToYear;
	
	@FXML
	private DatePicker dpFrom;
	
	@FXML
	private DatePicker dpTo;
	
	/*@FXML
	private ComboBox<String> comboBoxQuarters;*/
	
	
	@FXML
	private TableView<Top10CityJavaFXBean> table;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, Integer> serialno;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, String> awb_no;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, String> dbtclient;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, String> bookingdate;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, String> branch;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, String> clientcode;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, String> city;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, String> network;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, String> service;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, String> doxnonDox;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, String> percentage;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, Integer> packets;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, Double> billingweight;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, Double> insuracneamount;
	 
	@FXML
	private TableColumn<Top10CityJavaFXBean, Double> shipercost;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, Double> cod;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, Double> fod;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, Double> docketcharge;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, Double> otherVas;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, Double> amount;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, Double> fuel;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, Double> servicetax;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, Double> total;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, String> profitlosspercentage;
	
	@FXML
	private TableColumn<Top10CityJavaFXBean, String> profitloss;
	
	
	@FXML
	public void loadData() throws SQLException, IOException
	{	
		pievalue=0.0;
		totalpievalue=0.0;
		setYear();
		//showTop10CityWiseData();
	}
	
	
	public void setYear() throws SQLException, IOException
	{
		String[] from=comboBoxFromYear.getValue().split("-");
		String[] to=comboBoxToYear.getValue().split("-");
		
		String firstyear=from[0];
		String secondyear=to[1];
		
		int fst=Integer.parseInt(firstyear);
		int scnd=Integer.parseInt(secondyear);
		
		if(fst<scnd)
		{	
			System.out.println("First year: "+firstyear);
			System.out.println("Second year: "+secondyear);
			String datesql="dbt.booking_date between '"+firstyear+"-04-01' AND '"+secondyear+"-03-31'";
			System.out.println("financial date filter: "+datesql);
			
			setFilterForTable(datesql);
				
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Wrong Years selected");
			alert.setHeaderText(null);
			alert.setContentText("Please select a valid From and To year...");
			alert.showAndWait();
		}
		
	}
	
	
	
	public void loadBranch() throws SQLException
	{
		new	LoadBranch().loadBranch();
		
		System.out.println("Top 10 City Compare Contorller >> LoadBranch from LoadBranch Class: >>>>>>>> ");
		
		//System.out.println("Network Contorller >> Load from Global set object");
		for(String s:LoadBranch.SETLOADBRANCHFORALL)
		{
			comboBoxBranchItems.add(s);
		}
		comboBoxBranch.setItems(comboBoxBranchItems);
		
		
		/*DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		//Top10CityPOJO citybean=new Top10CityPOJO();
		
		
		Set<String> set = new HashSet<String>();
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select code from brndetail";
			rs=st.executeQuery(sql);
	
			while(rs.next())
			{
				set.add(rs.getString(1));
			}
		
			for(String s:set)
			{
				comboBoxBranchItems.add(s);
			}
			comboBoxBranch.setItems(comboBoxBranchItems);
					
		}
		catch(Exception e)
		{
			 System.out.println(e +"From load network");
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}*/
	}
	
	
	public void loadDND() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		
		
		Set<String> set = new HashSet<String>();
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select dox_nondox from dailybookingtransaction";
			rs=st.executeQuery(sql);
	
			while(rs.next())
			{
				set.add(rs.getString(1));
			}
		
			for(String s:set)
			{
				comboBoxDNDItems.add(s);
			}
			comboBoxDND.setItems(comboBoxDNDItems);
					
			
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
	public void loadAllCity() throws SQLException
	{
		
		new	LoadCity().loadAllCity();
		
		System.out.println("City Contorller >> Load Cities from LoadCity  Class: >>>>>>>> ");
		
		
		for(String s:LoadCity.SET_LOAD_CITY_FOR_ALL)
		{
			comboBoxCityItems.add(s);
		}
		comboBoxCity.setItems(comboBoxCityItems);
		
		/*DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		Set<String> set = new HashSet<String>();
	
		try
		{	
			st=con.createStatement();
			String sql="SELECT dailybookingtransaction2city,ct.city_name,sum(dbt.total) as total from dailybookingtransaction as dbt, city as ct, clientmaster as cm where ct.code=dbt.dailybookingtransaction2city and dbt.dailybookingtransaction2client=cm.code  Group  by dbt.dailybookingtransaction2city,ct.city_name order by sum(dbt.total) DESC LIMIT 10";
			rs=st.executeQuery(sql);
			comboBoxCity.setValue(FilterUtils.ALLCITY);
			//comboBoxNetworkItems.add("All Networks");
			while(rs.next())
			{
				comboBoxCityItems.add(rs.getString(2));
			//	set.add(rs.getString(2));
			}
	
			for(String s:set)
			{
				//comboBoxNetworkItems.add(s);
			}
		
			comboBoxCity.setItems(comboBoxCityItems);
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}*/
	}
	
	
	
	@FXML
	public void setFilterClients() throws SQLException
	{
		comboBoxClient.getItems().clear();
		comboBoxClient.setItems(comboBoxClientItems);
		
		comboBoxClient.setValue(FilterUtils.CLIENT);
		comboBoxClientItems.add(FilterUtils.CLIENT);
			
		new	LoadClients().loadClient();
			
		Set<String> setClient =new HashSet<>();
				
			if(!comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH) && comboBoxType.getValue().equals(FilterUtils.TYPE1))
			{
				for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
				{	
					if(comboBoxBranch.getValue().equals(s.getBranchcode()))
					{
						setClient.add(s.getClientCode());
					}					 
				}
			}
				 
			else if(comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH) && !comboBoxType.getValue().equals(FilterUtils.TYPE1))
			{
				if(comboBoxType.getValue().equals(FilterUtils.CASH))
				{ 
					for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
					{
						if(comboBoxType.getValue().equals(s.getClientCode()))
						{
							setClient.add(s.getClientCode());
						}
					}
				}
				else
				{
					for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
					{
					 	if(!FilterUtils.CASH.equals(s.getClientCode()))
						{
							setClient.add(s.getClientCode());
						}
					}
				}
			}
				 
			else if(!comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH) && !comboBoxType.getValue().equals(FilterUtils.TYPE1))
			{
				if(comboBoxType.getValue().equals(FilterUtils.CASH))
				{
					for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
					{
						if(comboBoxBranch.getValue().equals(s.getBranchcode()) && comboBoxType.getValue().equals(s.getClientCode()))
						{
							setClient.add(s.getClientCode());
						}
					}
				}
				else
				{
					for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
					{
						if(comboBoxBranch.getValue().equals(s.getBranchcode()) && !FilterUtils.CASH.equals(s.getClientCode()))
						{
							setClient.add(s.getClientCode());
						}
					}
				}
			}
				 
			else
			{
				for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
				{
					setClient.add(s.getClientCode());
				}
			}
			 
			for(String set:setClient)
			{
				comboBoxClientItems.add(set);
			}
			
			comboBoxClient.setItems(comboBoxClientItems);
	}
	
	
	@FXML
	public void setClinetData() throws SQLException, FileNotFoundException
	{
		comboBoxClient.getItems().clear();
		comboBoxClient.setItems(comboBoxClientItems);
		
		comboBoxClient.setValue(FilterUtils.CLIENT);
		comboBoxClientItems.add(FilterUtils.CLIENT);
		new	LoadClients().loadClient();
		
		System.out.println("Top 10 City Compare Contorller Load Client  >> LoadClient from LoadCient Class: >>>>>>>> ");
		
		Set<String> setClient =new HashSet<>();
		
		for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
		{
			setClient.add(s.getClientCode());
		}
		
		for(String set:setClient)
		{
		//	System.out.println("From Load Clinet Top 10 City SET: "+set);
			comboBoxClientItems.add(set);
		}
		
		comboBoxClient.setItems(comboBoxClientItems);
		
		/*comboBoxClient.getItems().clear();
		comboBoxClient.setItems(comboBoxClientItems);
		 
		System.out.println(comboBoxClient.getValue()+" ----==============>>>>>>>>>>>>>>>>>");
		
	
		 
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		String sql=null;
		
	
		 Set<String> setclient = new HashSet<String>();
		
		
		
		try
		{	
			 
			prop.load(in);
			
			 if(!comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH) && comboBoxType.getValue().equals(FilterUtils.TYPE1))
			 {
				sql="select dbt.dailybookingtransaction2client from dailybookingtransaction as dbt, clientmaster as cm where dbt.dailybookingtransaction2client=cm.code "
							+ "and dbt.dailybookingtransaction2Branch='"+comboBoxBranch.getValue()+"'";
					System.out.println("from if: =======>"+comboBoxBranch.getValue()+" "+comboBoxType.getValue());
			 }
			 
			 else if(comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH) && !comboBoxType.getValue().equals(FilterUtils.TYPE1))
			 {
				 sql="select dbt.dailybookingtransaction2client from dailybookingtransaction as dbt, clientmaster as cm where dbt.dailybookingtransaction2client=cm.code "
							+ "and cm.client_type='"+prop.getProperty(comboBoxType.getValue())+"'";
					System.out.println("from Ist else if: =======>"+comboBoxBranch.getValue()+" "+prop.getProperty(comboBoxType.getValue()));
			 }
			 
			 else if(!comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH) && !comboBoxType.getValue().equals(FilterUtils.TYPE1))
			{
				sql="select dbt.dailybookingtransaction2client from dailybookingtransaction as dbt, clientmaster as cm where dbt.dailybookingtransaction2client=cm.code "
						+ "and cm.client_type='"+prop.getProperty(comboBoxType.getValue())+"' and dbt.dailybookingtransaction2Branch='"+comboBoxBranch.getValue()+"'";
				System.out.println("from IInd else if: =======>"+comboBoxBranch.getValue()+" "+prop.getProperty(comboBoxType.getValue()));
			}
			 
			else
			{
				sql="select code from clientmaster";
				System.out.println("from else: =======>"+comboBoxBranch.getValue()+" "+comboBoxType.getValue());
			}
			
			setclient.clear();
			 
			System.out.println("sql from client data: "+sql);
			
			
			
			st=con.createStatement();
			
			rs=st.executeQuery(sql);
			comboBoxClient.setValue(FilterUtils.CLIENT);
			comboBoxClientItems.add(FilterUtils.CLIENT);
			
			if(!rs.next())
			{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Database Alert");
				alert.setHeaderText(null);
				alert.setContentText("No match found");
				alert.showAndWait();
				
				comboBoxBranch.requestFocus();
				comboBoxClient.getItems().clear();
				comboBoxClient.setValue("No match found");
				comboBoxClientItems.add("No match found");
				
				
			}
			else
			{
			do
			{
				setclient.add(rs.getString(1));
			}while(rs.next());
			}
			for(String s:setclient)
			{
				comboBoxClientItems.add(s);
			}
			
			comboBoxClient.setItems(comboBoxClientItems);
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		
		}*/
		
	}
	
	//--------------------------------------------------------

		
	/*public void showTop10CityWiseData() throws SQLException
	{
		
		if(dpFrom.getValue()==null)
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Empty Field Validation");
			alert.setHeaderText(null);
			alert.setContentText("From Date field is Empty!");
			alert.showAndWait();
			dpFrom.requestFocus();
		}
    	else if( dpTo.getValue()==null)
    	{
    		Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Empty Field Validation");
			alert.setHeaderText(null);
			alert.setContentText("To Date field is Empty!");
			alert.showAndWait();
			dpTo.requestFocus();
    	}
		
    	else
    		{
    		
    		LocalDate from=dpFrom.getValue();
    		LocalDate to=dpTo.getValue();
    		Date stdate=Date.valueOf(from);
    		Date edate=Date.valueOf(to);
    		
    		if(chkShowGraphData.isSelected()==false)
    		{
			
    			tabledata.clear();
    			setFilterForTable(stdate, edate);
				//System.out.println("Check box is selected");
			}
    		else
    		{
    			tabledata.clear();
    			awb_no.setVisible(true);
    			clientcode.setVisible(true);
    			dbtclient.setVisible(true);
    			branch.setVisible(true);
    			bookingdate.setVisible(true);
    			shipercost.setVisible(true);
    			city.setVisible(true);
    			network.setVisible(true);
    			service.setVisible(true);
    			doxnonDox.setVisible(true);
    			docketcharge.setVisible(true);
			
    			amount.setVisible(false);
    			percentage.setVisible(false);
				
				setFilterForTable(stdate,edate);
			}
    		}
		
	}*/
	
	
	//--------------------------------------------------------------------------------------------------------------------
	
	public void simpleTable(String sql) throws SQLException
	{
		tabledata.clear();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Top10CityBean citybean=new Top10CityBean();
		
		 // Normal java bean class
		String finalsql=null;
		ResultSet rs=null;
		Statement st=null;
		
		try{
			awb_no.setVisible(false);
			//bookingdate.setVisible(false);
			clientcode.setVisible(false);
			dbtclient.setVisible(false);
			branch.setVisible(false);
			city.setVisible(true);
			shipercost.setVisible(false);
			network.setVisible(false);
			service.setVisible(false);
			doxnonDox.setVisible(false);
			docketcharge.setVisible(false);
			amount.setVisible(true);
			pagination.setVisible(false);
			percentage.setVisible(true);
			
		//	bookingdate.setText("Networks");
			
			
			
			if(!comboBoxCity.getValue().equals(FilterUtils.ALLCITY))
			{
				finalsql="select to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY'),ct.city_name,sum(dbt.packets) as pcs,sum(dbt.billing_weight) as weight,"
						+ "sum(dbt.insurance_amount) as insuracne,sum(dbt.cod_amount) as cod,sum(dbt.fod_amount) as fod ,sum(dbt.vas_amount) as vas, SUM(dbt.amount) as amount,"
						+ "sum(dbt.fuel) as fuel,sum(dbt.tax_amount1+dbt.tax_amount2+dbt.tax_amount3) as tax,sum(dbt.total) as total from dailybookingtransaction as dbt, pincode_master as pm, clientmaster as cm "
						+ "where dbt.dailybookingtransaction2city=pm.city_name and dbt.dailybookingtransaction2client=cm.code "+sql+" Group by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY'),ct.city_name "
						+ "order by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY')";
			}
			else
			{
				finalsql="select to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY'),sum(dbt.packets) as pcs,sum(dbt.billing_weight) as weight,"
						+ "sum(dbt.insurance_amount) as insuracne,sum(dbt.cod_amount) as cod,sum(dbt.fod_amount) as fod ,sum(dbt.vas_amount) as vas, SUM(dbt.amount) as amount,"
						+ "sum(dbt.fuel) as fuel,sum(dbt.tax_amount1+dbt.tax_amount2+dbt.tax_amount3) as tax,sum(dbt.total) as total from dailybookingtransaction as dbt, pincode_master as pm, clientmaster as cm "
						+ "where dbt.dailybookingtransaction2city=pm.city_name and dbt.dailybookingtransaction2client=cm.code "+sql+" Group by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY') "
						+ "order by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY')";
			}
			
			System.out.println("Sql from table: >>>"+finalsql);
			
			
			int financialyr=0;
			double per=0.0;
			st=con.createStatement();
			rs=st.executeQuery(finalsql);
			int serialno=1;
			citybean.setTotal(0.0);
		
			while (rs.next())
			{
				
				double profitloss=Double.parseDouble(df.format(rs.getDouble("total")-citybean.getTotal()));
				double profitlosspercentage=(profitloss/citybean.getTotal())*100;
				
				if(Double.isInfinite(profitlosspercentage))
				{
					citybean.setProfitloss("--");
					citybean.setProfitlosspercentage("--");
				}
				else if(profitlosspercentage>0)
				{
					citybean.setProfitloss(profitloss+"");
					citybean.setProfitlosspercentage(df.format(profitlosspercentage)+"%");
				}
				else
				{
					citybean.setProfitloss(profitloss+"");
					citybean.setProfitlosspercentage(df.format(profitlosspercentage)+"%");
				}
				
				
				if(!comboBoxCity.getValue().equals(FilterUtils.ALLCITY))
				{
				citybean.setSerialno(serialno);
				citybean.setBookingdate(rs.getString(1));
				financialyr=((Integer.parseInt(citybean.getBookingdate()))-2000)+1;
				citybean.setCity(rs.getString(2));
				citybean.setPackets(rs.getInt(3));
				citybean.setBillingweight(Double.parseDouble(df.format(rs.getDouble(4))));
				citybean.setInsuracneamount(Double.parseDouble(df.format(rs.getDouble(5))));
				citybean.setCod(Double.parseDouble(df.format(rs.getDouble(6))));
				citybean.setFod(Double.parseDouble(df.format(rs.getDouble(7))));
				citybean.setOtherVas(Double.parseDouble(df.format(rs.getDouble(8))));
				citybean.setAmount(Double.parseDouble(df.format(rs.getDouble(9))));
				citybean.setFuel(Double.parseDouble(df.format(rs.getDouble(10))));
				citybean.setServicetax(Double.parseDouble(df.format(rs.getDouble(11))));
				citybean.setTotal(Double.parseDouble(df.format(rs.getDouble(12))));
				}
				else
				{
					citybean.setSerialno(serialno);
					citybean.setBookingdate(rs.getString(1));
					financialyr=((Integer.parseInt(citybean.getBookingdate()))-2000)+1;
					citybean.setCity(FilterUtils.ALLCITY);
					citybean.setPackets(rs.getInt(2));
					citybean.setBillingweight(Double.parseDouble(df.format(rs.getDouble(3))));
					citybean.setInsuracneamount(Double.parseDouble(df.format(rs.getDouble(4))));
					citybean.setCod(Double.parseDouble(df.format(rs.getDouble(5))));
					citybean.setFod(Double.parseDouble(df.format(rs.getDouble(6))));
					citybean.setOtherVas(Double.parseDouble(df.format(rs.getDouble(7))));
					citybean.setAmount(Double.parseDouble(df.format(rs.getDouble(8))));
					citybean.setFuel(Double.parseDouble(df.format(rs.getDouble(9))));
					citybean.setServicetax(Double.parseDouble(df.format(rs.getDouble(10))));
					citybean.setTotal(Double.parseDouble(df.format(rs.getDouble(11))));
				}
				
				per=Double.parseDouble(df.format((citybean.getTotal()/totalpievalue)*100));
				
				tabledata.add(new Top10CityJavaFXBean(citybean.getSerialno(),null,null,citybean.getBookingdate()+"-"+financialyr,null,null,citybean.getCity(),null,null,null,
						citybean.getPackets(),citybean.getBillingweight(), citybean.getInsuracneamount(), 0, citybean.getCod(),citybean.getFod(),0,
						citybean.getOtherVas(),citybean.getAmount(),citybean.getFuel(), citybean.getServicetax(), citybean.getTotal(),per+"%",citybean.getProfitloss(),citybean.getProfitlosspercentage()));
				
				serialno++;
			}
			table.setItems(tabledata);
			
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{
			dbcon.disconnect(null, st, rs, con);
			totalpievalue=0.0;
		}
	}
	
	
	
	// ------------------------------------------------ Method for Bar Chart data ---------------------------------------
	
		public void showGraph(String sql) throws SQLException
		{
			XYChart.Series series1 = new XYChart.Series();
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			
			Statement st=null;
			ResultSet rs=null;
			Top10CityBean citybean=new Top10CityBean();
			
				try
				{
					st=con.createStatement();
					String finalsql=null;
					
					/*finalsql="SELECT to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY'),sum(dbt.total) as total from dailybookingtransaction as dbt,"
							+ " city as ct, clientmaster as cm where ct.code=dbt.dailybookingtransaction2city and dbt.dailybookingtransaction2client=cm.code "+sql+" "
									+ "Group  by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY') "
									+ "order by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY')";	*/
					
					finalsql="SELECT to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY'),sum(dbt.total) as total from "
							+ "dailybookingtransaction as dbt,pincode_master as pm, clientmaster as cm where pm.city_name=dbt.dailybookingtransaction2city "
							+ "and dbt.dailybookingtransaction2client=cm.code "+sql+" Group by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY') "
									+ "order by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY')";
					System.out.println("Form Graph: "+finalsql);
					
					//System.out.println(sql);
					rs=st.executeQuery(finalsql);
					
					if(!rs.next())
					{
						bc.getData().clear();
						pcdata.clear();
						tabledata.clear();
					
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Database Alert");
						alert.setHeaderText(null);
						alert.setContentText("Data not found");
						alert.showAndWait();
						
					}
					else
					{	
						do
						{	
							citybean.setBookingdate(rs.getString(1));
							citybean.setTotal(Double.parseDouble(df.format(rs.getDouble(2))));
							int date=((Integer.parseInt(citybean.getBookingdate()))-2000)+1;
							
							series1.getData().add(new XYChart.Data(citybean.getBookingdate()+"-"+date,citybean.getTotal()));
							series1.setName("Financial Years");
						}
						while(rs.next());
						
						bc.getData().clear();
						bc.getData().add(series1);
					}
				}
				catch(Exception e)
				{
					System.out.println(e);
					e.printStackTrace();
				}
			
				finally
				{
					dbcon.disconnect(null, st, rs, con);
				}
		}
		
		
		//------------------------------------------------ Method for Pie Chart data ---------------------------------------
		
	public void showPie(String sql) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		ArrayList<Top10CityBean> list=new ArrayList<Top10CityBean>();
			try
			{
				pcdata.clear();
				st=con.createStatement();
				String finalsql=null;
				
				/*finalsql="SELECT to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY'),sum(dbt.total) as total from dailybookingtransaction as dbt,"
						+ " city as ct, clientmaster as cm where ct.code=dbt.dailybookingtransaction2city and dbt.dailybookingtransaction2client=cm.code "+sql+" "
								+ "Group  by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY') "
								+ "order by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY')";*/
				
				finalsql="SELECT to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY'),sum(dbt.total) as total from "
						+ "dailybookingtransaction as dbt,pincode_master as pm, clientmaster as cm where pm.city_name=dbt.dailybookingtransaction2city "
						+ "and dbt.dailybookingtransaction2client=cm.code "+sql+" Group by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY') "
								+ "order by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY')";
				
				System.out.println("Form Pie Chart: "+finalsql);
				rs=st.executeQuery(finalsql);
				if(!rs.next())
				{
					tabledata.clear();
					btnexcel.setVisible(false);
					btnExportPDF.setVisible(false);
				}
				else
				{
					btnexcel.setVisible(true);
					btnExportPDF.setVisible(true);
					do
					{
						Top10CityBean citybean=new Top10CityBean();
						citybean.setBookingdate(rs.getString(1));
						int date=((Integer.parseInt(citybean.getBookingdate()))-2000)+1;
						citybean.setBookingdate(rs.getString(1)+"-"+date);
						citybean.setTotal(Double.parseDouble(df.format(rs.getDouble(2))));
						
						
						pievalue=pievalue+citybean.getTotal();
						totalpievalue=totalpievalue+citybean.getTotal();
					
					list.add(citybean);
					}
					while(rs.next());
					
					for(Top10CityBean bean: list)
					{	
						pcdata.add(new PieChart.Data(bean.getBookingdate()+" ("+String.valueOf(df.format((bean.getTotal()/pievalue)*100) + "%)"), bean.getTotal()));
						
					}
					
					
					pc.setData(pcdata);
				}
				
			}
			catch(Exception e)
			{
				 System.out.println(e);
				 e.printStackTrace();
			}	
			finally
			{
				dbcon.disconnect(null, st, rs, con);
				pievalue=0.0;
			}
	}
	
	
	//--------------------------------------------------------------------------
	
	
			@FXML
			public void exportToPDF() throws FileNotFoundException, InterruptedException, DocumentException
			{
			
				Document my_pdf_report = new Document();
				 
				FileChooser fc = new FileChooser();
				fc.getExtensionFilters().addAll(new ExtensionFilter("PDF Files", "*.pdf"));
				File file = fc.showSaveDialog(null);
				PdfWriter.getInstance(my_pdf_report, new FileOutputStream(file.getAbsolutePath()));
				my_pdf_report.open();
					
		         //we have four columns in our table
		        PdfPTable my_report_table = new PdfPTable(13);

		        my_report_table.setWidthPercentage(100);
		        my_report_table.setHorizontalAlignment(Element.ALIGN_LEFT);
		        
		        Font headingfont = new Font(Font.FontFamily.UNDEFINED, 6, Font.BOLD);
		        Font tfont = new Font(Font.FontFamily.UNDEFINED, 6, Font.NORMAL);
		       
		      //create a cell object
		       
		        PdfPCell table_cell = null;
		      
		      table_cell=new PdfPCell(new Phrase("Sr_No.",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		      table_cell=new PdfPCell(new Phrase("Fyncl Yr",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		      table_cell=new PdfPCell(new Phrase("City",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		      table_cell=new PdfPCell(new Phrase("PCS",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		      table_cell=new PdfPCell(new Phrase("Weight",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		      table_cell=new PdfPCell(new Phrase("Ins Amt",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		      
		      table_cell=new PdfPCell(new Phrase("COD",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		      table_cell=new PdfPCell(new Phrase("FOD",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		    
		      table_cell=new PdfPCell(new Phrase("VAS",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		    
		      table_cell=new PdfPCell(new Phrase("Amount",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		     table_cell=new PdfPCell(new Phrase("Fuel",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		         table_cell=new PdfPCell(new Phrase("Service Tax",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		      table_cell=new PdfPCell(new Phrase("Total",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		  
		      
		         
		         /*my_report_table.addCell(new PdfPCell(new Phrase("Serial No",tfont)));
		         my_report_table.addCell(new Phrase("Booking Date",tfont));
		         my_report_table.addCell(new Phrase("Srvc Grp",tfont));
		       */
		      
		         for(Top10CityJavaFXBean fxBean: tabledata)
					{
		        	  
					/* my_report_table.addCell(new PdfPCell(new Phrase(String.valueOf(fxBean.getSerialno()),tfont)));
		        	 my_report_table.addCell(new PdfPCell(new Phrase(fxBean.getBookingdate(),tfont)));
		        	 my_report_table.addCell(new PdfPCell(new Phrase(String.valueOf(fxBean.getPackets()),tfont)));
		        	 my_report_table.addCell(new PdfPCell(new Phrase(String.valueOf(fxBean.getBillingweight()),tfont)));
		        	*/

		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getSerialno()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		             
		            table_cell=new PdfPCell(new Phrase(fxBean.getBookingdate(),tfont));
		            table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(fxBean.getCity(),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getPackets()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getBillingweight()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getInsuracneamount()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getCod()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getFod()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getOtherVas()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		            table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getAmount()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		         	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getFuel()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		         	
		             table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getServicetax()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		         	
		            table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getTotal()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		        	 
		            table_cell.setBorder(0);
				
					}
		     
		         
		         	my_pdf_report.add(my_report_table); 
		           
		         System.out.println("Data export to pdf successfully complete...");
		         my_pdf_report.close();
					
			}
	
	//------------------------------------------------------------------------------
		
	@FXML
	public void exportToExcel() throws SQLException, IOException
	{
			
		HSSFWorkbook workbook=new HSSFWorkbook();
		HSSFSheet sheet=workbook.createSheet("Sales Data");
		HSSFRow rowhead=sheet.createRow(0);
		
		rowhead.createCell(0).setCellValue("Serial No");
		rowhead.createCell(1).setCellValue("AWB_No");
		rowhead.createCell(2).setCellValue("Client Name");
		rowhead.createCell(3).setCellValue("Booking Date");
		
		rowhead.createCell(4).setCellValue("Branch");
		rowhead.createCell(5).setCellValue("Client Code");
		rowhead.createCell(6).setCellValue("City");
		rowhead.createCell(7).setCellValue("Network");
		
		rowhead.createCell(8).setCellValue("Service");
		rowhead.createCell(9).setCellValue("Dox Non-Dox");
		rowhead.createCell(10).setCellValue("Packets");
		rowhead.createCell(11).setCellValue("Billing Weight");
		
		rowhead.createCell(12).setCellValue("Insurance Amount");
		rowhead.createCell(13).setCellValue("Shiper Cost");
		rowhead.createCell(14).setCellValue("COD Amount");
		rowhead.createCell(15).setCellValue("FOD Amount");
		
		rowhead.createCell(16).setCellValue("Docket Charge");
		rowhead.createCell(17).setCellValue("VAS");
		rowhead.createCell(18).setCellValue("Fuel");
		rowhead.createCell(19).setCellValue("Amount");
		
		rowhead.createCell(20).setCellValue("Service Tax");
		rowhead.createCell(21).setCellValue("Total");
		rowhead.createCell(22).setCellValue("Percentage");
		rowhead.createCell(23).setCellValue("Profit/Loss");
		rowhead.createCell(24).setCellValue("Profit/Loss Percentage");
		
		
			int i=2;
			
			HSSFRow row;
			
			for(Top10CityJavaFXBean fxBean: tabledata)
			{
				row = sheet.createRow((short) i);
			
				row.createCell(0).setCellValue(fxBean.getSerialno());
				row.createCell(1).setCellValue(fxBean.getAwb_no());
				row.createCell(2).setCellValue(fxBean.getDbtclient());
				row.createCell(3).setCellValue(fxBean.getBookingdate());
				
				row.createCell(4).setCellValue(fxBean.getBranch());
				row.createCell(5).setCellValue(fxBean.getClientcode());
				row.createCell(6).setCellValue(fxBean.getCity());
				row.createCell(7).setCellValue(fxBean.getNetwork());
				
				row.createCell(8).setCellValue(fxBean.getService());
				row.createCell(9).setCellValue(fxBean.getDoxnonDox());
				row.createCell(10).setCellValue(fxBean.getPackets());
				row.createCell(11).setCellValue(fxBean.getBillingweight());
				
				row.createCell(12).setCellValue(fxBean.getInsuracneamount());
				row.createCell(13).setCellValue(fxBean.getShipercost());
				row.createCell(14).setCellValue(fxBean.getCod());
				row.createCell(15).setCellValue(fxBean.getFod());
			
				row.createCell(16).setCellValue(fxBean.getDocketcharge());
				row.createCell(17).setCellValue(fxBean.getOtherVas());
				row.createCell(18).setCellValue(fxBean.getFuel());
				row.createCell(19).setCellValue(fxBean.getAmount());
			
				row.createCell(20).setCellValue(fxBean.getServicetax());
				row.createCell(21).setCellValue(fxBean.getTotal());
				row.createCell(22).setCellValue(fxBean.getPercentage());
				row.createCell(23).setCellValue(fxBean.getProfitloss());
				row.createCell(24).setCellValue(fxBean.getProfitlosspercentage());
				
				i++;
			}
			
			FileChooser fc = new FileChooser();
			fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files","*.xls"));
			File file = fc.showSaveDialog(null);
			FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
			System.out.println("file chooser: "+file.getAbsoluteFile());
			
		    workbook.write(fileOut);
		    fileOut.close();
			
			System.out.println("Top10CityYearlyComparisonReport.xls written successfully");
	      
	}	
		
		
		
	public void setFilterForTable(String datesql) throws SQLException, IOException
	{
		
		prop.load(in);
		
		String sql="";
		String branchsql="";
		String typesql="";
		String clientsql="";
		String dndsql="";
		String citysql="";

		//String dateRange="and dbt.booking_date between '"+ stdate+"' AND '"+edate+"'";
		
		
		if(!comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH))
		{
			branchsql="and dbt.dailybookingtransaction2branch='"+comboBoxBranch.getValue()+"' ";
		}
	
		
		if(!comboBoxType.getValue().equals(FilterUtils.TYPE1))
		{
			typesql="and cm.client_type='"+prop.getProperty(comboBoxType.getValue())+"' ";
		}
		
		
		if(!comboBoxClient.getValue().equals(FilterUtils.CLIENT))
		{
			clientsql="and dbt.dailybookingtransaction2client='"+comboBoxClient.getValue()+"' ";
		}
		
		
		if(!comboBoxDND.getValue().equals(FilterUtils.DND))
		{
			dndsql="and dbt.dox_nondox='"+comboBoxDND.getValue()+"' ";
		}
	
		
		if(!comboBoxCity.getValue().equals(FilterUtils.ALLCITY))
		{
			citysql="and ct.city_name='"+comboBoxCity.getValue()+"' ";
		}
		
		
		sql=branchsql+""+typesql+""+clientsql+""+dndsql+""+citysql+" and "+datesql;
		
		System.out.println(" Filter sql: ====>> "+sql);
	
		
		showPie(sql);
		showGraph(sql);
		simpleTable(sql);
		
		/*if(chkShowGraphData.isSelected()==true)
		{
			showDetailTable(sql);
			showGraph(sql);
			showPie(sql);
		}
		else
		{
			showPie(sql);
			showGraph(sql);
			simpleTable(sql);
		}*/
	
	}
	

	//------------------------------------------------ Method for Show Table data ---------------------------------------
		
	public void showDetailTable(String sql) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Top10CityBean citybean=new Top10CityBean();
		
		// Normal java bean class
		
		ResultSet rs=null;
		Statement st=null;
		
		/*awb_no.setVisible(true);
		clientcode.setVisible(true);
		dbtclient.setVisible(true);
		//branch.setVisible(true);
		city.setVisible(true);
		shipercost.setVisible(true);
		network.setVisible(true);
		service.setVisible(true);
		doxnonDox.setVisible(true);
		docketcharge.setVisible(true);
		amount.setVisible(false);*/
		
		//bookingdate.setText("Booking Date");
	
		try{
			tabledata.clear();
			st=con.createStatement();
			String finalsql="select dbt.booking_date,dbt.air_way_bill_number,dbt.dailybookingtransaction2client,dbt.dailybookingtransaction2branch,dbt.sub_client_code,ct.city_name,"
					+ "dbt.dailybookingtransaction2network,dbt.dailybookingtransaction2service,dbt.dox_nondox,sum(dbt.packets) as pcs,sum(dbt.billing_weight) as weight,"
					+ "sum(dbt.insurance_amount) as insurance_amount,sum(dbt.shiper_cost) as shiper_cost,sum(dbt.cod_amount) as cod_amount,sum(dbt.fod_amount) as fod_amount,"
					+ "sum(dbt.docket_charge) as docket_charge,sum(dbt.vas_amount) as vas_amount,sum(dbt.fuel) as fuel,sum(dbt.service_tax+dbt.cess1+dbt.cess2),"
					+ "sum(dbt.total) as total from dailybookingtransaction as dbt ,city as ct, clientmaster as cm where dbt.dailybookingtransaction2city=ct.code and dbt.dailybookingtransaction2client=cm.code "+sql+" GROUP by "
							+ "dbt.dailybookingtransaction2client,dbt.booking_date,dbt.air_way_bill_number,dbt.dailybookingtransaction2branch,dbt.sub_client_code,"
							+ "dbt.dailybookingtransaction2network,dbt.dailybookingtransaction2service,dbt.dox_nondox,ct.city_name order by dbt.booking_date";
			
			System.out.println("Table: "+finalsql);
			rs=st.executeQuery(finalsql);
			
			int serialno=1;
		
			if(!rs.next())
			{
				
				tabledata.clear();
			
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Database Alert");
				alert.setHeaderText(null);
				alert.setContentText("Table Data not found");
				alert.showAndWait();
				
			}
			else
			{
			
			do
			{
				citybean.setSerialno(serialno);
				citybean.setAwb_no(rs.getString("air_way_bill_number"));
				citybean.setDbtClient(rs.getString("dailybookingtransaction2client"));
				
				citybean.setBookingdate(date.format(rs.getDate("booking_date")));
				citybean.setBranch(rs.getString("dailybookingtransaction2branch"));
				citybean.setClientcode(rs.getString("sub_client_code"));
				
				citybean.setCity(rs.getString("city_name"));
				citybean.setNetwork(rs.getString("dailybookingtransaction2network"));
				citybean.setService(rs.getString("dailybookingtransaction2service"));
				
				citybean.setDoxnonDox(rs.getString("dox_nondox"));
				citybean.setPackets(rs.getInt("pcs"));
				citybean.setBillingweight(Double.parseDouble(dfWeight.format(rs.getDouble("weight"))));
				
				citybean.setInsuracneamount(Double.parseDouble(df.format(rs.getDouble("insurance_amount"))));
				citybean.setShipercost(Double.parseDouble(df.format(rs.getDouble("shiper_cost"))));
				citybean.setCod(Double.parseDouble(df.format(rs.getDouble("cod_amount"))));
				
				citybean.setFod(Double.parseDouble(df.format(rs.getDouble("fod_amount"))));
				citybean.setDocketcharge(Double.parseDouble(df.format(rs.getDouble("docket_charge"))));
				citybean.setOtherVas(Double.parseDouble(df.format(rs.getDouble("vas_amount"))));
				
				citybean.setFuel(Double.parseDouble(df.format(rs.getDouble("fuel"))));
				citybean.setServicetax(Double.parseDouble(df.format(rs.getDouble(19))));  // here, service tax contain the value of "sum(dbt.service_tax+dbt.cess1+dbt.cess2)"
				citybean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
				
				tabledata.add(new Top10CityJavaFXBean(citybean.getSerialno(),citybean.getAwb_no(),citybean.getDbtClient(),citybean.getBookingdate(),citybean.getBranch(),citybean.getClientcode(),
								citybean.getCity(),citybean.getNetwork(),citybean.getService(),citybean.getDoxnonDox(),citybean.getPackets(),
									citybean.getBillingweight(), citybean.getInsuracneamount(), citybean.getShipercost(), citybean.getCod(),citybean.getFod(),
										citybean.getDocketcharge(), citybean.getOtherVas(),0,citybean.getFuel(), citybean.getServicetax(), citybean.getTotal(),null,null,null));
				serialno++;
			}
			while (rs.next());
			
			table.setItems(tabledata);
			
			pagination.setVisible(true);
			pagination.setPageCount((tabledata.size() / rowsPerPage() + 1));
			pagination.setCurrentPageIndex(0);
			pagination.setPageFactory((Integer pageIndex) -> createPage(pageIndex));
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
		
	}
	
	//--------------------------------- Code for pagination ----------------------------------------
	
	public int itemsPerPage()
	{
		return 1;
	}

	public int rowsPerPage() 
	{
		return 12;
	}

	public GridPane createPage(int pageIndex)
	{
		int lastIndex = 0;
	
		GridPane pane=new GridPane();
		int displace = tabledata.size() % rowsPerPage();
            
			if (displace > 0)
			{
				lastIndex = tabledata.size() / rowsPerPage();
			}
			else
			{
				lastIndex = tabledata.size() / rowsPerPage() -1;
				System.out.println("Pagination lastindex from else condition: "+lastIndex);
			}
	  
	        
			int page = pageIndex * itemsPerPage();
	        for (int i = page; i < page + itemsPerPage(); i++)
	        {
	            if (lastIndex == pageIndex)
	            {
	                table.setItems(FXCollections.observableArrayList(tabledata.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
	            }
	            else
	            {
	                table.setItems(FXCollections.observableArrayList(tabledata.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
	            }
	        }
	       return pane;
	      
	    }
	
	
	@FXML
	public void resetData() throws IOException
	{
		Main m=new Main();
		m.compareTopCity();
			
	}
	
	
	@FXML
	public void close() throws IOException, SQLException
	{
		Main m=new Main();
		m.homePage();
	}
	
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		comboBoxBranch.setValue(FilterUtils.ALLBRANCH);
		comboBoxType.setValue(FilterUtils.TYPE1);
		
		comboBoxDND.setValue(FilterUtils.DND);
		
	try {
		loadBranch();
		loadAllCity();
		loadDND();
		setClinetData();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		System.out.println(e);
		e.printStackTrace();
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
		comboBoxType.setItems(comboBoxTypeItems);
		comboBoxClient.setItems(comboBoxClientItems);
		
		comboBoxClient.setValue(FilterUtils.CLIENT);
		
		comboBoxFromYear.setItems(comboBoxFromItems);
		comboBoxToYear.setItems(comboBoxToItems);
		
		comboBoxFromYear.setValue("2012-2013");
		comboBoxToYear.setValue(FilterUtils.YEAR1);
		comboBoxCity.setValue(FilterUtils.ALLCITY);
		
		btnexcel.setVisible(false);
		btnExportPDF.setVisible(false);
		
		serialno.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,Integer>("serialno"));
		awb_no.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,String>("awb_no"));
		dbtclient.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,String>("dbtclient"));
		bookingdate.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,String>("bookingdate"));
		branch.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,String>("branch"));
		clientcode.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,String>("clientcode"));
		city.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,String>("city"));
		network.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,String>("network"));
		service.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,String>("service"));
		doxnonDox.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,String>("doxnonDox"));
		percentage.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,String>("percentage"));
		
		profitlosspercentage.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,String>("profitlosspercentage"));
		profitloss.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,String>("profitloss"));
		
		packets.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,Integer>("packets"));
		
		billingweight.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,Double>("billingweight"));
		insuracneamount.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,Double>("insuracneamount"));
		shipercost.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,Double>("shipercost"));
		cod.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,Double>("cod"));
		fod.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,Double>("fod"));
		docketcharge.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,Double>("docketcharge"));
		otherVas.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,Double>("otherVas"));
		amount.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,Double>("amount"));
		fuel.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,Double>("fuel"));
		servicetax.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,Double>("servicetax"));
		total.setCellValueFactory(new PropertyValueFactory<Top10CityJavaFXBean,Double>("total"));
		//btnGenerate.setDisable(true);
		pagination.setVisible(false);
		
	}

}