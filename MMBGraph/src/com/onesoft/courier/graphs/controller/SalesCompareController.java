package com.onesoft.courier.graphs.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import com.DB.DBconnection;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.graphs.beans.SalesJavaFXBean;
import com.onesoft.courier.graphs.beans.SalesBean;
import com.onesoft.courier.graphs.utils.FilterUtils;
import com.onesoft.courier.graphs.utils.SalesReportUtils;
import com.onesoft.courier.main.Main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class SalesCompareController implements Initializable
{
	
	private ObservableList<SalesJavaFXBean> tabledata=FXCollections.observableArrayList();
	
	private ObservableList<PieChart.Data> pcdata=FXCollections.observableArrayList();
	
	private ObservableList<String> comboBoxTypeItems=FXCollections.observableArrayList(FilterUtils.TYPE1,FilterUtils.TYPE2,FilterUtils.TYPE3);

	private ObservableList<String> comboBoxFromItems=FXCollections.observableArrayList(FilterUtils.YEAR5,FilterUtils.YEAR4,
																						FilterUtils.YEAR3,FilterUtils.YEAR2,FilterUtils.YEAR1);
	
	private ObservableList<String> comboBoxToItems=FXCollections.observableArrayList(FilterUtils.YEAR1,FilterUtils.YEAR2,
																				FilterUtils.YEAR3,FilterUtils.YEAR4,FilterUtils.YEAR5);

	
	
	public static String configfilePath = "typeproperties.properties";
	Properties prop = new Properties();
	InputStream in = this.getClass().getClassLoader().getResourceAsStream(configfilePath);
	
	DecimalFormat df=new DecimalFormat(".##");
	DecimalFormat dfWeight=new DecimalFormat(".###");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	
	double pievalue=0.0;
	double totalpievalue=0.0;
	
	@FXML
	private NumberAxis yAxis;
	
	@FXML
	private CategoryAxis xAxis;
	
	@FXML
	private BarChart<?,?> bc;
	
	@FXML
	private PieChart pc;
	
	@FXML
	private Button btnGenerate;
	
	@FXML
	private VBox vbox;
	
	@FXML
	private Button btnexcel;
	
	@FXML
	private Button btnExportPDF;
	
	@FXML
	private Button btnreset;
	
	@FXML
	private Button btnClose;
	
	@FXML
	private Pagination pagination;
	
	@FXML
	private CheckBox chkShowGraphData;
	
	@FXML
	private ComboBox<String> comboBoxBranch;
	
	@FXML
	private ComboBox<String> comboBoxType;
	
	@FXML
	private ComboBox<String> comboBoxFromYear;
	
	@FXML
	private ComboBox<String> comboBoxToYear;
	
	/*@FXML
	private ComboBox<String> comboBoxSubCategory;*/
	
	/*@FXML
	private ComboBox<String> comboBoxQuarters;*/
	
	
	@FXML
	private TableView<SalesJavaFXBean> table;
	
	@FXML
	private TableColumn<SalesJavaFXBean, Integer> serialno;
	
	@FXML
	private TableColumn<SalesJavaFXBean, String> awb_no;
	
	@FXML
	private TableColumn<SalesJavaFXBean, String> dbtclient;
	
	@FXML
	private TableColumn<SalesJavaFXBean, String> bookingdate;
	
	@FXML
	private TableColumn<SalesJavaFXBean, String> branch;
	
	@FXML
	private TableColumn<SalesJavaFXBean, String> clientcode;
	
	@FXML
	private TableColumn<SalesJavaFXBean, String> city;
	
	@FXML
	private TableColumn<SalesJavaFXBean, String> network;
	
	@FXML
	private TableColumn<SalesJavaFXBean, String> service;
	
	@FXML
	private TableColumn<SalesJavaFXBean, String> doxnonDox;
	
	@FXML
	private TableColumn<SalesJavaFXBean, String> percentage;
	
	@FXML
	private TableColumn<SalesJavaFXBean, Integer> packets;
	
	@FXML
	private TableColumn<SalesJavaFXBean, Double> billingweight;
	
	@FXML
	private TableColumn<SalesJavaFXBean, Double> insuracneamount;
	 
	@FXML
	private TableColumn<SalesJavaFXBean, Double> shipercost;
	
	@FXML
	private TableColumn<SalesJavaFXBean, Double> cod;
	
	@FXML
	private TableColumn<SalesJavaFXBean, Double> fod;
	
	@FXML
	private TableColumn<SalesJavaFXBean, Double> docketcharge;
	
	@FXML
	private TableColumn<SalesJavaFXBean, Double> otherVas;
	
	@FXML
	private TableColumn<SalesJavaFXBean, Double> amount;
	
	@FXML
	private TableColumn<SalesJavaFXBean, Double> fuel;
	
	@FXML
	private TableColumn<SalesJavaFXBean, Double> servicetax;
	
	@FXML
	private TableColumn<SalesJavaFXBean, Double> total;
	
	@FXML
	private TableColumn<SalesJavaFXBean, String> profitlosspercentage;
	
	@FXML
	private TableColumn<SalesJavaFXBean, String> profitloss;
	
	
	private GridPane  pane;
	
	int lastIndex = 0;
	
	ObservableList<String> comboBoxBranchItems=FXCollections.observableArrayList("All Branches");
	
	
	public void loadBranch() throws SQLException
	{
		
		new	LoadBranch().loadBranch();
		
		System.out.println("Sales Compare Contorller >> LoadBranch from LoadBranch Class >>>>>>>> ");
		
		//System.out.println("Network Contorller >> Load from Global set object");
		for(String s:LoadBranch.SETLOADBRANCHFORALL)
		{
			comboBoxBranchItems.add(s);
		}
		comboBoxBranch.setItems(comboBoxBranchItems);
		
		/*DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		//BookingPOJO salesbean=new BookingPOJO();
		
		
		Set<String> set = new HashSet<String>();
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select code from brndetail";
			rs=st.executeQuery(sql);
	
			while(rs.next())
			{
				set.add(rs.getString(1));
			}
		
			for(String s:set)
			{
				comboBoxBranchItems.add(s);
			}
			comboBoxBranch.setItems(comboBoxBranchItems);
					
			
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}*/
	}
	
	

	
	
	@FXML
	public void resetData() throws IOException
	{
		
		Main m=new Main();
		m.compareSales();
	
		
	}
	

	
	
	
	public void simpleTable(String sql) throws SQLException
	{
		tabledata.clear();
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		SalesBean salesbean=new SalesBean(); // Normal java bean class
		
		String finalsql=null;
		ResultSet rs=null;
		Statement st=null;
		
		try{
			awb_no.setVisible(false);
			//bookingdate.setVisible(false);
			clientcode.setVisible(false);
			dbtclient.setVisible(false);
			branch.setVisible(true);
			city.setVisible(false);
			shipercost.setVisible(false);
			network.setVisible(false);
			service.setVisible(false);
			doxnonDox.setVisible(false);
			docketcharge.setVisible(false);
			amount.setVisible(true);
			pagination.setVisible(false);
			percentage.setVisible(true);
			
			//bookingdate.setText("Months");
			
			
			if(!comboBoxBranch.getValue().equals("All Branches"))
			{
				finalsql="select to_char(date_trunc('month',booking_date) - interval '3' month, 'YYYY'),dailybookingtransaction2branch,sum(dbt.packets) as pcs,"
						+ "sum(dbt.billing_weight) as weight,sum(dbt.insurance_amount) as insuracne,sum(dbt.cod_amount) as cod,sum(dbt.fod_amount) as fod ,"
						+ "sum(dbt.vas_amount) as vas,sum(dbt.amount) as amount,sum(dbt.fuel) as fuel,sum(dbt.tax_amount1+dbt.tax_amount2+dbt.tax_amount3) as tax,sum(dbt.total) as total "
						+ "from dailybookingtransaction as dbt, clientmaster as cm  where dbt.dailybookingtransaction2client=cm.code "+sql+" group by to_char(date_trunc('month',booking_date) - interval '3' month, 'YYYY'),"
								+ "dailybookingtransaction2branch ORDER BY to_char(date_trunc('month',booking_date) - interval '3' month, 'YYYY')";
			}
			else
			{
				finalsql="select to_char(date_trunc('month',booking_date) - interval '3' month, 'YYYY'),sum(dbt.packets) as pcs,"
						+ "sum(dbt.billing_weight) as weight,sum(dbt.insurance_amount) as insuracne,sum(dbt.cod_amount) as cod,sum(dbt.fod_amount) as fod ,"
						+ "sum(dbt.vas_amount) as vas,sum(dbt.amount) as amount,sum(dbt.fuel) as fuel,sum(dbt.tax_amount1+dbt.tax_amount2+dbt.tax_amount3) as tax,sum(dbt.total) as total "
						+ "from dailybookingtransaction as dbt, clientmaster as cm  where dbt.dailybookingtransaction2client=cm.code "+sql+" group by to_char(date_trunc('month',booking_date) - interval '3' month, 'YYYY')"
								+ " ORDER BY to_char(date_trunc('month',booking_date) - interval '3' month, 'YYYY')";
			}
			
			
			/*String sql="select to_char(dbt.booking_date,'Mon,YY'),sum(dbt.packets) as pcs,sum(dbt.billing_weight) as weight,sum(dbt.insurance_amount) as insuracne,"
					+ "sum(dbt.cod_amount) as cod,sum(dbt.fod_amount) as fod ,sum(dbt.vas_amount) as vas,sum(dbt.amount) as amount,sum(dbt.fuel) as fuel,"
					+ "sum(dbt.service_tax+dbt.cess1+dbt.cess2) as tax,sum(dbt.total) as total from dailybookingtransaction as dbt "
					+ " where "+s+" group by to_char(dbt.booking_date,'Mon,YY') ORDER BY to_date(to_char(dbt.booking_date,'Mon,YY'),'Mon,YY')";*/
			
			System.out.println("Sql from Simple table: "+finalsql);
			st=con.createStatement();
			rs=st.executeQuery(finalsql);
			int serialno=1;
			double per=0.0;
			int financialyr=0;
			
			salesbean.setTotal(0.0);
			
			while (rs.next())
			{
			
				double profitloss=Double.parseDouble(df.format(rs.getDouble("total")-salesbean.getTotal()));
				double profitlosspercentage=(profitloss/salesbean.getTotal())*100;
				
				if(Double.isInfinite(profitlosspercentage))
				{
					salesbean.setProfitloss("--");
					salesbean.setProfitlosspercentage("--");
				}
				else if(profitlosspercentage>0)
				{
					salesbean.setProfitloss(profitloss+"");
					salesbean.setProfitlosspercentage(df.format(profitlosspercentage)+"%");
				}
				else
				{
					salesbean.setProfitloss(profitloss+"");
					salesbean.setProfitlosspercentage(df.format(profitlosspercentage)+"%");
				}
				
				
				if(!comboBoxBranch.getValue().equals("All Branches"))
				{
				
				salesbean.setSerialno(serialno);
				salesbean.setBookingdate(rs.getString(1));
				financialyr=((Integer.parseInt(salesbean.getBookingdate()))-2000)+1;
				salesbean.setBranch(rs.getString(2));
				salesbean.setPackets(rs.getInt(3));
				salesbean.setBillingweight(Double.parseDouble(df.format(rs.getDouble(4))));
				salesbean.setInsuracneamount(Double.parseDouble(df.format(rs.getDouble(5))));
				salesbean.setCod(Double.parseDouble(df.format(rs.getDouble(6))));
				salesbean.setFod(Double.parseDouble(df.format(rs.getDouble(7))));
				salesbean.setOtherVas(Double.parseDouble(df.format(rs.getDouble(8))));
				salesbean.setAmount(Double.parseDouble(df.format(rs.getDouble(9))));
				salesbean.setFuel(Double.parseDouble(df.format(rs.getDouble(10))));
				salesbean.setServicetax(Double.parseDouble(df.format(rs.getDouble(11))));
				salesbean.setTotal(Double.parseDouble(df.format(rs.getDouble(12))));
				
				}
				else
				{
					
					salesbean.setSerialno(serialno);
					salesbean.setBookingdate(rs.getString(1));
					financialyr=((Integer.parseInt(salesbean.getBookingdate()))-2000)+1;
					salesbean.setBranch("All Branches");
					salesbean.setPackets(rs.getInt(2));
					salesbean.setBillingweight(Double.parseDouble(df.format(rs.getDouble(3))));
					salesbean.setInsuracneamount(Double.parseDouble(df.format(rs.getDouble(4))));
					salesbean.setCod(Double.parseDouble(df.format(rs.getDouble(5))));
					salesbean.setFod(Double.parseDouble(df.format(rs.getDouble(6))));
					salesbean.setOtherVas(Double.parseDouble(df.format(rs.getDouble(7))));
					salesbean.setAmount(Double.parseDouble(df.format(rs.getDouble(8))));
					salesbean.setFuel(Double.parseDouble(df.format(rs.getDouble(9))));
					salesbean.setServicetax(Double.parseDouble(df.format(rs.getDouble(10))));
					salesbean.setTotal(Double.parseDouble(df.format(rs.getDouble(11))));
				}
				per=Double.parseDouble(df.format((salesbean.getTotal()/totalpievalue)*100));
			
				
				tabledata.add(new SalesJavaFXBean(salesbean.getSerialno(),null,null,salesbean.getBookingdate()+"-"+financialyr,salesbean.getBranch(),null,null,null,null,null,
						salesbean.getPackets(),salesbean.getBillingweight(), salesbean.getInsuracneamount(), 0, salesbean.getCod(),salesbean.getFod(),0,
						salesbean.getOtherVas(),salesbean.getAmount(),salesbean.getFuel(), salesbean.getServicetax(), salesbean.getTotal(),per+"%",salesbean.getProfitloss(),salesbean.getProfitlosspercentage()));
				
				serialno++;
			}
			
			table.setItems(tabledata);
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{
			dbcon.disconnect(null, st, rs, con);
			totalpievalue=0.0;
		}
	}

	
	// ------------------------------------------------ Method for generate button ---------------------------------------
	
	@FXML
	public void loaddata() throws SQLException, ParseException, IOException
	{
		
		pievalue=0.0;
		totalpievalue=0.0;
		setYear();
		
		
	}
	
	
	public void setYear() throws SQLException, IOException
	{
		String[] from=comboBoxFromYear.getValue().split("-");
		String[] to=comboBoxToYear.getValue().split("-");
		
		String firstyear=from[0];
		String secondyear=to[1];
		
		int fst=Integer.parseInt(firstyear);
		int scnd=Integer.parseInt(secondyear);
		
		if(fst<scnd)
		{	
			System.out.println("First year: "+firstyear);
			System.out.println("Second year: "+secondyear);
			String datesql="booking_date between '"+firstyear+"-04-01' AND '"+secondyear+"-03-31'";
			System.out.println("financial date filter: "+datesql);
			
			setFilterForTable(datesql);
				
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Wrong Years selected");
			alert.setHeaderText(null);
			alert.setContentText("Please select a valid From and To year...");
			alert.showAndWait();
		}
		
	}
	

	//------------------------------------------------ Method for Set StartDate & EndDate data ---------------------------------------
	
	
	
	//---------------------------------------- Code for pagination ---------------------------------------------------------------------------
	
	public int itemsPerPage()
	{
		return 1;
	}

	public int rowsPerPage() 
	{
		return 12;
	}

	public GridPane createPage(int pageIndex)
	{
		int lastIndex = 0;
	
		GridPane pane=new GridPane();
		int displace = tabledata.size() % rowsPerPage();
            
			if (displace >= 0)
			{
				lastIndex = tabledata.size() / rowsPerPage();
			}
			/*else
			{
				lastIndex = tabledata.size() / rowsPerPage() -1;
				System.out.println("Pagination lastindex from else condition: "+lastIndex);
			}*/
	  
	        
			int page = pageIndex * itemsPerPage();
	        for (int i = page; i < page + itemsPerPage(); i++)
	        {
	            if (lastIndex == pageIndex)
	            {
	                table.setItems(FXCollections.observableArrayList(tabledata.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
	            }
	            else
	            {
	                table.setItems(FXCollections.observableArrayList(tabledata.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
	            }
	        }
	       return pane;
	      
	    }
	
	

	// ------------------------------------------------ Method for Bar Chart data ---------------------------------------
	
	public void showGraph(String sql) throws SQLException
	{
		XYChart.Series series1 = new XYChart.Series();
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		SalesBean salesbean=new SalesBean();
		
			try
			{
				st=con.createStatement();
				String finalsql="select to_char(date_trunc('month',booking_date) - interval '3' month, 'YYYY'),sum(total) as total from dailybookingtransaction as dbt, clientmaster as cm "
						+ "where dbt.dailybookingtransaction2client=cm.code "+sql+" group by to_char(date_trunc('month',booking_date) - interval '3' month, 'YYYY')ORDER BY to_char(date_trunc('month',booking_date) - interval '3' month, 'YYYY')";
				System.out.println("Sql from Graph: "+finalsql);
				rs=st.executeQuery(finalsql);
				
				if(!rs.next())
				{
					bc.getData().clear();
					pcdata.clear();
					tabledata.clear();
				
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Database Alert");
					alert.setHeaderText(null);
					alert.setContentText("Data not found");
					alert.showAndWait();
					
				}
				else
				{	
					do
					{	
						salesbean.setBookingdate(rs.getString(1));
						salesbean.setTotal(Double.parseDouble(df.format(rs.getDouble(2))));
						int date=((Integer.parseInt(salesbean.getBookingdate()))-2000)+1;
						
						series1.getData().add(new XYChart.Data(salesbean.getBookingdate()+"-"+date,salesbean.getTotal()));
						series1.setName("Financial Years");
					}
					while(rs.next());
					
					bc.getData().clear();
					bc.getData().add(series1);
				}
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}
		
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
	}
	
	
	//------------------------------------------------ Method for Pie Chart data ---------------------------------------
	
	public void showPie(String sql) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		ArrayList<SalesBean> list=new ArrayList<SalesBean>();
			try
			{
				pcdata.clear();
				st=con.createStatement();
				String finalsql="select to_char(date_trunc('month',booking_date) - interval '3' month, 'YYYY'),sum(total) as total from dailybookingtransaction as dbt, clientmaster as cm "
						+ "where dbt.dailybookingtransaction2client=cm.code "+sql+" group by to_char(date_trunc('month',booking_date) - interval '3' month, 'YYYY')ORDER BY to_char(date_trunc('month',booking_date) - interval '3' month, 'YYYY')";					
			System.out.println("Sql from piechart: "+finalsql);
				rs=st.executeQuery(finalsql);
				if(!rs.next())
				{
					tabledata.clear();
					btnexcel.setVisible(false);
					btnExportPDF.setVisible(false);
				}
				else
				{
					btnexcel.setVisible(true);
					btnExportPDF.setVisible(true);
				do
					{
					SalesBean salesbean=new SalesBean();
						salesbean.setBookingdate(rs.getString(1));
						int date=((Integer.parseInt(salesbean.getBookingdate()))-2000)+1;
						salesbean.setBookingdate(rs.getString(1)+"-"+date);
						salesbean.setTotal(Double.parseDouble(df.format(rs.getDouble(2))));
						
						
						pievalue=pievalue+salesbean.getTotal();
						totalpievalue=totalpievalue+salesbean.getTotal();
					
					list.add(salesbean);
					}
				while(rs.next());
				}
					
					
				for(SalesBean bean: list)
				{	
					pcdata.add(new PieChart.Data(bean.getBookingdate()+" ("+String.valueOf(df.format((bean.getTotal()/pievalue)*100) + "%)"), bean.getTotal()));
					
				}
				
					pc.setData(pcdata);
				//}
				
			}
			catch(Exception e)
			{
				 System.out.println(e);
				 e.printStackTrace();
			}	
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
	
	}
	
	
	//--------------------------------------------------------------------------
	
	
			@FXML
			public void exportToPDF() throws FileNotFoundException, InterruptedException, DocumentException
			{

				Document my_pdf_report = new Document();
				 
				FileChooser fc = new FileChooser();
				fc.getExtensionFilters().addAll(new ExtensionFilter("PDF Files", "*.pdf"));
				File file = fc.showSaveDialog(null);
				PdfWriter.getInstance(my_pdf_report, new FileOutputStream(file.getAbsolutePath()));
				my_pdf_report.open();
				        
		         //we have four columns in our table
		        PdfPTable my_report_table = new PdfPTable(11);
		        
		        my_report_table.setWidthPercentage(100);
		        my_report_table.setHorizontalAlignment(Element.ALIGN_LEFT);
		        
		        Font headingfont = new Font(Font.FontFamily.UNDEFINED, 6, Font.BOLD);
		        Font tfont = new Font(Font.FontFamily.UNDEFINED, 6, Font.NORMAL);
		      
		      //create a cell object
		       
		      PdfPCell table_cell = null;
		      
		      table_cell=new PdfPCell(new Phrase("Sr_No.",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		      table_cell=new PdfPCell(new Phrase("Fyncl Yr",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		      table_cell=new PdfPCell(new Phrase("Branch",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		      table_cell=new PdfPCell(new Phrase("PCS",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		      table_cell=new PdfPCell(new Phrase("Weight",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		      table_cell=new PdfPCell(new Phrase("Ins Amt",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		      
		      table_cell=new PdfPCell(new Phrase("COD",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		      table_cell=new PdfPCell(new Phrase("FOD",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		    
		      table_cell=new PdfPCell(new Phrase("VAS",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		    
		      table_cell=new PdfPCell(new Phrase("Amount",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		/*     table_cell=new PdfPCell(new Phrase("Fuel",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		         table_cell=new PdfPCell(new Phrase("Service Tax",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);*/
		     
		      table_cell=new PdfPCell(new Phrase("Total",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		  
		      
		         
		         /*my_report_table.addCell(new PdfPCell(new Phrase("Serial No",tfont)));
		         my_report_table.addCell(new Phrase("Booking Date",tfont));
		         my_report_table.addCell(new Phrase("Srvc Grp",tfont));
		       */
		      
		         for(SalesJavaFXBean fxBean: tabledata)
					{
		        	  
					/* my_report_table.addCell(new PdfPCell(new Phrase(String.valueOf(fxBean.getSerialno()),tfont)));
		        	 my_report_table.addCell(new PdfPCell(new Phrase(fxBean.getBookingdate(),tfont)));
		        	 my_report_table.addCell(new PdfPCell(new Phrase(String.valueOf(fxBean.getPackets()),tfont)));
		        	 my_report_table.addCell(new PdfPCell(new Phrase(String.valueOf(fxBean.getBillingweight()),tfont)));
		        	*/

		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getSerialno()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		             
		            table_cell=new PdfPCell(new Phrase(fxBean.getBookingdate(),tfont));
		            table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(fxBean.getBranch(),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getPackets()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getBillingweight()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getInsuracneamount()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getCod()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getFod()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getOtherVas()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		            table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getAmount()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		           /* 
		         	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getFuel()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		         	
		             table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getServicetax()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);*/
		         	
		            table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getTotal()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		        	 
		            table_cell.setBorder(0);
				
					}
		     
		         	my_pdf_report.add(my_report_table); 
		           
		         	System.out.println("File "+file.getName()+" successfully saved");
		         my_pdf_report.close();
					
			}
	
	
	//------------------------------------------------ Method for Export to EXCEL Button data ---------------------------------------
	
	@FXML
	public void exportToExcel() throws SQLException, IOException
	{
			
		HSSFWorkbook workbook=new HSSFWorkbook();
		HSSFSheet sheet=workbook.createSheet("Sales Data");
		HSSFRow rowhead=sheet.createRow(0);
		
		rowhead.createCell(0).setCellValue("Serial No");
		rowhead.createCell(1).setCellValue("AWB_No");
		rowhead.createCell(2).setCellValue("Client Name");
		rowhead.createCell(3).setCellValue("Booking Date");
		
		rowhead.createCell(4).setCellValue("Branch");
		rowhead.createCell(5).setCellValue("Client Code");
		rowhead.createCell(6).setCellValue("City");
		rowhead.createCell(7).setCellValue("Network");
		
		rowhead.createCell(8).setCellValue("Service");
		rowhead.createCell(9).setCellValue("Dox Non-Dox");
		rowhead.createCell(10).setCellValue("Packets");
		rowhead.createCell(11).setCellValue("Billing Weight");
		
		rowhead.createCell(12).setCellValue("Insurance Amount");
		rowhead.createCell(13).setCellValue("Shiper Cost");
		rowhead.createCell(14).setCellValue("COD Amount");
		rowhead.createCell(15).setCellValue("FOD Amount");
		
		rowhead.createCell(16).setCellValue("Docket Charge");
		rowhead.createCell(17).setCellValue("VAS");
		rowhead.createCell(18).setCellValue("Fuel");
		rowhead.createCell(19).setCellValue("Amount");
		
		rowhead.createCell(20).setCellValue("Service Tax");
		rowhead.createCell(21).setCellValue("Total");
		rowhead.createCell(22).setCellValue("Percentage");
		rowhead.createCell(23).setCellValue("Profit/Loss");
		rowhead.createCell(24).setCellValue("Profit/Loss Percentage");
		
			int i=2;
			
			HSSFRow row;
			
			for(SalesJavaFXBean fxBean: tabledata)
			{
				row = sheet.createRow((short) i);
			
				row.createCell(0).setCellValue(fxBean.getSerialno());
				row.createCell(1).setCellValue(fxBean.getAwb_no());
				row.createCell(2).setCellValue(fxBean.getDbtclient());
				row.createCell(3).setCellValue(fxBean.getBookingdate());
				
				row.createCell(4).setCellValue(fxBean.getBranch());
				row.createCell(5).setCellValue(fxBean.getClientcode());
				row.createCell(6).setCellValue(fxBean.getCity());
				row.createCell(7).setCellValue(fxBean.getNetwork());
				
				row.createCell(8).setCellValue(fxBean.getService());
				row.createCell(9).setCellValue(fxBean.getDoxnonDox());
				row.createCell(10).setCellValue(fxBean.getPackets());
				row.createCell(11).setCellValue(fxBean.getBillingweight());
				
				row.createCell(12).setCellValue(fxBean.getInsuracneamount());
				row.createCell(13).setCellValue(fxBean.getShipercost());
				row.createCell(14).setCellValue(fxBean.getCod());
				row.createCell(15).setCellValue(fxBean.getFod());
			
				row.createCell(16).setCellValue(fxBean.getDocketcharge());
				row.createCell(17).setCellValue(fxBean.getOtherVas());
				row.createCell(18).setCellValue(fxBean.getFuel());
				row.createCell(19).setCellValue(fxBean.getAmount());
			
				row.createCell(20).setCellValue(fxBean.getServicetax());
				row.createCell(21).setCellValue(fxBean.getTotal());
				row.createCell(22).setCellValue(fxBean.getPercentage());
				row.createCell(23).setCellValue(fxBean.getProfitloss());
				row.createCell(24).setCellValue(fxBean.getProfitlosspercentage());
				
				i++;
			}
			
			FileChooser fc = new FileChooser();
			fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files","*.xls"));
			File file = fc.showSaveDialog(null);
			FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
			System.out.println("file chooser: "+file.getAbsoluteFile());
			
		    workbook.write(fileOut);
		    fileOut.close();
			
		    System.out.println("File "+file.getName()+" successfully saved");
	      
	}
		
	//------------------------------------------------------------------------------------------------------------------
	
	public void setFilterForTable(String datesql) throws SQLException, IOException
	{
		prop.load(in);
		String sql=null;
		String branchsql="";
		String typesql="";
		
		if(!comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH))
		{
			branchsql="and dbt.dailybookingtransaction2branch='"+comboBoxBranch.getValue()+"' ";
		}
		
		if(!comboBoxType.getValue().equals(FilterUtils.TYPE1))
		{
			typesql="and cm.client_type='"+prop.getProperty(comboBoxType.getValue())+"' ";
		}

		System.out.println(" Filter sql 1: ====>> "+sql);
		
		sql=branchsql+""+typesql+" and "+datesql;
		
		System.out.println(" Filter sql 2: ====>> "+sql);
		
		showPie(sql);
		showGraph(sql);
		simpleTable(sql);
		
		/*if(chkShowGraphData.isSelected()==true)
		{
			showDetailTable(sql);
			showGraph(sql);
			showPie(sql);
		}
		else
		{
			showPie(sql);
			showGraph(sql);
			simpleTable(sql);
		}*/
		
	}
	
	
	

	
	
	//------------------------------------------------ Method for Show Table data ---------------------------------------
	
	/*public void showDetailTable(String sql) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		SalesBean salesbean=new SalesBean(); // Normal java bean class
		
		ResultSet rs=null;
		Statement st=null;
		
		//chkShowGraphData.setSelected(false);
		
		awb_no.setVisible(true);
		clientcode.setVisible(true);
		dbtclient.setVisible(true);
		//branch.setVisible(true);
		city.setVisible(true);
		shipercost.setVisible(true);
		network.setVisible(true);
		service.setVisible(true);
		doxnonDox.setVisible(true);
		docketcharge.setVisible(true);
		amount.setVisible(false);
		
		//showBranch();
		bookingdate.setText("Booking Date");
		
		try{
			tabledata.clear();
			st=con.createStatement();
			
			String finalsql="select booking_date,air_way_bill_number,dailybookingtransaction2client,dailybookingtransaction2branch,sub_client_code,dailybookingtransaction2city,"
					+ "cmt.city_name,dailybookingtransaction2network,dailybookingtransaction2service,dox_nondox,sum(packets) as pcs,sum(billing_weight) as weight,"
					+ "sum(insurance_amount) as insurance_amount, sum(shiper_cost) as shiper_cost, sum(cod_amount) as cod_amount, sum(fod_amount) as fod_amount,"
					+ "sum(docket_charge) as docket_charge,sum(vas_amount) as vas_amount, sum(fuel) as fuel,sum(service_tax+cess1+cess2),sum(total) as total from"
					+ " dailybookingtransaction, city as cmt where cmt.code=dailybookingtransaction2city and "+sql+" GROUP by dailybookingtransaction2client,booking_date,air_way_bill_number, dailybookingtransaction2branch,"
				+ "sub_client_code,dailybookingtransaction2city, cmt.city_name,dailybookingtransaction2network,dailybookingtransaction2service,dox_nondox order by booking_date";
			
			System.out.println("Sql from Detail Table: "+finalsql);
			rs=st.executeQuery(finalsql);
			
			int serialno=1;
		
			if(!rs.next())
			{
				
				branch.setVisible(true);
			
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Database Alert");
				alert.setHeaderText(null);
				alert.setContentText("Table Data not found");
				alert.showAndWait();
				
				pc.getData().clear();
				bc.getData().clear();
				pcdata.clear();
				tabledata.clear();
				
			}
			else
			{
				
			do
			{
				
				salesbean.setSerialno(serialno);
				salesbean.setAwb_no(rs.getString("air_way_bill_number"));
				salesbean.setDbtClient(rs.getString("dailybookingtransaction2client"));
				salesbean.setBookingdate(date.format(rs.getDate("booking_date")));
				salesbean.setBranch(rs.getString("dailybookingtransaction2branch"));
				salesbean.setClientcode(rs.getString("sub_client_code"));
				//salesbean.setCity(rs.getString("dailybookingtransaction2city"));
				salesbean.setCity(rs.getString("city_name"));
				
				salesbean.setNetwork(rs.getString("dailybookingtransaction2network"));
				salesbean.setService(rs.getString("dailybookingtransaction2service"));
				salesbean.setDoxnonDox(rs.getString("dox_nondox"));
				salesbean.setPackets(rs.getInt("pcs"));
				salesbean.setBillingweight(Double.parseDouble(dfWeight.format(rs.getDouble("weight"))));
				
				salesbean.setInsuracneamount(Double.parseDouble(df.format(rs.getDouble("insurance_amount"))));
				salesbean.setShipercost(Double.parseDouble(df.format(rs.getDouble("shiper_cost"))));
				salesbean.setCod(Double.parseDouble(df.format(rs.getDouble("cod_amount"))));
				salesbean.setFod(Double.parseDouble(df.format(rs.getDouble("fod_amount"))));
				salesbean.setDocketcharge(Double.parseDouble(df.format(rs.getDouble("docket_charge"))));
				salesbean.setOtherVas(Double.parseDouble(df.format(rs.getDouble("vas_amount"))));
				salesbean.setFuel(Double.parseDouble(df.format(rs.getDouble("fuel"))));
				salesbean.setServicetax(Double.parseDouble(df.format(rs.getDouble(18))));  // here, service tax contain the value of "sum(dbt.service_tax+dbt.cess1+dbt.cess2)"
				salesbean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
				
			
				tabledata.add(new SalesJavaFXBean(salesbean.getSerialno(),salesbean.getAwb_no(),salesbean.getDbtClient(),salesbean.getBookingdate(),salesbean.getBranch(),salesbean.getClientcode(),
								salesbean.getCity(),salesbean.getNetwork(),salesbean.getService(),salesbean.getDoxnonDox(),salesbean.getPackets(),
									salesbean.getBillingweight(), salesbean.getInsuracneamount(), salesbean.getShipercost(), salesbean.getCod(),salesbean.getFod(),
										salesbean.getDocketcharge(), salesbean.getOtherVas(),0,salesbean.getFuel(), salesbean.getServicetax(), salesbean.getTotal(),null));
				serialno++;
			}
			while (rs.next());
			
			table.setItems(tabledata);
			pagination.setVisible(true);
			 
			pagination.setPageCount((tabledata.size() / rowsPerPage() + 1));
			pagination.setCurrentPageIndex(0);
			pagination.setPageFactory((Integer pageIndex) -> createPage(pageIndex));
			}
		}
		
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
		
	}*/

	
	
	@FXML
	public void close() throws IOException, SQLException
	{
		Main m=new Main();
		m.homePage();
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		
		
		try {
			loadBranch();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
	
		comboBoxBranch.setValue(FilterUtils.ALLBRANCH);
		comboBoxType.setValue(FilterUtils.TYPE1);
		comboBoxType.setItems(comboBoxTypeItems);
		
		//comboBoxCategory.setItems(comboBoxCategoryItems);
		comboBoxFromYear.setItems(comboBoxFromItems);
		comboBoxToYear.setItems(comboBoxToItems);
		
		comboBoxFromYear.setValue("2012-2013");
		comboBoxToYear.setValue("2016-2017");
		
		btnexcel.setVisible(false);
		btnExportPDF.setVisible(false);
		
		//comboBoxQuarters.setDisable(true);
		
		serialno.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,Integer>("serialno"));
		awb_no.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,String>("awb_no"));
		dbtclient.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,String>("dbtclient"));
		bookingdate.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,String>("bookingdate"));
		branch.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,String>("branch"));
		clientcode.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,String>("clientcode"));
		city.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,String>("city"));
		network.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,String>("network"));
		service.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,String>("service"));
		doxnonDox.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,String>("doxnonDox"));
		percentage.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,String>("percentage"));
		
		profitlosspercentage.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,String>("profitlosspercentage"));
		profitloss.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,String>("profitloss"));
		
		packets.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,Integer>("packets"));
		
		billingweight.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,Double>("billingweight"));
		insuracneamount.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,Double>("insuracneamount"));
		shipercost.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,Double>("shipercost"));
		cod.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,Double>("cod"));
		fod.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,Double>("fod"));
		docketcharge.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,Double>("docketcharge"));
		otherVas.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,Double>("otherVas"));
		amount.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,Double>("amount"));
		fuel.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,Double>("fuel"));
		servicetax.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,Double>("servicetax"));
		total.setCellValueFactory(new PropertyValueFactory<SalesJavaFXBean,Double>("total"));
		//btnGenerate.setDisable(true);
		pagination.setVisible(false);
		
	}
	
}
