package com.onesoft.courier.graphs.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.DB.DBconnection;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.common.LoadClientBean;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadServiceGroups;
import com.onesoft.courier.common.LoadZone;
import com.onesoft.courier.graphs.beans.ZoneJavaFXBean;
import com.onesoft.courier.graphs.beans.ZoneBean;
import com.onesoft.courier.graphs.utils.FilterUtils;
import com.onesoft.courier.graphs.utils.SalesReportUtils;
import com.onesoft.courier.graphs.utils.ZoneReportUtils;
import com.onesoft.courier.main.Main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class ZoneCompareController implements Initializable{
	
	
	ObservableList<String> comboBoxBranchItems=FXCollections.observableArrayList(FilterUtils.ALLBRANCH);
	ObservableList<String> comboBoxZoneItems=FXCollections.observableArrayList(FilterUtils.ALLZONE);
	ObservableList<String> comboBoxClientItems=FXCollections.observableArrayList();
	ObservableList<String> comboBoxDNDItems=FXCollections.observableArrayList(FilterUtils.DND);
	ObservableList<String> comboBoxTypeItems=FXCollections.observableArrayList(FilterUtils.TYPE1,FilterUtils.CASH,FilterUtils.CREDIT);
	
	
	private ObservableList<ZoneJavaFXBean> tabledata=FXCollections.observableArrayList();
	
	private ObservableList<PieChart.Data> pcdata=FXCollections.observableArrayList();
		
	
	private ObservableList<String> comboBoxFromItems=FXCollections.observableArrayList(FilterUtils.YEAR5,FilterUtils.YEAR4,
																						FilterUtils.YEAR3,FilterUtils.YEAR2,FilterUtils.YEAR1);

	private ObservableList<String> comboBoxToItems=FXCollections.observableArrayList(FilterUtils.YEAR1,FilterUtils.YEAR2,
																						FilterUtils.YEAR3,FilterUtils.YEAR4,FilterUtils.YEAR5);
	
	
	public static String configfilePath = "typeproperties.properties";
	Properties prop = new Properties();
	InputStream in = this.getClass().getClassLoader().getResourceAsStream(configfilePath);
	
	
	DecimalFormat df=new DecimalFormat(".##");
	DecimalFormat dfWeight=new DecimalFormat(".###");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	
	double pievalue=0.0;
	double totalpievalue=0.0;
	
	@FXML
	private NumberAxis yAxis;
	

	
	@FXML
	private CategoryAxis xAxis;
	
	@FXML
	private BarChart<?,?> bc;
	
	@FXML
	private PieChart pc;
	
	@FXML
	private Button btnGenerate;
	
	@FXML
	private VBox vbox;
	
	
	@FXML
	private Button btnexcel;
	
	@FXML
	private Button btnExportPDF;
	
	@FXML
	private Button btnreset;
	
	@FXML
	private Button btnClose;
	
	@FXML
	private Pagination pagination;
	
	@FXML
	private CheckBox chkShowGraphData;
	
	@FXML
	private ComboBox<String> comboBoxBranch;
	
	@FXML
	private ComboBox<String> comboBoxType;
	
	@FXML
	private ComboBox<String> comboBoxClient;
	
	@FXML
	private ComboBox<String> comboBoxDND;
	
	@FXML
	private ComboBox<String> comboBoxZone;
	
	@FXML
	private ComboBox<String> comboBoxFromYear;
	
	@FXML
	private ComboBox<String> comboBoxToYear;
	
	@FXML
	private DatePicker dpFrom;
	
	@FXML
	private DatePicker dpTo;
	
	/*@FXML
	private ComboBox<String> comboBoxQuarters;*/
	
	
	@FXML
	private TableView<ZoneJavaFXBean> table;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, Integer> serialno;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, String> awb_no;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, String> dbtclient;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, String> bookingdate;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, String> zone;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, String> branch;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, String> clientcode;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, String> city;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, String> network;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, String> service;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, String> doxnonDox;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, String> percentage;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, Integer> packets;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, Double> billingweight;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, Double> insuracneamount;
	 
	@FXML
	private TableColumn<ZoneJavaFXBean, Double> shipercost;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, Double> cod;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, Double> fod;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, Double> docketcharge;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, Double> otherVas;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, Double> amount;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, Double> fuel;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, Double> servicetax;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, Double> total;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, String> profitlosspercentage;
	
	@FXML
	private TableColumn<ZoneJavaFXBean, String> profitloss;
	
	
	
	
	public void loadBranch() throws SQLException
	{
		
		new	LoadBranch().loadBranch();
		
		System.out.println("Zone Compare Contorller >> LoadBranch from LoadBranch Class: >>>>>>>> ");
		
		//System.out.println("Network Contorller >> Load from Global set object");
		for(String s:LoadBranch.SETLOADBRANCHFORALL)
		{
			comboBoxBranchItems.add(s);
		}
		comboBoxBranch.setItems(comboBoxBranchItems);
		
		/*DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		
		
		Set<String> set = new HashSet<String>();
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select code from brndetail";
			rs=st.executeQuery(sql);
	
			while(rs.next())
			{
				set.add(rs.getString(1));
			}
		
			for(String s:set)
			{
				comboBoxBranchItems.add(s);
			}
			comboBoxBranch.setItems(comboBoxBranchItems);
					
			
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}*/
	}
	
	
	public void loadDND() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		
		
		Set<String> set = new HashSet<String>();
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select dox_nondox from dailybookingtransaction";
			rs=st.executeQuery(sql);
	
			while(rs.next())
			{
				set.add(rs.getString(1));
			}
		
			for(String s:set)
			{
				comboBoxDNDItems.add(s);
			}
			comboBoxDND.setItems(comboBoxDNDItems);
					
			
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
	
	public void loadZone() throws SQLException
	{
		new	LoadZone().loadZone();
		
		System.out.println("Zone Contorller ============= LoadZones from LoadZone Class: =========== ");
		
		//System.out.println("Network Contorller >> Load from Global set object");
		for(String s:LoadZone.SET_LOAD_ZONE_FOR_ALL)
		{
			comboBoxZoneItems.add(s);
		}
		comboBoxZone.setItems(comboBoxZoneItems);
		
		/*
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		
		
		Set<String> setZone = new HashSet<String>();
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select zonedetail2zonetype from zonedetail";
			rs=st.executeQuery(sql);
	
			while(rs.next())
			{
				setZone.add(rs.getString(1));
			}
		
			for(String zone:setZone)
			{
				comboBoxZoneItems.add(zone);
			}
			comboBoxZone.setItems(comboBoxZoneItems);
					
			
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}*/
	}
	
	
	@FXML
	public void loadData() throws SQLException, IOException
	{	
		
		pievalue=0.0;
		totalpievalue=0.0;
		setYear();
		//showZoneWiseData();
		

	}
	
	
	
	public void setYear() throws SQLException, IOException
	{
		String[] from=comboBoxFromYear.getValue().split("-");
		String[] to=comboBoxToYear.getValue().split("-");
		
		String firstyear=from[0];
		String secondyear=to[1];
		
		int fst=Integer.parseInt(firstyear);
		int scnd=Integer.parseInt(secondyear);
		
		if(fst<scnd)
		{	
			System.out.println("First year: "+firstyear);
			System.out.println("Second year: "+secondyear);
			String datesql="booking_date between '"+firstyear+"-04-01' AND '"+secondyear+"-03-31'";
			System.out.println("financial date filter: "+datesql);
			
			setFilterForTable(datesql);
				
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Wrong Years selected");
			alert.setHeaderText(null);
			alert.setContentText("Please select a valid From and To year...");
			alert.showAndWait();
		}
		
	}
	
	
	
	@FXML
	public void setFilterClients() throws SQLException
	{
		comboBoxClient.getItems().clear();
		comboBoxClient.setItems(comboBoxClientItems);
		
		comboBoxClient.setValue(FilterUtils.CLIENT);
		comboBoxClientItems.add(FilterUtils.CLIENT);
			
		new	LoadClients().loadClient();
			
		Set<String> setClient =new HashSet<>();
				
			if(!comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH) && comboBoxType.getValue().equals(FilterUtils.TYPE1))
			{
				for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
				{	
					if(comboBoxBranch.getValue().equals(s.getBranchcode()))
					{
						setClient.add(s.getClientCode());
					}					 
				}
			}
				 
			else if(comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH) && !comboBoxType.getValue().equals(FilterUtils.TYPE1))
			{
				if(comboBoxType.getValue().equals(FilterUtils.CASH))
				{ 
					for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
					{
						if(comboBoxType.getValue().equals(s.getClientCode()))
						{
							setClient.add(s.getClientCode());
						}
					}
				}
				else
				{
					for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
					{
					 	if(!FilterUtils.CASH.equals(s.getClientCode()))
						{
							setClient.add(s.getClientCode());
						}
					}
				}
			}
				 
			else if(!comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH) && !comboBoxType.getValue().equals(FilterUtils.TYPE1))
			{
				if(comboBoxType.getValue().equals(FilterUtils.CASH))
				{
					for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
					{
						if(comboBoxBranch.getValue().equals(s.getBranchcode()) && comboBoxType.getValue().equals(s.getClientCode()))
						{
							setClient.add(s.getClientCode());
						}
					}
				}
				else
				{
					for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
					{
						if(comboBoxBranch.getValue().equals(s.getBranchcode()) && !FilterUtils.CASH.equals(s.getClientCode()))
						{
							setClient.add(s.getClientCode());
						}
					}
				}
			}
				 
			else
			{
				for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
				{
					setClient.add(s.getClientCode());
				}
			}
			 
			for(String set:setClient)
			{
				comboBoxClientItems.add(set);
			}
			
			comboBoxClient.setItems(comboBoxClientItems);
	}
	
	
	
	
	@FXML
	public void setClinetData() throws SQLException, FileNotFoundException
	{
		
		comboBoxClient.getItems().clear();
		comboBoxClient.setItems(comboBoxClientItems);
		
		 comboBoxClient.setValue(FilterUtils.CLIENT);
			comboBoxClientItems.add(FilterUtils.CLIENT);
		  new	LoadClients().loadClient();
		
		System.out.println("Zone Compare Contorller  >> LoadClient from LoadCient Class: >>>>>>>> ");
		
		Set<String> setClient =new HashSet<>();
		
		//System.out.println("Network Contorller >> Load from Global set object");
		for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
		{
			//System.out.println("From Load Clinet N/s: "+s.getBranchcode()+" "+s.getClientCode());
			setClient.add(s.getClientCode());
			//comboBoxBranchItems.add(s.getClientCode());
		}
		//comboBoxBranch.setItems(comboBoxBranchItems);
		
		for(String set:setClient)
		{
			//System.out.println("From Load Clinet Zone SET: "+set);
			comboBoxClientItems.add(set);
		}
		
		comboBoxClient.setItems(comboBoxClientItems);
		
		
		/*comboBoxClient.getItems().clear();
		comboBoxClient.setItems(comboBoxClientItems);
		 
		System.out.println(comboBoxClient.getValue()+" ----==============>>>>>>>>>>>>>>>>>");
		
	
		 
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		String sql=null;
		
	
		 Set<String> setclient = new HashSet<String>();
		
		
		
		try
		{	
			 
			prop.load(in);
			
			 if(!comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH) && comboBoxType.getValue().equals(FilterUtils.TYPE1))
			 {
				sql="select dbt.dailybookingtransaction2client from dailybookingtransaction as dbt, clientmaster as cm where dbt.dailybookingtransaction2client=cm.code "
							+ "and dbt.dailybookingtransaction2Branch='"+comboBoxBranch.getValue()+"'";
					System.out.println("from if: =======>"+comboBoxBranch.getValue()+" "+comboBoxType.getValue());
			 }
			 
			 else if(comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH) && !comboBoxType.getValue().equals(FilterUtils.TYPE1))
			 {
				 sql="select dbt.dailybookingtransaction2client from dailybookingtransaction as dbt, clientmaster as cm where dbt.dailybookingtransaction2client=cm.code "
							+ "and cm.client_type='"+prop.getProperty(comboBoxType.getValue())+"'";
					System.out.println("from Ist else if: =======>"+comboBoxBranch.getValue()+" "+prop.getProperty(comboBoxType.getValue()));
			 }
			 
			 else if(!comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH) && !comboBoxType.getValue().equals(FilterUtils.TYPE1))
			{
				sql="select dbt.dailybookingtransaction2client from dailybookingtransaction as dbt, clientmaster as cm where dbt.dailybookingtransaction2client=cm.code "
						+ "and cm.client_type='"+prop.getProperty(comboBoxType.getValue())+"' and dbt.dailybookingtransaction2Branch='"+comboBoxBranch.getValue()+"'";
				System.out.println("from IInd else if: =======>"+comboBoxBranch.getValue()+" "+prop.getProperty(comboBoxType.getValue()));
			}
			 
			else
			{
				sql="select code from clientmaster";
				System.out.println("from else: =======>"+comboBoxBranch.getValue()+" "+comboBoxType.getValue());
			}
			
			setclient.clear();
			 
			System.out.println("sql from client data: "+sql);
			
			
			
			st=con.createStatement();
			
			rs=st.executeQuery(sql);
			comboBoxClient.setValue(FilterUtils.CLIENT);
			comboBoxClientItems.add(FilterUtils.CLIENT);
			
			if(!rs.next())
			{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Database Alert");
				alert.setHeaderText(null);
				alert.setContentText("No match found");
				alert.showAndWait();
				
				comboBoxBranch.requestFocus();
				comboBoxClient.getItems().clear();
				comboBoxClient.setValue("No match found");
				comboBoxClientItems.add("No match found");
				
				
			}
			else
			{
			do
			{
				setclient.add(rs.getString(1));
			}while(rs.next());
			}
			for(String s:setclient)
			{
				comboBoxClientItems.add(s);
			}
			
			comboBoxClient.setItems(comboBoxClientItems);
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		
		}*/
		
	}
	
	
	
	//--------------------------------------------------------
	
	/*public void showZoneWiseData() throws SQLException
	{
		
		if(dpFrom.getValue()==null)
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Empty Field Validation");
			alert.setHeaderText(null);
			alert.setContentText("From Date field is Empty!");
			alert.showAndWait();
			dpFrom.requestFocus();
		}
    	else if( dpTo.getValue()==null)
    	{
    		Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Empty Field Validation");
			alert.setHeaderText(null);
			alert.setContentText("To Date field is Empty!");
			alert.showAndWait();
			dpTo.requestFocus();
    	}
		
    	else
    	{
    		LocalDate from=dpFrom.getValue();
			LocalDate to=dpTo.getValue();
    		Date stdate=Date.valueOf(from);
			Date edate=Date.valueOf(to);
    		
    	
    		if(chkShowGraphData.isSelected()==false)
				{
			
			tabledata.clear();
			
			setFilterForTable(stdate, edate);
			
			System.out.println("Check box is selected");
				}
		else
		{
		
				tabledata.clear();
	    		awb_no.setVisible(true);
				clientcode.setVisible(true);
				dbtclient.setVisible(true);
				branch.setVisible(true);
				shipercost.setVisible(true);
				city.setVisible(true);
				network.setVisible(true);
				service.setVisible(true);
				doxnonDox.setVisible(true);
				docketcharge.setVisible(true);
				percentage.setVisible(false);
				amount.setVisible(false);			
				setFilterForTable(stdate,edate);
				
		    	//showZoneWiseData(stdate, edate);
	    	}
			
			
		}
	}*/
	
	
	//-----------------------------------------------------------------------------------------------------------------
	
	public void simpleTable(String sql) throws SQLException
	{
		
  tabledata.clear();
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		

	ZoneBean zonebean=new ZoneBean();
		
		
		
		ResultSet rs=null;
		Statement st=null;
		
		try{
			awb_no.setVisible(false);
			clientcode.setVisible(false);
			dbtclient.setVisible(false);
			branch.setVisible(false);
			city.setVisible(false);
			shipercost.setVisible(false);
			network.setVisible(false);
			service.setVisible(false);
			doxnonDox.setVisible(false);
			docketcharge.setVisible(false);
			amount.setVisible(true);
			pagination.setVisible(false);
			percentage.setVisible(true);
			
			String finalsql=null;
			
		//	bookingdate.setText("Zones");
			if(!comboBoxZone.getValue().equals(FilterUtils.ALLZONE))
			{
			finalsql="select to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY'),zdtl.zonedetail2zonetype,sum(dbt.packets) as pcs,sum(dbt.billing_weight) as weight,sum(dbt.insurance_amount) as insuracne,"
					+ "sum(dbt.cod_amount) as cod,sum(fod_amount) as fod ,sum(dbt.vas_amount) as vas, SUM(dbt.amount) as amount,"
					+ "sum(dbt.fuel) as fuel,sum(dbt.tax_amount1+dbt.tax_amount2+dbt.tax_amount3) as tax,sum(dbt.total) as total "
					+ "from dailybookingtransaction as dbt, zonedetail as zdtl, clientmaster as cm where dbt.dailybookingtransaction2city=zdtl.zone2city and dbt.dailybookingtransaction2client=cm.code "+sql+" Group by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY'),zdtl.zonedetail2zonetype Order by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY')";
			}
			else
			{
				finalsql="select to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY'),sum(dbt.packets) as pcs,sum(dbt.billing_weight) as weight,"
						+ "sum(dbt.insurance_amount) as insuracne,sum(dbt.cod_amount) as cod,sum(dbt.fod_amount) as fod ,sum(dbt.vas_amount) as vas, SUM(dbt.amount) as amount,"
						+ "sum(dbt.fuel) as fuel,sum(dbt.tax_amount1+dbt.tax_amount2+dbt.tax_amount3) as tax,sum(dbt.total) as total from dailybookingtransaction as dbt, zonedetail as zdtl, clientmaster as cm "
						+ "where dbt.dailybookingtransaction2city=zdtl.zone2city and dbt.dailybookingtransaction2client=cm.code "+sql+" Group by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY') "
								+ "Order by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY')";
			}
			
			System.out.println("Query form simple table: "+finalsql);
			
			st=con.createStatement();
			rs=st.executeQuery(finalsql);
			int serialno=1;
			int financialyr=0;
			double per=0.0;
			zonebean.setTotal(0.0);
			
			while (rs.next())
			{
			
				double profitloss=Double.parseDouble(df.format(rs.getDouble("total")-zonebean.getTotal()));
				double profitlosspercentage=(profitloss/zonebean.getTotal())*100;
				
				if(Double.isInfinite(profitlosspercentage))
				{
					zonebean.setProfitloss("--");
					zonebean.setProfitlosspercentage("--");
				}
				else if(profitlosspercentage>0)
				{
					zonebean.setProfitloss(profitloss+"");
					zonebean.setProfitlosspercentage(df.format(profitlosspercentage)+"%");
				}
				else
				{
					zonebean.setProfitloss(profitloss+"");
					zonebean.setProfitlosspercentage(df.format(profitlosspercentage)+"%");
				}
				
				
				if(!comboBoxZone.getValue().equals(FilterUtils.ALLZONE))
				{
				zonebean.setSerialno(serialno);
				zonebean.setBookingdate(rs.getString(1));
				financialyr=((Integer.parseInt(zonebean.getBookingdate()))-2000)+1;
				zonebean.setZone(rs.getString(2));
				zonebean.setPackets(rs.getInt(3));
				zonebean.setBillingweight(Double.parseDouble(dfWeight.format(rs.getDouble(4))));
				zonebean.setInsuracneamount(Double.parseDouble(df.format(rs.getDouble(5))));
				zonebean.setCod(Double.parseDouble(df.format(rs.getDouble(6))));
				zonebean.setFod(Double.parseDouble(df.format(rs.getDouble(7))));
				zonebean.setOtherVas(Double.parseDouble(df.format(rs.getDouble(8))));
				zonebean.setAmount(Double.parseDouble(df.format(rs.getDouble(9))));
				zonebean.setFuel(Double.parseDouble(df.format(rs.getDouble(10))));
				//zonebean.setServicetax(rs.getDouble(10));
				zonebean.setServicetax(Double.parseDouble(df.format(rs.getDouble(11))));
				zonebean.setTotal(Double.parseDouble(df.format(rs.getDouble(12))));
				
				per=Double.parseDouble(df.format((zonebean.getTotal()/totalpievalue)*100));
				
				}
				else
				{
					zonebean.setSerialno(serialno);
					zonebean.setBookingdate(rs.getString(1));
					financialyr=((Integer.parseInt(zonebean.getBookingdate()))-2000)+1;
					zonebean.setZone(FilterUtils.ALLZONE);
					zonebean.setPackets(rs.getInt(2));
					zonebean.setBillingweight(Double.parseDouble(dfWeight.format(rs.getDouble(3))));
					zonebean.setInsuracneamount(Double.parseDouble(df.format(rs.getDouble(4))));
					zonebean.setCod(Double.parseDouble(df.format(rs.getDouble(5))));
					zonebean.setFod(Double.parseDouble(df.format(rs.getDouble(6))));
					zonebean.setOtherVas(Double.parseDouble(df.format(rs.getDouble(7))));
					zonebean.setAmount(Double.parseDouble(df.format(rs.getDouble(8))));
					zonebean.setFuel(Double.parseDouble(df.format(rs.getDouble(9))));
					//zonebean.setServicetax(rs.getDouble(10));
					zonebean.setServicetax(Double.parseDouble(df.format(rs.getDouble(10))));
					zonebean.setTotal(Double.parseDouble(df.format(rs.getDouble(11))));
					
					per=Double.parseDouble(df.format((zonebean.getTotal()/totalpievalue)*100));
					
				}
				
				tabledata.add(new ZoneJavaFXBean(zonebean.getSerialno(),null,null,zonebean.getBookingdate()+"-"+financialyr,zonebean.getZone(),null,null,null,null,null,null,
						zonebean.getPackets(),zonebean.getBillingweight(), zonebean.getInsuracneamount(), 0, zonebean.getCod(),zonebean.getFod(),0,
						zonebean.getOtherVas(),zonebean.getAmount(),zonebean.getFuel(), zonebean.getServicetax(), zonebean.getTotal(),per+"%",zonebean.getProfitloss(),zonebean.getProfitlosspercentage()));
				
				serialno++;
			}
			table.setItems(tabledata);
			
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{
			dbcon.disconnect(null, st, rs, con);
			totalpievalue=0.0;
			//sql=null;
		}
	}
	
	
	// ------------------------------------------------ Method for Bar Chart data ---------------------------------------

		public void showGraph(String sql) throws SQLException
		{
			XYChart.Series series1 = new XYChart.Series();
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			
			Statement st=null;
			ResultSet rs=null;
			ZoneBean zonebean=new ZoneBean();
				try
				{
					
					st=con.createStatement();
					String finalsql="select to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY'),sum(dbt.total) as total from dailybookingtransaction as dbt, zonedetail as zdtl, clientmaster as cm "
							+ "where dbt.dailybookingtransaction2city=zdtl.zone2city and dbt.dailybookingtransaction2client=cm.code "+sql+" Group by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY') order by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY')";			
					
					
					System.out.println("Query form graph: "+finalsql);
					rs=st.executeQuery(finalsql);
					
					if(!rs.next())
					{
						bc.getData().clear();
						pcdata.clear();
						tabledata.clear();
					
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Database Alert");
						alert.setHeaderText(null);
						alert.setContentText("Data not found");
						alert.showAndWait();
						
					}
					else
					{	
						do
						{	
							
							zonebean.setBookingdate(rs.getString(1));
							zonebean.setTotal(Double.parseDouble(df.format(rs.getDouble(2))));
							int date=((Integer.parseInt(zonebean.getBookingdate()))-2000)+1;
							
							series1.getData().add(new XYChart.Data(zonebean.getBookingdate()+"-"+date,zonebean.getTotal()));
							series1.setName("Financial Years");
							
						}
						while(rs.next());
						
						bc.getData().clear();
						bc.getData().add(series1);
					}
				}
				catch(Exception e)
				{
					System.out.println(e);
					e.printStackTrace();
				}
			
				finally
				{
					dbcon.disconnect(null, st, rs, con);
				}
		}
		
		
	//------------------------------------------------ Method for Pie Chart data ---------------------------------------
		
		
	
	public void showPie(String sql) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		
		ArrayList<ZoneBean> list=new ArrayList<ZoneBean>();
				
			try
			{
				pcdata.clear();
				st=con.createStatement();
				String finalsql="select to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY'),sum(dbt.total) as total from dailybookingtransaction as dbt, zonedetail as zdtl, clientmaster as cm "
						+ "where dbt.dailybookingtransaction2city=zdtl.zone2city and dbt.dailybookingtransaction2client=cm.code "+sql+" Group by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY') order by to_char(date_trunc('month',dbt.booking_date) - interval '3' month, 'YYYY')";					
				
				System.out.println("Query form Pie chart: "+finalsql);
				rs=st.executeQuery(finalsql);
				
				
				//ArrayList<ZoneBean> list=new ArrayList<ZoneBean>();
				if(!rs.next())
				{
					tabledata.clear();
					btnexcel.setVisible(false);
					btnExportPDF.setVisible(false);
				}
				else
				{
					btnexcel.setVisible(true);
					btnExportPDF.setVisible(true);
					do 
					{
						ZoneBean zonebean=new ZoneBean();
						zonebean.setBookingdate(rs.getString(1));
						int date=((Integer.parseInt(zonebean.getBookingdate()))-2000)+1;
						zonebean.setBookingdate(rs.getString(1)+"-"+date);
						
						zonebean.setTotal(Double.parseDouble(df.format(rs.getDouble(2))));
					
						pievalue=pievalue+zonebean.getTotal();
						totalpievalue=totalpievalue+zonebean.getTotal();
					
					list.add(zonebean);		
					}
					while(rs.next());
	
				
		
					for(ZoneBean bean: list)
					{	
						pcdata.add(new PieChart.Data(bean.getBookingdate()+" ("+String.valueOf(df.format((bean.getTotal()/pievalue)*100) + "%)"), bean.getTotal()));
						
					}
					
					pc.setData(pcdata);
					
	
					
					
			     /*    pcdata.stream().forEach(pieData -> {
					    pieData.getNode().addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
					        Bounds b1 = pieData.getNode().getBoundsInLocal();
					        double newX = (b1.getWidth()) / 2 + b1.getMinX();
					        double newY = (b1.getHeight()) / 2 + b1.getMinY();
					        // Make sure pie wedge location is reset
					    //    pieData.getNode().setTranslateX(0);
					      //  pieData.getNode().setTranslateY(0);
					        TranslateTransition tt = new TranslateTransition(
					                Duration.millis(1500), pieData.getNode());
					        tt.setByX(newX);
					        tt.setByY(newY);
					        tt.setAutoReverse(true);
					        tt.setCycleCount(2);
					        tt.play();
					    });
					});*/
					
			        
				}
				
			}
			catch(Exception e)
			{
				 System.out.println(e);
				 e.printStackTrace();
			}	
			finally
			{
				dbcon.disconnect(null, st, rs, con);
				}
	}
	

	//--------------------------------------------------------------------------
	
	
			@FXML
			public void exportToPDF() throws FileNotFoundException, InterruptedException, DocumentException
			{

				Document my_pdf_report = new Document();
				 
				FileChooser fc = new FileChooser();
				fc.getExtensionFilters().addAll(new ExtensionFilter("PDF Files", "*.pdf"));
				File file = fc.showSaveDialog(null);
				PdfWriter.getInstance(my_pdf_report, new FileOutputStream(file.getAbsolutePath()));
				my_pdf_report.open();
					
		         //we have four columns in our table
		        PdfPTable my_report_table = new PdfPTable(11);
		        
		        my_report_table.setWidthPercentage(100);
		        my_report_table.setHorizontalAlignment(Element.ALIGN_LEFT);
		        
		        Font headingfont = new Font(Font.FontFamily.UNDEFINED, 6, Font.BOLD);
		        Font tfont = new Font(Font.FontFamily.UNDEFINED, 6, Font.NORMAL);
		      
		      //create a cell object
		       
		      PdfPCell table_cell = null;
		      
		      table_cell=new PdfPCell(new Phrase("Sr_No.",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		      table_cell=new PdfPCell(new Phrase("Fyncl Yr",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		      table_cell=new PdfPCell(new Phrase("Zone",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		      table_cell=new PdfPCell(new Phrase("PCS",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		      table_cell=new PdfPCell(new Phrase("Weight",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		      table_cell=new PdfPCell(new Phrase("Ins Amt",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		      
		      table_cell=new PdfPCell(new Phrase("COD",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		      table_cell=new PdfPCell(new Phrase("FOD",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		    
		      table_cell=new PdfPCell(new Phrase("VAS",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		    
		      table_cell=new PdfPCell(new Phrase("Amount",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		/*     table_cell=new PdfPCell(new Phrase("Fuel",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		         table_cell=new PdfPCell(new Phrase("Service Tax",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);*/
		     
		      table_cell=new PdfPCell(new Phrase("Total",headingfont));
		      table_cell.setBorder(Rectangle.NO_BORDER);
		      my_report_table.addCell(table_cell);
		     
		  
		      
		         
		         /*my_report_table.addCell(new PdfPCell(new Phrase("Serial No",tfont)));
		         my_report_table.addCell(new Phrase("Booking Date",tfont));
		         my_report_table.addCell(new Phrase("Srvc Grp",tfont));
		       */
		      
		         for(ZoneJavaFXBean fxBean: tabledata)
					{
		        	  
					/* my_report_table.addCell(new PdfPCell(new Phrase(String.valueOf(fxBean.getSerialno()),tfont)));
		        	 my_report_table.addCell(new PdfPCell(new Phrase(fxBean.getBookingdate(),tfont)));
		        	 my_report_table.addCell(new PdfPCell(new Phrase(String.valueOf(fxBean.getPackets()),tfont)));
		        	 my_report_table.addCell(new PdfPCell(new Phrase(String.valueOf(fxBean.getBillingweight()),tfont)));
		        	*/

		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getSerialno()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		             
		            table_cell=new PdfPCell(new Phrase(fxBean.getBookingdate(),tfont));
		            table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(fxBean.getZone(),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getPackets()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getBillingweight()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getInsuracneamount()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getCod()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getFod()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		        	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getOtherVas()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		            
		            table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getAmount()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		           /* 
		         	table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getFuel()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		         	
		             table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getServicetax()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);*/
		         	
		            table_cell=new PdfPCell(new Phrase(String.valueOf(fxBean.getTotal()),tfont));
		        	table_cell.setBorder(Rectangle.NO_BORDER);
		            my_report_table.addCell(table_cell);
		        	 
		            table_cell.setBorder(0);
				
					}
		     
		         	my_pdf_report.add(my_report_table); 
		           
		         	System.out.println("File "+file.getName()+" successfully saved");
		         my_pdf_report.close();
					
			}
	
	//--------------------------------------------------------------------
		
	@FXML
	public void exportToExcel() throws SQLException, IOException
	{
			
		HSSFWorkbook workbook=new HSSFWorkbook();
		HSSFSheet sheet=workbook.createSheet("Sales Data");
		HSSFRow rowhead=sheet.createRow(0);
		
		rowhead.createCell(0).setCellValue("Serial No");
		rowhead.createCell(1).setCellValue("AWB_No");
		rowhead.createCell(2).setCellValue("Client Name");
		
		rowhead.createCell(3).setCellValue("Booking Date");
		rowhead.createCell(4).setCellValue("Zone Name");
		rowhead.createCell(5).setCellValue("Branch");
		rowhead.createCell(6).setCellValue("Client Code");
		
		rowhead.createCell(7).setCellValue("City");
		rowhead.createCell(8).setCellValue("Network");
		rowhead.createCell(9).setCellValue("Service");
		
		rowhead.createCell(10).setCellValue("Dox Non-Dox");
		rowhead.createCell(11).setCellValue("Packets");
		rowhead.createCell(12).setCellValue("Billing Weight");
	
		rowhead.createCell(13).setCellValue("Insurance Amount");
		rowhead.createCell(14).setCellValue("Shiper Cost");
		rowhead.createCell(15).setCellValue("COD Amount");
		
		rowhead.createCell(16).setCellValue("FOD Amount");
		rowhead.createCell(17).setCellValue("Docket Charge");
		rowhead.createCell(18).setCellValue("VAS");
		
		rowhead.createCell(19).setCellValue("Fuel");
		rowhead.createCell(20).setCellValue("Amount");
		rowhead.createCell(21).setCellValue("Service Tax");
		rowhead.createCell(22).setCellValue("Total");
		rowhead.createCell(23).setCellValue("Percentage");
		rowhead.createCell(23).setCellValue("Profit/Loss");
		rowhead.createCell(24).setCellValue("Profit/Loss Percentage");
		
		
			int i=2;
			
			HSSFRow row;
			
			for(ZoneJavaFXBean fxBean: tabledata)
			{
				row = sheet.createRow((short) i);
			
				row.createCell(0).setCellValue(fxBean.getSerialno());
				row.createCell(1).setCellValue(fxBean.getAwb_no());
				row.createCell(2).setCellValue(fxBean.getDbtclient());
				
				row.createCell(3).setCellValue(fxBean.getBookingdate());
				row.createCell(4).setCellValue(fxBean.getZone());
				row.createCell(5).setCellValue(fxBean.getBranch());
				row.createCell(6).setCellValue(fxBean.getClientcode());
				
				row.createCell(7).setCellValue(fxBean.getCity());
				row.createCell(8).setCellValue(fxBean.getNetwork());
				row.createCell(9).setCellValue(fxBean.getService());
				
				row.createCell(10).setCellValue(fxBean.getDoxnonDox());
				row.createCell(11).setCellValue(fxBean.getPackets());
				row.createCell(12).setCellValue(fxBean.getBillingweight());
			
				row.createCell(13).setCellValue(fxBean.getInsuracneamount());
				row.createCell(14).setCellValue(fxBean.getShipercost());
				row.createCell(15).setCellValue(fxBean.getCod());
				
				row.createCell(16).setCellValue(fxBean.getFod());
				row.createCell(17).setCellValue(fxBean.getDocketcharge());
				row.createCell(18).setCellValue(fxBean.getOtherVas());
				
				row.createCell(19).setCellValue(fxBean.getFuel());
				row.createCell(20).setCellValue(fxBean.getAmount());
				row.createCell(21).setCellValue(fxBean.getServicetax());
				row.createCell(22).setCellValue(fxBean.getTotal());
				row.createCell(23).setCellValue(fxBean.getPercentage());
				row.createCell(23).setCellValue(fxBean.getProfitloss());
				row.createCell(24).setCellValue(fxBean.getProfitlosspercentage()); 
				
				i++;
			}
			
			FileChooser fc = new FileChooser();
			fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files","*.xls"));
			File file = fc.showSaveDialog(null);
			FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
			System.out.println("file chooser: "+file.getAbsoluteFile());
			
		    workbook.write(fileOut);
		    fileOut.close();
			
			System.out.println("File "+file.getName()+" successfully saved");
	      
	}	
		
		
		
	public void setFilterForTable(String datesql) throws SQLException, IOException
	{
		prop.load(in);
		
		String sql=null;
		String branchsql="";
		String typesql="";
		String clientsql="";
		String dndsql="";
		String zonesql="";

		//String dateRange="and dbt.booking_date between '"+ stdate+"' AND '"+edate+"'";
		
		if(!comboBoxBranch.getValue().equals(FilterUtils.ALLBRANCH))
		{
			branchsql="and dbt.dailybookingtransaction2branch='"+comboBoxBranch.getValue()+"' ";
		}
		
		if(!comboBoxType.getValue().equals(FilterUtils.TYPE1))
		{
			typesql="and cm.client_type='"+prop.getProperty(comboBoxType.getValue())+"' ";
			
			System.out.println("Type value: ======================================== "+prop.getProperty(comboBoxType.getValue()) );
		}
		
		if(!comboBoxClient.getValue().equals(FilterUtils.CLIENT))
		{
			clientsql="and dbt.dailybookingtransaction2client='"+comboBoxClient.getValue()+"' ";
		}
		
		if(!comboBoxDND.getValue().equals(FilterUtils.DND))
		{
			dndsql="and dbt.dox_nondox='"+comboBoxDND.getValue()+"' ";
		}
		
		System.out.println("Zone values:>>>>>> "+comboBoxZone.getValue());
		
		if(!comboBoxZone.getValue().equals(FilterUtils.ALLZONE))
		{
			zonesql="and zdtl.zonedetail2zonetype='"+comboBoxZone.getValue()+"' ";
		}
		
		sql=branchsql+""+typesql+""+clientsql+""+dndsql+""+zonesql+" and "+datesql;
		
		System.out.println(" Filter sql: ====>> "+sql);
		
		
		showPie(sql);
		showGraph(sql);
		simpleTable(sql);
		
		/*if(chkShowGraphData.isSelected()==false)
		{
			simpleTable(sql);
		}*/
		
		
		
	}
	

	//------------------------------------------------ Method for Show Table data ---------------------------------------
		
	public void showDetailTable(String sql) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		ZoneBean zonebean=new ZoneBean(); // Normal java bean class
		
		ResultSet rs=null;
		Statement st=null;
		
		/*awb_no.setVisible(true);
		clientcode.setVisible(true);
		dbtclient.setVisible(true);
		//branch.setVisible(true);
		city.setVisible(true);
		shipercost.setVisible(true);
		network.setVisible(true);
		service.setVisible(true);
		doxnonDox.setVisible(true);
		docketcharge.setVisible(true);
		amount.setVisible(false);*/
		
		bookingdate.setText("Booking Date");
	
		try{
			tabledata.clear();
			st=con.createStatement();
			String finalsql="select dbt.booking_date,dbt.air_way_bill_number,dbt.dailybookingtransaction2client,dbt.dailybookingtransaction2branch,dbt.sub_client_code,dbt.ct.city_name,dbt.dailybookingtransaction2city,"
					+ "zdtl.zonedetail2zonetype, dbt.dailybookingtransaction2network,dbt.dailybookingtransaction2service,dbt.dox_nondox,sum(dbt.packets) as pcs,"
					+ "sum(dbt.billing_weight) as weight,sum(dbt.insurance_amount) as insurance_amount,sum(dbt.shiper_cost) as shiper_cost,sum(dbt.cod_amount) as cod_amount,"
					+ "sum(dbt.fod_amount) as fod_amount,sum(dbt.docket_charge) as docket_charge,sum(dbt.vas_amount) as vas_amount,sum(dbt.fuel) as fuel,sum(dbt.service_tax+cess1+cess2),"
					+ "sum(dbt.total) as total from dailybookingtransaction as dbt,zonedetail as zdtl, city as ct, clientmaster as cm where dbt.dailybookingtransaction2city=zdtl.zone2city and dbt.dailybookingtransaction2city=ct.code and dbt.dailybookingtransaction2client=cm.code "+sql+" GROUP by dbt.dailybookingtransaction2client,dbt.booking_date,dbt.air_way_bill_number,"
							+ "dbt.dailybookingtransaction2branch,dbt.sub_client_code,dbt.dailybookingtransaction2city,dbt.dailybookingtransaction2network,"
							+ "dbt.dailybookingtransaction2service,dbt.dox_nondox,zdtl.zonedetail2zonetype,ct.city_name order by dbt.booking_date";
			
			System.out.println("Query form detailed table: "+finalsql);
			rs=st.executeQuery(finalsql);
			
			int serialno=1;
		
			if(!rs.next())
			{
				//bc.getData().clear();
			//	pcdata.clear();
				tabledata.clear();
			
				/*Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Database Alert");
				alert.setHeaderText(null);
				alert.setContentText("from detail table: Data not found");
				alert.showAndWait();*/
	
			}
			else
			{
			
			do
			{
				zonebean.setSerialno(serialno);
				zonebean.setAwb_no(rs.getString("air_way_bill_number"));
				zonebean.setDbtClient(rs.getString("dailybookingtransaction2client"));
				zonebean.setBookingdate(date.format(rs.getDate("booking_date")));
				zonebean.setBranch(rs.getString("dailybookingtransaction2branch"));
				zonebean.setClientcode(rs.getString("sub_client_code"));
				zonebean.setCity(rs.getString("city_name"));
				zonebean.setNetwork(rs.getString("dailybookingtransaction2network"));
				zonebean.setService(rs.getString("dailybookingtransaction2service"));
				zonebean.setDoxnonDox(rs.getString("dox_nondox"));
				zonebean.setPackets(rs.getInt("pcs"));
				zonebean.setBillingweight(Double.parseDouble(dfWeight.format(rs.getDouble("weight"))));
				zonebean.setInsuracneamount(Double.parseDouble(df.format(rs.getDouble("insurance_amount"))));
				zonebean.setShipercost(Double.parseDouble(df.format(rs.getDouble("shiper_cost"))));
				zonebean.setCod(Double.parseDouble(df.format(rs.getDouble("cod_amount"))));
				zonebean.setFod(Double.parseDouble(df.format(rs.getDouble("fod_amount"))));
				zonebean.setDocketcharge(Double.parseDouble(df.format(rs.getDouble("docket_charge"))));
				zonebean.setOtherVas(Double.parseDouble(df.format(rs.getDouble("vas_amount"))));
				zonebean.setFuel(Double.parseDouble(df.format(rs.getDouble("fuel"))));
				zonebean.setServicetax(Double.parseDouble(df.format(rs.getDouble(19))));  // here, service tax contain the value of "sum(dbt.service_tax+dbt.cess1+dbt.cess2)"
				zonebean.setTotal(Double.parseDouble(df.format(rs.getDouble("total"))));
				
				tabledata.add(new ZoneJavaFXBean(zonebean.getSerialno(),zonebean.getAwb_no(),zonebean.getDbtClient(),zonebean.getBookingdate(),null,zonebean.getBranch(),zonebean.getClientcode(),
						zonebean.getCity(),zonebean.getNetwork(),zonebean.getService(),zonebean.getDoxnonDox(),zonebean.getPackets(),
						zonebean.getBillingweight(),zonebean.getInsuracneamount(), zonebean.getShipercost(), zonebean.getCod(),zonebean.getFod(),
						zonebean.getDocketcharge(), zonebean.getOtherVas(),0,zonebean.getFuel(), zonebean.getServicetax(), zonebean.getTotal(),null,null,null));
				serialno++;
			}
			while (rs.next());
			
			table.setItems(tabledata);
			
			//LocalDate from=dpFrom.getValue();
		//	LocalDate to=dpTo.getValue();
    		//Date stdate=Date.valueOf(from);
			//Date edate=Date.valueOf(to);
			
			
		
			pagination.setVisible(true);
			pagination.setPageCount((tabledata.size() / rowsPerPage() + 1));
			pagination.setCurrentPageIndex(0);
			pagination.setPageFactory((Integer pageIndex) -> createPage(pageIndex));
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{
			dbcon.disconnect(null, st, rs, con);
			//dpFrom.getEditor().clear();
			//dpTo.getEditor().clear();
		}
		
	}
	
	//--------------------------------- Code for pagination ----------------------------------------
	
	public int itemsPerPage()
	{
		return 1;
	}

	public int rowsPerPage() 
	{
		return 12;
	}

	public GridPane createPage(int pageIndex)
	{
		int lastIndex = 0;
		GridPane pane=new GridPane();
		int displace = tabledata.size() % rowsPerPage();
	        
			if (displace >= 0)
			{
				lastIndex = tabledata.size() / rowsPerPage();
			}
			/*else
			{
				lastIndex = tabledata.size() / rowsPerPage() - 1;
			}*/
	  
	        
			int page = pageIndex * itemsPerPage();
	        for (int i = page; i < page + itemsPerPage(); i++)
	        {
	            if (lastIndex == pageIndex)
	            {
	                table.setItems(FXCollections.observableArrayList(tabledata.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
	            }
	            else
	            {
	                table.setItems(FXCollections.observableArrayList(tabledata.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
	            }
	        }
	       return pane;
			
	    }
	
	
	@FXML
	public void resetData() throws IOException
	{
		Main m=new Main();
		m.compareZone();
			
	}
	
	
	@FXML
	public void close() throws IOException, SQLException
	{
		Main m=new Main();
		m.homePage();
	}
	
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		comboBoxBranch.setValue(FilterUtils.ALLBRANCH);
		comboBoxType.setValue(FilterUtils.TYPE1);
		comboBoxZone.setValue(FilterUtils.ALLZONE);
	//	comboBoxClient.setValue(FilterUtils.CLIENT);
		comboBoxDND.setValue(FilterUtils.DND);
		
		try {
			loadBranch();
			loadZone();
			loadDND();
			//loadAllType();
			setClinetData();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		comboBoxType.setItems(comboBoxTypeItems);
		comboBoxZone.setItems(comboBoxZoneItems);
		//comboBoxClient.setItems(comboBoxClientItems);
		
		comboBoxFromYear.setItems(comboBoxFromItems);
		comboBoxToYear.setItems(comboBoxToItems);
		
		comboBoxFromYear.setValue("2012-2013");
		comboBoxToYear.setValue("2016-2017");
	
		btnexcel.setVisible(false);
		btnExportPDF.setVisible(false);
		
		serialno.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,Integer>("serialno"));
		awb_no.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,String>("awb_no"));
		dbtclient.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,String>("dbtclient"));
		bookingdate.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,String>("bookingdate"));
		zone.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,String>("zone"));
		branch.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,String>("branch"));
		clientcode.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,String>("clientcode"));
		city.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,String>("city"));
		network.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,String>("network"));
		service.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,String>("service"));
		doxnonDox.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,String>("doxnonDox"));
		percentage.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,String>("percentage"));
		
		profitlosspercentage.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,String>("profitlosspercentage"));
		profitloss.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,String>("profitloss"));
		
		packets.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,Integer>("packets"));
		
		billingweight.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,Double>("billingweight"));
		insuracneamount.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,Double>("insuracneamount"));
		shipercost.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,Double>("shipercost"));
		cod.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,Double>("cod"));
		fod.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,Double>("fod"));
		docketcharge.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,Double>("docketcharge"));
		otherVas.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,Double>("otherVas"));
		amount.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,Double>("amount"));
		fuel.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,Double>("fuel"));
		servicetax.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,Double>("servicetax"));
		total.setCellValueFactory(new PropertyValueFactory<ZoneJavaFXBean,Double>("total"));
		//btnGenerate.setDisable(true);
		pagination.setVisible(false);
		
	}

}
