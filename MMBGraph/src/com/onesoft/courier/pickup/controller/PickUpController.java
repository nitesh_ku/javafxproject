package com.onesoft.courier.pickup.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.DB.DBconnection;
import com.onesoft.courier.common.LoadDestination;
import com.onesoft.courier.common.LoadNetworks;
import com.onesoft.courier.common.LoadServiceGroups;
import com.onesoft.courier.complaint.bean.ComplaintPendingTableBean;
import com.onesoft.courier.complaint.bean.ComplaintRegisterBean;
import com.onesoft.courier.complaint.utils.ComplaintUtils;
import com.onesoft.courier.graphs.utils.FilterUtils;
import com.onesoft.courier.pickup.bean.*;
import com.onesoft.courier.pickup.utils.MailUtils;
import com.onesoft.courier.pickup.utils.PickupUtils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class PickUpController implements Initializable {
	
	private ObservableList<PendingPickupTableBean> pendingPickupTabledata=FXCollections.observableArrayList();
	private ObservableList<PendingPickupTableBean> receivePickupTabledata=FXCollections.observableArrayList();
	
	private ObservableList<String> comboBoxStatusItems=FXCollections.observableArrayList(PickupUtils.STATUSPENDING,PickupUtils.STATUSRECEIVE,PickupUtils.STATUSCANCEL);
	private ObservableList<String> comboBoxCountryItems=FXCollections.observableArrayList(PickupUtils.COUNTRYINDIA);
	private ObservableList<String> comboBoxCityItems=FXCollections.observableArrayList(PickupUtils.CITYDELHI,PickupUtils.CITYGURGAON,PickupUtils.CITYNOIDA,
																				PickupUtils.CITYBHATINDA,PickupUtils.CITYCHANDIGARH,PickupUtils.CITYFARIDABAD,
																				PickupUtils.CITYHISAR,PickupUtils.CITYMUMBAI,PickupUtils.CITYSURAT);
	
	private ObservableList<String> comboBoxStateItems=FXCollections.observableArrayList(PickupUtils.STATEDELHI,PickupUtils.STATAHARYANA,PickupUtils.STATAGUJRAT,
																				PickupUtils.STATAMAHARASTRA,PickupUtils.STATAPUNJAB,PickupUtils.STATAUP);
	
	private ObservableList<String> comboBoxTypeItems=FXCollections.observableArrayList(PickupUtils.TYPEDOX,PickupUtils.TYPENONDOX);
	private ObservableList<String> comboBoxNetworkItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxServiceItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxDestinationItems=FXCollections.observableArrayList();
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	
	@FXML
	private ImageView imgSearchIcon;
	
	@FXML
	private ImageView imgSearchIcon1;
	
	@FXML
	private RadioButton rdnew;
	
	@FXML
	private RadioButton rdmodify;

	@FXML
	private TextField txtpickupno;
	
	@FXML
	private TextField txtphoneno;
	
	@FXML
	private TextField txtname;
	
	@FXML
	private TextField txtaddress;
	
	@FXML
	private TextField txtzipcode;
	
	@FXML
	private TextField txtemail;
	
	@FXML
	private TextField txtpkgs;
	
	@FXML
	private TextField txtbillingweight;
	
	@FXML
	private TextField txtamount;
	
	@FXML
	private TextField txttime;
	
	@FXML
	private TextField txtpickupBoyName;
	
	@FXML
	private TextField txtremarks;
	
	@FXML
	private TextArea txtareaTrackPickup;
	
	@FXML
	private ComboBox<String> comboBoxStatus;
	
	@FXML
	private ComboBox<String> comboBoxCity;
	
	@FXML
	private ComboBox<String> comboBoxState;
	
	@FXML
	private ComboBox<String> comboBoxCountry;
	
	@FXML
	private ComboBox<String> comboBoxDestination;
	
	@FXML
	private ComboBox<String> comboBoxService;
	
	@FXML
	private ComboBox<String> comboBoxNetwork;
	
	@FXML
	private ComboBox<String> comboBoxType;
	
	@FXML
	private ComboBox<String> comboBoxPickupBoyCode;
	
	@FXML
	private DatePicker dpkPickupDate;
	
	@FXML
	private CheckBox chkSMS;
	
	@FXML
	private CheckBox chkEmail;
	
	@FXML
	private Button btnNewPickup;
	
	@FXML
	private Button btnExit;
	
	@FXML
	private Button btnReset;
	
//================================================================

	@FXML
	private TableView<PendingPickupTableBean> pendingPickupTable;
		
	@FXML
	private TableColumn<PendingPickupTableBean, Integer> pendingtable_serialno;
	
	@FXML
	private TableColumn<PendingPickupTableBean, String> pendingtable_pickupno;
	
	@FXML
	private TableColumn<PendingPickupTableBean, String> pendingtable_status;
	
	@FXML
	private TableColumn<PendingPickupTableBean, String> pendingtable_pickupBy;
	
	@FXML
	private TableColumn<PendingPickupTableBean, String> pendingtable_destination;
	
	@FXML
	private TableColumn<PendingPickupTableBean, String> pendingtable_network;
	
	@FXML
	private TableColumn<PendingPickupTableBean, String> pendingtable_service;
	
	@FXML
	private TableColumn<PendingPickupTableBean, String> pendingtable_indate;
	
	@FXML
	private TableColumn<PendingPickupTableBean, String> pendingtable_pickupDate;
	
	@FXML
	private TableColumn<PendingPickupTableBean, Long> pendingtable_phoneno;
	
	@FXML
	private TableColumn<PendingPickupTableBean, String> pendingtable_remarks;
	
	
//=================================================================================================================================

	@FXML
	private TableView<PendingPickupTableBean> receivePickupTable;
		
	@FXML
	private TableColumn<PendingPickupTableBean, Integer> receivetable_serialno;
	
	@FXML
	private TableColumn<PendingPickupTableBean, String> receivetable_pickupno;
	
	/*@FXML
	private TableColumn<PendingPickupTableBean, String> receivetable_status;*/
	
	@FXML
	private TableColumn<PendingPickupTableBean, String> receivetable_pickupBy;
	
	@FXML
	private TableColumn<PendingPickupTableBean, String> receivetable_destination;
	
	@FXML
	private TableColumn<PendingPickupTableBean, String> receivetable_network;
	
	@FXML
	private TableColumn<PendingPickupTableBean, String> receivetable_service;
	
	@FXML
	private TableColumn<PendingPickupTableBean, String> receivetable_indate;
	
	@FXML
	private TableColumn<PendingPickupTableBean, String> receivetable_pickupDate;
	
	@FXML
	private TableColumn<PendingPickupTableBean, Long> receivetable_phoneno;
	
	@FXML
	private TableColumn<PendingPickupTableBean, String> receivetable_remarks;
	
	
// ===============================================================================================================================
	
	public void loadAllNetworks() throws SQLException
	{
		
		new	LoadNetworks().loadAllNetworks();
		
		System.out.println("Network Contorller ============= LoadNetworks from LoadNetwork Class: =========== ");
		
		//System.out.println("Network Contorller >> Load from Global set object");
		for(String s:LoadNetworks.SET_LOAD_NETWORK_FOR_ALL)
		{
			comboBoxNetworkItems.add(s);
			
		}
		comboBoxNetwork.setItems(comboBoxNetworkItems);		
		
		/*DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		Set<String> set = new HashSet<String>();
	
		try
		{	
			st=con.createStatement();
			String sql="select code from vendormaster where vendor_type='N'";
			rs=st.executeQuery(sql);
			//comboBoxNetwork.setValue(FilterUtils.NETWORK1);
			//comboBoxNetworkItems.add(NetworkReportUtils.NETWORK1);
			while(rs.next())
			{
				set.add(rs.getString(1));
			}
	
			for(String s:set)
			{
				comboBoxNetworkItems.add(s);
			}
		
			comboBoxNetwork.setItems(comboBoxNetworkItems);
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}*/
	}
	
	
// ================================================================================================================================
	
	public void loadAllServices() throws SQLException
	{
		
		new	LoadServiceGroups().loadServiceGroup();
		
		System.out.println("Service Group Contorller ============= LoadService from LoadServiceGrp Class: =========== ");
		
		//System.out.println("Network Contorller >> Load from Global set object");
		for(String s:LoadServiceGroups.SET_LOAD_SERVICES_FOR_ALL)
		{
			comboBoxServiceItems.add(s);
		}
		comboBoxService.setItems(comboBoxServiceItems);
		
		/*DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		//Set<String> set = new HashSet<String>();
	
		try
		{	
			st=con.createStatement();
			String sql="select sgtp.code from servicetype as stp, servicegrouptype as sgtp where stp.service2group=sgtp.code GROUP BY sgtp.code";
			rs=st.executeQuery(sql);
			//comboBoxService.setValue(FilterUtils.ALLSERVICEGROUP);
			//comboBoxNetworkItems.add("All Networks");
			while(rs.next())
			{
				comboBoxServiceItems.add(rs.getString("code"));
			//	set.add(rs.getString(2));
			}
	
			for(String s:set)
			{
				//comboBoxNetworkItems.add(s);
			}
		
			comboBoxService.setItems(comboBoxServiceItems);
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}*/
	}
	
	
// ================================================================================================================================
	
	public void loadAllDestinations() throws SQLException
	{
		new	LoadDestination().loadDestinations();
		
		System.out.println("Pickup Contorller ============= LoadDestination from LoadDestination Class: =========== ");
		
		//System.out.println("Network Contorller >> Load from Global set object");
		for(String s:LoadDestination.SET_LOAD_DESTINATION_FOR_ALL)
		{
			comboBoxDestinationItems.add(s);
		}
		comboBoxDestination.setItems(comboBoxDestinationItems);
		
		
		
		/*DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		//Set<String> set = new HashSet<String>();
	
		try
		{	
			st=con.createStatement();
			String sql="select city_name from city";
			rs=st.executeQuery(sql);
			//comboBoxService.setValue(FilterUtils.ALLSERVICEGROUP);
			//comboBoxNetworkItems.add("All Networks");
			while(rs.next())
			{
				comboBoxDestinationItems.add(rs.getString("city_name"));
			//	set.add(rs.getString(2));
			}
	
			for(String s:set)
			{
				//comboBoxNetworkItems.add(s);
			}
		
			comboBoxDestination.setItems(comboBoxDestinationItems);
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}*/
	}

	
// ================================================================================================================================	

	public void setValidation() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		
		String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		String email=txtemail.getText();
	    Pattern pattern = Pattern.compile(regex);
	    Matcher matcher = pattern.matcher((CharSequence) email);
	    boolean checkmail=matcher.matches();
		
	    
	    if(txtpickupno.isDisable()==false)
	  	{
	  		if(txtpickupno.getText()!=null && txtpickupno.getText().isEmpty())
	  		{
	  			alert.setTitle("Empty Field");
	  			alert.setContentText("Pickup No. field is Empty!");
	  			alert.showAndWait();
	  			txtpickupno.requestFocus();
	  		}
	  	}
	     
	    else if(txtphoneno.getText()!=null && txtphoneno.getText().isEmpty())
		{
			alert.setTitle("Empty Field");
			alert.setContentText("Phone no. field is Empty!");
			alert.showAndWait();
			txtphoneno.requestFocus();
		}
		else if(!txtphoneno.getText().matches("[0-9]*"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Please enter only numeric value");
			alert.showAndWait();
			txtphoneno.requestFocus();
		}
		else if(txtphoneno.getText().length()>10 || txtphoneno.getText().length()<10)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Please enter 10 digit mobile number");
			alert.showAndWait();
			txtphoneno.requestFocus();
		}
		else if(txtname.getText()!=null && txtname.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Name field is Empty!");
			alert.showAndWait();
			txtname.requestFocus();
		}
		else if(txtaddress.getText()!=null && txtaddress.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Address field is Empty!");
			alert.showAndWait();
			txtaddress.requestFocus();
		}
		else if(comboBoxCity.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("City field is Empty!");
			alert.showAndWait();
			comboBoxCity.requestFocus();
		}
		else if(comboBoxState.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("State field is Empty!");
			alert.showAndWait();
			comboBoxState.requestFocus();
		}
		else if(comboBoxCountry.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Country field is Empty!");
			alert.showAndWait();
			comboBoxCountry.requestFocus();
		}
		else if(txtzipcode.getText()!=null && txtzipcode.getText().isEmpty())
		{
			alert.setTitle("Empty Field");
			alert.setContentText("Zipcode field is Empty!");
			alert.showAndWait();
			txtzipcode.requestFocus();
		}
		else if(!txtzipcode.getText().matches("[0-9]*"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Please enter only numeric value");
			alert.showAndWait();
			txtzipcode.requestFocus();
		}
		else if(txtzipcode.getText().length()>6 || txtzipcode.getText().length()<6)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Please enter 6 digit mobile number");
			alert.showAndWait();
			txtzipcode.requestFocus();
		}
		else if(txtemail.getText()!=null && txtemail.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Email field is Empty!");
			alert.showAndWait();
			txtemail.requestFocus();
		}
		else if(checkmail==false)
    	{
    		alert.setTitle("Complaint Registration");
			alert.setContentText("Please enter a valid Email ID");
			alert.showAndWait();
			txtemail.requestFocus();
    	}
		
		/*else if(comboBoxDestination.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Destination field is Empty!");
			alert.showAndWait();
			comboBoxDestination.requestFocus();
		}
		else if(comboBoxNetwork.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Network field is Empty!");
			alert.showAndWait();
			comboBoxNetwork.requestFocus();
		}
		else if(comboBoxService.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Service field is Empty!");
			alert.showAndWait();
			comboBoxService.requestFocus();
		}*/
		
		else if(comboBoxType.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Type field is Empty!");
			alert.showAndWait();
			comboBoxType.requestFocus();
		}
		else if(txtpkgs.getText()!=null && txtpkgs.getText().isEmpty())
		{
			alert.setTitle("Empty Field");
			alert.setContentText("Pkgs field is Empty!");
			alert.showAndWait();
			txtpkgs.requestFocus();
		}
		else if(!txtpkgs.getText().matches("[0-9]*"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Please enter only numeric value");
			alert.showAndWait();
			txtpkgs.requestFocus();
		}
		else if(txtbillingweight.getText()!=null && txtbillingweight.getText().isEmpty())
		{
			alert.setTitle("Empty Field");
			alert.setContentText("Billing Weight field is Empty!");
			alert.showAndWait();
			txtbillingweight.requestFocus();
		}
		//else if(!txtTest.getText().matches("[0-9]*-?\\d+(\\.\\d+)?"))
		else if(!txtbillingweight.getText().matches("-?\\d+(\\.\\d+)?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Please enter only numeric value");
			alert.showAndWait();
			txtbillingweight.requestFocus();
		}
		else if(txtamount.getText()!=null && txtamount.getText().isEmpty())
		{
			alert.setTitle("Empty Field");
			alert.setContentText("Amount field is Empty!");
			alert.showAndWait();
			txtamount.requestFocus();
		}
		else if(!txtamount.getText().matches("-?\\d+(\\.\\d+)?"))
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Please enter only numeric value");
			alert.showAndWait();
			txtamount.requestFocus();
		}
		else if(dpkPickupDate.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Pickup Date field is Empty!");
			alert.showAndWait();
			dpkPickupDate.requestFocus();
		}	
		else if(txttime.getText()!=null && txttime.getText().isEmpty())
		{
			alert.setTitle("Empty Field");
			alert.setContentText("Time field is Empty!");
			alert.showAndWait();
			txttime.requestFocus();
		}
		else if(txtpickupBoyName.getText()!=null && txtpickupBoyName.getText().isEmpty())
		{
			alert.setTitle("Empty Field");
			alert.setContentText("Pickup Boy Name field is Empty!");
			alert.showAndWait();
			txtpickupBoyName.requestFocus();
		}
		else if(txtremarks.getText()!=null && txtremarks.getText().isEmpty())
		{
			alert.setTitle("Empty Field");
			alert.setContentText("Remarks field is Empty!");
			alert.showAndWait();
			txtremarks.requestFocus();
		}
		else if(txtremarks.getText().length()>160)
		{
			alert.setTitle("Empty Field");
			alert.setContentText("Remarks should be less then 160 character!");
			alert.showAndWait();
			txtremarks.requestFocus();
		}
		else
		{
			savePickupDetails();
		}
			
		
	}
	
// ================================================================================================================================
	
	public String generatePickupnumber() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		PickupBean pkbean=new PickupBean();
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select pickup_no from pickupdetails order by pickup_no DESC limit 1";
			rs=st.executeQuery(sql);
			
			if(!rs.next())
			{
				pkbean.setPickupno("null");
			}
			else
			{
				pkbean.setPickup_startno(rs.getString("pickup_no").replaceAll("[^A-Za-z]+", ""));
				pkbean.setPickup_endno(Integer.parseInt(rs.getString("pickup_no").replaceAll("\\D+",""))+1);
				pkbean.setPickupno(pkbean.getPickup_startno()+pkbean.getPickup_endno());
				
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
		return pkbean.getPickupno();
	}
	
	
// ================================================================================================================================	
	
	public void savePickupDetails() throws SQLException
	{
		
		LocalDate localindate = LocalDate.now();
		LocalDate pickuplocatdate=dpkPickupDate.getValue();
		
		Date pickupdate=Date.valueOf(pickuplocatdate);
		Date indate=Date.valueOf(localindate);
		
		java.util.Date now = new java.util.Date();
		String currentdatetime = sdfDate.format(now);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;
		String query=null;
		
		PickupBean pkbean=new PickupBean();
		pkbean.setAutogeneratepickupno(generatePickupnumber());
		
		String firstTimepickupno="PIC1100001";
	
		pkbean.setStatus(comboBoxStatus.getValue());
		pkbean.setPhoneno(Long.parseLong(txtphoneno.getText()));
		pkbean.setName(txtname.getText());
		pkbean.setAddress(txtaddress.getText());
		pkbean.setCity(comboBoxCity.getValue());
		pkbean.setState(comboBoxState.getValue());
		pkbean.setCountry(comboBoxCountry.getValue());
		pkbean.setZipcode(Integer.parseInt(txtzipcode.getText()));
		pkbean.setEmail(txtemail.getText());
		pkbean.setPkgs(Integer.parseInt(txtpkgs.getText()));
		pkbean.setBilling_weight(Double.parseDouble(txtbillingweight.getText()));
		pkbean.setAmount(Double.parseDouble(txtamount.getText()));
		pkbean.setPickuptime(txttime.getText());
		pkbean.setPickup_boy_name(txtpickupBoyName.getText());
		pkbean.setRemarks(txtremarks.getText());
		pkbean.setType(comboBoxType.getValue());
		pkbean.setDestination(comboBoxDestination.getValue());
		pkbean.setNetwork(comboBoxNetwork.getValue());
		pkbean.setService(comboBoxService.getValue());
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Pickup Details");
		alert.setHeaderText(null);
		
		try
		{	
			
			
			if(rdnew.isSelected()==true && txtpickupno.isDisable()==true)
			{
				if(pkbean.getAutogeneratepickupno().equals("null"))
				{
					pkbean.setPickupno(firstTimepickupno);
				}
				
				else
				{
					pkbean.setPickupno(pkbean.getAutogeneratepickupno());
				}
				
			query = "insert into pickupdetails(pickup_no,name,phoneno,address,zipcode,city,state,country,email,status,package,billing_weight,amount,"
							+ "pickupdate,indate,pickuptime,pickup_boy_name,remarks,pickuptrackdetails,type,destination,network,service,read_unread_status) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			
			preparedStmt = con.prepareStatement(query);
		
			preparedStmt.setString(1, pkbean.getPickupno());
			preparedStmt.setString(2, pkbean.getName());
			preparedStmt.setLong(3, pkbean.getPhoneno());
			preparedStmt.setString(4, pkbean.getAddress());
			preparedStmt.setInt(5, pkbean.getZipcode());
			preparedStmt.setString(6, pkbean.getCity());
			preparedStmt.setString(7, pkbean.getState());
			preparedStmt.setString(8, pkbean.getCountry());
			preparedStmt.setString(9, pkbean.getEmail());
			preparedStmt.setString(10, pkbean.getStatus());
			preparedStmt.setInt(11, pkbean.getPkgs());
			preparedStmt.setDouble(12, pkbean.getBilling_weight());
			preparedStmt.setDouble(13, pkbean.getAmount());
			preparedStmt.setDate(14,pickupdate);
			preparedStmt.setDate(15, indate);
			preparedStmt.setString(16, pkbean.getPickuptime());
			preparedStmt.setString(17, pkbean.getPickup_boy_name());
			preparedStmt.setString(18, pkbean.getRemarks());
			preparedStmt.setString(19, "User1 "+currentdatetime+" "+pkbean.getStatus());
			preparedStmt.setString(20, pkbean.getType());
			preparedStmt.setString(21, pkbean.getDestination());
			preparedStmt.setString(22, pkbean.getNetwork());
			preparedStmt.setString(23, pkbean.getService());
			preparedStmt.setBoolean(24, true);
			
			
			}
			else
			{
				String userdetails=null;
				pkbean.setPickupno(txtpickupno.getText());
				
				String[] trackuser=txtareaTrackPickup.getText().split("\\,");
				
				query="update pickupdetails set  name=?,phoneno=?,address=?,zipcode=?,city=?,state=?,country=?,email=?,status=?,package=?,billing_weight=?,"
								+ "amount=?,pickupdate=?,indate=?,pickuptime=?,pickup_boy_name=?,remarks=?,pickuptrackdetails=?,type=?,"
										+ "destination=?,network=?,service=? where pickup_no=?";
				
				preparedStmt = con.prepareStatement(query);
				
				preparedStmt.setString(1, pkbean.getName());
				preparedStmt.setLong(2, pkbean.getPhoneno());
				preparedStmt.setString(3, pkbean.getAddress());
				preparedStmt.setInt(4, pkbean.getZipcode());
				preparedStmt.setString(5, pkbean.getCity());
				preparedStmt.setString(6, pkbean.getState());
				preparedStmt.setString(7, pkbean.getCountry());
				preparedStmt.setString(8, pkbean.getEmail());
				preparedStmt.setString(9, pkbean.getStatus());
				preparedStmt.setInt(10, pkbean.getPkgs());
				preparedStmt.setDouble(11, pkbean.getBilling_weight());
				preparedStmt.setDouble(12, pkbean.getAmount());
				preparedStmt.setDate(13,pickupdate);
				preparedStmt.setDate(14, indate);
				preparedStmt.setString(15, pkbean.getPickuptime());
				preparedStmt.setString(16, pkbean.getPickup_boy_name());
				preparedStmt.setString(17, pkbean.getRemarks());
				
				if(trackuser.length>4)
				{
					userdetails=trackuser[0]+","+trackuser[1]+","+trackuser[2]+","+trackuser[3];	
					preparedStmt.setString(18, "User1 "+currentdatetime+" , "+userdetails);
				}
				else
				{
					preparedStmt.setString(18, "User1 "+currentdatetime+" "+pkbean.getStatus()+" , "+txtareaTrackPickup.getText());
				}
				
				preparedStmt.setString(19, pkbean.getType());
				preparedStmt.setString(20, pkbean.getDestination());
				preparedStmt.setString(21, pkbean.getNetwork());
				preparedStmt.setString(22, pkbean.getService());
				preparedStmt.setString(23, pkbean.getPickupno());
				
				
			}
			
			preparedStmt.executeUpdate();
			
			mailCheckbox(pkbean.getPickupno());
			reset();
		
			loadRevcivePickupTable();
			loadPendingPickupTable();
			txtpickupno.setEditable(true);
			
			if(rdnew.isSelected()==true && txtpickupno.isDisable()==true)
			{
				alert.setContentText("Pickup details is successfully received!"
						+ "\n"
						+ "Pickup no. is: "+pkbean.getPickupno());
				alert.showAndWait();
			}
			else
			{
				alert.setContentText("Pickup details against pickup no.: "+pkbean.getPickupno()+" \nis successfully Updated!");
				alert.showAndWait();
			}
			
		txtphoneno.requestFocus();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
	}
	
	
// ================================================================================================================================
	
	public void getdetailsViaPickupNo() throws SQLException
	{
		boolean checkPickupDate=false;
	
		if(!txtphoneno.getText().equals(""))
		{
			txtpickupno.clear();
		}
		
		if(rdmodify.isSelected()==true)
		{
		PickupBean pkbean=new PickupBean();
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		ResultSet rs=null;
		
		PreparedStatement preparedStmt=null;
		
		try
		{	
			
			if(!txtpickupno.getText().equals(""))
			{
				
				System.out.println("pickup number is runnings");
			pkbean.setPickupno(txtpickupno.getText());
			String query = "select pickup_no,phoneno,name,address,city,state,country,zipcode,email,package,billing_weight,amount,pickupdate,pickuptime,"
							+ "pickup_boy_name,remarks,status,pickuptrackdetails,type,destination,network,service from pickupdetails where pickup_no=?";
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setString(1, pkbean.getPickupno());
			rs = preparedStmt.executeQuery();
			}
			else
			{
				System.out.println("phone number is runnings");
			pkbean.setPhoneno(Long.parseLong(txtphoneno.getText()));
			String query = "select pickup_no,phoneno,name,address,city,state,country,zipcode,email,package,billing_weight,amount,pickupdate,pickuptime,"
							+ "pickup_boy_name,remarks,status,pickuptrackdetails,type,destination,network,service from pickupdetails where phoneno=?";
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setLong(1, pkbean.getPhoneno());
			rs = preparedStmt.executeQuery();
			}
			
			
			
			
			if(!rs.next())
			{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Record not found");
			alert.setHeaderText(null);
			alert.setContentText("Eetered Pickup No./Phone No. does not exist");
			alert.showAndWait();
			txtpickupno.requestFocus();
				
			}
			else
			{
				do{
					System.out.println("Pickup date: "+rs.getDate("pickupdate"));
					
					pkbean.setPickupno(rs.getString("pickup_no"));
					pkbean.setPhoneno(rs.getLong("phoneno"));
					pkbean.setName(rs.getString("name"));
					pkbean.setAddress(rs.getString("address"));
					pkbean.setCity(rs.getString("city"));
					pkbean.setState(rs.getString("state"));
					pkbean.setCountry(rs.getString("country"));
					pkbean.setZipcode(rs.getInt("zipcode"));
					pkbean.setEmail(rs.getString("email"));
					pkbean.setPkgs(rs.getInt("package"));
					pkbean.setBilling_weight(rs.getDouble("billing_weight"));
					pkbean.setAmount(rs.getDouble("amount"));
					
					if(rs.getDate("pickupdate")!=null)
					{
						checkPickupDate=true;
					pkbean.setPickupdate(date.format(rs.getDate("pickupdate")));
					}
					
					pkbean.setPickuptime(rs.getString("pickuptime"));
					pkbean.setPickup_boy_name(rs.getString("pickup_boy_name"));
					pkbean.setRemarks(rs.getString("remarks"));
					pkbean.setStatus(rs.getString("status"));
					pkbean.setPickuptrackdetails(rs.getString("pickuptrackdetails"));
					pkbean.setType(rs.getString("type"));
					pkbean.setDestination(rs.getString("destination"));
					pkbean.setNetwork(rs.getString("network"));
					pkbean.setService(rs.getString("service"));
				
				}while(rs.next());
				
				//reset();
				txtpickupno.setText(pkbean.getPickupno());
				txtphoneno.setText(String.valueOf(pkbean.getPhoneno()));
				txtname.setText(pkbean.getName());
				txtaddress.setText(pkbean.getAddress());
				comboBoxCity.setValue(pkbean.getCity());
				comboBoxState.setValue(pkbean.getState());
				comboBoxCountry.setValue(pkbean.getCountry());
				txtzipcode.setText(String.valueOf(pkbean.getZipcode()));
				txtemail.setText(pkbean.getEmail());
				txtpkgs.setText(String.valueOf(pkbean.getPkgs()));
				txtbillingweight.setText(String.valueOf(pkbean.getBilling_weight()));
				txtamount.setText(String.valueOf(pkbean.getAmount()));
				
				if(checkPickupDate==true)
				{
				dpkPickupDate.setValue(LocalDate.parse(pkbean.getPickupdate(),localdateformatter));
				checkPickupDate=false;
				}
				
				txttime.setText(pkbean.getPickuptime());
				txtpickupBoyName.setText(pkbean.getPickup_boy_name());
				txtremarks.setText(pkbean.getRemarks());
				comboBoxStatus.setValue(pkbean.getStatus());
				txtareaTrackPickup.setText(pkbean.getPickuptrackdetails());
				comboBoxType.setValue(pkbean.getType());
				comboBoxDestination.setValue(pkbean.getDestination());
				comboBoxNetwork.setValue(pkbean.getNetwork());
				comboBoxService.setValue(pkbean.getService());
				
				txtpickupno.setEditable(false);
				
				//System.out.println(LocalDate.parse(pkbean.getPickupdate(),localdateformatter));
			}
		
			
			
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
	}	
	}

// ================================================================================================================================	
	
	public void loadPendingPickupTable() throws SQLException
	{
		pendingPickupTabledata.clear();
		
		PickupBean pkbean=new PickupBean();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		try
		{	
			st=con.createStatement();
			String sql="select pickup_no,status,pickup_boy_name,indate,pickupdate,phoneno,remarks,destination,network,service from pickupdetails where status!='"+PickupUtils.STATUSRECEIVE+"' order by pickup_no";
			rs=st.executeQuery(sql);
	
			int serialno=1;
			
			if(!rs.next())
			{
			
			}
			else
			{
				do
				{
					pkbean.setSerialno(serialno);
					pkbean.setPickupno(rs.getString("pickup_no"));
					pkbean.setStatus(rs.getString("status"));
					pkbean.setPickup_boy_name(rs.getString("pickup_boy_name"));
					pkbean.setIndate(date.format(rs.getDate("indate")));
					pkbean.setPickupdate(date.format(rs.getDate("pickupdate")));
					pkbean.setPhoneno(rs.getLong("phoneno"));
					pkbean.setRemarks(rs.getString("remarks"));
					pkbean.setDestination(rs.getString("destination"));
					pkbean.setNetwork(rs.getString("network"));
					pkbean.setService(rs.getString("service"));
			
					pendingPickupTabledata.addAll(new PendingPickupTableBean(pkbean.getSerialno(), pkbean.getPickupno(),pkbean.getStatus(),
											pkbean.getPickup_boy_name(),pkbean.getIndate(),pkbean.getPickupdate(),pkbean.getPhoneno(),
											pkbean.getRemarks(),pkbean.getDestination(),pkbean.getNetwork(),pkbean.getService()));
					
					serialno++;
				}
				while(rs.next());
				
				pendingPickupTable.setItems(pendingPickupTabledata);
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
	

// ================================================================================================================================	

	public void loadRevcivePickupTable() throws SQLException
	{
		receivePickupTabledata.clear();
		
		PickupBean pkbean=new PickupBean();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		try
		{	
			st=con.createStatement();
			String sql="select pickup_no,pickup_boy_name,indate,pickupdate,phoneno,remarks,destination,network,service from pickupdetails where status='"+PickupUtils.STATUSRECEIVE+"' order by pickup_no";
			rs=st.executeQuery(sql);
	
			int serialno=1;
			
			if(!rs.next())
			{
			
			}
			else
			{
				do
				{
					pkbean.setSerialno(serialno);
					pkbean.setPickupno(rs.getString("pickup_no"));
					//pkbean.setStatus(rs.getString("status"));
					pkbean.setPickup_boy_name(rs.getString("pickup_boy_name"));
					pkbean.setIndate(date.format(rs.getDate("indate")));
					pkbean.setPickupdate(date.format(rs.getDate("pickupdate")));
					pkbean.setPhoneno(rs.getLong("phoneno"));
					pkbean.setRemarks(rs.getString("remarks"));
					pkbean.setDestination(rs.getString("destination"));
					pkbean.setNetwork(rs.getString("network"));
					pkbean.setService(rs.getString("service"));
			
					receivePickupTabledata.addAll(new PendingPickupTableBean(pkbean.getSerialno(), pkbean.getPickupno(),null,
											pkbean.getPickup_boy_name(),pkbean.getIndate(),pkbean.getPickupdate(),
											pkbean.getPhoneno(),pkbean.getRemarks(),pkbean.getDestination(),pkbean.getNetwork(),pkbean.getService()));
					
					serialno++;
				}
				while(rs.next());
				
				receivePickupTable.setItems(receivePickupTabledata);
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}	

	
	
// ====================================================================================================================	

	public void mailCheckbox(String pickupno) throws SQLException
	{
		if(chkEmail.isSelected()==true)
		{
			sendMail(pickupno);
		}
	}	

	
// ====================================================================================================================	

	public void sendMail(String pickupno)
	{	
		LocalDate pickuplocatdate=dpkPickupDate.getValue();
		Date pickupdate=Date.valueOf(pickuplocatdate);
	
		String msg= "Pickup Details:"
				+ "<table border=2 bordercolor=black>"
					+ "<tr>"
						+ "<td><b>Pickup No.</b></td>"
						+ "<td><b>Name</b></td>"
						+ "<td><b>Phone No.</b></td>"
						+ "<td><b>Status</b></td>"
						+ "<td><b>Pickup By</b></td>"
						+ "<td><b>Remarks</b></td>"
						+ "<td><b>Pickup Date</b></td>"
						+ "<td><b>Pickup Time</b></td>"
					+ "</tr>"
					+ "<tr>"
						+ "<td>"+pickupno+"</td>"
						+ "<td>"+txtname.getText()+"</td>"
						+ "<td>"+txtphoneno.getText()+"</td>"
						+ "<td>"+comboBoxStatus.getValue()+"</td>"
						+ "<td>"+txtpickupBoyName.getText()+"</td>"
						+ "<td>"+txtremarks.getText()+"</td>"
						+ "<td>"+date.format(pickupdate)+"</td>"
						+ "<td>"+txttime.getText()+"</td>"
					+ "</tr>"
				+ "</table>";
		
		
		Properties props = new Properties(); 
		props.put("mail.smtp.host",MailUtils.HOST); 
		props.put("mail.smtp.auth", MailUtils.SMTPTRUE);
		props.put("mail.smtp.port",MailUtils.SMTPPORTNO);
					
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
		protected PasswordAuthentication getPasswordAuthentication() {  
		   return new PasswordAuthentication(MailUtils.USERNAME,MailUtils.PASSWORD);  
				      }  
				  });  
						   
		try
		{  
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(MailUtils.FROM));
			message.addRecipient(Message.RecipientType.TO,new InternetAddress(MailUtils.TO));
			message.setSubject("Pickup Details");
		
			
			message.setContent(msg,"text/html");
			Transport.send(message);
						    	
			System.out.println("Email send Successfully");
		}
		catch (MessagingException e)
		{
			e.printStackTrace();
		} 
		
		 System.out.println("Authentication method is closed");
		 System.out.println("\n");
		
	}		
	

// ================================================================================================================================	
	
	public void testradio()
	{
		if(rdnew.isSelected()==true)
		{
			txtpickupno.setDisable(true);
			reset();
		}
		else
		{
			txtpickupno.setDisable(false);
			txtpickupno.setEditable(true);
			txtpickupno.requestFocus();
			reset();
		}
	}
	
// ================================================================================================================================	

	public void reset()
	{
		txtpickupno.clear();
		comboBoxStatus.setValue(PickupUtils.STATUSPENDING);
		txtphoneno.clear();
		txtname.clear();;
		txtaddress.clear();
		comboBoxCity.getSelectionModel().clearSelection();
		comboBoxState.getSelectionModel().clearSelection();
		comboBoxCountry.getSelectionModel().clearSelection();
		txtzipcode.clear();
		txtemail.clear();
		comboBoxDestination.getSelectionModel().clearSelection();
		comboBoxService.getSelectionModel().clearSelection();
		comboBoxNetwork.getSelectionModel().clearSelection();
		comboBoxType.getSelectionModel().clearSelection();
		txtpkgs.clear();
		txtbillingweight.clear();
		txtamount.clear();
		dpkPickupDate.getEditor().clear();
		txttime.clear();
		
		comboBoxPickupBoyCode.getSelectionModel().clearSelection();
		txtpickupBoyName.clear();
		txtremarks.clear();
		txtareaTrackPickup.clear();
		txtpickupno.setEditable(true);
		
	}
	
	
	
// ================================================================================================================================
	

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException{

		Node n=(Node) e.getSource();
		
		
		if(n.getId().equals("txtpickupno"))						// Enter Key date picker
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(txtpickupno.getText().equals("") || txtpickupno.getText().isEmpty())
				{
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Empty field");
					alert.setHeaderText(null);
					alert.setContentText("Please enter the Pickup No.");
					alert.showAndWait();
					txtpickupno.requestFocus();
				}
				else
				{
				comboBoxStatus.requestFocus();
				}
			}
		}
		
		else if(n.getId().equals("comboBoxStatus"))				// Enter Keyevent for Expense Textfield
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtphoneno.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtphoneno"))						// Enter Keyevent for Amount Textfield
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtname.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtname"))						// Enter Keyevent for CGST Textfield
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtaddress.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtaddress"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxCity.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxCity"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxState.requestFocus();
			}
		}
		else if(n.getId().equals("comboBoxState"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxCountry.requestFocus();
			}
		}
		else if(n.getId().equals("comboBoxCountry"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtzipcode.requestFocus();
			}
		}
		else if(n.getId().equals("txtzipcode"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtemail.requestFocus();
			}
		}
		else if(n.getId().equals("txtemail"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxDestination.requestFocus();
			}
		}
		else if(n.getId().equals("comboBoxDestination"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxNetwork.requestFocus();
			}
		}
		else if(n.getId().equals("comboBoxNetwork"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxService.requestFocus();
			}
		}
		else if(n.getId().equals("comboBoxService"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxType.requestFocus();
			}
		}
		else if(n.getId().equals("comboBoxType"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtpkgs.requestFocus();
			}
		}
		else if(n.getId().equals("txtpkgs"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtbillingweight.requestFocus();
			}
		}
		else if(n.getId().equals("txtbillingweight"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtamount.requestFocus();
			}
		}
		else if(n.getId().equals("txtamount"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				dpkPickupDate.requestFocus();
			}
		}
		else if(n.getId().equals("dpkPickupDate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txttime.requestFocus();
			}
		}
		else if(n.getId().equals("txttime"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxPickupBoyCode.requestFocus();
			}
		}
		else if(n.getId().equals("comboBoxPickupBoyCode"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtpickupBoyName.requestFocus();
			}
		}
		else if(n.getId().equals("txtpickupBoyName"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtremarks.requestFocus();
			}
		}
		else if(n.getId().equals("txtremarks"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				chkSMS.requestFocus();
			}
		}
		else if(n.getId().equals("chkSMS"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				chkEmail.requestFocus();
			}
		}
		
		else if(n.getId().equals("chkEmail"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				btnNewPickup.requestFocus();
			}
		}
		else if(n.getId().equals("btnNewPickup"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				setValidation();
				
				
			}
		}
		
		
	}

// ================================================================================================================================	

	@Override
	public void initialize(URL location, ResourceBundle resources) 
	{
		imgSearchIcon.setImage(new Image("/com/onesoft/courier/icon/searchicon_blue.png"));
		imgSearchIcon1.setImage(new Image("/com/onesoft/courier/icon/searchicon_blue.png"));
		
		txtpickupno.setDisable(true);
		comboBoxStatus.setItems(comboBoxStatusItems);
		comboBoxCity.setItems(comboBoxCityItems);
		comboBoxState.setItems(comboBoxStateItems);
		comboBoxCountry.setItems(comboBoxCountryItems);
		comboBoxType.setItems(comboBoxTypeItems);
		comboBoxStatus.setValue(PickupUtils.STATUSPENDING);
		txtareaTrackPickup.setEditable(false);
	
		
		try
		{
			loadAllNetworks();
			loadAllServices();
			loadAllDestinations();
			loadRevcivePickupTable();
			loadPendingPickupTable();
			
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		
		// pending pickup's
		
		pendingtable_serialno.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,Integer>("serialno"));
		pendingtable_pickupno.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,String>("pickupno"));
		pendingtable_status.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,String>("status"));
		pendingtable_pickupBy.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,String>("pickupBy"));
		pendingtable_indate.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,String>("indate"));
		pendingtable_pickupDate.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,String>("pickupdate"));
		pendingtable_phoneno.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,Long>("phoneno"));
		pendingtable_remarks.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,String>("remarks"));
		pendingtable_destination.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,String>("destination"));
		pendingtable_network.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,String>("network"));
		pendingtable_service.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,String>("service"));
		
		// receive pickup's
		
		receivetable_serialno.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,Integer>("serialno"));
		receivetable_pickupno.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,String>("pickupno"));
	//	receivetable_status.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,String>("status"));
		receivetable_pickupBy.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,String>("pickupBy"));
		receivetable_indate.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,String>("indate"));
		receivetable_pickupDate.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,String>("pickupdate"));
		receivetable_phoneno.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,Long>("phoneno"));
		receivetable_remarks.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,String>("remarks"));
		receivetable_destination.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,String>("destination"));
		receivetable_network.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,String>("network"));
		receivetable_service.setCellValueFactory(new PropertyValueFactory<PendingPickupTableBean,String>("service"));
	
	}
	
}
