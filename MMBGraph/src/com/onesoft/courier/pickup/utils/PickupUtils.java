package com.onesoft.courier.pickup.utils;

public class PickupUtils {

	public static final String STATUSPENDING="Pending";
	public static final String STATUSRECEIVE="Receive";
	public static final String STATUSCANCEL="Cancel";
	
	public static final String CITYDELHI="Delhi";
	public static final String CITYGURGAON="Gurgaon";
	public static final String CITYNOIDA="Noida";
	public static final String CITYHISAR="Hisar";
	public static final String CITYMUMBAI="Mumbai";
	public static final String CITYSURAT="Surat";
	public static final String CITYCHANDIGARH="Chandigarh";
	public static final String CITYBHATINDA="Bhatinda";
	public static final String CITYFARIDABAD="Faridabad";
	
	public static final String STATEDELHI="DL";
	public static final String STATAMAHARASTRA="MH";
	public static final String STATAPUNJAB="PB";
	public static final String STATAGUJRAT="GJ";
	public static final String STATAHARYANA="HN";
	public static final String STATAUP="UP";
	
	public static final String COUNTRYINDIA="IN";
	
	public static final String TYPEDOX="Dox";
	public static final String TYPENONDOX="NonDox";
		
}
