package com.onesoft.courier.pickup.bean;

public class PickupBean {
	
	private int serialno;
	private String pickup_startno;
	private int pickup_endno;
	private String autogeneratepickupno;
	private String pickupno;
	private String name;
	private long phoneno;
	private String address;
	private int zipcode;
	private String city;
	private String state;
	private String country;
	private String email;
	private String status;
	private String destination;
	private String network;
	private String service;
	private String type;
	private int pkgs;
	private double billing_weight;
	private double amount;
	private String pickupdate;
	private String pickuptime;
	private String indate;
	private String pickup_boy_code;
	private String pickup_boy_name;
	private String remarks;
	private String pickuptrackdetails;
	
	
	public String getPickupno() {
		return pickupno;
	}
	public void setPickupno(String pickupno) {
		this.pickupno = pickupno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(long phoneno) {
		this.phoneno = phoneno;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getZipcode() {
		return zipcode;
	}
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getNetwork() {
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getPkgs() {
		return pkgs;
	}
	public void setPkgs(int pkgs) {
		this.pkgs = pkgs;
	}
	public double getBilling_weight() {
		return billing_weight;
	}
	public void setBilling_weight(double billing_weight) {
		this.billing_weight = billing_weight;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getPickupdate() {
		return pickupdate;
	}
	public void setPickupdate(String pickupdate) {
		this.pickupdate = pickupdate;
	}
	public String getPickuptime() {
		return pickuptime;
	}
	public void setPickuptime(String pickuptime) {
		this.pickuptime = pickuptime;
	}
	public String getIndate() {
		return indate;
	}
	public void setIndate(String indate) {
		this.indate = indate;
	}
	public String getPickup_boy_code() {
		return pickup_boy_code;
	}
	public void setPickup_boy_code(String pickup_boy_code) {
		this.pickup_boy_code = pickup_boy_code;
	}
	public String getPickup_boy_name() {
		return pickup_boy_name;
	}
	public void setPickup_boy_name(String pickup_boy_name) {
		this.pickup_boy_name = pickup_boy_name;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getPickup_startno() {
		return pickup_startno;
	}
	public void setPickup_startno(String pickup_startno) {
		this.pickup_startno = pickup_startno;
	}
	public int getPickup_endno() {
		return pickup_endno;
	}
	public void setPickup_endno(int pickup_endno) {
		this.pickup_endno = pickup_endno;
	}
	public String getAutogeneratepickupno() {
		return autogeneratepickupno;
	}
	public void setAutogeneratepickupno(String autogeneratepickupno) {
		this.autogeneratepickupno = autogeneratepickupno;
	}
	public int getSerialno() {
		return serialno;
	}
	public void setSerialno(int serialno) {
		this.serialno = serialno;
	}
	public String getPickuptrackdetails() {
		return pickuptrackdetails;
	}
	public void setPickuptrackdetails(String pickuptrackdetails) {
		this.pickuptrackdetails = pickuptrackdetails;
	}

}
