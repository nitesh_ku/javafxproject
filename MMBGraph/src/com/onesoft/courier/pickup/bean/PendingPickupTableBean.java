package com.onesoft.courier.pickup.bean;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public class PendingPickupTableBean {
	
	private SimpleIntegerProperty serialno;
	private SimpleStringProperty pickupno;
	private SimpleStringProperty status;
	private SimpleStringProperty pickupBy;
	private SimpleStringProperty destination;
	private SimpleStringProperty network;
	private SimpleStringProperty service;
	private SimpleStringProperty indate;
	private SimpleStringProperty pickupdate;
	private SimpleLongProperty phoneno;
	private SimpleStringProperty remarks;
	
	
	public PendingPickupTableBean(int serialno, String pickupno, String status, String pickupby, String indate, String pickupdate, long phoneno, String remarks,
				String destination, String network, String service)
	{
		super();
		this.serialno=new SimpleIntegerProperty(serialno);
		this.pickupno=new SimpleStringProperty(pickupno);
		this.status=new SimpleStringProperty(status);
		this.pickupBy=new SimpleStringProperty(pickupby);
		this.indate=new SimpleStringProperty(indate);
		this.pickupdate=new SimpleStringProperty(pickupdate);
		this.phoneno=new SimpleLongProperty(phoneno);
		this.remarks=new SimpleStringProperty(remarks);
		this.setDestination(new SimpleStringProperty(destination));
		this.setNetwork(new SimpleStringProperty(network));
		this.setService(new SimpleStringProperty(service));
	
	}
	
	
	public int getSerialno() {
		return serialno.get();
	}
	public void setSerialno(SimpleIntegerProperty serialno) {
		this.serialno = serialno;
	}
	public String getPickupno() {
		return pickupno.get();
	}
	public void setPickupno(SimpleStringProperty pickupno) {
		this.pickupno = pickupno;
	}
	public String getStatus() {
		return status.get();
	}
	public void setStatus(SimpleStringProperty status) {
		this.status = status;
	}
	public String getPickupBy() {
		return pickupBy.get();
	}
	public void setPickupBy(SimpleStringProperty pickupBy) {
		this.pickupBy = pickupBy;
	}
	public String getIndate() {
		return indate.get();
	}
	public void setIndate(SimpleStringProperty indate) {
		this.indate = indate;
	}
	public String getPickupdate() {
		return pickupdate.get();
	}
	public void setPickupdate(SimpleStringProperty pickupdate) {
		this.pickupdate = pickupdate;
	}
	public long getPhoneno() {
		return phoneno.get();
	}
	public void setPhoneno(SimpleLongProperty phoneno) {
		this.phoneno = phoneno;
	}
	public String getRemarks() {
		return remarks.get();
	}
	public void setRemarks(SimpleStringProperty remarks) {
		this.remarks = remarks;
	}


	public String getDestination() {
		return destination.get();
	}
	public void setDestination(SimpleStringProperty destination) {
		this.destination = destination;
	}
	public String getNetwork() {
		return network.get();
	}
	public void setNetwork(SimpleStringProperty network) {
		this.network = network;
	}
	public String getService() {
		return service.get();
	}
	public void setService(SimpleStringProperty service) {
		this.service = service;
	}

	

}
