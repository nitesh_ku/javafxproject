package com.onesoft.courier.account.beans;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

public class ProfitLossFXTableBean {
	
	private SimpleStringProperty exp_description;
	private SimpleStringProperty incm_description;
	
	private SimpleDoubleProperty netLoss;
	private SimpleDoubleProperty netProfit;
	
	
	
	public ProfitLossFXTableBean(String expDescription,String incmDescription, double netLoss, double netProfit)
	{
		this.exp_description=new SimpleStringProperty(expDescription);
		this.netProfit=new SimpleDoubleProperty(netProfit);
		
		this.incm_description=new SimpleStringProperty(incmDescription);
		this.netLoss=new SimpleDoubleProperty(netLoss);
	}
	
	
	public String getExp_description() {
		return exp_description.get();
	}
	public void setExp_description(SimpleStringProperty exp_description) {
		this.exp_description = exp_description;
	}
	public String getIncm_description() {
		return incm_description.get();
	}
	public void setIncm_description(SimpleStringProperty incm_description) {
		this.incm_description = incm_description;
	}
	public Double getNetLoss() {
		return netLoss.get();
	}
	public void setNetLoss(SimpleDoubleProperty netLoss) {
		this.netLoss = netLoss;
	}
	public Double getNetProfit() {
		return netProfit.get();
	}
	public void setNetProfit(SimpleDoubleProperty netProfit) {
		this.netProfit = netProfit;
	}
	
	
}
