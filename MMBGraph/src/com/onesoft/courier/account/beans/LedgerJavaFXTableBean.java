package com.onesoft.courier.account.beans;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class LedgerJavaFXTableBean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty total;
	private SimpleStringProperty party;
	private SimpleStringProperty invoiceNo;
	private SimpleStringProperty invoiceDate;
	private SimpleStringProperty fromDate;
	private SimpleStringProperty toDate;
	private SimpleStringProperty branch;
	private SimpleDoubleProperty billAmount;
	private SimpleDoubleProperty receivedAmount;
	private SimpleDoubleProperty balanceAmount;
	private SimpleDoubleProperty adjustedAmount;
	
	
	
	public LedgerJavaFXTableBean(int slno,String party,String invoiceno,String invoicedate,String fromdate,String todate,String branch,
			double billamount,double receivedamount,double adjustedamount,double balanceamount)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.party=new SimpleStringProperty(party);
		this.invoiceNo=new SimpleStringProperty(invoiceno);
		this.invoiceDate=new SimpleStringProperty(invoicedate);
		this.fromDate=new SimpleStringProperty(fromdate);
		this.toDate=new SimpleStringProperty(todate);
		this.branch=new SimpleStringProperty(branch);
		this.billAmount=new SimpleDoubleProperty(billamount);
		this.receivedAmount=new SimpleDoubleProperty(receivedamount);
		this.adjustedAmount=new SimpleDoubleProperty(adjustedamount);
		this.balanceAmount=new SimpleDoubleProperty(balanceamount);
		
	}
	
	public LedgerJavaFXTableBean(String total,String party,String invoiceno,String invoicedate,String fromdate,String todate,String branch,
			double billamount,double receivedamount,double adjustedamount,double balanceamount)
	{
		this.total=new SimpleStringProperty(total);
		this.party=new SimpleStringProperty(party);
		this.invoiceNo=new SimpleStringProperty(invoiceno);
		this.invoiceDate=new SimpleStringProperty(invoicedate);
		this.fromDate=new SimpleStringProperty(fromdate);
		this.toDate=new SimpleStringProperty(todate);
		this.branch=new SimpleStringProperty(branch);
		this.billAmount=new SimpleDoubleProperty(billamount);
		this.receivedAmount=new SimpleDoubleProperty(receivedamount);
		this.adjustedAmount=new SimpleDoubleProperty(adjustedamount);
		this.balanceAmount=new SimpleDoubleProperty(balanceamount);
		
	}
	
	

	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public String getParty() {
		return party.get();
	}
	public void setParty(SimpleStringProperty party) {
		this.party = party;
	}
	public String getInvoiceNo() {
		return invoiceNo.get();
	}
	public void setInvoiceNo(SimpleStringProperty invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getInvoiceDate() {
		return invoiceDate.get();
	}
	public void setInvoiceDate(SimpleStringProperty invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getFromDate() {
		return fromDate.get();
	}
	public void setFromDate(SimpleStringProperty fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate.get();
	}
	public void setToDate(SimpleStringProperty toDate) {
		this.toDate = toDate;
	}
	public String getBranch() {
		return branch.get();
	}
	public void setBranch(SimpleStringProperty branch) {
		this.branch = branch;
	}
	public double getBillAmount() {
		return billAmount.get();
	}
	public void setBillAmount(SimpleDoubleProperty billAmount) {
		this.billAmount = billAmount;
	}
	public double getReceivedAmount() {
		return receivedAmount.get();
	}
	public void setReceivedAmount(SimpleDoubleProperty receivedAmount) {
		this.receivedAmount = receivedAmount;
	}
	public double getBalanceAmount() {
		return balanceAmount.get();
	}
	public void setBalanceAmount(SimpleDoubleProperty balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	public double getAdjustedAmount() {
		return adjustedAmount.get();
	}
	public void setAdjustedAmount(SimpleDoubleProperty adjustedAmount) {
		this.adjustedAmount = adjustedAmount;
	}

	public String getTotal() {
		return total.get();
	}

	public void setTotal(SimpleStringProperty total) {
		this.total = total;
	}
	


}
