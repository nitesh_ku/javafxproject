package com.onesoft.courier.account.beans;

public class BalanceSheetBean {
	
	private String serialno;
	private String exp_serialno;
	private String expenseMonths;
	private double expenseAmount;
	private String incomeMonths;
	private double incomeAmount;
	
	private String exp_description;
	private String incm_description;
	
	private double expenseTotal;
	private double incomeTotal;
	
	private double netLoss;
	private double netProfit;
	
	
	
	
	
	public String getExp_description() {
		return exp_description;
	}
	public void setExp_description(String exp_description) {
		this.exp_description = exp_description;
	}
	public String getIncm_description() {
		return incm_description;
	}
	public void setIncm_description(String incm_description) {
		this.incm_description = incm_description;
	}
	public double getExpenseTotal() {
		return expenseTotal;
	}
	public void setExpenseTotal(double expenseTotal) {
		this.expenseTotal = expenseTotal;
	}
	public double getIncomeTotal() {
		return incomeTotal;
	}
	public void setIncomeTotal(double incomeTotal) {
		this.incomeTotal = incomeTotal;
	}
	public String getExpenseMonths() {
		return expenseMonths;
	}
	public void setExpenseMonths(String expenseMonths) {
		this.expenseMonths = expenseMonths;
	}
	public double getExpenseAmount() {
		return expenseAmount;
	}
	public void setExpenseAmount(double expenseAmount) {
		this.expenseAmount = expenseAmount;
	}
	public String getIncomeMonths() {
		return incomeMonths;
	}
	public void setIncomeMonths(String incomeMonths) {
		this.incomeMonths = incomeMonths;
	}
	public double getIncomeAmount() {
		return incomeAmount;
	}
	public void setIncomeAmount(double incomeAmount) {
		this.incomeAmount = incomeAmount;
	}
	public String getSerialno() {
		return serialno;
	}
	public void setSerialno(String serialno) {
		this.serialno = serialno;
	}
	public String getExp_serialno() {
		return exp_serialno;
	}
	public void setExp_serialno(String exp_serialno) {
		this.exp_serialno = exp_serialno;
	}
	public double getNetLoss() {
		return netLoss;
	}
	public void setNetLoss(double netLoss) {
		this.netLoss = netLoss;
	}
	public double getNetProfit() {
		return netProfit;
	}
	public void setNetProfit(double netProfit) {
		this.netProfit = netProfit;
	}
	
	
	

}
