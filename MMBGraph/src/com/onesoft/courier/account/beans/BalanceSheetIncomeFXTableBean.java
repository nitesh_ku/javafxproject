package com.onesoft.courier.account.beans;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class BalanceSheetIncomeFXTableBean {
	
	private SimpleStringProperty incomeSerialNo;
	private SimpleStringProperty incomeMonths;
	private SimpleDoubleProperty incomeAmount;

	public BalanceSheetIncomeFXTableBean(String slno, String months, double amount)
	{
		this.incomeSerialNo=new SimpleStringProperty(slno);
		this.incomeMonths=new SimpleStringProperty(months);
		this.incomeAmount=new SimpleDoubleProperty(amount);
	}
	
	
	
	public String getIncomeMonths() {
		return incomeMonths.get();
	}
	public void setIncomeMonths(SimpleStringProperty incomeMonths) {
		this.incomeMonths = incomeMonths;
	}
	public Double getIncomeAmount() {
		return incomeAmount.get();
	}
	public void setIncomeAmount(SimpleDoubleProperty incomeAmount) {
		this.incomeAmount = incomeAmount;
	}
	public String getIncomeSerialNo() {
		return incomeSerialNo.get();
	}
	public void setIncomeSerialNo(SimpleStringProperty incomeSerialNo) {
		this.incomeSerialNo = incomeSerialNo;
	}

}
