package com.onesoft.courier.account.beans;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class BalanceSheetExpenseFXTableBean {
	
	private SimpleStringProperty expenseSerialNo;
	private SimpleStringProperty expenseMonths;
	private SimpleDoubleProperty expenseAmount;
	
	public BalanceSheetExpenseFXTableBean(String slno, String months, double amount)
	{
		this.expenseSerialNo=new SimpleStringProperty(slno);
		this.expenseMonths=new SimpleStringProperty(months);
		this.expenseAmount=new SimpleDoubleProperty(amount);
	}
	
	
	public String getExpenseMonths() {
		return expenseMonths.get();
	}
	public void setExpenseMonths(SimpleStringProperty expenseMonths) {
		this.expenseMonths = expenseMonths;
	}
	public Double getExpenseAmount() {
		return expenseAmount.get();
	}
	public void setExpenseAmount(SimpleDoubleProperty expenseAmount) {
		this.expenseAmount = expenseAmount;
	}


	public String getExpenseSerialNo() {
		return expenseSerialNo.get();
	}


	public void setExpenseSerialNo(SimpleStringProperty expenseSerialNo) {
		this.expenseSerialNo = expenseSerialNo;
	}
	

}
