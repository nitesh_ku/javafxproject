package com.onesoft.courier.account.beans;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

public class BalanceSheetTotalFXTableBean {
	
	private SimpleStringProperty exp_description;
	private SimpleStringProperty incm_description;
	
	private SimpleDoubleProperty expenseTotal;
	private SimpleDoubleProperty incomeTotal;
	
	
	public BalanceSheetTotalFXTableBean(String expDescription,String incmDescription, double expTotal, double incmTotal)
	{
		this.exp_description=new SimpleStringProperty(expDescription);
		this.expenseTotal=new SimpleDoubleProperty(expTotal);
		
		this.incm_description=new SimpleStringProperty(incmDescription);
		this.incomeTotal=new SimpleDoubleProperty(incmTotal);
	}
	
	
	public String getExp_description() {
		return exp_description.get();
	}
	public void setExp_description(SimpleStringProperty exp_description) {
		this.exp_description = exp_description;
	}
	public String getIncm_description() {
		return incm_description.get();
	}
	public void setIncm_description(SimpleStringProperty incm_description) {
		this.incm_description = incm_description;
	}
	public Double getExpenseTotal() {
		return expenseTotal.get();
	}
	public void setExpenseTotal(SimpleDoubleProperty expenseTotal) {
		this.expenseTotal = expenseTotal;
	}
	public Double getIncomeTotal() {
		return incomeTotal.get();
	}
	public void setIncomeTotal(SimpleDoubleProperty incomeTotal) {
		this.incomeTotal = incomeTotal;
	}
	
}
