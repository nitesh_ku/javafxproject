package com.onesoft.courier.account.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

import com.DB.DBconnection;
import com.onesoft.courier.account.beans.BalanceSheetBean;
import com.onesoft.courier.account.beans.BalanceSheetExpenseFXTableBean;
import com.onesoft.courier.account.beans.BalanceSheetIncomeFXTableBean;
import com.onesoft.courier.account.beans.BalanceSheetTotalFXTableBean;
import com.onesoft.courier.account.beans.ProfitLossFXTableBean;
import com.onesoft.courier.account.utils.AccountUtils;
import com.onesoft.courier.common.LoadBranch;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.util.Callback;

public class BalanceSheetController implements Initializable {
	
	private ObservableList<String> comboBoxYearItems=FXCollections.observableArrayList(AccountUtils.YEAR1,AccountUtils.YEAR2,
														AccountUtils.YEAR3,AccountUtils.YEAR4,AccountUtils.YEAR5);
	
	private ObservableList<String> comboBoxBranchNameItems=FXCollections.observableArrayList(AccountUtils.ALLBRANCH);

	private ObservableList<BalanceSheetExpenseFXTableBean> expenseTableData=FXCollections.observableArrayList();
	private ObservableList<BalanceSheetIncomeFXTableBean> incomeTableData=FXCollections.observableArrayList();
	private ObservableList<BalanceSheetTotalFXTableBean> resultTableData=FXCollections.observableArrayList();
	private ObservableList<ProfitLossFXTableBean> profitLossTableData=FXCollections.observableArrayList();

	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	
	double checkProfitItem;
	double checkLossItem;
	
	
	@FXML
	private ComboBox<String> comboBoxYear;
	
	@FXML
	private ComboBox<String> comboBoxBranch;
	
	@FXML
	private Button btnBalanceSheet;
	
	@FXML
	private ImageView imgSearchIcon;
	

// ===============================================================================	
	
	@FXML
	private TableView<BalanceSheetExpenseFXTableBean> expenseTable;
	
	@FXML
	private TableColumn<BalanceSheetExpenseFXTableBean, Integer> expenseSerialNo_Column;
	
	@FXML
	private TableColumn<BalanceSheetExpenseFXTableBean, String> expenseMonth_Column;
	
	@FXML
	private TableColumn<BalanceSheetExpenseFXTableBean, Double> expenseAmount_Column;

// ===============================================================================	
	
	@FXML
	private TableView<BalanceSheetIncomeFXTableBean> incomeTable;
	
	@FXML
	private TableColumn<BalanceSheetIncomeFXTableBean, Integer> incomeSerialNo_Column;
	
	@FXML
	private TableColumn<BalanceSheetIncomeFXTableBean, String> incomeMonth_Column;
	
	@FXML
	private TableColumn<BalanceSheetIncomeFXTableBean, Double> incomeAmount_Column;

// ===============================================================================	
	
	@FXML
	private TableView<BalanceSheetTotalFXTableBean> resultTable;
	
	@FXML
	private TableColumn<BalanceSheetTotalFXTableBean, Integer> resultExpenseSerialNo_Column;
	
	@FXML
	private TableColumn<BalanceSheetTotalFXTableBean, Integer> resultIncomeSerialNo_Column;
	
	@FXML
	private TableColumn<BalanceSheetTotalFXTableBean, String> resultExpenseDescription_Column;
	
	@FXML
	private TableColumn<BalanceSheetTotalFXTableBean, String> resultIncomeDescription_Column;
	
	@FXML
	private TableColumn<BalanceSheetTotalFXTableBean, Double> resultIncomeTotal_Column;
	
	@FXML
	private TableColumn<BalanceSheetTotalFXTableBean, Double> resultExpenseTotal_Column;
	
	
// ===============================================================================	
	
	@FXML
	private TableView<ProfitLossFXTableBean> profitLossTable;
	
	@FXML
	private TableColumn<ProfitLossFXTableBean, Integer> profitLossExp_SerialNo_Column;
	
	@FXML
	private TableColumn<ProfitLossFXTableBean, Integer> profitLossIncm_SerialNo_Column;
	
	@FXML
	private TableColumn<ProfitLossFXTableBean, String> profitLossExp_Description_Column;
	
	@FXML
	private TableColumn<ProfitLossFXTableBean, String> profitLossIncm_Description_Column;
	
	@FXML
	private TableColumn<ProfitLossFXTableBean, Double> profitLoss_NetLoss_Column;
	
	@FXML
	private TableColumn<ProfitLossFXTableBean, Double> profitLoss_NetProfit_Column;
	

// ===============================================================================		

	public void loadBranch() throws SQLException
	{
		new LoadBranch().loadBranch();
		
		for(String s:LoadBranch.SETLOADBRANCHFORALL)
		{
			
			comboBoxBranchNameItems.add(s);
		}
		comboBoxBranch.setItems(comboBoxBranchNameItems);
		
	}
	

// ===============================================================================	
	
	public void loadIncomeExpenseTable() throws SQLException, IOException
	{
		System.out.println("FY values: ==== "+comboBoxYear.getValue());
		
		if(comboBoxYear.getValue()==null)
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Validation Alert");
			alert.setHeaderText(null);
			alert.setContentText("Please select Financial Year (FY)...");
			alert.showAndWait();
		}
		else
		{
		
		expenseTableData.clear();
		incomeTableData.clear();
		
		resultTable.setVisible(true);
		profitLossTable.setVisible(true);
		
		String financialYear=comboBoxYear.getValue();
		//String branch=comboBoxBranch.getValue();
		
		String[] year=financialYear.split("-");
		String startYear=year[0]+AccountUtils.CATEGORY1SDATE;
		String endYear=year[1]+AccountUtils.CATEGORY1EDATE;
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		Statement st=null;
		ResultSet rs=null;

		BalanceSheetBean balBean=new BalanceSheetBean();
		int serialno=1;
		int exp_serialno=1;
		
		try
		{	
			st=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			
			// ====== First Query =======
			
			String sql="select sum(amtreceived) as payment_received, to_char(paymentdate,'Mon,YY') as months from invoice "
							+ "where invoice2branchcode='"+comboBoxBranch.getValue()+"' and paymentdate between '"+startYear+"' and '"+endYear+"' group by to_char(paymentdate,'Mon,YY')";
			rs=st.executeQuery(sql);
			
			if(!rs.next())
			{
				incomeTableData.add(new BalanceSheetIncomeFXTableBean("","N/A",0.0));
			}
			else
			{
				do{
				balBean.setSerialno(String.valueOf(serialno));
				balBean.setIncomeAmount(rs.getDouble("payment_received"));
				balBean.setIncomeMonths(rs.getString("months"));
				
				System.out.println("from income table: >>> "+rs.getString("months")+" "+rs.getDouble("payment_received"));
				
				incomeTableData.add(new BalanceSheetIncomeFXTableBean(balBean.getSerialno(),balBean.getIncomeMonths(),balBean.getIncomeAmount()));
				serialno++;	
			
				}while(rs.next());
				
			}
			rs.last();
			
			// ====== Second Query =======
			
			String sql1="select sum(amount+cgst+igst+sgst) as totalexpense, to_char(expense_date,'Mon,YY') as months from expenses "
							+ "where branch='"+comboBoxBranch.getValue()+"' and expense_date between '"+startYear+"' and '"+endYear+"' group by to_char(expense_date,'Mon,YY')";
			
			rs=st.executeQuery(sql1);
			
			if(!rs.next())
			{
				expenseTableData.add(new BalanceSheetExpenseFXTableBean("","N/A",0.0));
			}
			else
			{
				do
				{
				balBean.setExp_serialno(String.valueOf(exp_serialno));
				balBean.setExpenseMonths(rs.getString("months"));
				balBean.setExpenseAmount(rs.getDouble("totalexpense"));
				
				System.out.println("from Expense table: >>> "+rs.getString("months")+" "+rs.getDouble("totalexpense"));
				
				expenseTableData.add(new BalanceSheetExpenseFXTableBean(balBean.getExp_serialno(), balBean.getExpenseMonths(), balBean.getExpenseAmount()));
				exp_serialno++;
				
				}while(rs.next());
			}	
				expenseTable.setItems(expenseTableData);
				incomeTable.setItems(incomeTableData);
		
				loadTotalOfBalanceSheet();
				
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}
		finally
		{
			dbcon.disconnect(null, st,rs, con);
		}
		}
	}
	
	
// ===============================================================================		

	public void loadTotalOfBalanceSheet()
	{
		profitLossTableData.clear();
		resultTableData.clear();
		
		double expenseTotal=0;
		double incomeTotal=0;
		double balanceSheetAmount=0;
		
		BalanceSheetBean bean=new BalanceSheetBean();
		
		for(BalanceSheetIncomeFXTableBean incomeBean:incomeTableData)
		{
			incomeTotal=incomeTotal+incomeBean.getIncomeAmount();
		}
		
		for(BalanceSheetExpenseFXTableBean expenseBean:expenseTableData)
		{
			expenseTotal=expenseTotal+expenseBean.getExpenseAmount();
		}
		
		if(incomeTotal>expenseTotal)
		{
			balanceSheetAmount=incomeTotal;
			bean.setIncomeTotal(Double.valueOf(df.format(incomeTotal)));
			resultTableData.add(new BalanceSheetTotalFXTableBean("Total Expenses", "Total Income", bean.getIncomeTotal(), bean.getIncomeTotal()));
			resultTable.setItems(resultTableData);
		}
		else
		{	
			balanceSheetAmount=expenseTotal;
			bean.setExpenseTotal(Double.valueOf(df.format(expenseTotal)));
			resultTableData.add(new BalanceSheetTotalFXTableBean("Total Expenses", "Total Income", bean.getExpenseTotal(), bean.getExpenseTotal()));
			resultTable.setItems(resultTableData);
		}
		
		loadProfitLossTable(expenseTotal, incomeTotal,balanceSheetAmount);
		
	}
	
	
// ===============================================================================		
	 
	
	public void loadProfitLossTable(double exp_Total,double inc_Total,double balSheetAmount)
	{
		BalanceSheetBean bean=new BalanceSheetBean();
		
	// ================== Change the color of Amount column in Profit & Loss Table =============
		
		if(balSheetAmount==inc_Total)
		{
			bean.setNetProfit(Double.valueOf(df.format(balSheetAmount-exp_Total)));
			profitLossTableData.add(new ProfitLossFXTableBean("Net Profit","Net Loss",  0.0,bean.getNetProfit()));
			
			profitLoss_NetProfit_Column.setCellFactory(column -> 
			{
		        return new TableCell<ProfitLossFXTableBean, Double>() 
		        {
		            protected void updateItem(Double item, boolean empty) 
		            {
		                super.updateItem(item, empty);
		                
		                if(item==null)
		                {
		                	setText("0.0");
		                }
		                else if(item==0.0)
		                {
		                	setText("0.0");
		                }
		                else
		                {
		                	setText(String.valueOf(bean.getNetProfit()));
		                	setStyle("-fx-background-color: #46bc42;-fx-text-fill: white;-fx-font-weight: bold;");
		                }
		            }
		        };
		    });
		
		}
		else
		{
			bean.setNetLoss(Double.valueOf(df.format(balSheetAmount-inc_Total)));
			profitLossTableData.add(new ProfitLossFXTableBean("Net Profit","Net Loss", bean.getNetLoss(),0.0));
			
			profitLoss_NetLoss_Column.setCellFactory(column -> 
			{
		        return new TableCell<ProfitLossFXTableBean, Double>() 
		        {
		            protected void updateItem(Double item, boolean empty) 
		            {
		                super.updateItem(item, empty);

		                 if(item==null)
		                {
		                	setText("0.0");
		                }
		                else if(item==0.0)
			            {
			            	setText("0.0");
			            }
		                else
		                {
		                	setText(String.valueOf(bean.getNetLoss()));
		                	setStyle("-fx-background-color: #b22e25;-fx-text-fill: white; -fx-font-weight: bold;");
		                }	
		            }
		        };
		    });
			
		}
		
		profitLossTable.setItems(profitLossTableData);
		
		for(ProfitLossFXTableBean pbean: profitLossTableData)
		{
			checkProfitItem=pbean.getNetProfit();
			checkLossItem=pbean.getNetLoss();
			
		}
		
		
	// ================== Change the color of Description column in Profit & Loss Table =============
		
		if(checkProfitItem==0.0)
		{
			profitLossIncm_Description_Column.setCellFactory(column -> 
			{
		        return new TableCell<ProfitLossFXTableBean, String>() 
		        {
		            protected void updateItem(String item, boolean empty) 
		            {
		                super.updateItem(item, empty);
		              
		                if(checkProfitItem>0)
		                {
		                	setText("Net Loss");
		                }
		                else if(checkLossItem==0.0 && checkProfitItem==0.0)
		                {
		                	setText("Net Loss");
		                }
		                else
		                {
		                	setText("Net Loss");
			                setStyle("-fx-background-color: #b22e25;-fx-text-fill: white; -fx-font-weight: bold;");
		                }
		            }
		        };
		    });
		}
		else
		{
			profitLossExp_Description_Column.setCellFactory(column -> 
			{
		        return new TableCell<ProfitLossFXTableBean, String>() 
		        {
		            protected void updateItem(String item, boolean empty) 
		            {
		                super.updateItem(item, empty);
		                
		                if(checkLossItem>0)
		                {
		                	setText("Net Profit");
		                }
		                else if(checkLossItem==0.0 && checkProfitItem==0.0)
		                {
		                	setText("Net Loss");
		                }
		                else
		                {
		                	setText("Net Profit");
		                    setStyle("-fx-background-color: #46bc42;-fx-text-fill: white;-fx-font-weight: bold;");
		                }   
		            }
		        };
		    });
		}
		
	}
		

// ===============================================================================


	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		imgSearchIcon.setImage(new Image("/com/onesoft/courier/icon/searchicon_blue.png"));
		
		comboBoxBranch.setValue(AccountUtils.ALLBRANCH);
		comboBoxYear.setItems(comboBoxYearItems);
		
		resultTable.setVisible(false);
		profitLossTable.setVisible(false);
		
		try
		{
			loadBranch();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
	//============= Hide the header of ResultTable ====================
		
		resultTable.widthProperty().addListener(new ChangeListener<Number>()
		{
			@Override
			public void changed(ObservableValue<? extends Number> source, Number oldWidth, Number newWidth)
			{
				Pane header = (Pane) resultTable.lookup("TableHeaderRow");
				if (header.isVisible())
				{
					header.setMaxHeight(0);
					header.setMinHeight(0);
					header.setPrefHeight(0);
					header.setVisible(false);
					header.setDisable(true);
				}
			}
		});
		
		
	//============= Hide the header of Profit&Loss Table ====================
		
		profitLossTable.widthProperty().addListener(new ChangeListener<Number>()
		{
			
			
			@Override
			public void changed(ObservableValue<? extends Number> source, Number oldWidth, Number newWidth)
			{
				Pane header = (Pane) profitLossTable.lookup("TableHeaderRow");
				if (header.isVisible())
				{
					header.setMaxHeight(0);
					header.setMinHeight(0);
					header.setPrefHeight(0);
					header.setVisible(false);
					header.setDisable(true);
				}
			}
		});
		
		
	// =========== Income Table  =============
		
		incomeSerialNo_Column.setCellValueFactory(new PropertyValueFactory<BalanceSheetIncomeFXTableBean,Integer>("incomeSerialNo"));
		incomeMonth_Column.setCellValueFactory(new PropertyValueFactory<BalanceSheetIncomeFXTableBean,String>("incomeMonths"));
		incomeAmount_Column.setCellValueFactory(new PropertyValueFactory<BalanceSheetIncomeFXTableBean,Double>("incomeAmount"));
		
	// =========== Expense Table  =============
		
		expenseSerialNo_Column.setCellValueFactory(new PropertyValueFactory<BalanceSheetExpenseFXTableBean,Integer>("expenseSerialNo"));
		expenseMonth_Column.setCellValueFactory(new PropertyValueFactory<BalanceSheetExpenseFXTableBean,String>("expenseMonths"));
		expenseAmount_Column.setCellValueFactory(new PropertyValueFactory<BalanceSheetExpenseFXTableBean,Double>("expenseAmount"));
		
	// =========== Result/Total Table  =============
		
		resultExpenseDescription_Column.setCellValueFactory(new PropertyValueFactory<BalanceSheetTotalFXTableBean,String>("exp_description"));
		resultIncomeDescription_Column.setCellValueFactory(new PropertyValueFactory<BalanceSheetTotalFXTableBean,String>("incm_description"));
		resultIncomeTotal_Column.setCellValueFactory(new PropertyValueFactory<BalanceSheetTotalFXTableBean,Double>("incomeTotal"));
		resultExpenseTotal_Column.setCellValueFactory(new PropertyValueFactory<BalanceSheetTotalFXTableBean,Double>("expenseTotal"));
	
	// =========== Profit&Loss Table  =============
		
		profitLossExp_Description_Column.setCellValueFactory(new PropertyValueFactory<ProfitLossFXTableBean,String>("exp_description"));
		profitLossIncm_Description_Column.setCellValueFactory(new PropertyValueFactory<ProfitLossFXTableBean,String>("incm_description"));
		profitLoss_NetLoss_Column.setCellValueFactory(new PropertyValueFactory<ProfitLossFXTableBean,Double>("netLoss"));
		profitLoss_NetProfit_Column.setCellValueFactory(new PropertyValueFactory<ProfitLossFXTableBean,Double>("netProfit"));
	
	}
	
}
