package com.onesoft.courier.account.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.DB.DBconnection;
import com.onesoft.courier.account.beans.LedgerBean;
import com.onesoft.courier.account.beans.LedgerJavaFXTableBean;
import com.onesoft.courier.account.utils.AccountUtils;
import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.common.LoadClientBean;
import com.onesoft.courier.common.LoadClients;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class LedgerController implements Initializable {
	
	private ObservableList<LedgerJavaFXTableBean> ledgerTableData=FXCollections.observableArrayList();
//	private ObservableList<LedgerJavaFXTableBean> resultTableData=FXCollections.observableArrayList();
	
	private ObservableList<String> comboBoxBranchNameItems=FXCollections.observableArrayList(AccountUtils.ALLBRANCH);
	private ObservableList<String> comboBoxClientItems=FXCollections.observableArrayList();
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	
	@FXML
	private ImageView imgSearchIcon;
	
	@FXML
	private ComboBox<String> comboBoxBranch;
	
	@FXML
	private ComboBox<String> comboBoxClient;
	
	@FXML
	private DatePicker dpkFrom;
	
	@FXML
	private DatePicker dpkTo;
	
	@FXML
	private Pagination pagination;
	
	@FXML
	private Button btnExportTOExcel;
	
	@FXML
	private Button btnExportTOPDF;
	
	@FXML
	private Label labBillAmount;
	
	@FXML
	private Label labReceivedAmount;
	
	@FXML
	private Label labAdjustedAmount;
	
	@FXML
	private Label labBalanceAmount;
	
	@FXML
	private Label labBillTag;
	
	@FXML
	private Label labReceivedTag;
	
	@FXML
	private Label labAdjustedTag;
	
	@FXML
	private Label labBalanceTag;
	
	@FXML
	private Label labTotalTag;
	
	
	@FXML
	private Button btnShowLedger;
	
	
	// ====================================================================
	
	
	
	@FXML
	private TableView<LedgerJavaFXTableBean> ledgerTable;
	
	@FXML
	private TableColumn<LedgerJavaFXTableBean, Integer> col_serialno;
	
	@FXML
	private TableColumn<LedgerJavaFXTableBean, String> col_branch;
	
	@FXML
	private TableColumn<LedgerJavaFXTableBean, String> col_InvoiceNumber;
	
	@FXML
	private TableColumn<LedgerJavaFXTableBean, String> col_InvoiceDate;
	
	@FXML
	private TableColumn<LedgerJavaFXTableBean, String> col_FromDate;
	
	@FXML
	private TableColumn<LedgerJavaFXTableBean, String> col_ToDate;
	
	@FXML
	private TableColumn<LedgerJavaFXTableBean, String> col_ClientCode;

	@FXML
	private TableColumn<LedgerJavaFXTableBean, Double> col_BillAmount;
	
	@FXML
	private TableColumn<LedgerJavaFXTableBean, Double> col_ReceivedAmount;
	
	@FXML
	private TableColumn<LedgerJavaFXTableBean, Double> col_AdjustedAmount;
	
	@FXML
	private TableColumn<LedgerJavaFXTableBean, Double> col_BalanceAmount;
	
	
	
	// ====================================================================
	
	
		/*@FXML
		private TableView<LedgerJavaFXTableBean> resultTable;

		@FXML
		private TableColumn<LedgerJavaFXTableBean, String> resultcol_total;
		
		@FXML
		private TableColumn<LedgerJavaFXTableBean, String> resultcol_branch;
		
		@FXML
		private TableColumn<LedgerJavaFXTableBean, String> resultcol_InvoiceNumber;
		
		@FXML
		private TableColumn<LedgerJavaFXTableBean, String> resultcol_InvoiceDate;
		
		@FXML
		private TableColumn<LedgerJavaFXTableBean, String> resultcol_FromDate;
		
		@FXML
		private TableColumn<LedgerJavaFXTableBean, String> resultcol_ToDate;
		
		@FXML
		private TableColumn<LedgerJavaFXTableBean, String> resultcol_ClientCode;

		@FXML
		private TableColumn<LedgerJavaFXTableBean, Double> resultcol_BillAmount;
		
		@FXML
		private TableColumn<LedgerJavaFXTableBean, Double> resultcol_ReceivedAmount;
		
		@FXML
		private TableColumn<LedgerJavaFXTableBean, Double> resultcol_AdjustedAmount;
		
		@FXML
		private TableColumn<LedgerJavaFXTableBean, Double> resultcol_BalanceAmount;
*/
	
	// ====================================================================
	
	
	
	
	public void loadBranch() throws SQLException
	{
		new LoadBranch().loadBranch();
		
		for(String s:LoadBranch.SETLOADBRANCHFORALL)
		{
			
			comboBoxBranchNameItems.add(s);
		}
		comboBoxBranch.setItems(comboBoxBranchNameItems);
		
	}

	// ====================================================================	
	
	@FXML
	public void loadClients() throws SQLException
	{
		new	LoadClients().loadClient();
		
		Set<String> setClient =new HashSet<>();
		
		for(LoadClientBean s:LoadClients.SETLOADCLIENTFORALL)
		{
			setClient.add(s.getClientCode());
		}
		
		for(String set:setClient)
		{
			comboBoxClientItems.add(set);
		}
		
		comboBoxClient.setItems(comboBoxClientItems);
		
	}

	// ====================================================================	

	public void showLedgerData() throws SQLException
	{
		
		if(dpkFrom.getValue()==null)
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Empty Field Validation");
			alert.setHeaderText(null);
			alert.setContentText("From Date field is Empty!");
			alert.showAndWait();
			dpkFrom.requestFocus();
		}
		else if( dpkTo.getValue()==null)
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Empty Field Validation");
			alert.setHeaderText(null);
			alert.setContentText("To Date field is Empty!");
			alert.showAndWait();
			dpkTo.requestFocus();
		}
		else
		{
		
		ledgerTableData.clear();
		LocalDate localFromdata=dpkFrom.getValue();
		LocalDate localTodata=dpkTo.getValue();
		Date fromDate=Date.valueOf(localFromdata);
		Date toDate=Date.valueOf(localTodata);
		
		String branch=comboBoxBranch.getValue();
		String client=comboBoxClient.getValue();
		
		System.out.println(branch+" ======== "+client+" ======== "+localFromdata+" ======== "+localTodata);
		
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
			
		ResultSet rs=null;
		Statement st=null;
		String finalsql=null;
		
		LedgerBean ledgerBean=new LedgerBean();
		int serialno=1;
		try
		{
			st=con.createStatement();
			finalsql="select invoice_number,clientcode,invoice_date,invoice2branchcode,from_date,to_date,grandtotal,amtreceived,due,tds_amount,db_note,adjustment from invoice where invoice2branchcode='"+branch+"' and clientcode='"+client+"' and invoice_date between '"+fromDate+"' and '"+toDate+"' order by invoice_number";
			System.out.println(finalsql);
			rs=st.executeQuery(finalsql);
		
			if(!rs.next())
			{
				
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Not Found Alert");
				alert.setHeaderText(null);
				alert.setContentText("Data not available");
				alert.showAndWait();
			
				System.err.println("Data not found");
				
				//resultTable.setVisible(false);
				btnExportTOExcel.setVisible(false);
				btnExportTOPDF.setVisible(false);
				pagination.setVisible(false);
				labAdjustedAmount.setVisible(false);
				labBalanceAmount.setVisible(false);
				labBillAmount.setVisible(false);
				labReceivedAmount.setVisible(false);
				
				labBalanceTag.setVisible(false);
				labReceivedTag.setVisible(false);
				labAdjustedTag.setVisible(false);
				labBillTag.setVisible(false);
				labTotalTag.setVisible(false);
				
			}
			else
			{
				//resultTable.setVisible(true);
				btnExportTOExcel.setVisible(true);
				btnExportTOPDF.setVisible(true);
				pagination.setVisible(true);
				labAdjustedAmount.setVisible(true);
				labBalanceAmount.setVisible(true);
				labBillAmount.setVisible(true);
				labReceivedAmount.setVisible(true);
				
				labBalanceTag.setVisible(true);
				labReceivedTag.setVisible(true);
				labAdjustedTag.setVisible(true);
				labBillTag.setVisible(true);
				labTotalTag.setVisible(true);
				
				do
				{	
					ledgerBean.setSlno(serialno);
					ledgerBean.setParty(rs.getString("clientCode"));
					ledgerBean.setInvoiceNo(rs.getString("invoice_number"));
					ledgerBean.setInvoiceDate(date.format(rs.getDate("invoice_date")));
					ledgerBean.setBranch(rs.getString("invoice2branchcode"));
					ledgerBean.setFromDate(date.format(rs.getDate("from_date")));
					ledgerBean.setToDate(date.format(rs.getDate("to_date")));
					ledgerBean.setBillAmount(rs.getDouble("grandtotal"));
					ledgerBean.setReceivedAmount(rs.getDouble("amtreceived"));
					ledgerBean.setAdjustedAmount((rs.getDouble("tds_amount")+rs.getDouble("db_note")+rs.getDouble("adjustment")));
					ledgerBean.setBalanceAmount(rs.getDouble("due"));
					
					serialno++;
					
					ledgerTableData.add(new LedgerJavaFXTableBean(ledgerBean.getSlno(), ledgerBean.getParty(), ledgerBean.getInvoiceNo(),
								ledgerBean.getInvoiceDate(), ledgerBean.getFromDate(), ledgerBean.getToDate(), ledgerBean.getBranch(),
									ledgerBean.getBillAmount(), ledgerBean.getReceivedAmount(), ledgerBean.getAdjustedAmount(), ledgerBean.getBalanceAmount()));					
					
				}while(rs.next());
				showTableResult();
			}
			
			
			ledgerTable.setItems(ledgerTableData);
			
			
			//pagination.setVisible(true);
			pagination.setPageCount((ledgerTableData.size() / rowsPerPage() + 1));
			pagination.setCurrentPageIndex(0);
			pagination.setPageFactory((Integer pageIndex) -> createPage(pageIndex));
		
		
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{	
			dbcon.disconnect(null, st, rs, con);
		}
		}
		
	}

	
	public void showTableResult()
	{
		double totalBill=0;
		double totalReceived=0;
		double totalAdjusted=0;
		double totalDue=0;
		
		for(LedgerJavaFXTableBean tableDate: ledgerTableData)
		{
			totalBill=totalBill+tableDate.getBillAmount();
			totalReceived=totalReceived+tableDate.getReceivedAmount();
			totalAdjusted=totalAdjusted+tableDate.getAdjustedAmount();
			totalDue=totalDue+tableDate.getBalanceAmount();
			
		}
		
		
		if(totalBill!=0)
		{
			labBillAmount.setText(df.format(totalBill));
		}
		else
		{
			labBillAmount.setText("0");
		}
		
		if(totalReceived!=0)
		{
			labReceivedAmount.setText(df.format(totalReceived));
		}
		else
		{
			labReceivedAmount.setText("0");
		}
		
		if(totalAdjusted!=0)
		{
			labAdjustedAmount.setText(df.format(totalAdjusted));
		}
		else
		{
			labAdjustedAmount.setText("0");
		}
		
		if(totalDue!=0)
		{
			labBalanceAmount.setText(df.format(totalDue));
		}
		else
		{
			labBalanceAmount.setText("0");
		}
		
		
		
		/*resultTableData.clear();
		
		
		
		
        resultTable.setStyle("-fx-control-inner-background: green;  -fx-accent: derive(-fx-control-inner-background, -40%);  -fx-cell-hover-color: derive(-fx-control-inner-background, -20%);");
		
		LedgerBean ledgerbean=new LedgerBean();
		for(LedgerJavaFXTableBean tableDate: ledgerTableData)
		{
			totalBill=Double.parseDouble(df.format(totalBill+tableDate.getBillAmount()));
			totalReceived=Double.parseDouble(df.format(totalReceived+tableDate.getReceivedAmount()));
			totalAdjusted=Double.parseDouble(df.format(totalAdjusted+tableDate.getAdjustedAmount()));
			totalDue=Double.parseDouble(df.format(totalDue+tableDate.getBalanceAmount()));
			
		}
		
		
		resultTableData.add(new LedgerJavaFXTableBean("Total", null, null, null, null, null, null, totalBill, totalReceived, totalAdjusted, totalDue));
		
			resultTable.setItems(resultTableData);	*/	
		
	}
	
	
	
	// ============ Code for paginatation =======================
	
	
	public int itemsPerPage()
	{
		return 1;
	}

	public int rowsPerPage() 
	{
		return 3;
	}

	public GridPane createPage(int pageIndex)
	{
		int lastIndex = 0;
		
	
	
		GridPane pane=new GridPane();
		int displace = ledgerTableData.size() % rowsPerPage();
            
			if (displace >= 0)
			{
				lastIndex = ledgerTableData.size() / rowsPerPage();
			}
			/*else
			{
				lastIndex = tabledata.size() / rowsPerPage() -1;
				System.out.println("Pagination lastindex from else condition: "+lastIndex);
			}*/
	  
	        
			int page = pageIndex * itemsPerPage();
	        for (int i = page; i < page + itemsPerPage(); i++)
	        {
	            if (lastIndex == pageIndex)
	            {
	                ledgerTable.setItems(FXCollections.observableArrayList(ledgerTableData.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
	            }
	            else
	            {
	            	ledgerTable.setItems(FXCollections.observableArrayList(ledgerTableData.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
	            }
	        }
	       return pane;
	      
	    }
	
	
// ===================================================================
	
	@FXML
	public void exportToExcel() throws SQLException, IOException
	{
			
		HSSFWorkbook workbook=new HSSFWorkbook();
		HSSFSheet sheet=workbook.createSheet("Ledger Data");
		HSSFRow rowhead=sheet.createRow(0);
		
		rowhead.createCell(0).setCellValue("Serial No");
		rowhead.createCell(1).setCellValue("Party Name");
		rowhead.createCell(2).setCellValue("Invoice No");
		rowhead.createCell(3).setCellValue("Invoice Date");
		
		rowhead.createCell(4).setCellValue("From Date");
		rowhead.createCell(5).setCellValue("To Date");
		rowhead.createCell(6).setCellValue("Branch");
		rowhead.createCell(7).setCellValue("Bill Amt");
		
		rowhead.createCell(8).setCellValue("Received Amt");
		rowhead.createCell(9).setCellValue("Adjusted Amt");
		rowhead.createCell(10).setCellValue("Balance Amt");
		
		
		/*rowhead.createCell(11).setCellValue("Billing Weight");
		
		rowhead.createCell(12).setCellValue("Insurance Amount");
		rowhead.createCell(13).setCellValue("Shiper Cost");
		rowhead.createCell(14).setCellValue("COD Amount");
		rowhead.createCell(15).setCellValue("FOD Amount");
		
		rowhead.createCell(16).setCellValue("Docket Charge");
		rowhead.createCell(17).setCellValue("VAS");
		rowhead.createCell(18).setCellValue("Fuel");
		rowhead.createCell(19).setCellValue("Amount");
		
		rowhead.createCell(20).setCellValue("Service Tax");
		rowhead.createCell(21).setCellValue("Total");
		rowhead.createCell(22).setCellValue("Percentage");*/
		
		
			int i=2;
			
			HSSFRow row;
			
			for(LedgerJavaFXTableBean ledgerBean: ledgerTableData)
			{
				row = sheet.createRow((short) i);
			
				row.createCell(0).setCellValue(ledgerBean.getSlno());
				row.createCell(1).setCellValue(ledgerBean.getParty());
				row.createCell(2).setCellValue(ledgerBean.getInvoiceNo());
				
				row.createCell(3).setCellValue(ledgerBean.getInvoiceDate());
				row.createCell(4).setCellValue(ledgerBean.getFromDate());
				row.createCell(5).setCellValue(ledgerBean.getToDate());
				
				row.createCell(6).setCellValue(ledgerBean.getBranch());
				row.createCell(7).setCellValue(ledgerBean.getBillAmount());
				row.createCell(8).setCellValue(ledgerBean.getReceivedAmount());
				
				row.createCell(9).setCellValue(ledgerBean.getAdjustedAmount());
				row.createCell(10).setCellValue(ledgerBean.getBalanceAmount());
				
				
				/*row.createCell(11).setCellValue(fxBean.getBillingweight());
				
				row.createCell(12).setCellValue(fxBean.getInsuracneamount());
				row.createCell(13).setCellValue(fxBean.getShipercost());
				row.createCell(14).setCellValue(fxBean.getCod());
			
				row.createCell(15).setCellValue(fxBean.getFod());
				row.createCell(16).setCellValue(fxBean.getDocketcharge());
				row.createCell(17).setCellValue(fxBean.getOtherVas());
			
				row.createCell(18).setCellValue(fxBean.getFuel());
				row.createCell(19).setCellValue(fxBean.getAmount());
				row.createCell(20).setCellValue(fxBean.getServicetax());
				row.createCell(21).setCellValue(fxBean.getTotal());
				row.createCell(22).setCellValue(fxBean.getPercentage());*/
				
				i++;
			}
			
			int j=i+2;
			
			for(int z=0;z<1;z++)
			{
				row = sheet.createRow((short) j);
			
				row.createCell(0).setCellValue("Total");
				row.createCell(1).setCellValue("");
				row.createCell(2).setCellValue("");
				
				row.createCell(3).setCellValue("");
				row.createCell(4).setCellValue("");
				row.createCell(5).setCellValue("");
				
				row.createCell(6).setCellValue("");
				row.createCell(7).setCellValue(Double.parseDouble(labBillAmount.getText()));
				row.createCell(8).setCellValue(Double.parseDouble(labReceivedAmount.getText()));
				
				row.createCell(9).setCellValue(Double.parseDouble(labAdjustedAmount.getText()));
				row.createCell(10).setCellValue(Double.parseDouble(labBalanceAmount.getText()));
				
				
				j++;
			}
			
			
		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files","*.xls"));
		File file = fc.showSaveDialog(null);
		FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
		System.out.println("file chooser: "+file.getAbsoluteFile());
		
		workbook.write(fileOut);
		fileOut.close();
			
		System.out.println("File "+file.getName()+" successfully saved");
	      
	}	
	
	
	// ====================================================================	
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		imgSearchIcon.setImage(new Image("/com/onesoft/courier/icon/searchicon_blue.png"));
		
		comboBoxBranch.setValue(AccountUtils.ALLBRANCH);
		comboBoxClient.setValue("All Clients");
		//comboBoxBranch.setItems(comboBoxBranchNameItems);

		/*resultTable.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> source, Number oldWidth, Number newWidth) {
				Pane header = (Pane) resultTable.lookup("TableHeaderRow");
				if (header.isVisible()){
					header.setMaxHeight(0);
					header.setMinHeight(0);
					header.setPrefHeight(0);
					header.setVisible(false);
					header.setDisable(true);
				}
			}
		});*/
		
		try {
			loadBranch();
			loadClients();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		col_serialno.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,Integer>("slno"));
		col_InvoiceNumber.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,String>("invoiceNo"));
		col_branch.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,String>("branch"));
		col_InvoiceDate.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,String>("invoiceDate"));
		col_FromDate.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,String>("fromDate"));
		col_ToDate.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,String>("toDate"));
		col_ClientCode.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,String>("party"));
		col_BillAmount.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,Double>("billAmount"));
		col_ReceivedAmount.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,Double>("receivedAmount"));
		col_AdjustedAmount.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,Double>("adjustedAmount"));
		col_BalanceAmount.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,Double>("balanceAmount"));
		
		
		
		/*resultcol_total.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,String>("total"));
		resultcol_InvoiceNumber.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,String>("invoiceNo"));
		resultcol_branch.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,String>("branch"));
		resultcol_InvoiceDate.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,String>("invoiceDate"));
		resultcol_FromDate.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,String>("fromDate"));
		resultcol_ToDate.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,String>("toDate"));
		resultcol_ClientCode.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,String>("party"));
		resultcol_BillAmount.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,Double>("billAmount"));
		resultcol_ReceivedAmount.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,Double>("receivedAmount"));
		resultcol_AdjustedAmount.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,Double>("adjustedAmount"));
		resultcol_BalanceAmount.setCellValueFactory(new PropertyValueFactory<LedgerJavaFXTableBean,Double>("balanceAmount"));
		
		resultTable.setVisible(false);*/
		
		
		btnExportTOExcel.setVisible(false);
		btnExportTOPDF.setVisible(false);
		pagination.setVisible(false);
		labAdjustedAmount.setVisible(false);
		labBalanceAmount.setVisible(false);
		labBillAmount.setVisible(false);
		labReceivedAmount.setVisible(false);
		
		labBalanceTag.setVisible(false);
		labReceivedTag.setVisible(false);
		labAdjustedTag.setVisible(false);
		labBillTag.setVisible(false);
		labTotalTag.setVisible(false);
	}

}
