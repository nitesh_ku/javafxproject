package com.onesoft.courier.account.utils;

public class AccountUtils {

	
	public static final String ALLBRANCH = "All Branches";
	
	public static final String YEAR1 = "2017-2018";
	public static final String YEAR2 = "2016-2017";
	public static final String YEAR3 = "2015-2016";
	public static final String YEAR4 = "2014-2015";
	public static final String YEAR5 = "2013-2014";
	public static final String YEAR6 = "2012-2013";
	
	public static final String CATEGORY1SDATE = "-04-01";					// Date DD.MM format
	public static final String CATEGORY1EDATE = "-03-31";
	
}
