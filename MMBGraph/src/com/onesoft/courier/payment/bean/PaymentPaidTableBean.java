package com.onesoft.courier.payment.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class PaymentPaidTableBean {
	
	private SimpleIntegerProperty paidserialno;
	private SimpleStringProperty paidclientname;
	private SimpleStringProperty paidinvoicenumber;
	private SimpleStringProperty paidinvoicedate;
	private SimpleStringProperty paidpaymentDate;
	private SimpleStringProperty paidpaymentMode;
	private SimpleDoubleProperty paidbillamount;
	private SimpleDoubleProperty paidamtReceived;
	
	private SimpleStringProperty status;
	private SimpleStringProperty remarks;
	private SimpleStringProperty paidbankName;
	
	
	public PaymentPaidTableBean(int serialno, String clientname, String invoicenumber, String invoicedate, String paymentdate, double billamount, double receivedamount, String paymentmode) {
	
		super();
		this.paidserialno=new SimpleIntegerProperty(serialno);
		this.paidclientname=new SimpleStringProperty(clientname);
		this.paidinvoicenumber=new SimpleStringProperty(invoicenumber);
		this.paidinvoicedate=new SimpleStringProperty(invoicedate);
		this.paidpaymentDate=new SimpleStringProperty(paymentdate);
		this.paidbillamount=new SimpleDoubleProperty(billamount);
		this.paidamtReceived=new SimpleDoubleProperty(receivedamount);
		this.paidpaymentMode=new SimpleStringProperty(paymentmode);
	}
	
	
	public int getPaidserialno() {
		return paidserialno.get();
	}
	public void setPaidserialno(SimpleIntegerProperty paidserialno) {
		this.paidserialno = paidserialno;
	}
	public String getPaidclientname() {
		return paidclientname.get();
	}
	public void setPaidclientname(SimpleStringProperty paidclientname) {
		this.paidclientname = paidclientname;
	}
	public String getPaidinvoicenumber() {
		return paidinvoicenumber.get();
	}
	public void setPaidinvoicenumber(SimpleStringProperty paidinvoicenumber) {
		this.paidinvoicenumber = paidinvoicenumber;
	}
	public String getPaidinvoicedate() {
		return paidinvoicedate.get();
	}
	public void setPaidinvoicedate(SimpleStringProperty paidinvoicedate) {
		this.paidinvoicedate = paidinvoicedate;
	}
	public String getPaidpaymentDate() {
		return paidpaymentDate.get();
	}
	public void setPaidpaymentDate(SimpleStringProperty paidpaymentDate) {
		this.paidpaymentDate = paidpaymentDate;
	}
	public String getPaidpaymentMode() {
		return paidpaymentMode.get();
	}
	public void setPaidpaymentMode(SimpleStringProperty paidpaymentMode) {
		this.paidpaymentMode = paidpaymentMode;
	}
	public double getPaidbillamount() {
		return paidbillamount.get();
	}
	public void setPaidbillamount(SimpleDoubleProperty paidbillamount) {
		this.paidbillamount = paidbillamount;
	}
	public double getPaidamtReceived() {
		return paidamtReceived.get();
	}
	public void setPaidamtReceived(SimpleDoubleProperty paidamtReceived) {
		this.paidamtReceived = paidamtReceived;
	}
	public String getStatus() {
		return status.get();
	}
	public void setStatus(SimpleStringProperty status) {
		this.status = status;
	}
	public String getRemarks() {
		return remarks.get();
	}
	public void setRemarks(SimpleStringProperty remarks) {
		this.remarks = remarks;
	}
	public String getPaidbankName() {
		return paidbankName.get();
	}
	public void setPaidbankName(SimpleStringProperty paidbankName) {
		this.paidbankName = paidbankName;
	}
	
	

}
