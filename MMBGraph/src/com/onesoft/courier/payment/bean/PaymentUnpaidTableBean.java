package com.onesoft.courier.payment.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class PaymentUnpaidTableBean {
	
	private SimpleIntegerProperty paymentserialno;
	private SimpleIntegerProperty chequeNo;
	
	private SimpleStringProperty invoice_number;
	private SimpleStringProperty invoice_date;
	private SimpleStringProperty clientcode;
	private SimpleStringProperty clientname;
	private SimpleStringProperty paymentDate;
	private SimpleStringProperty paymentMode;
	private SimpleStringProperty bankName;
	private SimpleStringProperty chequeDate;
	private SimpleStringProperty from_date;
	private SimpleStringProperty to_date;
	private SimpleStringProperty status;
	private SimpleStringProperty remarks;
	
	private SimpleDoubleProperty amount;
	private SimpleDoubleProperty amtReceived;
	private SimpleDoubleProperty tds;
	private SimpleDoubleProperty dbNote;
	private SimpleDoubleProperty adjustment;
	private SimpleDoubleProperty due;
	private SimpleDoubleProperty balance;
	
	
	public PaymentUnpaidTableBean(int paymentserialno,String clientname,String invoicenumber, String invoicedate,double billamount,
			double amtreceived,double due,String paymentmode,String status) {
		
		super();
		this.paymentserialno=new SimpleIntegerProperty(paymentserialno);
		this.clientname=new SimpleStringProperty(clientname);
		this.invoice_number=new SimpleStringProperty(invoicenumber);
		this.invoice_date=new SimpleStringProperty(invoicedate);
		this.amount=new SimpleDoubleProperty(billamount);
		this.amtReceived=new SimpleDoubleProperty(amtreceived);
		this.due=new SimpleDoubleProperty(due);
		this.paymentMode=new SimpleStringProperty(paymentmode);
		this.status=new SimpleStringProperty(status);
	}
	
	
	public int getChequeNo() {
		return chequeNo.get();
	}
	public void setChequeNo(SimpleIntegerProperty chequeNo) {
		this.chequeNo = chequeNo;
	}
	
	
	public String getInvoice_number() {
		return invoice_number.get();
	}
	public void setInvoice_number(SimpleStringProperty invoice_number) {
		this.invoice_number = invoice_number;
	}
	
	
	public String getInvoice_date() {
		return invoice_date.get();
	}
	public void setInvoice_date(SimpleStringProperty invoice_date) {
		this.invoice_date = invoice_date;
	}
	
	
	public String getClientcode() {
		return clientcode.get();
	}
	public void setClientcode(SimpleStringProperty clientcode) {
		this.clientcode = clientcode;
	}
	
	
	public String getClientname() {
		return clientname.get();
	}
	public void setClientname(SimpleStringProperty clientname) {
		this.clientname = clientname;
	}
	
	
	public String getPaymentDate() {
		return paymentDate.get();
	}
	public void setPaymentDate(SimpleStringProperty paymentDate) {
		this.paymentDate = paymentDate;
	}
	
	
	public String getPaymentMode() {
		return paymentMode.get();
	}
	public void setPaymentMode(SimpleStringProperty paymentMode) {
		this.paymentMode = paymentMode;
	}
	
	
	public String getBankName() {
		return bankName.get();
	}
	public void setBankName(SimpleStringProperty bankName) {
		this.bankName = bankName;
	}
	
	
	public String getChequeDate() {
		return chequeDate.get();
	}
	public void setChequeDate(SimpleStringProperty chequeDate) {
		this.chequeDate = chequeDate;
	}
	
	
	public String getFrom_date() {
		return from_date.get();
	}
	public void setFrom_date(SimpleStringProperty from_date) {
		this.from_date = from_date;
	}
	
	
	public String getTo_date() {
		return to_date.get();
	}
	public void setTo_date(SimpleStringProperty to_date) {
		this.to_date = to_date;
	}
	
	
	public String getStatus() {
		return status.get();
	}
	public void setStatus(SimpleStringProperty status) {
		this.status = status;
	}
	
	
	public String getRemarks() {
		return remarks.get();
	}
	public void setRemarks(SimpleStringProperty remarks) {
		this.remarks = remarks;
	}
	
	
	public double getAmtReceived() {
		return amtReceived.get();
	}
	public void setAmtReceived(SimpleDoubleProperty amtReceived) {
		this.amtReceived = amtReceived;
	}
	
	
	public double getTds() {
		return tds.get();
	}
	public void setTds(SimpleDoubleProperty tds) {
		this.tds = tds;
	}
	
	
	public double getDbNote() {
		return dbNote.get();
	}
	public void setDbNote(SimpleDoubleProperty dbNote) {
		this.dbNote = dbNote;
	}
	
	
	public double getAdjustment() {
		return adjustment.get();
	}
	public void setAdjustment(SimpleDoubleProperty adjustment) {
		this.adjustment = adjustment;
	}
	
	
	public double getDue() {
		return due.get();
	}
	public void setDue(SimpleDoubleProperty due) {
		this.due = due;
	}


	public int getPaymentserialno() {
		return paymentserialno.get();
	}


	public void setPaymentserialno(SimpleIntegerProperty paymentserialno) {
		this.paymentserialno = paymentserialno;
	}


	public double getBalance() {
		return balance.get();
	}


	public void setBalance(SimpleDoubleProperty balance) {
		this.balance = balance;
	}


	public double getAmount() {
		return amount.get();
	}


	public void setAmount(SimpleDoubleProperty amount) {
		this.amount = amount;
	}
	
}
