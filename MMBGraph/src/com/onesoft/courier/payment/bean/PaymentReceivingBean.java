package com.onesoft.courier.payment.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class PaymentReceivingBean {
	
	private int serialno;
	private String currentDate;
	private String invoice_number;
	private String invoice_date;
	private String clientcode;
	private String clientname;
	private String paymentDate;
	private Double billamount;
	private Double amtReceived;
	private Double tds;
	private Double dbNote;
	private Double adjustment;
	private Double due;
	private String paymentMode;
	private String bankName;
	private String chequeDate;
	private int chequeNo;
	private String from_date;
	private String to_date;
	private String status;
	private String paymentremarks;
	private Double balance;
	
	
	private int paidserialno;
	private String paidclientname;
	private String paidinvoicenumber;
	private String paidinvoicedate;
	private String paidpaymentDate;
	private String paidpaymentMode;
	private Double paidbillamount;
	private Double paidamtReceived;
	
	
	
	
	public int getPaidserialno() {
		return paidserialno;
	}
	public void setPaidserialno(int paidserialno) {
		this.paidserialno = paidserialno;
	}
	public String getPaidclientname() {
		return paidclientname;
	}
	public void setPaidclientname(String paidclientname) {
		this.paidclientname = paidclientname;
	}
	public String getPaidinvoicenumber() {
		return paidinvoicenumber;
	}
	public void setPaidinvoicenumber(String paidinvoicenumber) {
		this.paidinvoicenumber = paidinvoicenumber;
	}
	public String getPaidinvoicedate() {
		return paidinvoicedate;
	}
	public void setPaidinvoicedate(String paidinvoicedate) {
		this.paidinvoicedate = paidinvoicedate;
	}
	public String getPaidpaymentDate() {
		return paidpaymentDate;
	}
	public void setPaidpaymentDate(String paidpaymentDate) {
		this.paidpaymentDate = paidpaymentDate;
	}
	public String getPaidpaymentMode() {
		return paidpaymentMode;
	}
	public void setPaidpaymentMode(String paidpaymentMode) {
		this.paidpaymentMode = paidpaymentMode;
	}
	public Double getPaidbillamount() {
		return paidbillamount;
	}
	public void setPaidbillamount(Double paidbillamount) {
		this.paidbillamount = paidbillamount;
	}
	public Double getPaidamtReceived() {
		return paidamtReceived;
	}
	public void setPaidamtReceived(Double paidamtReceived) {
		this.paidamtReceived = paidamtReceived;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getInvoice_number() {
		return invoice_number;
	}
	public void setInvoice_number(String invoice_number) {
		this.invoice_number = invoice_number;
	}
	public String getInvoice_date() {
		return invoice_date;
	}
	public void setInvoice_date(String invoice_date) {
		this.invoice_date = invoice_date;
	}
	public String getClientcode() {
		return clientcode;
	}
	public void setClientcode(String clientcode) {
		this.clientcode = clientcode;
	}
	public String getClientname() {
		return clientname;
	}
	public void setClientname(String clientname) {
		this.clientname = clientname;
	}
	public String getFrom_date() {
		return from_date;
	}
	public void setFrom_date(String from_date) {
		this.from_date = from_date;
	}
	public String getTo_date() {
		return to_date;
	}
	public void setTo_date(String to_date) {
		this.to_date = to_date;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public Double getAmtReceived() {
		return amtReceived;
	}
	public void setAmtReceived(Double amtReceived) {
		this.amtReceived = amtReceived;
	}
	public Double getTds() {
		return tds;
	}
	public void setTds(Double tds) {
		this.tds = tds;
	}
	public Double getDbNote() {
		return dbNote;
	}
	public void setDbNote(Double dbNote) {
		this.dbNote = dbNote;
	}
	public Double getAdjustment() {
		return adjustment;
	}
	public void setAdjustment(Double adjustment) {
		this.adjustment = adjustment;
	}
	public Double getDue() {
		return due;
	}
	public void setDue(Double due) {
		this.due = due;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}
	public int getChequeNo() {
		return chequeNo;
	}
	public void setChequeNo(int chequeNo) {
		this.chequeNo = chequeNo;
	}
	
	public String getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}
	public Double getBillamount() {
		return billamount;
	}
	public void setBillamount(Double billamount) {
		this.billamount = billamount;
	}
	public int getSerialno() {
		return serialno;
	}
	public void setSerialno(int serialno) {
		this.serialno = serialno;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public String getPaymentremarks() {
		return paymentremarks;
	}
	public void setPaymentremarks(String paymentremarks) {
		this.paymentremarks = paymentremarks;
	}
	
	
	

}
