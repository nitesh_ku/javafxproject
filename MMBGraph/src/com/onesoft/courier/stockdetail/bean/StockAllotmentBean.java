package com.onesoft.courier.stockdetail.bean;

public class StockAllotmentBean {
	
	
	private int slno;
	private String typeClientOrEmployee;
	private String allottedTo;
	private String branchCode;
	private String supplierCode;
	private String itemCode;
	private String seriesType;
	private String seriesTypeAwbFrom;
	private String seriesTypeAwbTo;
	private long awbFromNumeric;
	private long awbToNumeric;
	private String awbFrom;
	private String awbTo;
	private int expiryDays;
	private String expiryDate;

	private long inputAwbFromNumeric;
	private long inputAwbToNumeric;
	
	private int minimumStock;
	private String createDate;
	private int pcs;
	private double rate;
	private double amount;
	private String inDate;
	
	private int totalPcs;
	
	private String originalSeriesFrom;
	private String originalSeriesTo;
	private String remarks;
	
	private String allotmentStatus;
	
	private long checkAwbFromNumber;
	private long checkAwbToNumber;
	
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getSupplierCode() {
		return supplierCode;
	}
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getAwbFrom() {
		return awbFrom;
	}
	public void setAwbFrom(String awbFrom) {
		this.awbFrom = awbFrom;
	}
	public String getAwbTo() {
		return awbTo;
	}
	public void setAwbTo(String awbTo) {
		this.awbTo = awbTo;
	}
	public int getPcs() {
		return pcs;
	}
	public void setPcs(int pcs) {
		this.pcs = pcs;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getSeriesType() {
		return seriesType;
	}
	public void setSeriesType(String seriesType) {
		this.seriesType = seriesType;
	}
	public int getExpiryDays() {
		return expiryDays;
	}
	public void setExpiryDays(int expiryDays) {
		this.expiryDays = expiryDays;
	}
	public int getMinimumStock() {
		return minimumStock;
	}
	public void setMinimumStock(int minimumStock) {
		this.minimumStock = minimumStock;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	
	public long getAwbFromNumeric() {
		return awbFromNumeric;
	}
	public void setAwbFromNumeric(long awbFromNumeric) {
		this.awbFromNumeric = awbFromNumeric;
	}
	public long getAwbToNumeric() {
		return awbToNumeric;
	}
	public void setAwbToNumeric(long awbToNumeric) {
		this.awbToNumeric = awbToNumeric;
	}
	public String getInDate() {
		return inDate;
	}
	public void setInDate(String inDate) {
		this.inDate = inDate;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public int getSlno() {
		return slno;
	}
	public void setSlno(int slno) {
		this.slno = slno;
	}
	public String getSeriesTypeAwbFrom() {
		return seriesTypeAwbFrom;
	}
	public void setSeriesTypeAwbFrom(String seriesTypeAwbFrom) {
		this.seriesTypeAwbFrom = seriesTypeAwbFrom;
	}
	public String getSeriesTypeAwbTo() {
		return seriesTypeAwbTo;
	}
	public void setSeriesTypeAwbTo(String seriesTypeAwbTo) {
		this.seriesTypeAwbTo = seriesTypeAwbTo;
	}
	public String getAllottedTo() {
		return allottedTo;
	}
	public void setAllottedTo(String allottedTo) {
		this.allottedTo = allottedTo;
	}
	
	public long getCheckAwbFromNumber() {
		return checkAwbFromNumber;
	}
	public void setCheckAwbFromNumber(long checkAwbFromNumber) {
		this.checkAwbFromNumber = checkAwbFromNumber;
	}
	public long getCheckAwbToNumber() {
		return checkAwbToNumber;
	}
	public void setCheckAwbToNumber(long checkAwbToNumber) {
		this.checkAwbToNumber = checkAwbToNumber;
	}
	public int getTotalPcs() {
		return totalPcs;
	}
	public void setTotalPcs(int totalPcs) {
		this.totalPcs = totalPcs;
	}
	public String getTypeClientOrEmployee() {
		return typeClientOrEmployee;
	}
	public void setTypeClientOrEmployee(String typeClientOrEmployee) {
		this.typeClientOrEmployee = typeClientOrEmployee;
	}
	public String getOriginalSeriesFrom() {
		return originalSeriesFrom;
	}
	public void setOriginalSeriesFrom(String originalSeriesFrom) {
		this.originalSeriesFrom = originalSeriesFrom;
	}
	public String getOriginalSeriesTo() {
		return originalSeriesTo;
	}
	public void setOriginalSeriesTo(String originalSeriesTo) {
		this.originalSeriesTo = originalSeriesTo;
	}
	public String getAllotmentStatus() {
		return allotmentStatus;
	}
	public void setAllotmentStatus(String allotmentStatus) {
		this.allotmentStatus = allotmentStatus;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public long getInputAwbFromNumeric() {
		return inputAwbFromNumeric;
	}
	public void setInputAwbFromNumeric(long inputAwbFromNumeric) {
		this.inputAwbFromNumeric = inputAwbFromNumeric;
	}
	public long getInputAwbToNumeric() {
		return inputAwbToNumeric;
	}
	public void setInputAwbToNumeric(long inputAwbToNumeric) {
		this.inputAwbToNumeric = inputAwbToNumeric;
	}

	
	

}
