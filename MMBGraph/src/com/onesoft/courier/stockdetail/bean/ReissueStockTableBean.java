package com.onesoft.courier.stockdetail.bean;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class ReissueStockTableBean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty typeClientOrEmployee;
	private SimpleStringProperty allottedTo;
	
	private SimpleStringProperty supplierCode;
	private SimpleStringProperty itemCode;
	private SimpleStringProperty awbFrom;
	private SimpleStringProperty awbTo;
	private SimpleIntegerProperty expiryDays;
	private SimpleStringProperty expiryDate;
	private SimpleIntegerProperty pcs;
	private SimpleStringProperty indate;
	
	
	/*private SimpleDoubleProperty rate;
	private SimpleDoubleProperty amount;
	private SimpleIntegerProperty minimumStock;
	private SimpleStringProperty createDate;
	private SimpleStringProperty network;
	private SimpleStringProperty seriesType;
	private SimpleLongProperty awbFromNumeric;
	private SimpleLongProperty awbToNumeric;*/
	
	
	
	public ReissueStockTableBean(int slno,String type,String allottedTo,String supplier,String item,String awbFrom,String awbTo,String inDate,String expiryDate,int pcs)
	{
		
		this.slno=new SimpleIntegerProperty(slno);
		this.allottedTo=new SimpleStringProperty(allottedTo);
		this.typeClientOrEmployee=new SimpleStringProperty(type);
		this.supplierCode=new SimpleStringProperty(supplier);
		this.itemCode=new SimpleStringProperty(item);
		this.awbFrom=new SimpleStringProperty(awbFrom);
		this.awbTo=new SimpleStringProperty(awbTo);
		this.expiryDate=new SimpleStringProperty(expiryDate);
		this.indate=new SimpleStringProperty(inDate);
		this.pcs=new SimpleIntegerProperty(pcs);
		
		/*this.rate=new SimpleDoubleProperty(rate);
		this.amount=new SimpleDoubleProperty(amount);
		this.network=new SimpleStringProperty(network);*/
		
	}
	
	

	public String getSupplierCode() {
		return supplierCode.get();
	}
	public void setSupplierCode(SimpleStringProperty supplierCode) {
		this.supplierCode = supplierCode;
	}
	public String getItemCode() {
		return itemCode.get();
	}
	public void setItemCode(SimpleStringProperty itemCode) {
		this.itemCode = itemCode;
	}
	public String getAwbFrom() {
		return awbFrom.get();
	}
	public void setAwbFrom(SimpleStringProperty awbFrom) {
		this.awbFrom = awbFrom;
	}
	public String getAwbTo() {
		return awbTo.get();
	}
	public void setAwbTo(SimpleStringProperty awbTo) {
		this.awbTo = awbTo;
	}
	public int getExpiryDays() {
		return expiryDays.get();
	}
	public void setExpiryDays(SimpleIntegerProperty expiryDays) {
		this.expiryDays = expiryDays;
	}
	
	public int getPcs() {
		return pcs.get();
	}
	public void setPcs(SimpleIntegerProperty pcs) {
		this.pcs = pcs;
	}
	
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	public String getIndate() {
		return indate.get();
	}
	public void setIndate(SimpleStringProperty indate) {
		this.indate = indate;
	}
	public String getExpiryDate() {
		return expiryDate.get();
	}
	public void setExpiryDate(SimpleStringProperty expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getAllottedTo() {
		return allottedTo.get();
	}
	public void setAllottedTo(SimpleStringProperty allottedTo) {
		this.allottedTo = allottedTo;
	}



	public String getTypeClientOrEmployee() {
		return typeClientOrEmployee.get();
	}



	public void setTypeClientOrEmployee(SimpleStringProperty typeClientOrEmployee) {
		this.typeClientOrEmployee = typeClientOrEmployee;
	}


	
	/*public double getRate() {
	return rate.get();
}
public void setRate(SimpleDoubleProperty rate) {
	this.rate = rate;
}
public double getAmount() {
	return amount.get();
}
public void setAmount(SimpleDoubleProperty amount) {
	this.amount = amount;
}
public String getNetwork() {
	return network.get();
}
public void setNetwork(SimpleStringProperty network) {
	this.network = network;
}*/
	

}
