package com.onesoft.courier.stockdetail.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.DB.DBconnection;
import com.onesoft.courier.common.LoadClientBean;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadEmployee;
import com.onesoft.courier.common.LoadItem;
import com.onesoft.courier.common.LoadSupplier;
import com.onesoft.courier.common.bean.LoadEmployeeBean;
import com.onesoft.courier.common.bean.LoadItemBean;
import com.onesoft.courier.common.bean.LoadSupplierBean;
import com.onesoft.courier.stockdetail.bean.IncomingStockBean;
import com.onesoft.courier.stockdetail.bean.ReissueStockBean;
import com.onesoft.courier.stockdetail.bean.ReissueStockTableBean;
import com.onesoft.courier.stockdetail.bean.StockAllotmentBean;
import com.onesoft.courier.stockdetail.bean.StockAllotmentTableBean;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Pagination;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class ReIssueStockController implements Initializable{
	
	private ObservableList<ReissueStockTableBean> tabledata_ReissueAndAllocated=FXCollections.observableArrayList();
	
	private ObservableList<String> comboBoxItemItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxAllotToClientAndEmployeeItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxSupplierItems=FXCollections.observableArrayList("DEF | DEFAULT");
	
	ReissueStockBean reissueStockBean=new ReissueStockBean();
	List<Long> listCheckSeriesFrom=new ArrayList<>();
	List<Long> listCheckSeriesTo=new ArrayList<>();
	
	boolean checkIncomingStockAwbToValue=false;

	private Set<StockAllotmentBean> set_loadSeriesTypeData=new HashSet<>();
	
	Pattern pattern = Pattern.compile("\\d+");
	
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	// ===============================================	
	
	@FXML
	private ImageView imgSearch;
	
	@FXML
	private RadioButton rdBtnClient;
	
	@FXML
	private RadioButton rdBtnEmployee;
	
	@FXML
	private ComboBox<String> comboBoxAllotToEmpOrClient;

	@FXML
	private ComboBox<String> comboBoxItem;
	
	@FXML
	private ComboBox<String> comboBoxSupplier;
	
// ===============================================
	
	@FXML
	private TextField txtFrom;
	
	@FXML
	private TextField txtTo;
	
	@FXML
	private TextField txtPcs;
	
	@FXML
	private TextField txtRate;
	
	@FXML
	private TextField txtAmount;
	
// ===============================================	
	
	@FXML
	private DatePicker dpkInDate;
	
	@FXML
	private DatePicker dpkExpDate;
	
// ===============================================
	
	@FXML
	private Button btnAllotTo;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private Button btnExit;
	
	@FXML
	private Button btnExportToExcel;
	
// ===============================================
	
	@FXML
	private Pagination pagination;
	
	
// ===============================================	
	
	@FXML
	private TableView<ReissueStockTableBean> tableStockAllocated;
	
	@FXML
	private TableColumn<ReissueStockTableBean, Integer> allot_tableCol_Slno;
	
	@FXML
	private TableColumn<ReissueStockTableBean, String> allot_tableCol_AllottedTo;
	
	@FXML
	private TableColumn<ReissueStockTableBean, String> allot_tableCol_Type;
	
	@FXML
	private TableColumn<ReissueStockTableBean, String> allot_tableCol_Supplier;
	
	@FXML
	private TableColumn<ReissueStockTableBean, String> allot_tableCol_Item;
	
/*	@FXML
	private TableColumn<IncomingStockTableBean, String> allot_tableCol_Network;*/
	
	@FXML
	private TableColumn<ReissueStockTableBean, String> allot_tableCol_AwbFrom;
	
	@FXML
	private TableColumn<ReissueStockTableBean, String> allot_tableCol_AwbTo;
	
	@FXML
	private TableColumn<ReissueStockTableBean, String> allot_tableCol_InDate;
	
	@FXML
	private TableColumn<ReissueStockTableBean, String> allot_tableCol_ExpiryDate;
	
	@FXML
	private TableColumn<ReissueStockTableBean, Double> allot_tableCol_Pcs;
	
	/*@FXML
	private TableColumn<IncomingStockTableBean, Double> tableCol_Rate;
	
	@FXML
	private TableColumn<IncomingStockTableBean, Double> tableCol_Amount;*/
	
	
	
// =============================================================================================
	
	public void checkRadioButtonStatus() throws SQLException
	{
		if (rdBtnClient.isSelected()==true)
		{
				loadClientWithName();
		}
		else
		{
			loadEmployeeWithName();
		}
	}
		
// =============================================================================================
		
	public void loadItemWithName() throws SQLException
	{
		new LoadItem().loadItemWithName();
		
		for(LoadItemBean bean:LoadItem.SET_LOADITEMSWITHNAME)
		{
			comboBoxItemItems.add(bean.getItemCode()+" | "+bean.getItemName());
		}
		comboBoxItem.setItems(comboBoxItemItems);
	}
		

// =============================================================================================	

	public void loadSupplierWithName() throws SQLException
	{
		new LoadSupplier().loadSupplierWithName();
		
		for(LoadSupplierBean bean:LoadSupplier.SET_LOADSUPPLIERWITHNAME)
		{
			comboBoxSupplierItems.add(bean.getSupplierCode()+" | "+bean.getSupplierName());
		}
		comboBoxSupplier.setItems(comboBoxSupplierItems);
	}	
	
		
// =============================================================================================
		
	public void loadEmployeeWithName() throws SQLException
	{
		comboBoxAllotToClientAndEmployeeItems.clear();
		
		new LoadEmployee().loadEmployeeWithName();
		
		for(LoadEmployeeBean bean:LoadEmployee.SET_LOAD_EMPLOYEE)
		{
			comboBoxAllotToClientAndEmployeeItems.add(bean.getEmployeeCode()+" | "+bean.getEmployeeName());
		}
		comboBoxAllotToEmpOrClient.setItems(comboBoxAllotToClientAndEmployeeItems);
	}
		
			
// =============================================================================================
			
	public void loadClientWithName() throws SQLException
	{
		comboBoxAllotToClientAndEmployeeItems.clear();
		
		new LoadClients().loadClientWithName();
		
		for(LoadClientBean bean:LoadClients.SET_LOAD_CLIENTWITHNAME)
		{
			comboBoxAllotToClientAndEmployeeItems.add(bean.getClientCode()+" | "+bean.getClientName());
		}
		comboBoxAllotToEmpOrClient.setItems(comboBoxAllotToClientAndEmployeeItems);
	}	

// =============================================================================================

	public void getSeriesTypeData() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		String sql=null;
		Statement stmt=null;
		ResultSet rs=null;
		
		try
		{	
			stmt=con.createStatement();
			sql="select seriestypeid,series,series2vendor from seriestype order by seriestypeid";
			rs=stmt.executeQuery(sql);
			
			while(rs.next())
			{
				StockAllotmentBean inbean=new StockAllotmentBean();

				inbean.setSeriesType(rs.getString("series"));
				inbean.setSupplierCode(rs.getString("series2vendor"));
				
				set_loadSeriesTypeData.add(inbean);
				
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
		
		
// =============================================================================================	
		
	public void checkSeriesType() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Empty field alert");
		alert.setHeaderText(null);
		
		if(comboBoxAllotToEmpOrClient.getValue()==null || comboBoxAllotToEmpOrClient.getValue().isEmpty())
		{
			alert.setContentText("Allot To field is Empty!");
			alert.showAndWait();
			comboBoxAllotToEmpOrClient.requestFocus();
		}
		else if(comboBoxSupplier.getValue()==null || comboBoxSupplier.getValue().isEmpty())
		{
			alert.setContentText("Supplier field is Empty!");
			alert.showAndWait();
			comboBoxSupplier.requestFocus();
		}
		else if(comboBoxItem.getValue()==null || comboBoxItem.getValue().isEmpty())
		{
			alert.setContentText("Item field is Empty!");
			alert.showAndWait();
			comboBoxItem.requestFocus();
		}
		
		else if(txtFrom.getText()==null || txtFrom.getText().isEmpty())
		{
			alert.setContentText("From field is empty");
			alert.showAndWait();
			txtFrom.requestFocus();
		}
		else
		{
			boolean checkResult=false;
			
			Matcher matcher = pattern.matcher(txtFrom.getText());
			String[] supplierCode=comboBoxSupplier.getValue().replaceAll("\\s+","").split("\\|");
			String[] itemCode=comboBoxItem.getValue().replaceAll("\\s+","").split("\\|");
			
			StockAllotmentBean stockAllotBean=new StockAllotmentBean();
			
			if(matcher.matches())				// Condition to check, Entered "Awb no From" is alphanumeric or not
			{
				stockAllotBean.setSeriesType("DEF");
				stockAllotBean.setSupplierCode("DEFAULT");
			}
			else
			{
				stockAllotBean.setSeriesType(txtFrom.getText().replaceAll("[^A-Za-z]+", "").toUpperCase());
			 	stockAllotBean.setSupplierCode(supplierCode[0]);
			}
		
			
			for(StockAllotmentBean bean: set_loadSeriesTypeData)
			{
				if(stockAllotBean.getSeriesType().equals(bean.getSeriesType()) && stockAllotBean.getSupplierCode().equals(bean.getSupplierCode()))
				{
					checkResult=true;
				}
			}
				
			if(checkResult==false)
			{
				txtFrom.requestFocus();
				Alert alert1 = new Alert(AlertType.INFORMATION);
				alert1.setTitle("Record not found");
				alert1.setHeaderText(null);
				alert1.setContentText("Series does not exist for selected supplier...!!");
				alert1.showAndWait();
			}
			else
			{
				checkAwbFromNumber();
				checkResult=false;	 
			}
		}
	}	
		
		
// =============================================================================================
		
	public void checkAwbFromNumber() throws SQLException
	{
			
		ReissueStockBean stockAllotBean=new ReissueStockBean();
			
		txtTo.setText(txtFrom.getText());
		String[] itemCode=comboBoxItem.getValue().replaceAll("\\s+","").split("\\|");
		String[] supplierCode=comboBoxSupplier.getValue().replaceAll("\\s+","").split("\\|");
		
		stockAllotBean.setItemCode(itemCode[0]);
			
	    Matcher matcher = pattern.matcher(txtFrom.getText());
		 	  
	    if(matcher.matches())				// Condition to check, Entered "Awb no From" is alphanumeric or not
	    {
	    	stockAllotBean.setSeriesType("DEF");
	    	stockAllotBean.setSupplierCode("DEFAULT");
	    	stockAllotBean.setAwbFromNumeric(Long.valueOf(txtFrom.getText()));
	    }
	    else
	    {
	    	stockAllotBean.setSeriesType(txtFrom.getText().replaceAll("[^A-Za-z]+", "").toUpperCase());
	    	stockAllotBean.setAwbFromNumeric(Long.parseLong(txtFrom.getText().replaceAll("\\D+","")));
	    	stockAllotBean.setSupplierCode(supplierCode[0]);
	    }
				
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		ResultSet rs=null;
		PreparedStatement preparedStmt=null;
		
		try
		{	
			
			String sql="select supplier_code,stockallotment2item,stockallotment2seriestype,awb_from_number,awb_to_number from stock_Allotment where ? BETWEEN awb_from_number AND awb_to_number and supplier_code=? and stockallotment2seriestype=?";
			
			//String query = "select incomingstock2vendor,incomingstock2item,incomingstock2seriestype,awb_from_number,awb_to_number from stock_incoming where incomingstock2vendor=? and incomingstock2seriestype=?";
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setLong(1, stockAllotBean.getAwbFromNumeric());
			preparedStmt.setString(2, stockAllotBean.getSupplierCode());
			preparedStmt.setString(3, stockAllotBean.getSeriesType());
			//preparedStmt.setString(4, "A");
			//System.out.println("SQL Query: "+preparedStmt);
			rs = preparedStmt.executeQuery();
			
				
			if(!rs.next())
			{
				txtTo.clear();
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Record Alert");
				alert.setHeaderText(null);
				alert.setContentText("Record dosen't exist...");
				alert.showAndWait();
				txtFrom.requestFocus();
				resetFieldsForAwbTOField();
			}
			else
			{
				txtTo.setText(txtFrom.getText());
			}
				
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
		
	}	
	
	
// ========================================================================================		
		
	public void checkAwbToNumber()
	{
		long tatalpcs=0;
		
		ResultSet rs=null;
		Statement stmt=null;
		String sql=null;
		DBconnection dbcon=new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("AWB To series Alert");
		alert.setHeaderText(null);
		
		
		if(!txtFrom.getText().equals(""))
		{
			if(!txtTo.getText().equals(""))
			{
			
				String[] supplierCode=comboBoxSupplier.getValue().replaceAll("\\s+","").split("\\|");
				StockAllotmentBean stockAllotBean=new StockAllotmentBean();
			
				Matcher matcher = pattern.matcher(txtTo.getText());
			  
				if(matcher.matches())				// Condition to check, Entered "Awb no From" is alphanumeric or not
				{
					stockAllotBean.setSeriesType("DEF");
					stockAllotBean.setSeriesTypeAwbTo("DEF");
					stockAllotBean.setSupplierCode("DEFAULT");
					stockAllotBean.setAwbFromNumeric(Long.valueOf(txtFrom.getText()));
					stockAllotBean.setAwbToNumeric(Long.valueOf(txtTo.getText()));
					tatalpcs=stockAllotBean.getAwbToNumeric()-stockAllotBean.getAwbFromNumeric();
				}
				else
				{
					stockAllotBean.setSeriesType(txtFrom.getText().replaceAll("[^A-Za-z]+", "").toUpperCase());
					stockAllotBean.setSeriesTypeAwbTo(txtTo.getText().replaceAll("[^A-Za-z]+", "").toUpperCase());
					stockAllotBean.setSupplierCode(supplierCode[0]);
					stockAllotBean.setAwbFromNumeric(Long.parseLong(txtFrom.getText().replaceAll("\\D+","")));
					stockAllotBean.setAwbToNumeric(Long.parseLong(txtTo.getText().replaceAll("\\D+","")));
					tatalpcs=stockAllotBean.getAwbToNumeric()-stockAllotBean.getAwbFromNumeric();
				}
				
				try
				{	
					if(stockAllotBean.getSeriesType().equals(stockAllotBean.getSeriesTypeAwbTo()))
					{
						if(tatalpcs>=0)
						{
							
							stmt=con.createStatement();
							sql="select * from stock_allotment where "+stockAllotBean.getAwbToNumeric()+" BETWEEN awb_from_number AND "
									+ "awb_to_number and supplier_code='"+stockAllotBean.getSupplierCode()+"' and stockallotment2seriestype='"+stockAllotBean.getSeriesTypeAwbTo()+"'";
						
							rs=stmt.executeQuery(sql);
							
							if(!rs.next())
							{
								txtTo.requestFocus();
								alert.setContentText("Awb To series range does not exist");
								alert.showAndWait();
								resetFieldsForAwbTOField();
								checkIncomingStockAwbToValue=true;
							}
							else
							{
								checkBlankSeriesBetweenNewSeries(rs.getString("stockallotment2seriestype"),rs.getString("supplier_code"));
							}
						}
						else
						{
							alert.setContentText("Please enter the correct series range...!!");
							alert.showAndWait();
							txtTo.requestFocus();
							resetFieldsForAwbTOField();
						}
					}
					else
					{
						alert.setContentText("TO series type not matched with From Series type...!\nPlease enter correct Series Type");
						alert.showAndWait();
						txtTo.requestFocus();
						resetFieldsForAwbTOField();
					}
				}
				catch(Exception e)
				{
					System.out.println(e);
					e.printStackTrace();
				}	
			}
			else
			{
				alert.setContentText("To Field is Empty! Please enter the range");
				alert.showAndWait();
				txtTo.requestFocus();
				resetFieldsForAwbTOField();
			}
		}
		else
		{
			alert.setContentText("From Field is Empty! Please enter the range");
			alert.showAndWait();
			txtFrom.requestFocus();
			resetFieldsForAwbTOField();
		}
	}	
	

// =============================================================================================	

	public void checkBlankSeriesBetweenNewSeries(String seriestype,String supplierCode )
	{
		int checkDifferenceFromFirstSeries=0;

		int result=0;
		boolean status=false;
		
		ReissueStockBean reBean=new ReissueStockBean();
		
		reBean.setInputAwbFromNumeric(Long.parseLong(txtFrom.getText().replaceAll("\\D+","")));
	   	reBean.setInputAwbToNumeric(Long.parseLong(txtTo.getText().replaceAll("\\D+","")));
	   	
		ResultSet rs=null;
		Statement stmt=null;
		String sql=null;
		DBconnection dbcon=new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		try
		{
			stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			sql="select awb_from_number,awb_to_number,rate,in_date,expiry_date from stock_allotment where "
					+reBean.getInputAwbFromNumeric()+" BETWEEN awb_from_number AND awb_to_number and supplier_code='"
						+supplierCode+"' and stockallotment2seriestype='"+seriestype+"'";
			
			rs=stmt.executeQuery(sql);
					
			if(!rs.next())
			{
					
			}
			else
			{
				reBean.setAwbFromNumeric(rs.getLong("awb_from_number"));
				reBean.setAwbToNumeric(rs.getLong("awb_to_number"));
				reBean.setRate(rs.getDouble("rate"));
				
			}
			rs.last();
					
			
			sql="select awb_from_number,awb_to_number,rate,in_date,expiry_date from stock_allotment where "
					+reBean.getInputAwbToNumeric()+" BETWEEN awb_from_number AND awb_to_number and supplier_code='"
						+supplierCode+"' and stockallotment2seriestype='"+seriestype+"'";
			rs=stmt.executeQuery(sql);
				
			if(!rs.next())
			{
				
			}
			else
			{
				
				reBean.setCheckAwbFromNumber(rs.getLong("awb_from_number"));
				reBean.setCheckAwbToNumber(rs.getLong("awb_to_number"));
			}
			rs.last();	
			
		
			checkDifferenceFromFirstSeries=(int) (reBean.getCheckAwbFromNumber()-reBean.getAwbToNumeric());
		
			if(checkDifferenceFromFirstSeries<=1)
			{
				checkSeriesAndLoadData();
				
			}
			else
			{	
				sql="select awb_from_number,awb_to_number from stock_allotment where awb_to_number between "
						+reBean.getAwbFromNumeric()+" AND "+reBean.getCheckAwbToNumber()+" and supplier_code='"+supplierCode+"' and "
							+ "stockallotment2seriestype='"+seriestype+"' order by awb_to_number";
				
				System.out.println("third sql: "+sql);
				rs=stmt.executeQuery(sql);
			
				if(!rs.next())
				{
					
				}
				else
				{
					do
					{
						listCheckSeriesFrom.add(rs.getLong("awb_from_number"));
						listCheckSeriesTo.add(rs.getLong("awb_to_number"));
					}
					while(rs.next());
				}
						
				
				for(int i=0;i<listCheckSeriesFrom.size()-1;i++)
				{
					result=(int) (listCheckSeriesFrom.get(i+1)-listCheckSeriesTo.get(i));
					if(result==1)
					{
						status=true;
					}
					else
					{
						status=false;
						break;
					}
				}
				
				if(status==true)
				{
					checkSeriesAndLoadData();
				}
				else
				{
					resetFieldsForAwbTOField();
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Record not found");
					alert.setHeaderText(null);
					alert.setContentText("Please select correct range...!!");
					alert.showAndWait();
					txtTo.requestFocus();
				
				}
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}	
		
		
// =============================================================================================
	
	public void checkSeriesAndLoadData() throws SQLException
	{
		
		String[] typeAllottedTo=comboBoxAllotToEmpOrClient.getValue().replaceAll("\\s+","").split("\\|");
		String[] supplierCode=comboBoxSupplier.getValue().replaceAll("\\s+","").split("\\|");
		String[] itemCode=comboBoxItem.getValue().replaceAll("\\s+","").split("\\|");
		
		StockAllotmentBean stockAllBean=new StockAllotmentBean();
		stockAllBean.setBranchCode(typeAllottedTo[0]);
		
		stockAllBean.setItemCode(itemCode[0]);
		
		Pattern pattern = Pattern.compile("\\d+");
	    Matcher matcher = pattern.matcher(txtFrom.getText());
		  
	    if(matcher.matches())				// Condition to check, Entered "Awb no From" is alphanumeric or not
	    {
	    	stockAllBean.setSeriesType("DEF");
	    	stockAllBean.setSupplierCode("DEFAULT");
	    	stockAllBean.setAwbFromNumeric(Long.valueOf(txtFrom.getText()));
	    	stockAllBean.setAwbToNumeric(Long.valueOf(txtTo.getText()));
	    }
	    else
	    {
	    	stockAllBean.setSeriesType(txtFrom.getText().replaceAll("[^A-Za-z]+", "").toUpperCase());
	    	stockAllBean.setSupplierCode(supplierCode[0]);
	    	stockAllBean.setAwbFromNumeric(Long.parseLong(txtFrom.getText().replaceAll("\\D+","")));
	    	stockAllBean.setAwbToNumeric(Long.parseLong(txtTo.getText().replaceAll("\\D+","")));
		}
	    
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		ResultSet rs=null;
		PreparedStatement preparedStmt=null;
		
		try
		{	
			
			String query = "select stockallotment2item,stockallotment2seriestype,stockallotmenttype,stockallotment2clientoremp,supplier_code,remarks,rate,in_date,create_date,expiry_date,originalseries_from,originalseries_to "
							+ "from stock_allotment where ? BETWEEN awb_from_number AND awb_to_number "
								+ "and supplier_code=? and stockallotment2seriestype=?";
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setLong(1, stockAllBean.getAwbToNumeric());
			preparedStmt.setString(2, stockAllBean.getSupplierCode());
			preparedStmt.setString(3, stockAllBean.getSeriesType());
			System.out.println("SQL Query: "+preparedStmt);
			rs = preparedStmt.executeQuery();
			
			
			if(!rs.next())
			{
				txtTo.clear();
				txtFrom.requestFocus();
				
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Record not found");
				alert.setHeaderText(null);
				alert.setContentText("Series/Supplier not exist");
				alert.showAndWait();
			}
			else
			{
				do{
						
					stockAllBean.setItemCode(rs.getString("stockallotment2item"));
					stockAllBean.setSeriesType(rs.getString("stockallotment2seriestype"));
					stockAllBean.setTypeClientOrEmployee(rs.getString("stockallotmenttype"));
					stockAllBean.setAllottedTo(rs.getString("stockallotment2clientoremp"));
					stockAllBean.setSupplierCode(rs.getString("supplier_code"));
					stockAllBean.setRate(rs.getDouble("rate"));
					stockAllBean.setInDate(date.format(rs.getDate("in_date")));
					stockAllBean.setExpiryDate(date.format(rs.getDate("expiry_date")));
					stockAllBean.setCreateDate(date.format(rs.getDate("create_date")));
					stockAllBean.setOriginalSeriesFrom(rs.getString("originalseries_from"));
					stockAllBean.setOriginalSeriesTo(rs.getString("originalseries_to"));
					stockAllBean.setRemarks(rs.getString("remarks"));
					
					reissueStockBean.setInDate(rs.getString("in_date"));
					reissueStockBean.setExpiryDate(rs.getString("expiry_date"));
					reissueStockBean.setCreateDate(rs.getString("create_date"));
				
				}while(rs.next());
				
				dpkInDate.setValue(LocalDate.parse(stockAllBean.getInDate(),localdateformatter));
				txtRate.setText(String.valueOf(stockAllBean.getRate()));
				dpkExpDate.setValue(LocalDate.parse(stockAllBean.getExpiryDate(),localdateformatter));
			
				getTotalPcs();
				
				reissueStockBean.setAllottedTo(stockAllBean.getAllottedTo());
				reissueStockBean.setSupplierCode(stockAllBean.getSupplierCode());
				reissueStockBean.setSeriesType(stockAllBean.getSeriesType());
				reissueStockBean.setItemCode(stockAllBean.getItemCode());
				reissueStockBean.setOriginalSeriesFrom(stockAllBean.getOriginalSeriesFrom());
				reissueStockBean.setOriginalSeriesTo(stockAllBean.getOriginalSeriesTo());
				reissueStockBean.setTypeClientOrEmployee(stockAllBean.getTypeClientOrEmployee());
				reissueStockBean.setRemarks(stockAllBean.getRemarks());
				
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
		
	}
		
	
// =============================================================================================	

	public void saveReissueStockAllotment() throws SQLException
	{
		checkAwbToNumber();
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Stock Received Alert");
		alert.setHeaderText(null);
		
		if(checkIncomingStockAwbToValue==false)
		{	
			 Long totalpcs=0L;
		
		String[] allocatedTo=comboBoxAllotToEmpOrClient.getValue().replaceAll("\\s+","").split("\\|");
		String[] supplierCode=comboBoxSupplier.getValue().replaceAll("\\s+","").split("\\|");
		String[] itemCode=comboBoxItem.getValue().replaceAll("\\s+","").split("\\|");
		
		Matcher matcher = pattern.matcher(txtFrom.getText());
		    
		LocalDate localIndate=dpkInDate.getValue();
		LocalDate localExpiryDate=dpkExpDate.getValue();
		Date inDate=Date.valueOf(localIndate);
		Date expiryDate=Date.valueOf(localExpiryDate);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		PreparedStatement preparedStmt=null;
		
		StockAllotmentBean stockAllotBean=new StockAllotmentBean();
		
		
		if(matcher.matches())				// Condition to check, Entered "Awb no From" is alphanumeric or not
		{
			stockAllotBean.setSeriesType("DEF");
			stockAllotBean.setSupplierCode("DEFAULT");
			stockAllotBean.setAwbFromNumeric(Long.valueOf(txtFrom.getText()));
			stockAllotBean.setAwbToNumeric(Long.valueOf(txtTo.getText()));
			totalpcs=stockAllotBean.getAwbToNumeric()-stockAllotBean.getAwbFromNumeric();
		}
		else
		{
			stockAllotBean.setSeriesType(txtFrom.getText().replaceAll("[^A-Za-z]+", "").toUpperCase());
			stockAllotBean.setSupplierCode(supplierCode[0]);
			stockAllotBean.setAwbFromNumeric(Long.parseLong(txtFrom.getText().replaceAll("\\D+","")));
			stockAllotBean.setAwbToNumeric(Long.parseLong(txtTo.getText().replaceAll("\\D+","")));
			totalpcs=stockAllotBean.getAwbToNumeric()-stockAllotBean.getAwbFromNumeric();
		}
			
			if(rdBtnClient.isSelected()==true)
			{
				stockAllotBean.setTypeClientOrEmployee(rdBtnClient.getText());
			}
			else
			{
				stockAllotBean.setTypeClientOrEmployee(rdBtnEmployee.getText());
			}
			
			stockAllotBean.setAllottedTo(allocatedTo[0]);
			stockAllotBean.setItemCode(itemCode[0]);
			stockAllotBean.setAwbFrom(txtFrom.getText());
			stockAllotBean.setAwbTo(txtTo.getText());
			stockAllotBean.setPcs(Integer.valueOf(txtPcs.getText()));
			stockAllotBean.setRate(Double.valueOf(txtRate.getText()));
			stockAllotBean.setAmount(Double.valueOf(txtAmount.getText()));
			stockAllotBean.setOriginalSeriesFrom(reissueStockBean.getOriginalSeriesFrom());
			stockAllotBean.setOriginalSeriesTo(reissueStockBean.getOriginalSeriesTo());
			stockAllotBean.setRemarks("re-issue");
			
			
			try
			{	
				
				if(totalpcs>=0)
				{
				
					String query = "insert into stock_allotment(stockallotmenttype,stockallotment2clientoremp,stockallotment2item,"
							+ "stockallotment2seriestype,pcs,rate,amount,in_date,awb_from,awb_to,awb_from_number,awb_to_number,create_date,"
							+ "lastmodifieddate,expiry_date,originalseries_from,originalseries_to,supplier_code,remarks) values(?,?,?,?,?,?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?,?,?,?)";
				
					preparedStmt = con.prepareStatement(query);
				
					preparedStmt.setString(1,stockAllotBean.getTypeClientOrEmployee());
					preparedStmt.setString(2, stockAllotBean.getAllottedTo());
					preparedStmt.setString(3, stockAllotBean.getItemCode());
					preparedStmt.setString(4, stockAllotBean.getSeriesType());
					preparedStmt.setInt(5, stockAllotBean.getPcs());
					preparedStmt.setDouble(6, stockAllotBean.getRate());
					preparedStmt.setDouble(7, stockAllotBean.getAmount());
					preparedStmt.setDate(8, inDate);
					preparedStmt.setString(9, stockAllotBean.getAwbFrom());
					preparedStmt.setString(10, stockAllotBean.getAwbTo());
					preparedStmt.setLong(11, stockAllotBean.getAwbFromNumeric());
					preparedStmt.setLong(12, stockAllotBean.getAwbToNumeric());
					preparedStmt.setDate(13, expiryDate);
					preparedStmt.setString(14, stockAllotBean.getOriginalSeriesFrom());
					preparedStmt.setString(15, stockAllotBean.getOriginalSeriesTo());
					preparedStmt.setString(16, stockAllotBean.getSupplierCode());
					preparedStmt.setString(17, stockAllotBean.getRemarks());
				
					splitViaAwbFromAndToNumber(stockAllotBean.getAwbFromNumeric(), stockAllotBean.getAwbToNumeric(), stockAllotBean.getSupplierCode(), stockAllotBean.getSeriesType());
				
					preparedStmt.executeUpdate();
					
					loadReissueAndAllocatedStockTable();
					reset();
				
					alert.setContentText("Stock allotment successfully complete...!!");
					alert.showAndWait();
				}
				else
				{
					alert.setContentText("Please enter the correct series...!!");
					alert.showAndWait();
					txtTo.requestFocus();
					resetFieldsForAwbTOField();
				}	
				
				
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{	
				dbcon.disconnect(preparedStmt, null, null, con);
			}
		checkIncomingStockAwbToValue=false;
		}
		
	}
	

	
	
// =============================================================================================	

	public void splitViaAwbFromAndToNumber(Long awbFrom,Long awbTo,String supplierCode,String seriesType) throws SQLException
	{
		int seriesSplitStatus=0;
		
		ResultSet rs=null;
		Statement stmt=null;
		String sql=null;
		DBconnection dbcon=new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("AWB To series Alert");
		alert.setHeaderText(null);
		
		StockAllotmentBean stockAllotBean=new StockAllotmentBean();
		
		try
		{	
			stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			sql="select awb_from_number,awb_to_number,rate,in_date,expiry_date from stock_allotment where "
					+awbFrom+" BETWEEN awb_from_number AND awb_to_number and supplier_code='"
						+supplierCode+"' and stockallotment2seriestype='"+seriesType+"'";
			rs=stmt.executeQuery(sql);
					
			if(!rs.next())
			{
					
			}
			else
			{
				do{
				stockAllotBean.setAwbFromNumeric(rs.getLong("awb_from_number"));
				stockAllotBean.setAwbToNumeric(rs.getLong("awb_to_number"));
				stockAllotBean.setRate(rs.getDouble("rate"));

				}while(rs.next());
			}
			rs.last();
					
			
			sql="select awb_from_number,awb_to_number,rate,in_date,expiry_date from stock_allotment where "
					+awbTo+" BETWEEN awb_from_number AND awb_to_number and supplier_code='"
						+supplierCode+"' and stockallotment2seriestype='"+seriesType+"'";
			
			rs=stmt.executeQuery(sql);
				
			if(!rs.next())
			{
				
			}
			else
			{
				do{
				stockAllotBean.setCheckAwbFromNumber(rs.getLong("awb_from_number"));
				stockAllotBean.setCheckAwbToNumber(rs.getLong("awb_to_number"));
				
			}while(rs.next());
				
			}
			rs.last();	
				
			
			if(stockAllotBean.getAwbFromNumeric()==stockAllotBean.getCheckAwbFromNumber() 
				&& stockAllotBean.getAwbToNumeric()==stockAllotBean.getCheckAwbToNumber())
			{
				System.out.println("Main if running...");	
				
				if(awbFrom==stockAllotBean.getAwbFromNumeric() && awbTo==stockAllotBean.getAwbToNumeric())
				{	
					
					System.out.println("single series running...");	
					updateInBetweenSeriesInStockAllotment(awbFrom,awbTo);
				}
				
				else if(awbTo==stockAllotBean.getAwbToNumeric())
				{
					seriesSplitStatus=2;
					updateViaAwbFromAndToNumber(stockAllotBean.getAwbToNumeric(),stockAllotBean.getAwbFromNumeric(),awbFrom, awbTo,seriesType,stockAllotBean.getRate(),seriesSplitStatus);
				}
				
				else if(awbFrom==stockAllotBean.getAwbFromNumeric())
				{
					seriesSplitStatus=3;
					updateViaAwbFromAndToNumber(stockAllotBean.getAwbToNumeric(),stockAllotBean.getAwbFromNumeric(),awbFrom, awbTo,seriesType,stockAllotBean.getRate(),seriesSplitStatus);
				}
			
				else if(awbFrom>stockAllotBean.getAwbFromNumeric() && awbTo<stockAllotBean.getAwbToNumeric())
				{
					seriesSplitStatus=4;
					updateViaAwbFromAndToNumber(stockAllotBean.getAwbToNumeric(),stockAllotBean.getAwbFromNumeric(),awbFrom, awbTo,seriesType,stockAllotBean.getRate(),seriesSplitStatus);
				}
			}
			else
			{	
								
				if(listCheckSeriesFrom.size()>2)
				{
					if(listCheckSeriesFrom.get(0).equals(awbFrom) && listCheckSeriesTo.get(listCheckSeriesTo.size()-1).equals(awbTo))
					{
						for(int i=0;i<listCheckSeriesFrom.size();i++)
						{
							updateInBetweenSeriesInStockAllotment(listCheckSeriesFrom.get(i),listCheckSeriesTo.get(i));
						}
					}
					else 
					{
						for(int i=1;i<listCheckSeriesFrom.size()-1;i++)
						{
							updateInBetweenSeriesInStockAllotment(listCheckSeriesFrom.get(i),listCheckSeriesTo.get(i));
						}
						
						if(listCheckSeriesFrom.get(0)<awbFrom)
						{
							seriesSplitStatus=1;
							updateDoubleAwbSeriesInStockAllotment(stockAllotBean.getAwbFromNumeric(),stockAllotBean.getAwbToNumeric(),awbFrom, 0L,seriesType,stockAllotBean.getRate(),seriesSplitStatus);
						}
						
						if(awbTo<listCheckSeriesTo.get(listCheckSeriesTo.size()-1))
						{
							seriesSplitStatus=2;
							updateDoubleAwbSeriesInStockAllotment(stockAllotBean.getCheckAwbFromNumber(),stockAllotBean.getCheckAwbToNumber(),0L, awbTo,seriesType,stockAllotBean.getRate(),seriesSplitStatus);
						}
					}
				}
		
				else if(listCheckSeriesFrom.size()==0)
				{
					for(int i=0;i<2;i++)
					{
						if(i==0)
						{
							seriesSplitStatus=1;
							updateDoubleAwbSeriesInStockAllotment(stockAllotBean.getAwbFromNumeric(),stockAllotBean.getAwbToNumeric(),awbFrom, 0L,seriesType,stockAllotBean.getRate(),seriesSplitStatus);
						}
						else
						{
							seriesSplitStatus=2;
							updateDoubleAwbSeriesInStockAllotment(stockAllotBean.getCheckAwbFromNumber(),stockAllotBean.getCheckAwbToNumber(),0L, awbTo,seriesType,stockAllotBean.getRate(),seriesSplitStatus);
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(null, stmt, rs, con);
			listCheckSeriesFrom.clear();
			listCheckSeriesTo.clear();
		}
		
	}	
	

// =================================================================================================	
	
	public void updateInBetweenSeriesInStockAllotment(Long from, Long to) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;
		String query=null;
		
		try
		{
			query = "update stock_allotment SET remarks=?,awb_from=?,awb_to=?,awb_from_number=?,awb_to_number=? ,pcs=?,rate=?,"
					+ "amount=?,lastmodifieddate=CURRENT_TIMESTAMP where awb_from_number=? and awb_to_number=?";
			preparedStmt = con.prepareStatement(query);
		
			preparedStmt.setString(1,"stock finished");
			preparedStmt.setString(2," ");
			preparedStmt.setString(3, " ");
			preparedStmt.setInt(4,0);
			preparedStmt.setInt(5, 0);
			preparedStmt.setInt(6, 0);
			preparedStmt.setDouble(7, 0);
			preparedStmt.setDouble(8, 0);
			preparedStmt.setLong(9,from);
			preparedStmt.setLong(10, to);
			
			preparedStmt.executeUpdate();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}

// ========================================================================================	
	
	public void updateDoubleAwbSeriesInStockAllotment(Long incomingFrom,Long incomingTo,Long allotFrom,Long allotTo,String seriesType,double rate,int seriesSplitStatus) throws SQLException
	{
	
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;
		String query=null;
			
		StockAllotmentBean stockAllotBean=new StockAllotmentBean();
			
		stockAllotBean.setAllotmentStatus("UA");
		
		if(seriesSplitStatus==1)
		{
			
			System.out.println();
			stockAllotBean.setTotalPcs(Math.abs(Integer.valueOf(String.valueOf(incomingFrom-allotFrom))));
			stockAllotBean.setAmount(stockAllotBean.getTotalPcs() * rate);
		}
		else if(seriesSplitStatus==2)
		{
			stockAllotBean.setTotalPcs(Math.abs(Integer.valueOf(String.valueOf(incomingTo-allotTo))));
			stockAllotBean.setAmount(stockAllotBean.getTotalPcs() * rate);
		}
		
		try
		{
		
			if(seriesSplitStatus==1)
			{
				System.out.println("split status 1: awb to >> "+seriesType+(allotFrom-1)+ " awb to number: "+(allotFrom-1));
				System.out.println("split status 1: Range: "+incomingFrom+" to "+incomingTo);
				
				query = "update stock_allotment SET pcs=?, amount=? ,awb_to=?, awb_to_number=? ,lastmodifieddate=CURRENT_TIMESTAMP where awb_from_number=? and awb_to_number=?";
				preparedStmt = con.prepareStatement(query);
				
				preparedStmt.setInt(1,stockAllotBean.getTotalPcs());
				preparedStmt.setDouble(2,stockAllotBean.getAmount());
				preparedStmt.setString(3,seriesType+(allotFrom-1));
				preparedStmt.setLong(4,(allotFrom-1));
				preparedStmt.setLong(5,incomingFrom);
				preparedStmt.setLong(6,incomingTo);
			
				preparedStmt.executeUpdate();
			}
			else if(seriesSplitStatus==2)
			{
				
				System.out.println("split status 2: awb from >> "+seriesType+(allotTo+1)+ " awb from number: "+(allotTo+1));
				System.out.println("split status 2: Range: "+incomingFrom+" to "+incomingTo);
				
				query = "update stock_allotment SET pcs=?, amount=? ,awb_from=?, awb_from_number=? ,lastmodifieddate=CURRENT_TIMESTAMP where awb_from_number=? and awb_to_number=?";
				preparedStmt = con.prepareStatement(query);
				
				preparedStmt.setInt(1,stockAllotBean.getTotalPcs());
				preparedStmt.setDouble(2,stockAllotBean.getAmount());
				preparedStmt.setString(3,seriesType+(allotTo+1));
				preparedStmt.setLong(4,(allotTo+1));
				preparedStmt.setLong(5,incomingFrom);
				preparedStmt.setLong(6,incomingTo);
				
				preparedStmt.executeUpdate();
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}		

// =================================================================================================
	
	
	public void updateViaAwbFromAndToNumber(Long incomingTo,Long incomingFrom,Long allotFrom,Long allotTo,String seriesType,double rate,int seriesSplitStatus) throws SQLException
	{
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;
		String query=null;
			
		StockAllotmentBean stockAllotBean=new StockAllotmentBean();
		
		stockAllotBean.setAllotmentStatus("UA");
		
		if(seriesSplitStatus==2)
		{
			//System.err.println("From split 1 ========================");
			stockAllotBean.setTotalPcs(Math.abs(Integer.valueOf(String.valueOf(incomingFrom-allotFrom))));
			stockAllotBean.setAmount(stockAllotBean.getTotalPcs() * rate);
			
		}
		else if(seriesSplitStatus==3)
		{
			System.err.println("From split 2 ========================");
			stockAllotBean.setTotalPcs(Math.abs(Integer.valueOf(String.valueOf(incomingTo-allotTo))));
			stockAllotBean.setAmount(stockAllotBean.getTotalPcs() * rate);
		
		}
		else if(seriesSplitStatus==4)
		{
			seriesSplitStatus=2;
				
			stockAllotBean.setTotalPcs(Math.abs(Integer.valueOf(String.valueOf(incomingFrom-(allotFrom-1))))+1);
			stockAllotBean.setAmount(stockAllotBean.getTotalPcs() * rate);
			
			reissueStockBean.setAwbFrom(seriesType+(allotTo+1));
			reissueStockBean.setAwbTo(seriesType+incomingTo);
			reissueStockBean.setTotalPcs(Math.abs(Integer.valueOf(String.valueOf((allotTo+1)-incomingTo)))+1);
			reissueStockBean.setAmount(reissueStockBean.getTotalPcs() * rate);
			reissueStockBean.setRate(rate);
			reissueStockBean.setAwbFromNumeric(allotTo+1);
			reissueStockBean.setAwbToNumeric(incomingTo);
		
			saveSplitEntryInStockAllotment();
				
			}
			
			try
			{
				if(seriesSplitStatus==2)
				{
					query = "update stock_allotment SET pcs=?, amount=? ,awb_to=?, awb_to_number=? ,lastmodifieddate=CURRENT_TIMESTAMP,remarks=? where awb_from_number=? and awb_to_number=?";
					preparedStmt = con.prepareStatement(query);
					
					preparedStmt.setInt(1,stockAllotBean.getTotalPcs());
					preparedStmt.setDouble(2,stockAllotBean.getAmount());
					preparedStmt.setString(3,seriesType+(allotFrom-1));
					preparedStmt.setLong(4,(allotFrom-1));
					preparedStmt.setString(5, "issue");
					preparedStmt.setLong(6,incomingFrom);
					preparedStmt.setLong(7,incomingTo);
					
					preparedStmt.executeUpdate();
				
				}
				else if(seriesSplitStatus==3)
				{
					query = "update stock_allotment SET pcs=?, amount=? ,awb_from=?, awb_from_number=? ,lastmodifieddate=CURRENT_TIMESTAMP,remarks=? where awb_from_number=? and awb_to_number=?";
					preparedStmt = con.prepareStatement(query);
				
					preparedStmt.setInt(1,stockAllotBean.getTotalPcs());
					preparedStmt.setDouble(2,stockAllotBean.getAmount());
					preparedStmt.setString(3,seriesType+(allotTo+1));
					preparedStmt.setLong(4,(allotTo+1));
					preparedStmt.setString(5, "issue");
					preparedStmt.setLong(6,incomingFrom);
					preparedStmt.setLong(7,incomingTo);
					
					preparedStmt.executeUpdate();
				}
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{	
				dbcon.disconnect(preparedStmt, null, null, con);
			}
		}
			
	
// ========================================================================================
/*		
	private void saveSplitEntryIncomingStock() {
		// TODO Auto-generated method stub
		
	}
*/
	public void saveSplitEntryInStockAllotment() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;
		
		StockAllotmentBean stockAllotBean=new StockAllotmentBean();
		//Matcher matcher = pattern.matcher(txtFrom.getText());
		
		Date inDate=Date.valueOf(reissueStockBean.getInDate());
		Date expiryDate=Date.valueOf(reissueStockBean.getExpiryDate());
		Date createDate=Date.valueOf(reissueStockBean.getCreateDate());
		
			
			stockAllotBean.setTypeClientOrEmployee(reissueStockBean.getTypeClientOrEmployee());
			stockAllotBean.setAllottedTo(reissueStockBean.getAllottedTo());
			stockAllotBean.setItemCode(reissueStockBean.getItemCode());
			stockAllotBean.setSeriesType(reissueStockBean.getSeriesType());
			
			stockAllotBean.setTotalPcs(reissueStockBean.getTotalPcs());
			stockAllotBean.setRate(reissueStockBean.getRate());
			stockAllotBean.setAmount(reissueStockBean.getAmount());
			
			stockAllotBean.setAwbFrom(reissueStockBean.getAwbFrom());
			stockAllotBean.setAwbTo(reissueStockBean.getAwbTo());
			stockAllotBean.setAwbFromNumeric(reissueStockBean.getAwbFromNumeric());
			stockAllotBean.setAwbToNumeric(reissueStockBean.getAwbToNumeric());
			
			stockAllotBean.setOriginalSeriesFrom(reissueStockBean.getOriginalSeriesFrom());
			stockAllotBean.setOriginalSeriesTo(reissueStockBean.getOriginalSeriesTo());
			stockAllotBean.setSupplierCode(reissueStockBean.getSupplierCode());
			stockAllotBean.setRemarks(reissueStockBean.getRemarks());
			
			
		try
		{	
			String query = "insert into stock_allotment(stockallotmenttype,stockallotment2clientoremp,stockallotment2item,"
					+ "stockallotment2seriestype,pcs,rate,amount,in_date,awb_from,awb_to,awb_from_number,awb_to_number,create_date,"
					+ "lastmodifieddate,expiry_date,originalseries_from,originalseries_to,supplier_code,remarks) values(?,?,?,?,?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP,?,?,?,?,?)";
				
			preparedStmt = con.prepareStatement(query);
				
			preparedStmt.setString(1,stockAllotBean.getTypeClientOrEmployee());
			preparedStmt.setString(2, stockAllotBean.getAllottedTo());
			preparedStmt.setString(3, stockAllotBean.getItemCode());
			preparedStmt.setString(4, stockAllotBean.getSeriesType());
			preparedStmt.setInt(5, stockAllotBean.getTotalPcs());
			preparedStmt.setDouble(6, stockAllotBean.getRate());
			preparedStmt.setDouble(7, stockAllotBean.getAmount());
			preparedStmt.setDate(8, inDate);
			preparedStmt.setString(9, stockAllotBean.getAwbFrom());
			preparedStmt.setString(10, stockAllotBean.getAwbTo());
			preparedStmt.setLong(11, stockAllotBean.getAwbFromNumeric());
			preparedStmt.setLong(12, stockAllotBean.getAwbToNumeric());
			preparedStmt.setDate(13, createDate);
			preparedStmt.setDate(14, expiryDate);
			preparedStmt.setString(15, stockAllotBean.getOriginalSeriesFrom());
			preparedStmt.setString(16, stockAllotBean.getOriginalSeriesTo());
			preparedStmt.setString(17, stockAllotBean.getSupplierCode());
			preparedStmt.setString(18, stockAllotBean.getRemarks());
		
			preparedStmt.executeUpdate();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
	}	
			
		
// ========================================================================================	

	public void getTotalPcs()
	{
		IncomingStockBean inBean=new IncomingStockBean();
		
		inBean.setAwbFromNumeric(Long.parseLong(txtFrom.getText().replaceAll("\\D+","")));
		
		inBean.setAwbToNumeric(Long.parseLong(txtTo.getText().replaceAll("\\D+","")));
		inBean.setPcs(Integer.valueOf(String.valueOf((inBean.getAwbToNumeric()-inBean.getAwbFromNumeric())+1)));
		
		
		txtPcs.setText(String.valueOf(inBean.getPcs()));
			
	}		
	
	
// =============================================================================================
	
	public void resetFieldsForAwbTOField()
	{
		txtPcs.clear();
		txtRate.clear();
		txtAmount.clear();
		dpkExpDate.getEditor().clear();
		dpkInDate.getEditor().clear();
	}	

	
// =============================================================================================
	
	public void getTotalAmountUsingRateField()
	{
		txtAmount.setText(String.valueOf(Integer.parseInt(txtPcs.getText()) * Double.valueOf(txtRate.getText())));
	}		
	
	
// ========================================================================================		
		
	public void loadReissueAndAllocatedStockTable() throws SQLException
	{
		tabledata_ReissueAndAllocated.clear();
		
		ResultSet rs=null;
		Statement stmt=null;
		String sql=null;
		DBconnection dbcon=new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		ReissueStockBean reissueStockBean=new ReissueStockBean();
		
		int slno=1;
		try
		{
			stmt=con.createStatement();
			sql="select stockallotmenttype,stockallotment2clientoremp,stockallotment2item,awb_from,awb_to,in_date,expiry_date,"
					+ "pcs ,supplier_code from stock_allotment where remarks!='stock finished' order by stockallotmentid DESC";
			rs=stmt.executeQuery(sql);
				
			while(rs.next())
			{
				reissueStockBean.setSlno(slno);
				reissueStockBean.setTypeClientOrEmployee(rs.getString("stockallotmenttype"));
				reissueStockBean.setAllottedTo(rs.getString("stockallotment2clientoremp"));
				reissueStockBean.setItemCode(rs.getString("stockallotment2item"));
				reissueStockBean.setAwbFrom(rs.getString("awb_from"));
				reissueStockBean.setAwbTo(rs.getString("awb_to"));
				reissueStockBean.setInDate(date.format(rs.getDate("in_date")));
				reissueStockBean.setExpiryDate(date.format(rs.getDate("expiry_date")));
				reissueStockBean.setPcs(rs.getInt("pcs"));
				reissueStockBean.setSupplierCode(rs.getString("supplier_code"));
							
				tabledata_ReissueAndAllocated.add(new ReissueStockTableBean(reissueStockBean.getSlno(), reissueStockBean.getTypeClientOrEmployee(),
							reissueStockBean.getAllottedTo(), reissueStockBean.getSupplierCode(), reissueStockBean.getItemCode(), reissueStockBean.getAwbFrom(), reissueStockBean.getAwbTo(), reissueStockBean.getInDate(), reissueStockBean.getExpiryDate(), reissueStockBean.getPcs()));
							
				slno++;
			}
				
			tableStockAllocated.setItems(tabledata_ReissueAndAllocated);
					
			pagination.setPageCount((tabledata_ReissueAndAllocated.size() / rowsPerPage() + 1));
			pagination.setCurrentPageIndex(0);
			pagination.setPageFactory((Integer pageIndex) -> createPage(pageIndex));
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
		
		finally
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
	}		

// ============ Code for paginatation =====================================================
		
		
	public int itemsPerPage()
	{
		return 1;
	}

	public int rowsPerPage() 
	{
		return 10;
	}
		
	public GridPane createPage(int pageIndex)
	{
		int lastIndex = 0;
		GridPane pane=new GridPane();
		int displace = tabledata_ReissueAndAllocated.size() % rowsPerPage();
            
		if (displace >= 0)
		{
			lastIndex = tabledata_ReissueAndAllocated.size() / rowsPerPage();
		}
			
		int page = pageIndex * itemsPerPage();
        for (int i = page; i < page + itemsPerPage(); i++)
        {
            if (lastIndex == pageIndex)
            {
                tableStockAllocated.setItems(FXCollections.observableArrayList(tabledata_ReissueAndAllocated.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
            }
            else
            {
            	tableStockAllocated.setItems(FXCollections.observableArrayList(tabledata_ReissueAndAllocated.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
            }
        }
       return pane;
      
    }
	
	
// ========================================================================================

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException
	{
		Node n=(Node) e.getSource();
	
		if(n.getId().equals("rdBtnClient"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				rdBtnEmployee.requestFocus();
			}	
		}	
		else if(n.getId().equals("rdBtnEmployee"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxAllotToEmpOrClient.requestFocus();
			}	
		}
		else if(n.getId().equals("comboBoxAllotToEmpOrClient"))						
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxSupplier.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxSupplier"))						
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxItem.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxItem"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtFrom.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtFrom"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtTo.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtTo"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				dpkInDate.requestFocus();
			}
		}
		
		else if(n.getId().equals("dpkInDate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				//checkExpiryDate();
				dpkExpDate.requestFocus();
			}
		}
		
		else if(n.getId().equals("dpkExpDate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtRate.requestFocus();
			}
		}
			
		else if(n.getId().equals("txtRate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				//getTotalAmount();
				btnAllotTo.requestFocus();
			}
		}
		
		else if(n.getId().equals("btnAllotTo"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				
				//saveAllocatedStock();
				setValidation();
				//setValidation();
				//comboBoxBranch.requestFocus();
			}
		}
	
	}

	
// =============================================================================================	
	
	public void setValidation() throws SQLException
	{
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		
		/*String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		String email=txtemailid.getText();
	    Pattern pattern = Pattern.compile(regex);
	    Matcher matcher = pattern.matcher((CharSequence) email);
	    boolean checkmail=matcher.matches();*/
			
			
		if(comboBoxAllotToEmpOrClient.getValue()==null || comboBoxAllotToEmpOrClient.getValue().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Allot To field is Empty!");
			alert.showAndWait();
			comboBoxAllotToEmpOrClient.requestFocus();
		}
		else if(comboBoxSupplier.getValue()==null || comboBoxSupplier.getValue().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Supplier field is Empty!");
			alert.showAndWait();
			comboBoxSupplier.requestFocus();
		}
		else if(comboBoxItem.getValue()==null || comboBoxItem.getValue().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Item field is Empty!");
			alert.showAndWait();
			comboBoxItem.requestFocus();
		}
		
		else if(txtFrom.getText()!=null && txtFrom.getText().isEmpty())
		{
			alert.setTitle("Empty Field");
			alert.setContentText("From field is Empty!");
			alert.showAndWait();
			txtFrom.requestFocus();
		}
		else if(txtTo.getText()!=null && txtTo.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("To field is Empty!");
			alert.showAndWait();
			txtTo.requestFocus();
		}
		
		else if(dpkInDate.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("In Date field is Empty!");
			alert.showAndWait();
			dpkInDate.requestFocus();
		}
		
		else if(dpkExpDate.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Expiry Date field is Empty!");
			alert.showAndWait();
			dpkExpDate.requestFocus();
		}
		else if(txtPcs.getText()!=null && txtPcs.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("PCS field is Empty!");
			alert.showAndWait();
			txtPcs.requestFocus();
		}
		else if(txtRate.getText()!=null && txtRate.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Rate field is Empty!");
			alert.showAndWait();
			txtRate.requestFocus();
		}
		
		else if(txtAmount.getText()!=null && txtAmount.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Amount field is Empty!");
			alert.showAndWait();
			txtAmount.requestFocus();
		}
		
		else
		{
			
			saveReissueStockAllotment();
			comboBoxAllotToEmpOrClient.requestFocus();
			/*if(rdnew.isSelected()==true)
			{
				saveClientRegistrationData();
			}
			else
			{
				updateClientRegistrationData();
			}*/
		}
	}	
		
	
// ========================================================================================	
	
	@FXML
	public void exportToExcel_AllocatedStock() throws SQLException, IOException
	{
			
		HSSFWorkbook workbook=new HSSFWorkbook();
		HSSFSheet sheet=workbook.createSheet("Allocated Stock");
		HSSFRow rowhead=sheet.createRow(0);
		
		rowhead.createCell(0).setCellValue("Serial No");
		rowhead.createCell(1).setCellValue("Type");
		rowhead.createCell(2).setCellValue("Allotted To");
		
		rowhead.createCell(3).setCellValue("Supplier");
		rowhead.createCell(4).setCellValue("Item");
		rowhead.createCell(5).setCellValue("AWB From");
		rowhead.createCell(6).setCellValue("AWB To");

		rowhead.createCell(7).setCellValue("In Date");
		rowhead.createCell(8).setCellValue("Expiry Date");
		rowhead.createCell(9).setCellValue("PCS");
		
		int i=2;
			
		HSSFRow row;
			
		for(ReissueStockTableBean allotStockBean: tabledata_ReissueAndAllocated)
		{
			row = sheet.createRow((short) i);
		
			row.createCell(0).setCellValue(allotStockBean.getSlno());
			row.createCell(1).setCellValue(allotStockBean.getTypeClientOrEmployee());
			row.createCell(2).setCellValue(allotStockBean.getAllottedTo());
			row.createCell(3).setCellValue(allotStockBean.getSupplierCode());
			
			row.createCell(4).setCellValue(allotStockBean.getItemCode());
			row.createCell(5).setCellValue(allotStockBean.getAwbFrom());
			row.createCell(6).setCellValue(allotStockBean.getAwbTo());
			
			row.createCell(7).setCellValue(allotStockBean.getIndate());
			row.createCell(8).setCellValue(allotStockBean.getExpiryDate());
			row.createCell(9).setCellValue(allotStockBean.getPcs());
			
			i++;
		}
		
		
		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files","*.xls"));
		File file = fc.showSaveDialog(null);
		FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
		
		workbook.write(fileOut);
		fileOut.close();
			
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Export to excel alert");
		alert.setHeaderText(null);
		alert.setContentText("File "+file.getName()+" successfully saved");
		alert.showAndWait();
		
		   
	}

	
	// =============================================================================================	
	
	public void reset()
	{
		comboBoxAllotToEmpOrClient.getSelectionModel().clearSelection();
		comboBoxItem.getSelectionModel().clearSelection();
		comboBoxSupplier.getSelectionModel().clearSelection();
		
		txtFrom.clear();
		txtTo.clear();
		txtPcs.clear();
		txtRate.clear();
		txtAmount.clear();
		
		dpkExpDate.getEditor().clear();
		dpkInDate.getEditor().clear();
		
		
	}	
	

		@Override
		public void initialize(URL location, ResourceBundle resources) {
			// TODO Auto-generated method stub
			
			imgSearch.setImage(new Image("/com/onesoft/courier/icon/searchicon_blue.png"));
			
			allot_tableCol_Slno.setCellValueFactory(new PropertyValueFactory<ReissueStockTableBean,Integer>("slno"));
			allot_tableCol_Type.setCellValueFactory(new PropertyValueFactory<ReissueStockTableBean,String>("typeClientOrEmployee"));
			allot_tableCol_Supplier.setCellValueFactory(new PropertyValueFactory<ReissueStockTableBean,String>("supplierCode"));
			allot_tableCol_Item.setCellValueFactory(new PropertyValueFactory<ReissueStockTableBean,String>("itemCode"));
			allot_tableCol_AllottedTo.setCellValueFactory(new PropertyValueFactory<ReissueStockTableBean,String>("allottedTo"));
			allot_tableCol_AwbFrom.setCellValueFactory(new PropertyValueFactory<ReissueStockTableBean,String>("awbFrom"));
			allot_tableCol_AwbTo.setCellValueFactory(new PropertyValueFactory<ReissueStockTableBean,String>("awbTo"));
			allot_tableCol_ExpiryDate.setCellValueFactory(new PropertyValueFactory<ReissueStockTableBean,String>("expiryDate"));
			allot_tableCol_InDate.setCellValueFactory(new PropertyValueFactory<ReissueStockTableBean,String>("indate"));
			allot_tableCol_Pcs.setCellValueFactory(new PropertyValueFactory<ReissueStockTableBean,Double>("pcs"));
			
			try {
				getSeriesTypeData();
				checkRadioButtonStatus();
				loadItemWithName();
				loadReissueAndAllocatedStockTable();
				loadSupplierWithName();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	

}
