package com.onesoft.courier.stockdetail.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.DB.DBconnection;
import com.onesoft.courier.common.LoadClientBean;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.LoadEmployee;
import com.onesoft.courier.common.LoadItem;
import com.onesoft.courier.common.LoadSupplier;
import com.onesoft.courier.common.bean.LoadEmployeeBean;
import com.onesoft.courier.common.bean.LoadItemBean;
import com.onesoft.courier.common.bean.LoadSupplierBean;
import com.onesoft.courier.stockdetail.bean.IncomingStockBean;
import com.onesoft.courier.stockdetail.bean.StockAllotmentBean;
import com.onesoft.courier.stockdetail.bean.StockAllotmentTableBean;
import com.onesoft.courier.stockdetail.bean.StockUnAllotmentTableBean;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Pagination;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class StockAllotmentController implements Initializable {
	
	private ObservableList<StockUnAllotmentTableBean> tabledata_UnAllocated=FXCollections.observableArrayList();
	private ObservableList<StockAllotmentTableBean> tabledata_Allocated=FXCollections.observableArrayList();
	
	private ObservableList<String> comboBoxItemItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxAllotToClientAndEmployeeItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxSupplierItems=FXCollections.observableArrayList("DEF | DEFAULT");
	
	boolean checkIncomingStockAwbToValue=false;
	
	private Set<StockAllotmentBean> set_loadSeriesTypeData=new HashSet<>();
	
	Pattern pattern = Pattern.compile("\\d+");			// ====== Pattern to check only Digits =====
	
	IncomingStockBean incomingBean=new IncomingStockBean();			// ====== Pattern to check only Digits =====
	
	List<Long> listCheckSeriesFrom=new ArrayList<>();
	List<Long> listCheckSeriesTo=new ArrayList<>();
	
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	

// ==============================================	
	
	@FXML
	private ImageView imgSearchIcon_Allocated;
	
	@FXML
	private ImageView imgSearchIcon_UnAllocated;
	
	@FXML
	private RadioButton rdBtnClient;
	
	@FXML
	private RadioButton rdBtnEmployee;
	
	@FXML
	private ComboBox<String> comboBoxAllotToEmpOrClient;

	@FXML
	private ComboBox<String> comboBoxItem;
	
	@FXML
	private ComboBox<String> comboBoxSupplier;
	
// ===============================================
	
	@FXML
	private TextField txtFrom;
	
	@FXML
	private TextField txtTo;
	
	@FXML
	private TextField txtPcs;
	
	@FXML
	private TextField txtRate;
	
	@FXML
	private TextField txtAmount;
	
// ===============================================	
	
	@FXML
	private DatePicker dpkInDate;
	
	@FXML
	private DatePicker dpkExpDate;
	
// ===============================================
	
	@FXML
	private Button btnAllotTo;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private Button btnExit;
	
	@FXML
	private Button btnExportToExcel_Allocated;
	
	@FXML
	private Button btnExportToExcel_UnAllocated;
	
// ===============================================
	
	@FXML
	private Pagination pagination_Allocated;
	
	@FXML
	private Pagination pagination_UnAllocated;
	
	
// ===============================================	
	
	@FXML
	private TableView<StockAllotmentTableBean> tableStockAllocated;
	
	@FXML
	private TableColumn<StockAllotmentTableBean, Integer> allot_tableCol_Slno;
	
	@FXML
	private TableColumn<StockAllotmentTableBean, String> allot_tableCol_AllottedTo;
	
	@FXML
	private TableColumn<StockAllotmentTableBean, String> allot_tableCol_Type;
	
	@FXML
	private TableColumn<StockAllotmentTableBean, String> allot_tableCol_Supplier;
	
	@FXML
	private TableColumn<StockAllotmentTableBean, String> allot_tableCol_Item;
	
/*	@FXML
	private TableColumn<IncomingStockTableBean, String> allot_tableCol_Network;*/
	
	@FXML
	private TableColumn<StockAllotmentTableBean, String> allot_tableCol_AwbFrom;
	
	@FXML
	private TableColumn<StockAllotmentTableBean, String> allot_tableCol_AwbTo;
	
	@FXML
	private TableColumn<StockAllotmentTableBean, String> allot_tableCol_InDate;
	
	@FXML
	private TableColumn<StockAllotmentTableBean, String> allot_tableCol_ExpiryDate;
	
	@FXML
	private TableColumn<StockAllotmentTableBean, Double> allot_tableCol_Pcs;
	
	/*@FXML
	private TableColumn<IncomingStockTableBean, Double> tableCol_Rate;
	
	@FXML
	private TableColumn<IncomingStockTableBean, Double> tableCol_Amount;*/
	
	
// ===============================================	
	
	@FXML
	private TableView<StockUnAllotmentTableBean> tableStockUnAllocated;
	
	@FXML
	private TableColumn<StockUnAllotmentTableBean, Integer> unAllot_tableCol_Slno;
	
	@FXML
	private TableColumn<StockUnAllotmentTableBean, String> unAllot_tableCol_Branch;
	
	@FXML
	private TableColumn<StockUnAllotmentTableBean, String> unAllot_tableCol_Supplier;
	
	@FXML
	private TableColumn<StockUnAllotmentTableBean, String> unAllot_tableCol_Item;

/*	@FXML
	private TableColumn<IncomingStockTableBean, String> unAllot_tableCol_Network;*/
	
	@FXML
	private TableColumn<StockUnAllotmentTableBean, String> unAllot_tableCol_AwbFrom;
	
	@FXML
	private TableColumn<StockUnAllotmentTableBean, String> unAllot_tableCol_AwbTo;
	
	@FXML
	private TableColumn<StockUnAllotmentTableBean, String> unAllot_tableCol_InDate;
	
	@FXML
	private TableColumn<StockUnAllotmentTableBean, String> unAllot_tableCol_ExpiryDate;
	
	@FXML
	private TableColumn<StockUnAllotmentTableBean, Double> unAllot_tableCol_Pcs;
	
	/*@FXML
	private TableColumn<IncomingStockTableBean, Double> tableCol_Rate;
	
	@FXML
	private TableColumn<IncomingStockTableBean, Double> tableCol_Amount;*/
	
	
// =============================================================================================

	
	public void checkRadioButtonStatus() throws SQLException
	{
		
		if (rdBtnClient.isSelected()==true) {
			
			loadClientWithName();
		}
		else
		{
			loadEmployeeWithName();
		}
		
	}
	
// =============================================================================================
	
	public void loadItemWithName() throws SQLException
	{
		new LoadItem().loadItemWithName();
		
		for(LoadItemBean bean:LoadItem.SET_LOADITEMSWITHNAME)
		{
			comboBoxItemItems.add(bean.getItemCode()+" | "+bean.getItemName());
		}
		comboBoxItem.setItems(comboBoxItemItems);
	}
	

// =============================================================================================	

	public void loadSupplierWithName() throws SQLException
	{
		new LoadSupplier().loadSupplierWithName();
		
		for(LoadSupplierBean bean:LoadSupplier.SET_LOADSUPPLIERWITHNAME)
		{
			comboBoxSupplierItems.add(bean.getSupplierCode()+" | "+bean.getSupplierName());
		}
		comboBoxSupplier.setItems(comboBoxSupplierItems);
	}	
	
	
// =============================================================================================
	
	public void loadEmployeeWithName() throws SQLException
	{
		comboBoxAllotToClientAndEmployeeItems.clear();
		
		new LoadEmployee().loadEmployeeWithName();
		
		for(LoadEmployeeBean bean:LoadEmployee.SET_LOAD_EMPLOYEE)
		{
			comboBoxAllotToClientAndEmployeeItems.add(bean.getEmployeeCode()+" | "+bean.getEmployeeName());
		}
		comboBoxAllotToEmpOrClient.setItems(comboBoxAllotToClientAndEmployeeItems);
	}
		
		
// =============================================================================================
		
	public void loadClientWithName() throws SQLException
	{
		comboBoxAllotToClientAndEmployeeItems.clear();
		
		new LoadClients().loadClientWithName();
		
		for(LoadClientBean bean:LoadClients.SET_LOAD_CLIENTWITHNAME)
		{
			comboBoxAllotToClientAndEmployeeItems.add(bean.getClientCode()+" | "+bean.getClientName());
		}
		comboBoxAllotToEmpOrClient.setItems(comboBoxAllotToClientAndEmployeeItems);
	}
				
	

// ================ Method to Load series type ===============================	

	public void getSeriesTypeData() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		String sql=null;
		Statement stmt=null;
		ResultSet rs=null;
		
		try
		{	
		
			stmt=con.createStatement();
			sql="select incomingstock2vendor,incomingstock2seriestype,awb_from_number,awb_to_number,rate,in_date,expiry_date from stock_incoming";
			rs=stmt.executeQuery(sql);
			
			while(rs.next())
			{
				StockAllotmentBean stockAllotbean=new StockAllotmentBean();
				
				stockAllotbean.setSupplierCode(rs.getString("incomingstock2vendor"));
				stockAllotbean.setSeriesType(rs.getString("incomingstock2seriestype"));
				stockAllotbean.setRate(rs.getDouble("rate"));
				stockAllotbean.setInDate(date.format(rs.getDate("in_date")));
				stockAllotbean.setExpiryDate(date.format(rs.getDate("expiry_date")));
					
				set_loadSeriesTypeData.add(stockAllotbean);
					
			}
				
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(null, stmt, rs, con);
		}
	}	
	
	
// ================ Method to Check Existence of AWB from number  ===============================
	
	public void checkAwbFromNumber() throws SQLException
	{
		
		StockAllotmentBean stockAllotBean=new StockAllotmentBean();
		
		txtTo.setText(txtFrom.getText());
		String[] itemCode=comboBoxItem.getValue().replaceAll("\\s+","").split("\\|");
		String[] supplierCode=comboBoxSupplier.getValue().replaceAll("\\s+","").split("\\|");
		
		stockAllotBean.setItemCode(itemCode[0]);
			
		
	    Matcher matcher = pattern.matcher(txtFrom.getText());
	 	  
	    if(matcher.matches())				// Condition to check, Entered "Awb no From" is alphanumeric or not
	    {
	    	stockAllotBean.setSeriesType("DEF");
	    	stockAllotBean.setSupplierCode("DEFAULT");
	    	stockAllotBean.setAwbFromNumeric(Long.valueOf(txtFrom.getText()));
		
	    }
	    else
	    {
	    	stockAllotBean.setSeriesType(txtFrom.getText().replaceAll("[^A-Za-z]+", "").toUpperCase());
	    	stockAllotBean.setAwbFromNumeric(Long.parseLong(txtFrom.getText().replaceAll("\\D+","")));
	    	stockAllotBean.setSupplierCode(supplierCode[0]);
		
	    }
			
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		ResultSet rs=null;
		PreparedStatement preparedStmt=null;
		
		try
		{	
			
			String sql="select incomingstock2vendor,incomingstock2item,incomingstock2seriestype,awb_from_number,awb_to_number from stock_incoming where ? BETWEEN awb_from_number AND awb_to_number and incomingstock2vendor=? and incomingstock2seriestype=? and allotmentstatus!=?";
			
			//String query = "select incomingstock2vendor,incomingstock2item,incomingstock2seriestype,awb_from_number,awb_to_number from stock_incoming where incomingstock2vendor=? and incomingstock2seriestype=?";
			preparedStmt = con.prepareStatement(sql);
			preparedStmt.setLong(1, stockAllotBean.getAwbFromNumeric());
			preparedStmt.setString(2, stockAllotBean.getSupplierCode());
			preparedStmt.setString(3, stockAllotBean.getSeriesType());
			preparedStmt.setString(4, "A");
			System.out.println("SQL Query: "+preparedStmt);
			rs = preparedStmt.executeQuery();
			
			
			if(!rs.next())
			{
				
				txtTo.clear();
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Record Alert");
				alert.setHeaderText(null);
				alert.setContentText("Record dosen't exist...");
				alert.showAndWait();
				txtFrom.requestFocus();
			}
			else
			{
				txtTo.setText(txtFrom.getText());
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
		
	}

	
	
// ================ Method to Check Existence of AWB To number  ===============================
	
	public void checkAwbToNumber() throws SQLException
	{
		long tatalpcs=0;
		
		ResultSet rs=null;
		Statement stmt=null;
		String sql=null;
		DBconnection dbcon=new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("AWB To series Alert");
		alert.setHeaderText(null);
		
		
		if(!txtFrom.getText().equals(""))
		{
			if(!txtTo.getText().equals(""))
			{
				String[] supplierCode=comboBoxSupplier.getValue().replaceAll("\\s+","").split("\\|");
				StockAllotmentBean stockAllotBean=new StockAllotmentBean();
			
				Matcher matcher = pattern.matcher(txtTo.getText());
			  
				if(matcher.matches())				// Condition to check, Entered "Awb no From" is alphanumeric or not
				{
					stockAllotBean.setSeriesType("DEF");
					stockAllotBean.setSeriesTypeAwbTo("DEF");
					stockAllotBean.setSupplierCode("DEFAULT");
					stockAllotBean.setAwbFromNumeric(Long.valueOf(txtFrom.getText()));
					stockAllotBean.setAwbToNumeric(Long.valueOf(txtTo.getText()));
					tatalpcs=stockAllotBean.getAwbToNumeric()-stockAllotBean.getAwbFromNumeric();
				}
				else
				{
					stockAllotBean.setSeriesType(txtFrom.getText().replaceAll("[^A-Za-z]+", "").toUpperCase());
					stockAllotBean.setSeriesTypeAwbTo(txtTo.getText().replaceAll("[^A-Za-z]+", "").toUpperCase());
					stockAllotBean.setSupplierCode(supplierCode[0]);
					stockAllotBean.setAwbFromNumeric(Long.parseLong(txtFrom.getText().replaceAll("\\D+","")));
					stockAllotBean.setAwbToNumeric(Long.parseLong(txtTo.getText().replaceAll("\\D+","")));
					tatalpcs=stockAllotBean.getAwbToNumeric()-stockAllotBean.getAwbFromNumeric();
				}
				
				try
				{	
					if(stockAllotBean.getSeriesType().equals(stockAllotBean.getSeriesTypeAwbTo()))
					{
						System.out.println("Total pcs: "+tatalpcs);
						
						if(tatalpcs>=0)
						{
							
							stmt=con.createStatement();
							sql="select * from stock_incoming where "+stockAllotBean.getAwbToNumeric()+" BETWEEN awb_from_number AND "
									+ "awb_to_number and incomingstock2vendor='"+stockAllotBean.getSupplierCode()+"' and incomingstock2seriestype='"+stockAllotBean.getSeriesTypeAwbTo()+"' and allotmentstatus!='A'";
						
							System.out.println("Stock Allocated Sql: "+sql);
							rs=stmt.executeQuery(sql);
							
							
							if(!rs.next())
							{
								txtTo.requestFocus();
								
								alert.setContentText("Awb To series range does not exist");
								alert.showAndWait();
								resetFieldsForAwbTOField();
								checkIncomingStockAwbToValue=true;
							}
							else
							{
								//checkSeriesAndLoadData();
								checkBlankSeriesBetweenNewSeries(rs.getString("incomingstock2seriestype"),rs.getString("incomingstock2vendor"));
							}
						}
						else
						{
							alert.setContentText("Please enter the correct series range...!!");
							alert.showAndWait();
							txtTo.requestFocus();
							resetFieldsForAwbTOField();
						}
					}
					else
					{
						alert.setContentText("TO series type not matched with From Series type...!\nPlease enter correct Series Type");
						alert.showAndWait();
						txtTo.requestFocus();
						resetFieldsForAwbTOField();
					}
				}
				catch(Exception e)
				{
					System.out.println(e);
					e.printStackTrace();
				}
				finally
				{	
					dbcon.disconnect(null, stmt, rs, con);
				}
			}
			else
			{
				alert.setContentText("To Field is Empty! Please enter the range");
				alert.showAndWait();
				txtTo.requestFocus();
				resetFieldsForAwbTOField();
			}
		}
		else
		{
			alert.setContentText("From Field is Empty! Please enter the range");
			alert.showAndWait();
			txtFrom.requestFocus();
			resetFieldsForAwbTOField();
			
		}
	}	
	

// =================== Method to check any blank series between new entered series to allocat ====================	

	public void checkBlankSeriesBetweenNewSeries(String seriestype,String supplierCode ) throws SQLException
	{
		int checkDifferenceFromFirstSeries=0;
		int result=0;
		boolean status=false;
			
		//List<Long> listCheckSeriesFrom=new ArrayList<>();
		//List<Long> listCheckSeriesTo=new ArrayList<>();
		
		StockAllotmentBean stockAllBean=new StockAllotmentBean();
		
		stockAllBean.setInputAwbFromNumeric(Long.parseLong(txtFrom.getText().replaceAll("\\D+","")));
		stockAllBean.setInputAwbToNumeric(Long.parseLong(txtTo.getText().replaceAll("\\D+","")));
		
	   	
		ResultSet rs=null;
		Statement stmt=null;
		String sql=null;
		DBconnection dbcon=new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		try
		{
			stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			sql="select awb_from_number,awb_to_number,rate,in_date,expiry_date from stock_incoming where "
					+stockAllBean.getInputAwbFromNumeric()+" BETWEEN awb_from_number AND awb_to_number and incomingstock2vendor='"
						+supplierCode+"' and incomingstock2seriestype='"+seriestype+"'";
			
			rs=stmt.executeQuery(sql);
					
			if(!rs.next())
			{
					
			}
			else
			{
				stockAllBean.setAwbFromNumeric(rs.getLong("awb_from_number"));
				stockAllBean.setAwbToNumeric(rs.getLong("awb_to_number"));
				stockAllBean.setRate(rs.getDouble("rate"));
				
			}
			rs.last();
					
			
			sql="select awb_from_number,awb_to_number,rate,in_date,expiry_date from stock_incoming where "
					+stockAllBean.getInputAwbToNumeric()+" BETWEEN awb_from_number AND awb_to_number and incomingstock2vendor='"
						+supplierCode+"' and incomingstock2seriestype='"+seriestype+"'";
			rs=stmt.executeQuery(sql);
				
			if(!rs.next())
			{
				
			}
			else
			{
				
				stockAllBean.setCheckAwbFromNumber(rs.getLong("awb_from_number"));
				stockAllBean.setCheckAwbToNumber(rs.getLong("awb_to_number"));
			}
			rs.last();	
			
		
			checkDifferenceFromFirstSeries=(int) (stockAllBean.getCheckAwbFromNumber()-stockAllBean.getAwbToNumeric());
		
			if(checkDifferenceFromFirstSeries<=1)
			{
				checkSeriesAndLoadData();
				
			}
			else
			{	
				sql="select awb_from_number,awb_to_number from stock_incoming where awb_to_number between "
						+stockAllBean.getAwbFromNumeric()+" AND "+stockAllBean.getCheckAwbToNumber()+" and incomingstock2vendor='"+supplierCode+"' and "
							+ "incomingstock2seriestype='"+seriestype+"' order by awb_to_number";
				
				System.out.println("third sql: "+sql);
				rs=stmt.executeQuery(sql);
			
				if(!rs.next())
				{
					
				}
				else
				{
					do
					{
						listCheckSeriesFrom.add(rs.getLong("awb_from_number"));
						listCheckSeriesTo.add(rs.getLong("awb_to_number"));
					}
					while(rs.next());
				}
				rs.last();
				
				for(int i=0;i<listCheckSeriesFrom.size()-1;i++)
				{
					result=(int) (listCheckSeriesFrom.get(i+1)-listCheckSeriesTo.get(i));
					if(result==1)
					{
						status=true;
					}
					else
					{
						status=false;
						break;
					}
				}
				
				if(status==true)
				{
					checkSeriesAndLoadData();
				}
				else
				{
					resetFieldsForAwbTOField();
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Record not found");
					alert.setHeaderText(null);
					alert.setContentText("Please select correct range...!!");
					alert.showAndWait();
					txtTo.requestFocus();
				
				}
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		finally
		{	
			dbcon.disconnect(null, stmt, rs, con);
		}
	}	
		

// ================= Method to Load Data after checking series ====================
	
	public void checkSeriesAndLoadData() throws SQLException
	{
			
		String[] typeAllottedTo=comboBoxAllotToEmpOrClient.getValue().replaceAll("\\s+","").split("\\|");
		String[] supplierCode=comboBoxSupplier.getValue().replaceAll("\\s+","").split("\\|");
		String[] itemCode=comboBoxItem.getValue().replaceAll("\\s+","").split("\\|");
		
		StockAllotmentBean inBean=new StockAllotmentBean();
		inBean.setBranchCode(typeAllottedTo[0]);
		
		inBean.setItemCode(itemCode[0]);
		
		
		Pattern pattern = Pattern.compile("\\d+");
	    Matcher matcher = pattern.matcher(txtFrom.getText());
	 	  
	    if(matcher.matches())				// Condition to check, Entered "Awb no From" is alphanumeric or not
	    {
	    	inBean.setSeriesType("DEF");
	    	inBean.setSupplierCode("DEFAULT");
	    	inBean.setAwbFromNumeric(Long.valueOf(txtFrom.getText()));
			inBean.setAwbToNumeric(Long.valueOf(txtTo.getText()));
	    }
	    else
	    {
	    	inBean.setSeriesType(txtFrom.getText().replaceAll("[^A-Za-z]+", "").toUpperCase());
	    	inBean.setSupplierCode(supplierCode[0]);
	    	inBean.setAwbFromNumeric(Long.parseLong(txtFrom.getText().replaceAll("\\D+","")));
			inBean.setAwbToNumeric(Long.parseLong(txtTo.getText().replaceAll("\\D+","")));
			
	    }
	
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		ResultSet rs=null;
		PreparedStatement preparedStmt=null;
		
		try
		{	
		
			String query="select incomingstock2company,incomingstock2vendor,incomingstock2item,incomingstock2seriestype,awb_from_number,"
					+ "awb_to_number,rate,in_date,create_date,expiry_date,originalseries_from,originalseries_to from stock_incoming "
					+ "where ? BETWEEN awb_from_number AND awb_to_number and incomingstock2vendor=? and incomingstock2seriestype=? "
					+ "and allotmentstatus!=?";
			
				
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setLong(1, inBean.getAwbToNumeric());
			preparedStmt.setString(2, inBean.getSupplierCode());
			preparedStmt.setString(3, inBean.getSeriesType());
			preparedStmt.setString(4, "A");
			System.out.println("SQL Query: "+preparedStmt);
			rs = preparedStmt.executeQuery();
			
			
			if(!rs.next())
			{
				txtTo.clear();
				txtFrom.requestFocus();
				
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Record not found");
				alert.setHeaderText(null);
				alert.setContentText("Series/Supplier not exist");
				alert.showAndWait();
						
			}
			else
			{
				do{
					
					inBean.setSupplierCode(rs.getString("incomingstock2vendor"));
					inBean.setBranchCode(rs.getString("incomingstock2company"));
					inBean.setItemCode(rs.getString("incomingstock2item"));
					inBean.setInDate(date.format(rs.getDate("in_date")));
					inBean.setExpiryDate(date.format(rs.getDate("expiry_date")));
					inBean.setCreateDate(date.format(rs.getDate("create_date")));
					inBean.setOriginalSeriesFrom(rs.getString("originalseries_from"));
					inBean.setOriginalSeriesTo(rs.getString("originalseries_to"));
					inBean.setSeriesType(rs.getString("incomingstock2seriestype"));
					inBean.setRate(rs.getDouble("rate"));
					
					incomingBean.setInDate(rs.getString("in_date"));
					incomingBean.setExpiryDate(rs.getString("expiry_date"));
					incomingBean.setCreateDate(rs.getString("create_date"));
					
				
				}while(rs.next());
				
				dpkInDate.setValue(LocalDate.parse(inBean.getInDate(),localdateformatter));
				dpkExpDate.setValue(LocalDate.parse(inBean.getExpiryDate(),localdateformatter));
				txtRate.setText(String.valueOf(inBean.getRate()));
			
				getTotalPcs();
					
				incomingBean.setSupplierCode(inBean.getSupplierCode());
				incomingBean.setBranchCode(inBean.getBranchCode());
				incomingBean.setSeriesType(inBean.getSeriesType());
				incomingBean.setItemCode(inBean.getItemCode());
				incomingBean.setOriginalSeriesFrom(inBean.getOriginalSeriesFrom());
				incomingBean.setOriginalSeriesTo(inBean.getOriginalSeriesTo());
		
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
	}
			
		
// ============== Method to Count total number of PCS between new series ====================	

	public void getTotalPcs()
	{
		IncomingStockBean inBean=new IncomingStockBean();
		inBean.setAwbFromNumeric(Long.parseLong(txtFrom.getText().replaceAll("\\D+","")));
		inBean.setAwbToNumeric(Long.parseLong(txtTo.getText().replaceAll("\\D+","")));
		inBean.setPcs(Integer.valueOf(String.valueOf((inBean.getAwbToNumeric()-inBean.getAwbFromNumeric())+1)));
		txtPcs.setText(String.valueOf(inBean.getPcs()));
	}		
		
	

// =============================================================================================
	
	public void resetFieldsForAwbTOField()
	{
		txtPcs.clear();
		txtRate.clear();
		txtAmount.clear();
		dpkExpDate.getEditor().clear();
		dpkInDate.getEditor().clear();
	}


// ============ Method to get Total amount ===============
	
	public void getTotalAmountUsingRateField()
	{
		txtAmount.setText(String.valueOf(Integer.parseInt(txtPcs.getText()) * Double.valueOf(txtRate.getText())));
	}
	
	
// ================ Method to Check character type prefix in series ==================
	
	public void checkSeriesType() throws SQLException
	{
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Empty field alert");
		alert.setHeaderText(null);
		
		if(comboBoxAllotToEmpOrClient.getValue()==null || comboBoxAllotToEmpOrClient.getValue().isEmpty())
		{
			alert.setContentText("Allot To field is Empty!");
			alert.showAndWait();
			comboBoxAllotToEmpOrClient.requestFocus();
		}
		else if(comboBoxSupplier.getValue()==null || comboBoxSupplier.getValue().isEmpty())
		{
			alert.setContentText("Supplier field is Empty!");
			alert.showAndWait();
			comboBoxSupplier.requestFocus();
		}
		else if(comboBoxItem.getValue()==null || comboBoxItem.getValue().isEmpty())
		{
			alert.setContentText("Item field is Empty!");
			alert.showAndWait();
			comboBoxItem.requestFocus();
		}
		
		else if(txtFrom.getText()==null || txtFrom.getText().isEmpty())
		{
			
			alert.setContentText("From field is empty");
			alert.showAndWait();
			txtFrom.requestFocus();
		}
		else
		{
		boolean checkResult=false;
		
		Matcher matcher = pattern.matcher(txtFrom.getText());
		String[] supplierCode=comboBoxSupplier.getValue().replaceAll("\\s+","").split("\\|");
		String[] itemCode=comboBoxItem.getValue().replaceAll("\\s+","").split("\\|");
		
		StockAllotmentBean stockAllotBean=new StockAllotmentBean();
		
		if(matcher.matches())				// Condition to check, Entered "Awb no From" is alphanumeric or not
		{
			stockAllotBean.setSeriesType("DEF");
			stockAllotBean.setSupplierCode("DEFAULT");
		}
		else
		{
			stockAllotBean.setSeriesType(txtFrom.getText().replaceAll("[^A-Za-z]+", "").toUpperCase());
		 	stockAllotBean.setSupplierCode(supplierCode[0]);
		}
	
		
		for(StockAllotmentBean bean: set_loadSeriesTypeData)
		{
			if(stockAllotBean.getSeriesType().equals(bean.getSeriesType()) && stockAllotBean.getSupplierCode().equals(bean.getSupplierCode()))
			{
				checkResult=true;
			}
		}
			
			if(checkResult==false)
			{
				txtFrom.requestFocus();
				Alert alert1 = new Alert(AlertType.INFORMATION);
				alert1.setTitle("Record not found");
				alert1.setHeaderText(null);
				alert1.setContentText("Series does not exist for selected supplier...!!");
				alert1.showAndWait();
			 }
			 else
			 {
				checkAwbFromNumber();
				checkResult=false;	 
			 }
		}
		}	
	
	
	
// ================= Method to Load Un-allocated table data ==================		
	
	public void loadUnAllocatedStockTable() throws SQLException
	{
		tabledata_UnAllocated.clear();
		
		ResultSet rs=null;
		Statement stmt=null;
		String sql=null;
		DBconnection dbcon=new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		StockAllotmentBean stockAllBean=new StockAllotmentBean();
		
		int slno=1;
			try
			{
	
				stmt=con.createStatement();
				sql="select incomingstock2company,incomingstock2vendor,incomingstock2item,awb_from,awb_to,in_date,expiry_date,"
						+ "pcs,allotmentstatus from stock_incoming where allotmentstatus IS NULL OR allotmentstatus!='A' order by stockincomingid DESC";
				rs=stmt.executeQuery(sql);
				
				while(rs.next())
				{
					stockAllBean.setSlno(slno);
					stockAllBean.setBranchCode(rs.getString("incomingstock2company"));
					stockAllBean.setSupplierCode(rs.getString("incomingstock2vendor"));
					stockAllBean.setItemCode(rs.getString("incomingstock2item"));
					stockAllBean.setAwbFrom(rs.getString("awb_from"));
					stockAllBean.setAwbTo(rs.getString("awb_to"));
					stockAllBean.setInDate(date.format(rs.getDate("in_date")));
					stockAllBean.setExpiryDate(date.format(rs.getDate("expiry_date")));
					stockAllBean.setPcs(rs.getInt("pcs"));
				/*	stockAllBean.setRate(rs.getDouble("rate"));
					stockAllBean.setAmount(rs.getDouble("amount"));*/
					
					//System.out.println("Allotment status Row number is: ("+slno+") " +rs.getString("allotmentstatus"));
					
					tabledata_UnAllocated.add(new StockUnAllotmentTableBean(stockAllBean.getSlno(), stockAllBean.getBranchCode(), stockAllBean.getSupplierCode(), stockAllBean.getItemCode(),
										stockAllBean.getAwbFrom(), stockAllBean.getAwbTo(), stockAllBean.getInDate(), stockAllBean.getExpiryDate(), stockAllBean.getPcs()));
						
					slno++;
				}
			
				tableStockUnAllocated.setItems(tabledata_UnAllocated);
				
				
				pagination_UnAllocated.setPageCount((tabledata_UnAllocated.size() / rowsPerPage() + 1));
				pagination_UnAllocated.setCurrentPageIndex(0);
				pagination_UnAllocated.setPageFactory((Integer pageIndex) -> createPage_UnAllocatedStock(pageIndex));
				
				
			}
		
			catch (Exception e)
			{
				System.out.println(e);
			}
	
			finally
			{
				dbcon.disconnect(null, stmt, rs, con);
			}
	}	
	
	
// ============ Code for paginatation =====================================================
	
	
	public int itemsPerPage()
	{
		return 1;
	}

	public int rowsPerPage() 
	{
		return 10;
	}

	public GridPane createPage_UnAllocatedStock(int pageIndex)
	{
		int lastIndex = 0;
		
	
	
		GridPane pane=new GridPane();
		int displace = tabledata_UnAllocated.size() % rowsPerPage();
            
			if (displace >= 0)
			{
				lastIndex = tabledata_UnAllocated.size() / rowsPerPage();
			}
	        
			int page = pageIndex * itemsPerPage();
	        for (int i = page; i < page + itemsPerPage(); i++)
	        {
	            if (lastIndex == pageIndex)
	            {
	                tableStockUnAllocated.setItems(FXCollections.observableArrayList(tabledata_UnAllocated.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
	            }
	            else
	            {
	            	tableStockUnAllocated.setItems(FXCollections.observableArrayList(tabledata_UnAllocated.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
	            }
	        }
	       return pane;
	      
	    }	
	
	
	public GridPane createPage_AllocatedStock(int pageIndex)
	{
		int lastIndex = 0;
		
	
	
		GridPane pane=new GridPane();
		int displace = tabledata_Allocated.size() % rowsPerPage();
            
			if (displace >= 0)
			{
				lastIndex = tabledata_Allocated.size() / rowsPerPage();
			}
	
	        
			int page = pageIndex * itemsPerPage();
	        for (int i = page; i < page + itemsPerPage(); i++)
	        {
	            if (lastIndex == pageIndex)
	            {
	                tableStockAllocated.setItems(FXCollections.observableArrayList(tabledata_Allocated.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
	            }
	            else
	            {
	            	tableStockAllocated.setItems(FXCollections.observableArrayList(tabledata_Allocated.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
	            }
	        }
	       return pane;
	      
	    }
	
	
	
// ================= Method to Load Allocated table data ==================
	
	public void loadAllocatedStockTable() throws SQLException
	{
		tabledata_Allocated.clear();
		
		ResultSet rs=null;
		Statement stmt=null;
		String sql=null;
		DBconnection dbcon=new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		StockAllotmentBean stockAllBean=new StockAllotmentBean();
		
		int slno=1;
		try
		{

			stmt=con.createStatement();
			sql="select stockallotmenttype,stockallotment2clientoremp,stockallotment2item,awb_from,awb_to,in_date,expiry_date,"
					+ "pcs ,supplier_code from stock_allotment order by stockallotmentid DESC";
			rs=stmt.executeQuery(sql);
				
			while(rs.next())
			{
				stockAllBean.setSlno(slno);
				stockAllBean.setTypeClientOrEmployee(rs.getString("stockallotmenttype"));
				stockAllBean.setAllottedTo(rs.getString("stockallotment2clientoremp"));
				stockAllBean.setItemCode(rs.getString("stockallotment2item"));
				stockAllBean.setAwbFrom(rs.getString("awb_from"));
				stockAllBean.setAwbTo(rs.getString("awb_to"));
				stockAllBean.setInDate(date.format(rs.getDate("in_date")));
				stockAllBean.setExpiryDate(date.format(rs.getDate("expiry_date")));
				stockAllBean.setPcs(rs.getInt("pcs"));
				stockAllBean.setSupplierCode(rs.getString("supplier_code"));
				/*	stockAllBean.setRate(rs.getDouble("rate"));
						stockAllBean.setAmount(rs.getDouble("amount"));*/
						
				tabledata_Allocated.add(new StockAllotmentTableBean(stockAllBean.getSlno(), stockAllBean.getTypeClientOrEmployee(),
							stockAllBean.getAllottedTo(), stockAllBean.getSupplierCode(), stockAllBean.getItemCode(), stockAllBean.getAwbFrom(), stockAllBean.getAwbTo(), stockAllBean.getInDate(), stockAllBean.getExpiryDate(), stockAllBean.getPcs()));
							
				slno++;
			}
				
			tableStockAllocated.setItems(tabledata_Allocated);
					
			pagination_Allocated.setPageCount((tabledata_Allocated.size() / rowsPerPage() + 1));
			pagination_Allocated.setCurrentPageIndex(0);
			pagination_Allocated.setPageFactory((Integer pageIndex) -> createPage_AllocatedStock(pageIndex));
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
		
		finally
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
	}		
	
	
	
// ============== Method the move Cursor using Entry Key =================

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException
	{
		Node n=(Node) e.getSource();
	
		if(n.getId().equals("rdBtnClient"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				rdBtnEmployee.requestFocus();
			}	
		}	
		else if(n.getId().equals("rdBtnEmployee"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxAllotToEmpOrClient.requestFocus();
			}	
		}
		else if(n.getId().equals("comboBoxAllotToEmpOrClient"))						
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxSupplier.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxSupplier"))						
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxItem.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxItem"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtFrom.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtFrom"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtTo.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtTo"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				dpkInDate.requestFocus();
			}
		}
		
		else if(n.getId().equals("dpkInDate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				//checkExpiryDate();
				dpkExpDate.requestFocus();
			}
		}
		
		else if(n.getId().equals("dpkExpDate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtRate.requestFocus();
			}
		}
			
		else if(n.getId().equals("txtRate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				//getTotalAmount();
				btnAllotTo.requestFocus();
			}
		}
		
		else if(n.getId().equals("btnAllotTo"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				
				setValidation();
				//setValidation();
				//comboBoxBranch.requestFocus();
			}
		}
	
	}

	
// ======================= Method to Save Allocated Stock =========================

	public void saveAllocatedStock() throws SQLException
	{
		
		checkAwbToNumber();
		
		if(checkIncomingStockAwbToValue==false)

		{
			Long totalpcs=0L;
		
		String[] allocatedTo=comboBoxAllotToEmpOrClient.getValue().replaceAll("\\s+","").split("\\|");
		String[] supplierCode=comboBoxSupplier.getValue().replaceAll("\\s+","").split("\\|");
		String[] itemCode=comboBoxItem.getValue().replaceAll("\\s+","").split("\\|");
		
		Matcher matcher = pattern.matcher(txtFrom.getText());
		    
		LocalDate localIndate=dpkInDate.getValue();
		LocalDate localExpiryDate=dpkExpDate.getValue();
		Date inDate=Date.valueOf(localIndate);
		Date expiryDate=Date.valueOf(localExpiryDate);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		PreparedStatement preparedStmt=null;
		
		StockAllotmentBean stockAllotBean=new StockAllotmentBean();
		
		
		if(matcher.matches())				// Condition to check, Entered "Awb no From" is alphanumeric or not
		{
			stockAllotBean.setSeriesType("DEF");
			stockAllotBean.setSupplierCode("DEFAULT");
			stockAllotBean.setAwbFromNumeric(Long.valueOf(txtFrom.getText()));
			stockAllotBean.setAwbToNumeric(Long.valueOf(txtTo.getText()));
			totalpcs=stockAllotBean.getAwbToNumeric()-stockAllotBean.getAwbFromNumeric();
		}
		else
		{
			stockAllotBean.setSeriesType(txtFrom.getText().replaceAll("[^A-Za-z]+", "").toUpperCase());
			stockAllotBean.setSupplierCode(supplierCode[0]);
			stockAllotBean.setAwbFromNumeric(Long.parseLong(txtFrom.getText().replaceAll("\\D+","")));
			stockAllotBean.setAwbToNumeric(Long.parseLong(txtTo.getText().replaceAll("\\D+","")));
			totalpcs=stockAllotBean.getAwbToNumeric()-stockAllotBean.getAwbFromNumeric();
		}
			
			if(rdBtnClient.isSelected()==true)
			{
				stockAllotBean.setTypeClientOrEmployee(rdBtnClient.getText());
			}
			else
			{
				stockAllotBean.setTypeClientOrEmployee(rdBtnEmployee.getText());
			}
			
			stockAllotBean.setAllottedTo(allocatedTo[0]);
			stockAllotBean.setItemCode(itemCode[0]);
			stockAllotBean.setAwbFrom(txtFrom.getText());
			stockAllotBean.setAwbTo(txtTo.getText());
			stockAllotBean.setPcs(Integer.valueOf(txtPcs.getText()));
			stockAllotBean.setRate(Double.valueOf(txtRate.getText()));
			stockAllotBean.setAmount(Double.valueOf(txtAmount.getText()));
			stockAllotBean.setOriginalSeriesFrom(txtFrom.getText());
			stockAllotBean.setOriginalSeriesTo(txtTo.getText());
			stockAllotBean.setRemarks("issue");
			
			
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Stock Received Alert");
			alert.setHeaderText(null);
			
			
			try
			{	
				if(totalpcs>=0)
				{
				
				
				String query = "insert into stock_allotment(stockallotmenttype,stockallotment2clientoremp,stockallotment2item,"
						+ "stockallotment2seriestype,pcs,rate,amount,in_date,awb_from,awb_to,awb_from_number,awb_to_number,create_date,"
						+ "lastmodifieddate,expiry_date,originalseries_from,originalseries_to,supplier_code,remarks) values(?,?,?,?,?,?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?,?,?,?)";
				
				preparedStmt = con.prepareStatement(query);
				
				preparedStmt.setString(1,stockAllotBean.getTypeClientOrEmployee());
				preparedStmt.setString(2, stockAllotBean.getAllottedTo());
				preparedStmt.setString(3, stockAllotBean.getItemCode());
				preparedStmt.setString(4, stockAllotBean.getSeriesType());
				preparedStmt.setInt(5, stockAllotBean.getPcs());
				preparedStmt.setDouble(6, stockAllotBean.getRate());
				preparedStmt.setDouble(7, stockAllotBean.getAmount());
				preparedStmt.setDate(8, inDate);
				preparedStmt.setString(9, stockAllotBean.getAwbFrom());
				preparedStmt.setString(10, stockAllotBean.getAwbTo());
				preparedStmt.setLong(11, stockAllotBean.getAwbFromNumeric());
				preparedStmt.setLong(12, stockAllotBean.getAwbToNumeric());
				preparedStmt.setDate(13, expiryDate);
				preparedStmt.setString(14, stockAllotBean.getOriginalSeriesFrom());
				preparedStmt.setString(15, stockAllotBean.getOriginalSeriesTo());
				preparedStmt.setString(16, stockAllotBean.getSupplierCode());
				preparedStmt.setString(17, stockAllotBean.getRemarks());
				
				splitViaAwbFromAndToNumber(stockAllotBean.getAwbFromNumeric(), stockAllotBean.getAwbToNumeric(), stockAllotBean.getSupplierCode(), stockAllotBean.getSeriesType());
				
				preparedStmt.executeUpdate();
				
				
				
				loadUnAllocatedStockTable();
				loadAllocatedStockTable();
				reset();
				
				
				alert.setContentText("Stock allotment successfully complete...!!");
				alert.showAndWait();
				
				}
				else
				{
					alert.setContentText("Please enter the correct series...!!");
					alert.showAndWait();
					txtTo.requestFocus();
					resetFieldsForAwbTOField();
				}
				
				
			}
				
				
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{	
				dbcon.disconnect(preparedStmt, null, null, con);
			}
			
		checkIncomingStockAwbToValue=false;
		}
		
	}
	

// =============================================================================================
	
	public void splitViaAwbFromAndToNumber(Long awbFrom,Long awbTo,String supplierCode,String seriesType) throws SQLException
	{
		int seriesSplitStatus=0;
		
		ResultSet rs=null;
		Statement stmt=null;
		String sql=null;
		DBconnection dbcon=new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("AWB To series Alert");
		alert.setHeaderText(null);
		
		StockAllotmentBean stockAllotBean=new StockAllotmentBean();
		
		try
		{	
			stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			sql="select awb_from_number,awb_to_number,rate,in_date,expiry_date from stock_incoming where "
					+awbFrom+" BETWEEN awb_from_number AND awb_to_number and incomingstock2vendor='"
						+supplierCode+"' and incomingstock2seriestype='"+seriesType+"'";
			rs=stmt.executeQuery(sql);
					
			if(!rs.next())
			{
					
			}
			else
			{
				do{
				stockAllotBean.setAwbFromNumeric(rs.getLong("awb_from_number"));
				stockAllotBean.setAwbToNumeric(rs.getLong("awb_to_number"));
				stockAllotBean.setRate(rs.getDouble("rate"));

				}while(rs.next());
			}
			rs.last();
					
			
			sql="select awb_from_number,awb_to_number,rate,in_date,expiry_date from stock_incoming where "
					+awbTo+" BETWEEN awb_from_number AND awb_to_number and incomingstock2vendor='"
						+supplierCode+"' and incomingstock2seriestype='"+seriesType+"'";
			
			rs=stmt.executeQuery(sql);
				
			if(!rs.next())
			{
				
			}
			else
			{
				do{
				stockAllotBean.setCheckAwbFromNumber(rs.getLong("awb_from_number"));
				stockAllotBean.setCheckAwbToNumber(rs.getLong("awb_to_number"));
				
			}while(rs.next());
				
			}
			rs.last();	
				
			
			if(stockAllotBean.getAwbFromNumeric()==stockAllotBean.getCheckAwbFromNumber() 
				&& stockAllotBean.getAwbToNumeric()==stockAllotBean.getCheckAwbToNumber())
			{
				System.out.println("Main if running...");	
				
				if(awbFrom==stockAllotBean.getAwbFromNumeric() && awbTo==stockAllotBean.getAwbToNumeric())
				{	
					
					System.out.println("single series running...");	
					updateInBetweenSeriesInStockAllotment(awbFrom,awbTo);
				}
				
				else if(awbTo==stockAllotBean.getAwbToNumeric())
				{
					seriesSplitStatus=2;
					updateViaAwbFromAndToNumber(stockAllotBean.getAwbToNumeric(),stockAllotBean.getAwbFromNumeric(),awbFrom, awbTo,seriesType,stockAllotBean.getRate(),seriesSplitStatus);
				}
				
				else if(awbFrom==stockAllotBean.getAwbFromNumeric())
				{
					seriesSplitStatus=3;
					updateViaAwbFromAndToNumber(stockAllotBean.getAwbToNumeric(),stockAllotBean.getAwbFromNumeric(),awbFrom, awbTo,seriesType,stockAllotBean.getRate(),seriesSplitStatus);
				}
			
				else if(awbFrom>stockAllotBean.getAwbFromNumeric() && awbTo<stockAllotBean.getAwbToNumeric())
				{
					seriesSplitStatus=4;
					updateViaAwbFromAndToNumber(stockAllotBean.getAwbToNumeric(),stockAllotBean.getAwbFromNumeric(),awbFrom, awbTo,seriesType,stockAllotBean.getRate(),seriesSplitStatus);
				}
			}
			else
			{	
								
				if(listCheckSeriesFrom.size()>2)
				{
					if(listCheckSeriesFrom.get(0).equals(awbFrom) && listCheckSeriesTo.get(listCheckSeriesTo.size()-1).equals(awbTo))
					{
						for(int i=0;i<listCheckSeriesFrom.size();i++)
						{
							updateInBetweenSeriesInStockAllotment(listCheckSeriesFrom.get(i),listCheckSeriesTo.get(i));
						}
					}
					else 
					{
						for(int i=1;i<listCheckSeriesFrom.size()-1;i++)
						{
							updateInBetweenSeriesInStockAllotment(listCheckSeriesFrom.get(i),listCheckSeriesTo.get(i));
						}
						
						if(listCheckSeriesFrom.get(0)<awbFrom)
						{
							seriesSplitStatus=1;
							updateDoubleAwbSeriesInStockIncoming(stockAllotBean.getAwbFromNumeric(),stockAllotBean.getAwbToNumeric(),awbFrom, 0L,seriesType,stockAllotBean.getRate(),seriesSplitStatus);
						}
						
						if(awbTo<listCheckSeriesTo.get(listCheckSeriesTo.size()-1))
						{
							seriesSplitStatus=2;
							updateDoubleAwbSeriesInStockIncoming(stockAllotBean.getCheckAwbFromNumber(),stockAllotBean.getCheckAwbToNumber(),0L, awbTo,seriesType,stockAllotBean.getRate(),seriesSplitStatus);
						}
					}
				}
		
				else if(listCheckSeriesFrom.size()==0)
				{
					for(int i=0;i<2;i++)
					{
						if(i==0)
						{
							seriesSplitStatus=1;
							updateDoubleAwbSeriesInStockIncoming(stockAllotBean.getAwbFromNumeric(),stockAllotBean.getAwbToNumeric(),awbFrom, 0L,seriesType,stockAllotBean.getRate(),seriesSplitStatus);
						}
						else
						{
							seriesSplitStatus=2;
							updateDoubleAwbSeriesInStockIncoming(stockAllotBean.getCheckAwbFromNumber(),stockAllotBean.getCheckAwbToNumber(),0L, awbTo,seriesType,stockAllotBean.getRate(),seriesSplitStatus);
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(null, stmt, rs, con);
			listCheckSeriesFrom.clear();
			listCheckSeriesTo.clear();
		}
	
		
	}	
	
	
	
// ========================================================================================	
	
	public void updateDoubleAwbSeriesInStockIncoming(Long incomingFrom,Long incomingTo,Long allotFrom,Long allotTo,String seriesType,double rate,int seriesSplitStatus) throws SQLException
	{
	
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;
		String query=null;
			
		StockAllotmentBean stockAllotBean=new StockAllotmentBean();
			
		stockAllotBean.setAllotmentStatus("UA");
		
		if(seriesSplitStatus==1)
		{
			stockAllotBean.setTotalPcs(Math.abs(Integer.valueOf(String.valueOf(incomingFrom-allotFrom))));
			stockAllotBean.setAmount(stockAllotBean.getTotalPcs() * rate);
		}
		else if(seriesSplitStatus==2)
		{
			stockAllotBean.setTotalPcs(Math.abs(Integer.valueOf(String.valueOf(incomingTo-allotTo))));
			stockAllotBean.setAmount(stockAllotBean.getTotalPcs() * rate);
		}
		
		try
		{
		
			if(seriesSplitStatus==1)
			{
				query = "update stock_incoming SET pcs=?, amount=? ,awb_to=?, awb_to_number=? ,lastmodifieddate=CURRENT_TIMESTAMP where awb_from_number=? and awb_to_number=?";
				preparedStmt = con.prepareStatement(query);
				
				preparedStmt.setInt(1,stockAllotBean.getTotalPcs());
				preparedStmt.setDouble(2,stockAllotBean.getAmount());
				preparedStmt.setString(3,seriesType+(allotFrom-1));
				preparedStmt.setLong(4,(allotFrom-1));
				preparedStmt.setLong(5,incomingFrom);
				preparedStmt.setLong(6,incomingTo);
			
				preparedStmt.executeUpdate();
			}
			else if(seriesSplitStatus==2)
			{
				query = "update stock_incoming SET pcs=?, amount=? ,awb_from=?, awb_from_number=? ,lastmodifieddate=CURRENT_TIMESTAMP where awb_from_number=? and awb_to_number=?";
				preparedStmt = con.prepareStatement(query);
				
				preparedStmt.setInt(1,stockAllotBean.getTotalPcs());
				preparedStmt.setDouble(2,stockAllotBean.getAmount());
				preparedStmt.setString(3,seriesType+(allotTo+1));
				preparedStmt.setLong(4,(allotTo+1));
				preparedStmt.setLong(5,incomingFrom);
				preparedStmt.setLong(6,incomingTo);
				
				preparedStmt.executeUpdate();
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}	

// =================================================================================================
	
	
	public void updateViaAwbFromAndToNumber(Long incomingTo,Long incomingFrom,Long allotFrom,Long allotTo,String seriesType,double rate,int seriesSplitStatus) throws SQLException
	{
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;
		String query=null;
		
		StockAllotmentBean stockAllotBean=new StockAllotmentBean();
		
		stockAllotBean.setAllotmentStatus("UA");
		
		if(seriesSplitStatus==2)
		{
			stockAllotBean.setTotalPcs(Math.abs(Integer.valueOf(String.valueOf(incomingFrom-allotFrom))));
			stockAllotBean.setAmount(stockAllotBean.getTotalPcs() * rate);
		}
		else if(seriesSplitStatus==3)
		{
			stockAllotBean.setTotalPcs(Math.abs(Integer.valueOf(String.valueOf(incomingTo-allotTo))));
			stockAllotBean.setAmount(stockAllotBean.getTotalPcs() * rate);
		}
		else if(seriesSplitStatus==4)
		{
			seriesSplitStatus=2;
			
			stockAllotBean.setTotalPcs(Math.abs(Integer.valueOf(String.valueOf(incomingFrom-(allotFrom-1))))+1);
			stockAllotBean.setAmount(stockAllotBean.getTotalPcs() * rate);
			
			incomingBean.setAwbFrom(seriesType+(allotTo+1));
			incomingBean.setAwbTo(seriesType+incomingTo);
			incomingBean.setTotalPcs(Math.abs(Integer.valueOf(String.valueOf((allotTo+1)-incomingTo)))+1);
			incomingBean.setAmount(incomingBean.getTotalPcs() * rate);
			incomingBean.setRate(rate);
			incomingBean.setAwbFromNumeric(allotTo+1);
			incomingBean.setAwbToNumeric(incomingTo);
			incomingBean.setSeriesType(seriesType);
			
			saveSplitEntryIncomingStock();
		}
	
		try
		{
			
			if(seriesSplitStatus==2)
			{
			
				query = "update stock_incoming SET pcs=?, amount=? ,awb_to=?, awb_to_number=? ,lastmodifieddate=CURRENT_TIMESTAMP where awb_from_number=? and awb_to_number=?";
				preparedStmt = con.prepareStatement(query);
				
				preparedStmt.setInt(1,stockAllotBean.getTotalPcs());
				preparedStmt.setDouble(2,stockAllotBean.getAmount());
				preparedStmt.setString(3,seriesType+(allotFrom-1));
				preparedStmt.setLong(4,(allotFrom-1));
				preparedStmt.setLong(5,incomingFrom);
				preparedStmt.setLong(6,incomingTo);
				
				preparedStmt.executeUpdate();
			
			}
			else if(seriesSplitStatus==3)
			{
				query = "update stock_incoming SET pcs=?, amount=? ,awb_from=?, awb_from_number=? ,lastmodifieddate=CURRENT_TIMESTAMP where awb_from_number=? and awb_to_number=?";
				preparedStmt = con.prepareStatement(query);
			
				preparedStmt.setInt(1,stockAllotBean.getTotalPcs());
				preparedStmt.setDouble(2,stockAllotBean.getAmount());
				preparedStmt.setString(3,seriesType+(allotTo+1));
				preparedStmt.setLong(4,(allotTo+1));
				preparedStmt.setLong(5,incomingFrom);
				preparedStmt.setLong(6,incomingTo);
				
				preparedStmt.executeUpdate();
			}
				
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
		
	}
	
	
	
// =========================================================================================
	
	public void updateInBetweenSeriesInStockAllotment(Long from, Long to) throws SQLException
	{
		
		System.out.println("From: "+from+" To: "+to);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;
		String query=null;
		
		try
		{
			
			query = "update stock_incoming SET allotmentstatus=?,awb_from=?,awb_to=?,awb_from_number=?,awb_to_number=? ,pcs=?,rate=?,"
					+ "amount=?,lastmodifieddate=CURRENT_TIMESTAMP where awb_from_number=? and awb_to_number=?";
			preparedStmt = con.prepareStatement(query);
		
			preparedStmt.setString(1,"A");
			preparedStmt.setString(2," ");
			preparedStmt.setString(3, " ");
			preparedStmt.setInt(4,0);
			preparedStmt.setInt(5, 0);
			preparedStmt.setInt(6, 0);
			preparedStmt.setDouble(7, 0);
			preparedStmt.setDouble(8, 0);
			preparedStmt.setLong(9,from);
			preparedStmt.setLong(10, to);
			
			preparedStmt.executeUpdate();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
		
		
	}

	
// ========================================================================================
	
	public void saveSplitEntryIncomingStock() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;
		
		StockAllotmentBean stockAllotBean=new StockAllotmentBean();
		Matcher matcher = pattern.matcher(txtFrom.getText());
		
		
		Date inDate=Date.valueOf(incomingBean.getInDate());
		Date expiryDate=Date.valueOf(incomingBean.getExpiryDate());
		Date createDate=Date.valueOf(incomingBean.getCreateDate());
		stockAllotBean.setBranchCode(incomingBean.getBranchCode());
		stockAllotBean.setSupplierCode(incomingBean.getSupplierCode());
		stockAllotBean.setSeriesType(incomingBean.getSeriesType());
		stockAllotBean.setItemCode(incomingBean.getItemCode());
		stockAllotBean.setAwbFromNumeric(incomingBean.getAwbFromNumeric());
		stockAllotBean.setAwbToNumeric(incomingBean.getAwbToNumeric());
		stockAllotBean.setPcs(incomingBean.getTotalPcs());
		stockAllotBean.setRate(incomingBean.getRate());
		stockAllotBean.setAmount(incomingBean.getAmount());
		stockAllotBean.setOriginalSeriesFrom(incomingBean.getOriginalSeriesFrom());
		stockAllotBean.setOriginalSeriesTo(incomingBean.getOriginalSeriesTo());
		stockAllotBean.setAllotmentStatus("UA");

		if(matcher.matches())				// Condition to check, Entered "Awb no From" is alphanumeric or not
		{
		stockAllotBean.setAwbFrom(incomingBean.getAwbFrom().replaceAll("\\D+",""));
		stockAllotBean.setAwbTo(incomingBean.getAwbTo().replaceAll("\\D+",""));
		}
		else
		{
			stockAllotBean.setAwbFrom(incomingBean.getAwbFrom());
			stockAllotBean.setAwbTo(incomingBean.getAwbTo());
		}
		
		
		try
		{	
			
			String query = "insert into stock_incoming(incomingstock2company,incomingstock2vendor,incomingstock2item,"
					+ "incomingstock2seriestype,pcs,rate,amount,in_date,awb_from,awb_to,awb_from_number,awb_to_number,create_date,"
					+ "lastmodifieddate,expiry_date,originalseries_from,originalseries_to,allotmentstatus) values(?,?,?,?,?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP,?,?,?,?)";
			
			preparedStmt = con.prepareStatement(query);
			
			preparedStmt.setString(1,stockAllotBean.getBranchCode());
			preparedStmt.setString(2, stockAllotBean.getSupplierCode());
			preparedStmt.setString(3, stockAllotBean.getItemCode());
			preparedStmt.setString(4, stockAllotBean.getSeriesType());
			preparedStmt.setInt(5, stockAllotBean.getPcs());
			preparedStmt.setDouble(6, stockAllotBean.getRate());
			preparedStmt.setDouble(7, stockAllotBean.getAmount());
			preparedStmt.setDate(8, inDate);
			preparedStmt.setString(9, stockAllotBean.getAwbFrom());
			preparedStmt.setString(10, stockAllotBean.getAwbTo());
			preparedStmt.setLong(11, stockAllotBean.getAwbFromNumeric());
			preparedStmt.setLong(12, stockAllotBean.getAwbToNumeric());
			preparedStmt.setDate(13, createDate);
			preparedStmt.setDate(14, expiryDate);
			preparedStmt.setString(15,stockAllotBean.getOriginalSeriesFrom());
			preparedStmt.setString(16,stockAllotBean.getOriginalSeriesTo());
			preparedStmt.setString(17,stockAllotBean.getAllotmentStatus());
			
			preparedStmt.executeUpdate();
			
			
			
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Stock Received Alert");
			alert.setHeaderText(null);
			alert.setContentText("Incoming stock successfully recevied...!!");
			alert.showAndWait();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		
	}	
	
	
	
	
// =============================================================================================	
	
	
	public void setValidation() throws SQLException
	{
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		
		/*String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		String email=txtemailid.getText();
	    Pattern pattern = Pattern.compile(regex);
	    Matcher matcher = pattern.matcher((CharSequence) email);
	    boolean checkmail=matcher.matches();*/
		
		
		if(comboBoxAllotToEmpOrClient.getValue()==null || comboBoxAllotToEmpOrClient.getValue().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Allot To field is Empty!");
			alert.showAndWait();
			comboBoxAllotToEmpOrClient.requestFocus();
		}
		else if(comboBoxSupplier.getValue()==null || comboBoxSupplier.getValue().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Supplier field is Empty!");
			alert.showAndWait();
			comboBoxSupplier.requestFocus();
		}
		else if(comboBoxItem.getValue()==null || comboBoxItem.getValue().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Item field is Empty!");
			alert.showAndWait();
			comboBoxItem.requestFocus();
		}
		
		else if(txtFrom.getText()!=null && txtFrom.getText().isEmpty())
		{
			alert.setTitle("Empty Field");
			alert.setContentText("From field is Empty!");
			alert.showAndWait();
			txtFrom.requestFocus();
		}
		else if(txtTo.getText()!=null && txtTo.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("To field is Empty!");
			alert.showAndWait();
			txtTo.requestFocus();
		}
		
		else if(dpkInDate.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("In Date field is Empty!");
			alert.showAndWait();
			dpkInDate.requestFocus();
		}
		
		else if(dpkExpDate.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Expiry Date field is Empty!");
			alert.showAndWait();
			dpkExpDate.requestFocus();
		}
		else if(txtPcs.getText()!=null && txtPcs.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("PCS field is Empty!");
			alert.showAndWait();
			txtPcs.requestFocus();
		}
		else if(txtRate.getText()!=null && txtRate.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Rate field is Empty!");
			alert.showAndWait();
			txtRate.requestFocus();
		}
		
		else if(txtAmount.getText()!=null && txtAmount.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Amount field is Empty!");
			alert.showAndWait();
			txtAmount.requestFocus();
		}
		else
		{
			
			saveAllocatedStock();
			comboBoxAllotToEmpOrClient.requestFocus();
			/*if(rdnew.isSelected()==true)
			{
				saveClientRegistrationData();
			}
			else
			{
				updateClientRegistrationData();
			}*/
		}
	}	
	
	
// ========================================================================================		
	
	@FXML
	public void exportToExcel_UnAllocatedStock() throws SQLException, IOException
	{
			
		HSSFWorkbook workbook=new HSSFWorkbook();
		HSSFSheet sheet=workbook.createSheet("Un-Allocated Stock");
		HSSFRow rowhead=sheet.createRow(0);
		
		rowhead.createCell(0).setCellValue("Serial No");
		rowhead.createCell(1).setCellValue("Branch");
		rowhead.createCell(2).setCellValue("Supplier");
		
		rowhead.createCell(3).setCellValue("Item");
		rowhead.createCell(4).setCellValue("AWB From");
		rowhead.createCell(5).setCellValue("AWB To");
	
		rowhead.createCell(6).setCellValue("In Date");
		rowhead.createCell(7).setCellValue("Expiry Date");
		rowhead.createCell(8).setCellValue("PCS");
		
		int i=2;
			
		HSSFRow row;
			
		for(StockUnAllotmentTableBean unAllotStockBean: tabledata_UnAllocated)
		{
			row = sheet.createRow((short) i);
		
			row.createCell(0).setCellValue(unAllotStockBean.getSlno());
			row.createCell(1).setCellValue(unAllotStockBean.getBranchCode());
			row.createCell(2).setCellValue(unAllotStockBean.getSupplierCode());
			
			row.createCell(3).setCellValue(unAllotStockBean.getItemCode());
			row.createCell(4).setCellValue(unAllotStockBean.getAwbFrom());
			row.createCell(5).setCellValue(unAllotStockBean.getAwbTo());
			
			row.createCell(6).setCellValue(unAllotStockBean.getIndate());
			row.createCell(7).setCellValue(unAllotStockBean.getExpiryDate());
			row.createCell(8).setCellValue(unAllotStockBean.getPcs());
			
			i++;
		}
		
		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files","*.xls"));
		File file = fc.showSaveDialog(null);
		FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
		//System.out.println("file chooser: "+file.getAbsoluteFile());
		
		workbook.write(fileOut);
		fileOut.close();
			
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Export to excel alert");
		alert.setHeaderText(null);
		alert.setContentText("File "+file.getName()+" successfully saved");
		alert.showAndWait();
		
		   
	}	
	
	
	
// ========================================================================================	
	
	
	@FXML
	public void exportToExcel_AllocatedStock() throws SQLException, IOException
	{
			
		HSSFWorkbook workbook=new HSSFWorkbook();
		HSSFSheet sheet=workbook.createSheet("Allocated Stock");
		HSSFRow rowhead=sheet.createRow(0);
		
		rowhead.createCell(0).setCellValue("Serial No");
		rowhead.createCell(1).setCellValue("Type");
		rowhead.createCell(2).setCellValue("Allotted To");
		
		rowhead.createCell(3).setCellValue("Supplier");
		rowhead.createCell(4).setCellValue("Item");
		rowhead.createCell(5).setCellValue("AWB From");
		rowhead.createCell(6).setCellValue("AWB To");

		rowhead.createCell(7).setCellValue("In Date");
		rowhead.createCell(8).setCellValue("Expiry Date");
		rowhead.createCell(9).setCellValue("PCS");
		
		int i=2;
			
		HSSFRow row;
			
		for(StockAllotmentTableBean allotStockBean: tabledata_Allocated)
		{
			row = sheet.createRow((short) i);
		
			row.createCell(0).setCellValue(allotStockBean.getSlno());
			row.createCell(1).setCellValue(allotStockBean.getTypeClientOrEmployee());
			row.createCell(2).setCellValue(allotStockBean.getAllottedTo());
			row.createCell(3).setCellValue(allotStockBean.getSupplierCode());
			
			row.createCell(4).setCellValue(allotStockBean.getItemCode());
			row.createCell(5).setCellValue(allotStockBean.getAwbFrom());
			row.createCell(6).setCellValue(allotStockBean.getAwbTo());
			
			row.createCell(7).setCellValue(allotStockBean.getIndate());
			row.createCell(8).setCellValue(allotStockBean.getExpiryDate());
			row.createCell(9).setCellValue(allotStockBean.getPcs());
			
			i++;
		}
		
		
		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files","*.xls"));
		File file = fc.showSaveDialog(null);
		FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
		
		workbook.write(fileOut);
		fileOut.close();
			
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Export to excel alert");
		alert.setHeaderText(null);
		alert.setContentText("File "+file.getName()+" successfully saved");
		alert.showAndWait();
		
		   
	}	
	

// =============================================================================================	
	
	public void reset()
	{
		comboBoxAllotToEmpOrClient.getSelectionModel().clearSelection();
		comboBoxItem.getSelectionModel().clearSelection();
		comboBoxSupplier.getSelectionModel().clearSelection();
		
		txtFrom.clear();
		txtTo.clear();
		txtPcs.clear();
		txtRate.clear();
		txtAmount.clear();
		
		dpkExpDate.getEditor().clear();
		dpkInDate.getEditor().clear();
	}	
	
// =============================================================================================	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		txtPcs.setEditable(false);
		txtAmount.setEditable(false);
		dpkExpDate.setEditable(false);
		dpkInDate.setEditable(false);
		
		imgSearchIcon_Allocated.setImage(new Image("/com/onesoft/courier/icon/searchicon_blue.png"));
		imgSearchIcon_UnAllocated.setImage(new Image("/com/onesoft/courier/icon/searchicon_blue.png"));
		
		unAllot_tableCol_Slno.setCellValueFactory(new PropertyValueFactory<StockUnAllotmentTableBean,Integer>("slno"));
		unAllot_tableCol_Branch.setCellValueFactory(new PropertyValueFactory<StockUnAllotmentTableBean,String>("branchCode"));
		unAllot_tableCol_Supplier.setCellValueFactory(new PropertyValueFactory<StockUnAllotmentTableBean,String>("supplierCode"));
		unAllot_tableCol_Item.setCellValueFactory(new PropertyValueFactory<StockUnAllotmentTableBean,String>("itemCode"));
		/*tableCol_Network.setCellValueFactory(new PropertyValueFactory<IncomingStockTableBean,String>("network"));*/
		unAllot_tableCol_AwbFrom.setCellValueFactory(new PropertyValueFactory<StockUnAllotmentTableBean,String>("awbFrom"));
		unAllot_tableCol_AwbTo.setCellValueFactory(new PropertyValueFactory<StockUnAllotmentTableBean,String>("awbTo"));
		unAllot_tableCol_ExpiryDate.setCellValueFactory(new PropertyValueFactory<StockUnAllotmentTableBean,String>("expiryDate"));
		unAllot_tableCol_InDate.setCellValueFactory(new PropertyValueFactory<StockUnAllotmentTableBean,String>("indate"));
		unAllot_tableCol_Pcs.setCellValueFactory(new PropertyValueFactory<StockUnAllotmentTableBean,Double>("pcs"));
		/*tableCol_Rate.setCellValueFactory(new PropertyValueFactory<IncomingStockTableBean,Double>("rate"));
		tableCol_Amount.setCellValueFactory(new PropertyValueFactory<IncomingStockTableBean,Double>("amount"));*/
		
		
		
		allot_tableCol_Slno.setCellValueFactory(new PropertyValueFactory<StockAllotmentTableBean,Integer>("slno"));
		allot_tableCol_Type.setCellValueFactory(new PropertyValueFactory<StockAllotmentTableBean,String>("typeClientOrEmployee"));
		allot_tableCol_Supplier.setCellValueFactory(new PropertyValueFactory<StockAllotmentTableBean,String>("supplierCode"));
		allot_tableCol_Item.setCellValueFactory(new PropertyValueFactory<StockAllotmentTableBean,String>("itemCode"));
		allot_tableCol_AllottedTo.setCellValueFactory(new PropertyValueFactory<StockAllotmentTableBean,String>("allottedTo"));
		allot_tableCol_AwbFrom.setCellValueFactory(new PropertyValueFactory<StockAllotmentTableBean,String>("awbFrom"));
		allot_tableCol_AwbTo.setCellValueFactory(new PropertyValueFactory<StockAllotmentTableBean,String>("awbTo"));
		allot_tableCol_ExpiryDate.setCellValueFactory(new PropertyValueFactory<StockAllotmentTableBean,String>("expiryDate"));
		allot_tableCol_InDate.setCellValueFactory(new PropertyValueFactory<StockAllotmentTableBean,String>("indate"));
		allot_tableCol_Pcs.setCellValueFactory(new PropertyValueFactory<StockAllotmentTableBean,Double>("pcs"));
		
		
		try {
			getSeriesTypeData();
			checkRadioButtonStatus();
			loadItemWithName();
			loadUnAllocatedStockTable();
			loadSupplierWithName();
			loadAllocatedStockTable();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
