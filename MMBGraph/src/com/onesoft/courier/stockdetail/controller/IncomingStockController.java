package com.onesoft.courier.stockdetail.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.omg.CORBA.OMGVMCID;

import com.DB.DBconnection;
import com.onesoft.courier.account.beans.LedgerJavaFXTableBean;
import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.common.LoadItem;
import com.onesoft.courier.common.LoadSupplier;
import com.onesoft.courier.common.bean.LoadBranchBean;
import com.onesoft.courier.common.bean.LoadItemBean;
import com.onesoft.courier.common.bean.LoadSupplierBean;
import com.onesoft.courier.main.Main;
import com.onesoft.courier.stockdetail.bean.IncomingStockBean;
import com.onesoft.courier.stockdetail.bean.IncomingStockTableBean;
import com.sun.javafx.scene.control.skin.TabPaneSkin;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Pagination;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class IncomingStockController implements Initializable {
	
	private ObservableList<IncomingStockTableBean> tabledata=FXCollections.observableArrayList();
	
	//private ObservableList<String> comboBoxBranchItems=FXCollections.observableArrayList(AccountUtils.ALLBRANCH);
	private ObservableList<String> comboBoxBranchItems=FXCollections.observableArrayList();
	private ObservableList<String> comboBoxSupplierItems=FXCollections.observableArrayList("DEF | DEFAULT");
	private ObservableList<String> comboBoxItemItems=FXCollections.observableArrayList();
	
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	IncomingStockBean incomingBean=new IncomingStockBean();
	
	Pattern pattern = Pattern.compile("\\d+");
	
	boolean checkIncomingStockAwbToValue=false;
   
	

	Set<IncomingStockBean> set_loadSeriesTypeData=new HashSet<>();
	
	
	@FXML
	private Pagination pagination;
	
	@FXML
	private ImageView imgSearchIcon;
	
	@FXML
	private ComboBox<String> comboBoxBranch;
	
	@FXML
	private ComboBox<String> comboBoxSupplier;
	
	@FXML
	private ComboBox<String> comboBoxItem;
	
// ===============================================
	
	@FXML
	private TextField txtAwbFrom;
	
	@FXML
	private TextField txtAwbTo;
	
	@FXML
	private TextField txtPcs;
	
	@FXML
	private TextField txtRate;
	
	@FXML
	private TextField txtAmount;
	
// ===============================================	
	
	@FXML
	private DatePicker dpkInDate;
	
	@FXML
	private DatePicker dpkExpDate;
	
// ===============================================
	
	@FXML
	private Button btnReceived;
	
	@FXML
	private Button btnReset;
	
	@FXML
	private Button btnExportToExcel;
	
	@FXML
	private Tab tab;
	
	@FXML
	private TabPane tabPane;
	

// ===============================================	
	
	

	@FXML
	private TableView<IncomingStockTableBean> tableIncomingStock;
	
	@FXML
	private TableColumn<IncomingStockTableBean, Integer> tableCol_Slno;
	
	@FXML
	private TableColumn<IncomingStockTableBean, String> tableCol_Branch;
	
	@FXML
	private TableColumn<IncomingStockTableBean, String> tableCol_Supplier;
	
	@FXML
	private TableColumn<IncomingStockTableBean, String> tableCol_Item;
	
	@FXML
	private TableColumn<IncomingStockTableBean, String> tableCol_Network;
	
	@FXML
	private TableColumn<IncomingStockTableBean, String> tableCol_AwbFrom;
	
	@FXML
	private TableColumn<IncomingStockTableBean, String> tableCol_AwbTo;
	
	@FXML
	private TableColumn<IncomingStockTableBean, String> tableCol_InDate;
	
	@FXML
	private TableColumn<IncomingStockTableBean, String> tableCol_ExpiryDate;
	
	@FXML
	private TableColumn<IncomingStockTableBean, Double> tableCol_Pcs;
	
	@FXML
	private TableColumn<IncomingStockTableBean, Double> tableCol_Rate;
	
	@FXML
	private TableColumn<IncomingStockTableBean, Double> tableCol_Amount;
	
	
// =============================================================================================
	
	
	public void loadBranchWithName() throws SQLException
	{
		new LoadBranch().loadBranchWithName();
		
		for(LoadBranchBean bean:LoadBranch.SET_LOADBRANCHWITHNAME)
		{
			comboBoxBranchItems.add(bean.getBranchCode()+" | "+bean.getBranchName());
		}
		comboBoxBranch.setItems(comboBoxBranchItems);
	}
	
// =============================================================================================	
	
	public void loadSupplierWithName() throws SQLException
	{
		new LoadSupplier().loadSupplierWithName();
		
		for(LoadSupplierBean bean:LoadSupplier.SET_LOADSUPPLIERWITHNAME)
		{
			comboBoxSupplierItems.add(bean.getSupplierCode()+" | "+bean.getSupplierName());
		}
		comboBoxSupplier.setItems(comboBoxSupplierItems);
	}
	
// =============================================================================================
	
	public void loadItemWithName() throws SQLException
	{
		new LoadItem().loadItemWithName();
		
		for(LoadItemBean bean:LoadItem.SET_LOADITEMSWITHNAME)
		{
			comboBoxItemItems.add(bean.getItemCode()+" | "+bean.getItemName());
		}
		comboBoxItem.setItems(comboBoxItemItems);
	}
	
// =============================================================================================
	
	public void checkAwbFromNumber() throws SQLException
	{
		
		String[] branchCode=comboBoxBranch.getValue().replaceAll("\\s+","").split("\\|");
		String[] supplierCode=comboBoxSupplier.getValue().replaceAll("\\s+","").split("\\|");
		String[] itemCode=comboBoxItem.getValue().replaceAll("\\s+","").split("\\|");
		
		IncomingStockBean inBean=new IncomingStockBean();
		inBean.setBranchCode(branchCode[0]);
		inBean.setItemCode(itemCode[0]);
		
		Matcher matcher = pattern.matcher(txtAwbFrom.getText());
		  
	    if(matcher.matches())				// Condition to check, Entered "Awb no From" is alphanumeric or not
	    {
	    	inBean.setSeriesType("DEF");
	    	inBean.setSupplierCode("DEFAULT");
	    	inBean.setAwbFromNumeric(Long.valueOf(txtAwbFrom.getText()));
			
	    }
	    else
	    {
	    	inBean.setSeriesType(txtAwbFrom.getText().replaceAll("[^A-Za-z]+", "").toUpperCase());
	    	inBean.setSupplierCode(supplierCode[0]);
	    	inBean.setAwbFromNumeric(Long.parseLong(txtAwbFrom.getText().replaceAll("\\D+","")));
			
	    }
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		ResultSet rs=null;
		PreparedStatement preparedStmt=null;
		
		try
		{	
			
			/*String query = "select incomingstock2vendor,incomingstock2seriestype,awb_from_number,awb_to_number "
								+ "from stock_incoming where ? BETWEEN awb_from_number AND awb_to_number and "
										+ "incomingstock2vendor=? and incomingstock2seriestype=? and allotmentstatus!=?";
			*/
			String query = "select incomingstock2vendor,incomingstock2seriestype,awb_from_number,awb_to_number "
					+ "from stock_incoming where ? BETWEEN CAST(substring(originalseries_from FROM '[0-9]+') as Integer) AND CAST(substring(originalseries_to FROM '[0-9]+') as Integer)  and "
							+ "incomingstock2vendor=? and incomingstock2seriestype=?";
			
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setLong(1, inBean.getAwbFromNumeric());
			preparedStmt.setString(2, inBean.getSupplierCode());
			preparedStmt.setString(3, inBean.getSeriesType());
			//preparedStmt.setString(4, "A");
			System.out.println("SQL Query: "+preparedStmt);
			rs = preparedStmt.executeQuery();
			
			if(rs.next())
			{
				txtAwbTo.clear();
				txtAwbFrom.requestFocus();
				
				resetFieldsForAwbTOField();
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Record not found");
				alert.setHeaderText(null);
				alert.setContentText("Series already exist");
				alert.showAndWait();
			}
			else
			{
				txtAwbTo.setText(txtAwbFrom.getText());
			}
			
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
		
	}
	
// =============================================================================================
	
	public void checkAwbToNumber() throws SQLException
	{
		long tatalpcs=0;
		
	
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("AWB To series Alert");
		alert.setHeaderText(null);
		
		ResultSet rs=null;
		Statement stmt=null;
		String sql=null;
		DBconnection dbcon=new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		if(!txtAwbTo.getText().equals(""))
		{
			IncomingStockBean inBean=new IncomingStockBean();
			String[] supplierCode=comboBoxSupplier.getValue().replaceAll("\\s+","").split("\\|");
			Matcher matcher = pattern.matcher(txtAwbTo.getText());
			  
		    if(matcher.matches())				// Condition to check, Entered "Awb no From" is alphanumeric or not
		    {
		    	inBean.setSeriesType("DEF");
		    	inBean.setSeriesTypeAwbTo("DEF");
		    	inBean.setSupplierCode("DEFAULT");
		    	inBean.setAwbFromNumeric(Long.valueOf(txtAwbFrom.getText()));
				inBean.setAwbToNumeric(Long.valueOf(txtAwbTo.getText()));
				tatalpcs=inBean.getAwbToNumeric()-inBean.getAwbFromNumeric();
		    }
		    else
		    {
		    	inBean.setSeriesType(txtAwbFrom.getText().replaceAll("[^A-Za-z]+", "").toUpperCase());
		    	inBean.setSeriesTypeAwbTo(txtAwbTo.getText().replaceAll("[^A-Za-z]+", "").toUpperCase());
		    	inBean.setSupplierCode(supplierCode[0]);
		    	inBean.setAwbFromNumeric(Long.parseLong(txtAwbFrom.getText().replaceAll("\\D+","")));
		    	inBean.setAwbToNumeric(Long.parseLong(txtAwbTo.getText().replaceAll("\\D+","")));
				tatalpcs=inBean.getAwbToNumeric()-inBean.getAwbFromNumeric();
		    }
			
			try
			{	
				if(inBean.getSeriesType().equals(inBean.getSeriesTypeAwbTo()))
				{
					if(tatalpcs>=0)
					{
						stmt=con.createStatement();
						sql="select incomingstock2company,incomingstock2vendor,incomingstock2item,incomingstock2seriestype,awb_from_number,awb_to_number,rate,in_date,create_date,expiry_date from stock_incoming where "
								+inBean.getAwbToNumeric()+" BETWEEN CAST(substring(originalseries_from FROM '[0-9]+') as Integer) AND CAST(substring(originalseries_to FROM '[0-9]+') as Integer) and incomingstock2vendor='"
									+inBean.getSupplierCode()+"' and incomingstock2seriestype='"+inBean.getSeriesTypeAwbTo()+"'";
						
						System.out.println("To Query: "+sql);
						rs=stmt.executeQuery(sql);
						
						if(rs.next())
						{
							txtAwbTo.requestFocus();
							alert.setContentText("Awb To series number already exist");
							alert.showAndWait();
							resetFieldsForAwbTOField();
							checkIncomingStockAwbToValue=true;
							
						}
						else
						{
							checkExistingSeriesBetweenNewSeries();
							//checkSeriesAndLoadData();
						}
					}
					else
					{
						alert.setContentText("Please enter the correct series...!!");
						alert.showAndWait();
						txtAwbTo.requestFocus();
						resetFieldsForAwbTOField();
					}
				}
				else
				{
					alert.setContentText("AWB TO series type not matched with AWB From Series type...!\nPlease enter correct Series Type");
					alert.showAndWait();
					txtAwbTo.requestFocus();
					resetFieldsForAwbTOField();
				}
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}
			finally
			{	
				dbcon.disconnect(null, stmt, rs, con);
			}
		}
		else
		{
			alert.setContentText("Awb To Field is Empty! Please enter the range");
			alert.showAndWait();
			txtAwbTo.requestFocus();
			resetFieldsForAwbTOField();
		}
	}

	
// =============================================================================================	

	public void checkExistingSeriesBetweenNewSeries()
	{
		
	IncomingStockBean inBean=new IncomingStockBean();
	boolean status=true;
	
		inBean.setAwbFromNumeric(Long.parseLong(txtAwbFrom.getText().replaceAll("\\D+","")));
    	inBean.setAwbToNumeric(Long.parseLong(txtAwbTo.getText().replaceAll("\\D+","")));
		
		
		Set<Long> checkSeries=new HashSet<>();
		
		ResultSet rs=null;
		Statement stmt=null;
		String sql=null;
		DBconnection dbcon=new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		try{
		stmt=con.createStatement();
		sql="select CAST(substring(originalseries_to FROM '[0-9]+') as Integer) from stock_incoming";
		
		System.out.println("To Query: "+sql);
		rs=stmt.executeQuery(sql);
		
		
		while(rs.next())
		{
			checkSeries.add(rs.getLong(1));
		}
		
		
		for(Long value:checkSeries)
		{
			if(inBean.getAwbFromNumeric()< value && value< inBean.getAwbToNumeric())
			{
				status=false;
				break;
			}
		}

		if(status==false)
		{
			txtAwbTo.requestFocus();
			resetFieldsForAwbTOField();
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Record not found");
			alert.setHeaderText(null);
			alert.setContentText("old series exist between new series");
			alert.showAndWait();
			
		}
		else
		{
			checkSeriesAndLoadData();
		}
		
		}
		catch
		(Exception e)
		{
			System.out.println(e);
		}
		
	}
	
// =============================================================================================
	
	public void resetFieldsForAwbTOField()
	{
		txtPcs.clear();
		txtRate.clear();
		txtAmount.clear();
		dpkExpDate.getEditor().clear();
		dpkInDate.getEditor().clear();
	}
	
	
// =============================================================================================

	
	public void getSeriesTypeData() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		String sql=null;
		Statement stmt=null;
		ResultSet rs=null;
		
		try
		{	
		
			stmt=con.createStatement();
			sql="select seriestypeid,series,series2vendor,expiry_days,minimum_stock,amount as rate,create_date from seriestype order by seriestypeid";
			rs=stmt.executeQuery(sql);
			
			
			while(rs.next())
			{
			
				IncomingStockBean inbean=new IncomingStockBean();
				
				inbean.setSeriesType(rs.getString("series"));
				inbean.setSupplierCode(rs.getString("series2vendor"));
				inbean.setExpiryDays(rs.getInt("expiry_days"));
				inbean.setMinimumStock(rs.getInt("minimum_stock"));
				inbean.setAmount(rs.getDouble("rate"));
				inbean.setCreateDate(date.format(rs.getDate("create_date")));
				
				set_loadSeriesTypeData.add(inbean);
				
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(null, stmt, rs, con);
		}
	}
	

// =============================================================================================	
	
	public void checkSeriesType() throws SQLException
	{
		boolean checkResult=false;
		
		Matcher matcher = pattern.matcher(txtAwbFrom.getText());
		String[] supplierCode=comboBoxSupplier.getValue().replaceAll("\\s+","").split("\\|");
		
		IncomingStockBean inBean=new IncomingStockBean();
		 
		 if(matcher.matches())				// Condition to check, Entered "Awb no From" is alphanumeric or not
		 {
			 inBean.setSeriesType("DEF");
			   	inBean.setSupplierCode("DEFAULT");
		 }
		 else
		 {
		   	inBean.setSeriesType(txtAwbFrom.getText().replaceAll("[^A-Za-z]+", "").toUpperCase());
		   	inBean.setSupplierCode(supplierCode[0]);
		 }
		 
		 System.out.println(inBean.getSupplierCode()+" "+inBean.getSeriesType());
		 
		 for(IncomingStockBean bean: set_loadSeriesTypeData)
			{
				if(inBean.getSeriesType().equals(bean.getSeriesType()) && inBean.getSupplierCode().equals(bean.getSupplierCode()))
				{
					checkResult=true;
				}
				
			}
		 
		 if(checkResult==false)
		 {
			txtAwbFrom.requestFocus();
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Record not found");
			alert.setHeaderText(null);
			alert.setContentText("Series does exist for selected supplier...!!");
			alert.showAndWait();
		 }
		 else
		 {
			 checkAwbFromNumber();
			checkResult=false;	 
		 }
		 
	}
	
	
	
// =============================================================================================
	
	public void checkSeriesAndLoadData() throws SQLException
	{
		//txtAwbTo.setText(txtAwbFrom.getText());
		String[] branchCode=comboBoxBranch.getValue().replaceAll("\\s+","").split("\\|");
		String[] supplierCode=comboBoxSupplier.getValue().replaceAll("\\s+","").split("\\|");
		String[] itemCode=comboBoxItem.getValue().replaceAll("\\s+","").split("\\|");
		
		IncomingStockBean inBean=new IncomingStockBean();
		inBean.setBranchCode(branchCode[0]);
		
		inBean.setItemCode(itemCode[0]);
		
		
		Pattern pattern = Pattern.compile("\\d+");
	    Matcher matcher = pattern.matcher(txtAwbFrom.getText());
		
	 	  
	    if(matcher.matches())				// Condition to check, Entered "Awb no From" is alphanumeric or not
	    {
	    	inBean.setSeriesType("DEF");
	    	inBean.setSupplierCode("DEFAULT");
	    	inBean.setAwbFromNumeric(Long.valueOf(txtAwbFrom.getText()));
			inBean.setAwbToNumeric(Long.valueOf(txtAwbTo.getText()));
	    }
	    else
	    {
	    	inBean.setSeriesType(txtAwbFrom.getText().replaceAll("[^A-Za-z]+", "").toUpperCase());
	    	inBean.setSupplierCode(supplierCode[0]);
	    	inBean.setAwbFromNumeric(Long.parseLong(txtAwbFrom.getText().replaceAll("\\D+","")));
	    	
			inBean.setAwbToNumeric(Long.parseLong(txtAwbTo.getText().replaceAll("\\D+","")));
			
	    }
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		ResultSet rs=null;
		
		PreparedStatement preparedStmt=null;
		
		try
		{	
			
			String query = "select series,series2vendor,expiry_days,minimum_stock,amount as rate,create_date from seriestype where series2vendor=? and series=?";
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setString(1, inBean.getSupplierCode());
			preparedStmt.setString(2, inBean.getSeriesType());
			System.out.println("SQL Query: "+preparedStmt);
			rs = preparedStmt.executeQuery();
			
			
			if(!rs.next())
			{
			//	System.err.println("Supplier: "+inBean.getSupplierCode());
			//	System.err.println("Series Type: "+inBean.getSeriesType());
				
				txtAwbTo.clear();
				txtAwbFrom.requestFocus();
				
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Record not found");
			alert.setHeaderText(null);
			alert.setContentText("Series/Supplier not exist");
			alert.showAndWait();
			
			
			//txtpickupno.requestFocus();
				
			}
			else
			{
				do{
						
					inBean.setSeriesType(rs.getString("series"));
					inBean.setSupplierCode(rs.getString("series2vendor"));
					inBean.setExpiryDays(rs.getInt("expiry_days"));
					inBean.setMinimumStock(rs.getInt("minimum_stock"));
					inBean.setRate(rs.getDouble("rate"));
					inBean.setCreateDate(date.format(rs.getDate("create_date")));
					//inBean.setItemCode(itemCode);
					
				
				}while(rs.next());
				
			
				
				dpkInDate.setValue(LocalDate.parse(inBean.getCreateDate(),localdateformatter));
				txtRate.setText(String.valueOf(inBean.getRate()));
				dpkExpDate.setValue(dpkInDate.getValue().plusDays(inBean.getExpiryDays()));
			
				getTotalPcs();
			
			// set Data in global bean object	
				
				incomingBean.setBranchCode(branchCode[0]);
				incomingBean.setSupplierCode(inBean.getSupplierCode());
				incomingBean.setSeriesType(inBean.getSeriesType());
				incomingBean.setItemCode(itemCode[0]);
				incomingBean.setExpiryDays(inBean.getExpiryDays());
				incomingBean.setAwbFromNumeric(inBean.getAwbFromNumeric());
				incomingBean.setAwbToNumeric(inBean.getAwbToNumeric());
				
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, rs, con);
		}
		
	}

// ========================================================================================
	
	public void saveIncomingStock() throws SQLException
	{
		
		checkAwbToNumber();
		
		if(checkIncomingStockAwbToValue==false)

		{
		//Pattern pattern = Pattern.compile("\\d+");
	    Matcher matcher = pattern.matcher(txtAwbFrom.getText());
	    
	    Long totalpcs=0L;
	    
	  
	    
		LocalDate localIndate=dpkInDate.getValue();
		LocalDate localExpiryDate=dpkExpDate.getValue();
		Date inDate=Date.valueOf(localIndate);
		Date expiryDate=Date.valueOf(localExpiryDate);
		
		//InvoiceBean inbean=new InvoiceBean();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		PreparedStatement preparedStmt=null;
		
		
		IncomingStockBean inbean=new IncomingStockBean();
		
		inbean.setBranchCode(incomingBean.getBranchCode());
		inbean.setSupplierCode(incomingBean.getSupplierCode());
		inbean.setSeriesType(incomingBean.getSeriesType());
		inbean.setItemCode(incomingBean.getItemCode());
		inbean.setAwbFrom(txtAwbFrom.getText());
		inbean.setAwbTo(txtAwbTo.getText());
		inbean.setAwbFromNumeric(incomingBean.getAwbFromNumeric());
		inbean.setPcs(Integer.valueOf(txtPcs.getText()));
		inbean.setRate(Double.valueOf(txtRate.getText()));
		inbean.setAmount(Double.valueOf(txtAmount.getText()));
		inbean.setOriginalSeriesFrom(txtAwbFrom.getText());
		inbean.setOriginalSeriesTo(txtAwbTo.getText());
		inbean.setAllotmentStatus("UA");

		if(matcher.matches())				// Condition to check, Entered "Awb no From" is alphanumeric or not
	    {
			inbean.setAwbToNumeric(Long.valueOf(txtAwbTo.getText()));
			inbean.setAwbFromNumeric(Long.valueOf(txtAwbFrom.getText()));
			inbean.setAwbToNumeric(Long.valueOf(txtAwbTo.getText()));
			totalpcs=inbean.getAwbToNumeric()-inbean.getAwbFromNumeric();
	    }
		else
		{
			inbean.setAwbToNumeric(Long.valueOf(txtAwbTo.getText().replaceAll("\\D+","")));
			inbean.setAwbFromNumeric(Long.parseLong(txtAwbFrom.getText().replaceAll("\\D+","")));
	    	inbean.setAwbToNumeric(Long.parseLong(txtAwbTo.getText().replaceAll("\\D+","")));
	    	totalpcs=inbean.getAwbToNumeric()-inbean.getAwbFromNumeric();
		}
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Stock Received Alert");
		alert.setHeaderText(null);
		
		
		try
		{	
			if(totalpcs>=0)
			{
			
			String query = "insert into stock_incoming(incomingstock2company,incomingstock2vendor,incomingstock2item,"
					+ "incomingstock2seriestype,pcs,rate,amount,in_date,awb_from,awb_to,awb_from_number,awb_to_number,create_date,"
					+ "lastmodifieddate,expiry_date,originalseries_from,originalseries_to,allotmentstatus) values(?,?,?,?,?,?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?,?,?,?)";
			
			preparedStmt = con.prepareStatement(query);
			
			preparedStmt.setString(1,inbean.getBranchCode());
			preparedStmt.setString(2, inbean.getSupplierCode());
			preparedStmt.setString(3, inbean.getItemCode());
			preparedStmt.setString(4, inbean.getSeriesType());
			preparedStmt.setInt(5, inbean.getPcs());
			preparedStmt.setDouble(6, inbean.getRate());
			preparedStmt.setDouble(7, inbean.getAmount());
			preparedStmt.setDate(8, inDate);
			preparedStmt.setString(9, inbean.getAwbFrom());
			preparedStmt.setString(10, inbean.getAwbTo());
			preparedStmt.setLong(11, inbean.getAwbFromNumeric());
			preparedStmt.setLong(12, inbean.getAwbToNumeric());
			preparedStmt.setDate(13, expiryDate);
			preparedStmt.setString(14, inbean.getOriginalSeriesFrom());
			preparedStmt.setString(15, inbean.getOriginalSeriesTo());
			preparedStmt.setString(16,inbean.getAllotmentStatus());
			
			preparedStmt.executeUpdate();
			
			
			loadIncomingStockTable();
			reset();
			
			alert.setContentText("Incoming stock successfully recevied...!!");
			alert.showAndWait();
			/*Main m=new Main();
			m.showIncomingStockForm();*/
			
			}
			else
			{
				alert.setContentText("Please enter the correct series...!!");
				alert.showAndWait();
				txtAwbTo.requestFocus();
				resetFieldsForAwbTOField();
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			
			dbcon.disconnect(preparedStmt, null, null, con);
		}
		checkIncomingStockAwbToValue=false;
		}
			
		
	}
	
	
// ========================================================================================	

	public void getTotalPcs()
	{
		IncomingStockBean inBean=new IncomingStockBean();
		
		inBean.setAwbFromNumeric(Long.parseLong(txtAwbFrom.getText().replaceAll("\\D+","")));
		
		inBean.setAwbToNumeric(Long.parseLong(txtAwbTo.getText().replaceAll("\\D+","")));
		
		
		inBean.setRate(Double.valueOf(txtRate.getText()));
		inBean.setPcs(Integer.valueOf(String.valueOf((inBean.getAwbToNumeric()-inBean.getAwbFromNumeric())+1)));
		
		//txtPcs.setText(String.valueOf((inBean.getAwbToNumeric()-inBean.getAwbFromNumeric())+1));
		
		txtPcs.setText(String.valueOf(inBean.getPcs()));
		//txtAmount.setText(String.valueOf((inBean.getPcs() * inBean.getRate())));
		
	}
	

// ========================================================================================		
	
	public void loadIncomingStockTable() throws SQLException
	{
		tabledata.clear();
		
		ResultSet rs=null;
		Statement stmt=null;
		String sql=null;
		DBconnection dbcon=new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		IncomingStockBean inBean=new IncomingStockBean();
		
		int slno=1;
			try
			{
	
				stmt=con.createStatement();
				sql="select incomingstock2company,incomingstock2vendor,incomingstock2item,awb_from,awb_to,in_date,expiry_date,"
						+ "pcs,rate,amount from stock_incoming where allotmentstatus!='A' order by stockincomingid DESC";
				rs=stmt.executeQuery(sql);
				
				while(rs.next())
				{
					inBean.setSlno(slno);
					inBean.setBranchCode(rs.getString("incomingstock2company"));
					inBean.setSupplierCode(rs.getString("incomingstock2vendor"));
					inBean.setItemCode(rs.getString("incomingstock2item"));
					inBean.setAwbFrom(rs.getString("awb_from"));
					inBean.setAwbTo(rs.getString("awb_to"));
					inBean.setInDate(date.format(rs.getDate("in_date")));
					inBean.setExpiryDate(date.format(rs.getDate("expiry_date")));
					inBean.setPcs(rs.getInt("pcs"));
					inBean.setRate(rs.getDouble("rate"));
					inBean.setAmount(rs.getDouble("amount"));
					
					
					tabledata.add(new IncomingStockTableBean(inBean.getSlno(), inBean.getBranchCode(), inBean.getSupplierCode(), inBean.getItemCode(),
									null, inBean.getAwbFrom(), inBean.getAwbTo(), inBean.getInDate(), inBean.getExpiryDate(), inBean.getPcs(), inBean.getRate(), inBean.getAmount()));
					
					slno++;
				}
			
				tableIncomingStock.setItems(tabledata);
				
				
				pagination.setPageCount((tabledata.size() / rowsPerPage() + 1));
				pagination.setCurrentPageIndex(0);
				pagination.setPageFactory((Integer pageIndex) -> createPage(pageIndex));
				
				
			}
		
			catch (Exception e)
			{
				System.out.println(e);
			}
	
			finally
			{
				dbcon.disconnect(null, stmt, rs, con);
			}
	}
	
	
// ========================================================================================	

		public void checkExpiryDate()
		{
			dpkExpDate.setValue(dpkInDate.getValue().plusDays(incomingBean.getExpiryDays()));
		}	
	

// ============ Code for paginatation =====================================================
		
		
		public int itemsPerPage()
		{
			return 1;
		}

		public int rowsPerPage() 
		{
			return 20;
		}

		public GridPane createPage(int pageIndex)
		{
			int lastIndex = 0;
			
		
		
			GridPane pane=new GridPane();
			int displace = tabledata.size() % rowsPerPage();
	            
				if (displace >= 0)
				{
					lastIndex = tabledata.size() / rowsPerPage();
				}
				/*else
				{
					lastIndex = tabledata.size() / rowsPerPage() -1;
					System.out.println("Pagination lastindex from else condition: "+lastIndex);
				}*/
		  
		        
				int page = pageIndex * itemsPerPage();
		        for (int i = page; i < page + itemsPerPage(); i++)
		        {
		            if (lastIndex == pageIndex)
		            {
		                tableIncomingStock.setItems(FXCollections.observableArrayList(tabledata.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
		            }
		            else
		            {
		            	tableIncomingStock.setItems(FXCollections.observableArrayList(tabledata.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
		            }
		        }
		       return pane;
		      
		    }		
		
		
// ========================================================================================
	
	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException
	{
		Node n=(Node) e.getSource();
	
		if(n.getId().equals("comboBoxBranch"))						
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxSupplier.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxSupplier"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				comboBoxItem.requestFocus();
			}
		}
		
		else if(n.getId().equals("comboBoxItem"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtAwbFrom.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtAwbFrom"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtAwbTo.requestFocus();
			}
			/*else if(e.getCode().equals(KeyCode.TAB))
			{
				checkSeriesType();
			}*/
		}
		
		else if(n.getId().equals("txtAwbTo"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				
				dpkInDate.requestFocus();
			}
		}
		
		else if(n.getId().equals("dpkInDate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				
				checkExpiryDate();
				dpkExpDate.requestFocus();
			}
		}
		
		else if(n.getId().equals("dpkExpDate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtRate.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtRate"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				getTotalAmount();
				btnReceived.requestFocus();
			}
		}
		
		else if(n.getId().equals("btnReceived"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				setValidation();
				//comboBoxBranch.requestFocus();
			}
		}
	}	
	
	
	
// ==================================================================================
	
		@FXML
		public void exportToExcel() throws SQLException, IOException
		{
			
			double totalAmount=0;
				
			HSSFWorkbook workbook=new HSSFWorkbook();
			HSSFSheet sheet=workbook.createSheet("Incoming Stock Data");
			HSSFRow rowhead=sheet.createRow(0);
			
			rowhead.createCell(0).setCellValue("Serial No");
			rowhead.createCell(1).setCellValue("Branch");
			rowhead.createCell(2).setCellValue("Supplier");
			rowhead.createCell(3).setCellValue("Network");
			rowhead.createCell(4).setCellValue("Item");
			
			rowhead.createCell(5).setCellValue("AWB From");
			rowhead.createCell(6).setCellValue("AWB To");
			rowhead.createCell(7).setCellValue("In Date");
			rowhead.createCell(8).setCellValue("Expiry Date");
			
			rowhead.createCell(9).setCellValue("PCS");
			rowhead.createCell(10).setCellValue("Rate");
			rowhead.createCell(11).setCellValue("Amount");
			
			
				int i=2;
				
				HSSFRow row;
				
				for(IncomingStockTableBean ledgerBean: tabledata)
				{
					row = sheet.createRow((short) i);
				
					row.createCell(0).setCellValue(ledgerBean.getSlno());
					row.createCell(1).setCellValue(ledgerBean.getBranchCode());
					row.createCell(2).setCellValue(ledgerBean.getSupplierCode());
					
					row.createCell(3).setCellValue(ledgerBean.getNetwork());
					row.createCell(4).setCellValue(ledgerBean.getItemCode());
					row.createCell(5).setCellValue(ledgerBean.getAwbFrom());
					
					row.createCell(6).setCellValue(ledgerBean.getAwbTo());
					row.createCell(7).setCellValue(ledgerBean.getIndate());
					row.createCell(8).setCellValue(ledgerBean.getExpiryDate());
					
					row.createCell(9).setCellValue(ledgerBean.getPcs());
					row.createCell(10).setCellValue(ledgerBean.getRate());
					row.createCell(11).setCellValue(ledgerBean.getAmount());
					
					totalAmount=totalAmount+ledgerBean.getAmount();
					
					i++;
				}
				
				row = sheet.createRow((short) i+2);
				row.createCell(0).setCellValue("Total Amount:");
				row.createCell(11).setCellValue(totalAmount);
				
				
				
				
				
			FileChooser fc = new FileChooser();
			fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files","*.xls"));
			File file = fc.showSaveDialog(null);
			FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
			System.out.println("file chooser: "+file.getAbsoluteFile());
			
			workbook.write(fileOut);
			fileOut.close();
				
			System.out.println("File "+file.getName()+" successfully saved");
		      
		}	


// ========================================================================================	

		public void getTotalAmount()
		{
			IncomingStockBean inBean=new IncomingStockBean();
			
			
			
			inBean.setRate(Double.valueOf(txtRate.getText()));
			inBean.setPcs(Integer.valueOf(txtPcs.getText()));
			
			System.out.println("Total PCS: "+inBean.getPcs());
			System.out.println("Rate: "+inBean.getRate());
			
			
			txtAmount.setText(String.valueOf((inBean.getPcs() * inBean.getRate())));
			
		}
		
	
	
// =============================================================================================	
	
	public void reset()
	{
		comboBoxBranch.getSelectionModel().clearSelection();
		comboBoxItem.getSelectionModel().clearSelection();
		comboBoxSupplier.getSelectionModel().clearSelection();
		
		txtAwbFrom.clear();
		txtAwbTo.clear();
		txtPcs.clear();
		txtRate.clear();
		txtAmount.clear();
		
		dpkExpDate.getEditor().clear();
		dpkInDate.getEditor().clear();
		
		
	}	
	
// =============================================================================================	
	
	public void setValidation() throws SQLException
	{
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		
		/*String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		String email=txtemailid.getText();
	    Pattern pattern = Pattern.compile(regex);
	    Matcher matcher = pattern.matcher((CharSequence) email);
	    boolean checkmail=matcher.matches();*/
		
		
		if(comboBoxBranch.getValue()==null || comboBoxBranch.getValue().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Branch field is Empty!");
			alert.showAndWait();
			comboBoxBranch.requestFocus();
		}
		else if(comboBoxSupplier.getValue()==null || comboBoxSupplier.getValue().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Supplier field is Empty!");
			alert.showAndWait();
			comboBoxSupplier.requestFocus();
		}
		else if(comboBoxItem.getValue()==null || comboBoxItem.getValue().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Item field is Empty!");
			alert.showAndWait();
			comboBoxItem.requestFocus();
		}
		
		else if(txtAwbFrom.getText()!=null && txtAwbFrom.getText().isEmpty())
		{
			alert.setTitle("Empty Field");
			alert.setContentText("AWB From field is Empty!");
			alert.showAndWait();
			txtAwbFrom.requestFocus();
		}
		else if(txtAwbTo.getText()!=null && txtAwbTo.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("AWB To field is Empty!");
			alert.showAndWait();
			txtAwbTo.requestFocus();
		}
		
		else if(dpkInDate.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("In Date field is Empty!");
			alert.showAndWait();
			dpkInDate.requestFocus();
		}
		
		else if(dpkExpDate.getValue()==null)
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Expiry Date field is Empty!");
			alert.showAndWait();
			dpkExpDate.requestFocus();
		}
		else if(txtRate.getText()!=null && txtRate.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Rate field is Empty!");
			alert.showAndWait();
			txtRate.requestFocus();
		}
		else if(txtAmount.getText()!=null && txtAmount.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Amount field is Empty!");
			alert.showAndWait();
			txtAmount.requestFocus();
		}
		else
		{
			
			saveIncomingStock();
			comboBoxBranch.requestFocus();
			/*if(rdnew.isSelected()==true)
			{
				saveClientRegistrationData();
			}
			else
			{
				updateClientRegistrationData();
			}*/
		}
	}
	
// =============================================================================================

	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		//comboBoxBranch.setValue(AccountUtils.ALLBRANCH);
		
		
		
		
		txtPcs.setEditable(false);
		txtAmount.setEditable(false);
		dpkExpDate.setEditable(false);
		dpkInDate.setEditable(false);
		
		imgSearchIcon.setImage(new Image("/com/onesoft/courier/icon/searchicon_blue.png"));
		
		
		tableCol_Slno.setCellValueFactory(new PropertyValueFactory<IncomingStockTableBean,Integer>("slno"));
		tableCol_Branch.setCellValueFactory(new PropertyValueFactory<IncomingStockTableBean,String>("branchCode"));
		tableCol_Supplier.setCellValueFactory(new PropertyValueFactory<IncomingStockTableBean,String>("supplierCode"));
		tableCol_Item.setCellValueFactory(new PropertyValueFactory<IncomingStockTableBean,String>("itemCode"));
		tableCol_Network.setCellValueFactory(new PropertyValueFactory<IncomingStockTableBean,String>("network"));
		tableCol_AwbFrom.setCellValueFactory(new PropertyValueFactory<IncomingStockTableBean,String>("awbFrom"));
		tableCol_AwbTo.setCellValueFactory(new PropertyValueFactory<IncomingStockTableBean,String>("awbTo"));
		tableCol_ExpiryDate.setCellValueFactory(new PropertyValueFactory<IncomingStockTableBean,String>("expiryDate"));
		tableCol_InDate.setCellValueFactory(new PropertyValueFactory<IncomingStockTableBean,String>("indate"));
		tableCol_Pcs.setCellValueFactory(new PropertyValueFactory<IncomingStockTableBean,Double>("pcs"));
		tableCol_Rate.setCellValueFactory(new PropertyValueFactory<IncomingStockTableBean,Double>("rate"));
		tableCol_Amount.setCellValueFactory(new PropertyValueFactory<IncomingStockTableBean,Double>("amount"));
		
		try
		{
			getSeriesTypeData();
			loadBranchWithName();
			loadSupplierWithName();
			loadItemWithName();
			loadIncomingStockTable();
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
	}

}
