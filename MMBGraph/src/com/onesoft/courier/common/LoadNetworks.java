package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import com.DB.DBconnection;

public class LoadNetworks {
	
	public static  final Set<String> SET_LOAD_NETWORK_FOR_ALL=new HashSet<>();
	
	public void loadAllNetworks() throws SQLException
	{
		
		if(SET_LOAD_NETWORK_FOR_ALL.size()==0)
		{
			System.out.println("Connection open...");
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		Set<String> setNetworkFromDB = new HashSet<String>();
	
		try
		{	
			st=con.createStatement();
			String sql="select code from vendormaster where vendor_type='N'";
			rs=st.executeQuery(sql);
		
			 while(rs.next())
			{
				SET_LOAD_NETWORK_FOR_ALL.add(rs.getString("code"));
			}
	
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			System.out.println("Connection Close...");
			dbcon.disconnect(null, st, rs, con);
		}
		}
		else
		{
			System.out.println("Direct all Set...");
		}
	}

}
