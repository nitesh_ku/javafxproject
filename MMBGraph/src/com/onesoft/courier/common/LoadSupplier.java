package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.DB.DBconnection;
import com.onesoft.courier.common.bean.LoadSupplierBean;

public class LoadSupplier {
	
	
	public static List<LoadSupplierBean> SET_LOADSUPPLIERWITHNAME=new ArrayList<>();
	
	
	public void loadSupplierWithName() throws SQLException
	{
		
		if(SET_LOADSUPPLIERWITHNAME.size()==0)
		{
		
			System.out.println("Value getting from Set Value Using DB connection");
	
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
					 
			try
			{		
				st=con.createStatement();
				String sql="select code,name from vendormaster where vendor_type='S'";
				rs=st.executeQuery(sql);
				
				while(rs.next())
				{
					LoadSupplierBean bean=new LoadSupplierBean();
					bean.setSupplierCode(rs.getString("code"));
					bean.setSupplierName(rs.getString("name"));
					
					SET_LOADSUPPLIERWITHNAME.add(bean);
				}
			}
		
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		else
		{
			System.out.println("\n");
			
			/*System.out.println("Load Branch Setbranch size is: "+SETLOADBRANCHFORALL.size());
			System.out.println("Load Branch Else running...");
			System.out.println("Value getting only from Set Value");*/
			
		}
		
	}

}
