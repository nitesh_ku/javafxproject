package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import com.DB.DBconnection;

public class LoadServiceGroups {

	public static  final Set<String> SET_LOAD_SERVICES_FOR_ALL=new HashSet<>();
	
	public void loadServiceGroup() throws SQLException
	{
		
		if(SET_LOAD_SERVICES_FOR_ALL.size()==0)
		{
			System.out.println("Connection Open from LoadServiceGroup Class");
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		//Set<String> set = new HashSet<String>();
	
		try
		{	
			st=con.createStatement();
			String sql="select code from servicetype GROUP BY code";
			rs=st.executeQuery(sql);
			//comboBoxServiceGroup.setValue(FilterUtils.ALLSERVICEGROUP);
			//comboBoxNetworkItems.add("All Networks");
			while(rs.next())
			{
				SET_LOAD_SERVICES_FOR_ALL.add(rs.getString("code"));
				//comboBoxServiceGroupItems.add(rs.getString("code"));
			//	set.add(rs.getString(2));
			}
	
			/*for(String s:set)
			{
				//comboBoxNetworkItems.add(s);
			}
		*/
			//comboBoxServiceGroup.setItems(comboBoxServiceGroupItems);
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
			System.out.println("Connection Close from LoadServiceGroup Class");
		}
		
		}
		else
		{
			System.out.println("Direct all Set...  LoadServiceGroup");
		}
	}
	
}
