package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.DB.DBconnection;

public class LoadClients {

public static  final List<LoadClientBean> SETLOADCLIENTFORALL=new ArrayList<>();
public static  final List<LoadClientBean> SET_LOAD_CLIENTWITHNAME=new ArrayList<>();
	
	public void loadClient() throws SQLException
	{
		if(SETLOADCLIENTFORALL.size()==0)
		{
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
			
			
					 
			try
			{		
				st=con.createStatement();
				String sql="select cm.code as clientcode,dbt.dailybookingtransaction2branch as branchcode from dailybookingtransaction as dbt, clientmaster as cm where dbt.dailybookingtransaction2client=cm.code group by cm.code,dbt.dailybookingtransaction2branch";
				rs=st.executeQuery(sql);
				
				while(rs.next())
				{
					LoadClientBean loadClientBean=new LoadClientBean();
					loadClientBean.setBranchcode(rs.getString("branchcode"));
					loadClientBean.setClientCode(rs.getString("clientcode"));
				
					SETLOADCLIENTFORALL.add(loadClientBean);
				}
			}
		
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		else
		{
			System.out.println("\n");
			
		}
	}
	
	
	public void loadClientWithName() throws SQLException
	{
		if(SET_LOAD_CLIENTWITHNAME.size()==0)
		{
			System.out.println("Client with name load by DB...");
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			Statement st=null;
			ResultSet rs=null;
			
			try
			{		
				st=con.createStatement();
				String sql="select code, name from clientmaster";
				rs=st.executeQuery(sql);
				
				while(rs.next())
				{
					LoadClientBean loadClientBean=new LoadClientBean();
					loadClientBean.setClientName(rs.getString("name"));
					loadClientBean.setClientCode(rs.getString("code"));
					
					SET_LOAD_CLIENTWITHNAME.add(loadClientBean);
				}
			}
		
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}
		else
		{
			System.out.println("\n");
			System.out.println("Client with name load by list...");
			
		}
	}
	
	
}
