package com.onesoft.courier.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.DB.DBconnection;
import com.onesoft.courier.common.bean.LoadCityWithZipcodeBean;
import com.onesoft.courier.graphs.utils.FilterUtils;

public class LoadCity {
	
	public static  final Set<String> SET_LOAD_CITY_FOR_ALL=new HashSet<>();
	public static  final List<LoadCityWithZipcodeBean> LIST_LOAD_CITY_WITH_ZIPCODE=new ArrayList<>();

	public void loadAllCity() throws SQLException
	{
		
		if(SET_LOAD_CITY_FOR_ALL.size()==0)
		{
			
			System.out.println("First time Get values from DB to laod City Names..........");
			System.out.println("Connection open for city name,,,,");
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		//Set<String> set = new HashSet<String>();
	
		try
		{	
			st=con.createStatement();
			String sql="SELECT dailybookingtransaction2city,ct.city_name as cityname,sum(dbt.total) as total from dailybookingtransaction as dbt, city as ct, clientmaster as cm where ct.code=dbt.dailybookingtransaction2city and dbt.dailybookingtransaction2client=cm.code  Group  by dbt.dailybookingtransaction2city,ct.city_name order by sum(dbt.total) DESC LIMIT 10";
			rs=st.executeQuery(sql);
			while(rs.next())
			{
				SET_LOAD_CITY_FOR_ALL.add(rs.getString("cityname"));
			}
	
			
		}
		catch(Exception e)
		{
			 System.out.println(e);
			 e.printStackTrace();
		}	
		finally
		{
			System.out.println("Connection closed for city name,,,,");
			dbcon.disconnect(null, st, rs, con);
		}
		}
		else
		{
			System.out.println("Now get the values from variable of set for CIITY NAME =========");
		}
	}
	
	
	public void loadZipcodeCityDetails() throws SQLException
	 {
		if(LIST_LOAD_CITY_WITH_ZIPCODE.size()==0)
		{		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		Statement st=null;
		ResultSet rs=null;
			
				 
		try
		{		
			st=con.createStatement();
			String sql="select pm.pincode as pincode,pm.city_code as citycode,ctym.city_name as cityname from pincode_master as pm, city_master ctym where pm.city_code=ctym.city_code";
			rs=st.executeQuery(sql);
			
			while(rs.next())
			{
				LoadCityWithZipcodeBean bean=new LoadCityWithZipcodeBean();
				bean.setZipcode(rs.getLong("pincode"));
				bean.setCityCode(rs.getString("citycode"));
				bean.setCityName(rs.getString("cityname"));
				LIST_LOAD_CITY_WITH_ZIPCODE.add(bean);
				
				
			}

			/*for(ZipcodeCityBean zip:zipcodeList)
			{
				System.out.println("Pincode: "+zip.getPincode()+", City Name: "+zip.getCity_name());
			}*/
			
		}
	
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		
		finally
		{
			dbcon.disconnect(null, st, rs, con);
			
			
		}
		}
		else
		{
			System.out.println("Now get the values from variable of set for CIITY NAME with Zipcode =========");
		}
		
		
	}	
	
	
	
	
}
