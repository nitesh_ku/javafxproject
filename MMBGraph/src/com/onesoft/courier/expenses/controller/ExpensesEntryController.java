package com.onesoft.courier.expenses.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import org.controlsfx.control.textfield.TextFields;

import com.DB.DBconnection;
import com.onesoft.courier.common.LoadBranch;
import com.onesoft.courier.expenses.beans.ExpenseBean;
import com.onesoft.courier.expenses.beans.ExpensesTableBean;
import com.onesoft.courier.pickup.bean.PickupBean;



import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class ExpensesEntryController implements Initializable{
	
	private ObservableList<String> comboBoxBranchNameItems=FXCollections.observableArrayList();
	private ObservableList<ExpensesTableBean> ExprensesTabledata=FXCollections.observableArrayList();
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	
	
	@FXML
	private ImageView imgSearchIcon;
	
	@FXML
	ComboBox<String> comboBoxBranch;
	
	@FXML
	Label lblBranchName;
	
	@FXML
	DatePicker dpkExpenseDate;
	
	@FXML
	TextField txtSearchByExpense;
	
	@FXML
	TextField txtAmount;
	
	@FXML
	TextField txtCGST;
	
	@FXML
	TextField txtIGST;
	
	@FXML
	TextField txtSGST;
	
	@FXML
	TextField txtRemarks;
	
	@FXML
	Button btnSubmit;
	
	@FXML
	Button btnDelete;
	
	@FXML
	Button btnReset;
	
	@FXML
	Button btnExit;
	
	
	@FXML
	private TableView<ExpensesTableBean> expensetable;
	
	@FXML
	private TableColumn<ExpensesTableBean, String> uid;
	
	@FXML
	private TableColumn<ExpensesTableBean, String> branch;
	
	@FXML
	private TableColumn<ExpensesTableBean, String> expense_date;
	
	@FXML
	private TableColumn<ExpensesTableBean, String> expense_code;
	
	@FXML
	private TableColumn<ExpensesTableBean, String> expense_type;
	
	@FXML
	private TableColumn<ExpensesTableBean, Double> amount;
	
	@FXML
	private TableColumn<ExpensesTableBean, Double> cgst;
	 
	@FXML
	private TableColumn<ExpensesTableBean, Double> sgst;
	
	@FXML
	private TableColumn<ExpensesTableBean, Double> igst;
	
	@FXML
	private TableColumn<ExpensesTableBean, String> remark;
	
	
// ==============================================================================
	
	public void setValidation() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Empty field alert");
			
		if(comboBoxBranch.getValue()==null)
		{
	    	alert.setContentText("please select Branch Code...!");
			alert.showAndWait();
			comboBoxBranch.requestFocus();
		}
	    else if(txtSearchByExpense.getText()!=null && txtSearchByExpense.getText().isEmpty())
		{
	    	alert.setContentText("Expense field is empty...!");
			alert.showAndWait();
			txtSearchByExpense.requestFocus();
		}
	    else if(txtAmount.getText()!=null && txtAmount.getText().isEmpty())
		{
			//alert.setTitle("Empty Field");
			alert.setContentText("Amount field is Empty!");
			alert.showAndWait();
			txtAmount.requestFocus();
		}
		else if(!txtAmount.getText().matches("-?\\d+(\\.\\d+)?"))			// only number validation
		{
			alert.setContentText("Amount field: Please enter only numeric value");
			alert.showAndWait();
			txtAmount.requestFocus();
		}
		else if(txtCGST.getText()!=null && txtCGST.getText().isEmpty())
		{
			alert.setContentText("CGST field is Empty!");
			alert.showAndWait();
			txtCGST.requestFocus();
		}
		else if(!txtCGST.getText().matches("-?\\d+(\\.\\d+)?"))			// only number validation
		{
			alert.setContentText("CGST field: Please enter only numeric value");
			alert.showAndWait();
			txtAmount.requestFocus();
		}		
		else if(txtSGST.getText()!=null && txtSGST.getText().isEmpty())
		{
			alert.setContentText("SGST field is Empty!");
			alert.showAndWait();
			txtSGST.requestFocus();
		}
		else if(!txtSGST.getText().matches("-?\\d+(\\.\\d+)?"))			// only number validation
		{
			alert.setContentText("SGST field: Please enter only numeric value");
			alert.showAndWait();
			txtSGST.requestFocus();
		}
		else if(txtIGST.getText()!=null && txtIGST.getText().isEmpty())
		{
			alert.setContentText("Amount field is Empty!");
			alert.showAndWait();
			txtIGST.requestFocus();
		}
		else if(!txtIGST.getText().matches("-?\\d+(\\.\\d+)?"))			// only number validation
		{
			alert.setContentText("IGST field: Please enter only numeric value");
			alert.showAndWait();
			txtIGST.requestFocus();
		}
		else if(txtRemarks.getText()!=null && txtRemarks.getText().isEmpty())
		{
			alert.setContentText("Remarks field is Empty!");
			alert.showAndWait();
			txtRemarks.requestFocus();
		}
		else
		{
			saveExpensesDetail();
		}
		
	}
	
	
// ==============================================================================
	
	public void loadBranch() throws SQLException
	{
		
		//LoadBranch lb=new LoadBranch();
		
		System.out.println("Check value is: "+LoadBranch.check);
		//Set<String> set=lb.loadBranch()
		new LoadBranch().loadBranch();
		System.out.println("Check value after the method calling: "+LoadBranch.check);
	/*LoadBranch lb=new LoadBranch();
	lb.loadBranch();*/
	
	//System.out.println(lb.setBranch.size());
	
		System.out.println("Network Contorller ============= LoadBranch from LoadBranch Class: =========== ");
		
		//System.out.println("Network Contorller >> Load from Global set object");
		for(String s:LoadBranch.SETLOADBRANCHFORALL)
		{
			
			comboBoxBranchNameItems.add(s);
		}
		comboBoxBranch.setItems(comboBoxBranchNameItems);
		
		
	}
	
	
	
//--------------------------------------------------------------------------------------------------
	
		public void loadSearchByExpenses() throws SQLException
		{
			
			//KeyEvent e = null;
			System.out.println("Search running");
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			
			Statement st=null;
			ResultSet rs=null;
			
			List<String> expenseList=new ArrayList<String>();
		 
			try
			{	
				st=con.createStatement();
				String sql="select name, code from expensestype where applicationid='MMB'";
				rs=st.executeQuery(sql);
			
				if(!rs.next())
				{
					
					
				}
				else
				{
					do
					{
						expenseList.add(rs.getString("name")+" | "+rs.getString("code"));
					}
					while(rs.next());
				}
				
				TextFields.bindAutoCompletion(txtSearchByExpense, expenseList);
				
				
				
				
				/*new runkey(KeyEvent e){
				    if(e.getCode().equals(KeyCode.ENTER))
				    {    System.out.println(txtAmount.getText());
				    txtAmount.requestFocus();
				    }
				}*/
			
			}
			catch(Exception exp)
			{
				System.out.println(exp);
				exp.printStackTrace();
			}	
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}

//--------------------------------------------------------------------------------------------------
		
	public void showBranchName()
	{
		//lblBranchName.setText("ONESOFT SOFTWARES");
	}

	
	
// ================================================================================================================================
	
	public String generateUid() throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		
		ExpenseBean exBean= new ExpenseBean();
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select uid from expenses order by uid DESC limit 1";
			rs=st.executeQuery(sql);
			
			if(!rs.next())
			{
				exBean.setUid("null");
				
			}
			else
			{
				
				exBean.setUid_startno(rs.getString("uid").replaceAll("[^A-Za-z]+", ""));
				exBean.setUid_endno(Integer.parseInt(rs.getString("uid").replaceAll("\\D+",""))+1);
				exBean.setUid(exBean.getUid_startno()+exBean.getUid_endno());
				
				
			}
				
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
		return exBean.getUid();
	}
	
// =========================================================================================
	
	public void saveExpensesDetail() throws SQLException
	{
		
		
		
		ExpenseBean exBean=new ExpenseBean();
		LocalDate expense_locatdate=dpkExpenseDate.getValue();
		Date expensedate=Date.valueOf(expense_locatdate);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;
		String query=null;
		
		String firstTimepickupno="EXP1110001";
		
		exBean.setAutogenerateUID(generateUid());
		
		exBean.setBranch(comboBoxBranch.getValue());
		
		exBean.setExpenses(txtSearchByExpense.getText());
		String[] expenseNumber=exBean.getExpenses().replaceAll("\\s+","").split("\\|");
		exBean.setExpense_code(expenseNumber[1]);
		exBean.setExpense_type(expenseNumber[0]);
		
		exBean.setAmount(Double.parseDouble(txtAmount.getText()));
		exBean.setCgst(Double.parseDouble(txtCGST.getText()));
		exBean.setSgst(Double.parseDouble(txtSGST.getText()));
		exBean.setIgst(Double.parseDouble(txtIGST.getText()));
		exBean.setRemarks(txtRemarks.getText());
		
		try
		{
			if(exBean.getAutogenerateUID().equals("null"))
			{
				exBean.setUid(firstTimepickupno);
			}
				
			else
			{
				exBean.setUid(exBean.getAutogenerateUID());
			}
			
			query = "insert into expenses(uid,expense_date,branch,expense_code,expense_type,amount,cgst,sgst,igst,remarks) values(?,?,?,?,?,?,?,?,?,?)";
			
			preparedStmt = con.prepareStatement(query);
		
			preparedStmt.setString(1,exBean.getUid());
			preparedStmt.setDate(2,expensedate);
			preparedStmt.setString(3, exBean.getBranch());
			preparedStmt.setString(4, exBean.getExpense_code());
			preparedStmt.setString(5, exBean.getExpense_type());
			preparedStmt.setDouble(6, exBean.getAmount());
			preparedStmt.setDouble(7, exBean.getCgst());
			preparedStmt.setDouble(8, exBean.getSgst());
			preparedStmt.setDouble(9, exBean.getIgst());
			preparedStmt.setString(10, exBean.getRemarks());
			
			preparedStmt.executeUpdate();
			
			reset();
			loadExpensesTable();
			
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Expense Entry");
			alert.setHeaderText(null);
			alert.setContentText("New Expense entry successfully saved... \n"
					+ "Expense No.: "+exBean.getUid());
			alert.showAndWait();
		
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	
			dbcon.disconnect(preparedStmt, null, null, con);
		}
	}	

	// =========================================================================================
	
	public void deleteAlert() throws SQLException
	{
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Delete Confirmation Dialog");
		alert.setHeaderText("Are you sure you want to delete this record");
		//alert.setContentText("Are you sure you want to delete this record?");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			
			deleteExpenseRecord();
		    // ... user chose OK
		} else {
		    // ... user chose CANCEL or closed the dialog
		}
	}
	
	// =========================================================================================
	
		public void deleteExpenseRecord() throws SQLException
		{
			
			
			
			
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
			PreparedStatement preparedStmt=null;
			String query=null;
			
			
			
			try
			{
				
				
				query = "delete from expenses where uid=?";
				
				preparedStmt = con.prepareStatement(query);
			
				preparedStmt.setString(1,lblBranchName.getText());
				
				
				preparedStmt.executeUpdate();
				
				loadExpensesTable();
				btnDelete.setDisable(true);
				reset();
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{	
				dbcon.disconnect(preparedStmt, null, null, con);
			}
		}		
	
	
// ================================================================================================================================	

	public void loadExpensesTable() throws SQLException
	{
		//receivePickupTabledata.clear();
		
		ExprensesTabledata.clear();
		
		PickupBean pkbean=new PickupBean();
		ExpenseBean exBean=new ExpenseBean();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		try
		{	
			st=con.createStatement();
			String sql="select uid,expense_date,branch,expense_code,expense_type,amount,cgst,sgst,igst,remarks from expenses order by uid";
				rs=st.executeQuery(sql);
		
				if(!rs.next())
				{
				
				}
				else
				{
					do
					{
						
						exBean.setUid(rs.getString("uid"));
						exBean.setExpenseDate(date.format(rs.getDate("expense_date")));
						exBean.setBranch(rs.getString("branch"));
						exBean.setExpense_code(rs.getString("expense_code"));
						exBean.setExpense_type(rs.getString("expense_type"));
						exBean.setAmount(Double.parseDouble(df.format(rs.getDouble("amount"))));
						exBean.setCgst(Double.parseDouble(df.format(rs.getDouble("cgst"))));
						exBean.setSgst(Double.parseDouble(df.format(rs.getDouble("sgst"))));
						exBean.setIgst(Double.parseDouble(df.format(rs.getDouble("igst"))));
						exBean.setRemarks(rs.getString("remarks"));
						
						ExprensesTabledata.add(new ExpensesTableBean(exBean.getUid(), exBean.getExpenseDate(), exBean.getBranch() , exBean.getExpense_code(),
								exBean.getExpense_type(), exBean.getRemarks(), exBean.getAmount(), exBean.getCgst(), exBean.getSgst(), exBean.getIgst()));
						
						
					}
					while(rs.next());
					
					expensetable.setItems(ExprensesTabledata);
					clickInboxRow();
					
				//	receivePickupTable.setItems(receivePickupTabledata);
				}
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}	
			finally
			{
				dbcon.disconnect(null, st, rs, con);
			}
		}	
	
	
	
// =================================================================================================================================	

	public void clickInboxRow() throws Exception
	{
	
		 
		/*	// user to convert string date into localdate
			
		SimpleDateFormat utilDateFormatter = new SimpleDateFormat("dd-MM-yyyy");
		java.util.Date localdate=utilDateFormatter.parse(rowData.getExpenseDate());
		LocalDate expensedate = new java.sql.Date( localdate.getTime() ).toLocalDate();
		dpkExpenseDate.setValue(expensedate);
		*/	
			
		expensetable.setRowFactory( tv -> {
		TableRow<ExpensesTableBean> row = new TableRow<>();
		row.setOnMouseClicked(event -> {
			    	
		ExpensesTableBean rowData = row.getItem();
			  
		try
		{

			if (event.getClickCount() == 1 && (! row.isEmpty()) ) 
			{
				// use for single click
			}

			if (event.getClickCount() == 2 && (! row.isEmpty()))
			{
				comboBoxBranch.setValue(rowData.getBranch());
				dpkExpenseDate.setValue(LocalDate.parse(rowData.getExpenseDate(),localdateformatter));
				txtSearchByExpense.setText(rowData.getExpense_type()+" | "+rowData.getExpense_code());
				txtAmount.setText(df.format(rowData.getAmount()));
				txtCGST.setText(df.format(rowData.getCgst()));
				txtSGST.setText(df.format(rowData.getSgst()));
				txtIGST.setText(df.format(rowData.getIgst()));
				txtRemarks.setText(rowData.getRemarks());
				lblBranchName.setText(rowData.getUid());
				lblBranchName.setStyle("-fx-border-width: 2; -fx-border-color: #E9E9E9");
				
				//#045eef
				if(!lblBranchName.getText().equals(""))
				{
					btnDelete.setDisable(false);
				}
						
			}
		}
		
		catch (Exception e)
		{
			e.printStackTrace();
		}
					
			    });
		    return row ;
			});
		
		}	
	
	
// =========================================================================================
		
	public void reset()
	{
	
		comboBoxBranch.getSelectionModel().clearSelection();
		//dpkExpenseDate.getEditor().clear();
		dpkExpenseDate.setValue(LocalDate.now());
		txtSearchByExpense.clear();
		txtAmount.clear();
		txtSGST.clear();
		txtCGST.clear();
		txtIGST.clear();
		txtRemarks.clear();
		lblBranchName.setText("");
		lblBranchName.setStyle("-fx-border-width: 0;");
		
		if(lblBranchName.getText().equals(""))
		{
			btnDelete.setDisable(true);
		}
	}
	
	
// ========================================================================================
	
	
	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException{

		Node n=(Node) e.getSource();
		
		
		if(n.getId().equals("comboBoxBranch"))						
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				dpkExpenseDate.requestFocus();
			}
		}
		
		else if(n.getId().equals("dpkExpenseDate"))						// Enter Key date picker
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtSearchByExpense.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtSearchByExpense"))				// Enter Keyevent for Expense Textfield
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtAmount.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtAmount"))						// Enter Keyevent for Amount Textfield
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtCGST.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtCGST"))						// Enter Keyevent for CGST Textfield
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtSGST.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtSGST"))						// Enter Keyevent for SGST Textfield
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtIGST.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtIGST"))						// Enter Keyevent for IGST Textfield
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtRemarks.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtRemarks"))						// Enter Keyevent for Remarks Textfield
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				btnSubmit.requestFocus();
			}
		}
		
		else if(n.getId().equals("btnSubmit"))						// Enter Keyevent for Submit button
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				saveExpensesDetail();
				txtSearchByExpense.requestFocus();
				
			}
		}

	}

	
// =========================================================================================	

		@Override
		public void initialize(URL location, ResourceBundle resources) {
			// TODO Auto-generated method stub
			
			imgSearchIcon.setImage(new Image("/com/onesoft/courier/icon/searchicon_blue.png"));
			
			comboBoxBranch.setItems(comboBoxBranchNameItems);
			btnDelete.setDisable(true);
			dpkExpenseDate.setValue(LocalDate.now());
		
			
			try {
				loadBranch();
				loadSearchByExpenses();
				loadExpensesTable();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			uid.setCellValueFactory(new PropertyValueFactory<ExpensesTableBean,String>("uid"));
			branch.setCellValueFactory(new PropertyValueFactory<ExpensesTableBean,String>("branch"));
			expense_date.setCellValueFactory(new PropertyValueFactory<ExpensesTableBean,String>("expenseDate"));
			expense_code.setCellValueFactory(new PropertyValueFactory<ExpensesTableBean,String>("expense_code"));
			expense_type.setCellValueFactory(new PropertyValueFactory<ExpensesTableBean,String>("expense_type"));
			remark.setCellValueFactory(new PropertyValueFactory<ExpensesTableBean,String>("remarks"));
			
			
			amount.setCellValueFactory(new PropertyValueFactory<ExpensesTableBean,Double>("amount"));
			cgst.setCellValueFactory(new PropertyValueFactory<ExpensesTableBean,Double>("cgst"));
			sgst.setCellValueFactory(new PropertyValueFactory<ExpensesTableBean,Double>("sgst"));
			igst.setCellValueFactory(new PropertyValueFactory<ExpensesTableBean,Double>("igst"));
			
			
			
		}

}
