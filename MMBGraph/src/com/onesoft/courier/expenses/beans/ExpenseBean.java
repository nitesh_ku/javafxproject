package com.onesoft.courier.expenses.beans;

public class ExpenseBean {
	
	
	private String uid_startno;
	private int uid_endno;
	private String autogenerateUID;
	
	private String expenses;
	
	
	private String uid;
	private String expenseDate;
	private String branch;
	private String expense_code;
	private String expense_type;
	private String remarks;
	
	private double amount;
	private double cgst;
	private double sgst;
	private double igst;
	

	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getExpenseDate() {
		return expenseDate;
	}
	public void setExpenseDate(String expenseDate) {
		this.expenseDate = expenseDate;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getExpense_code() {
		return expense_code;
	}
	public void setExpense_code(String expense_code) {
		this.expense_code = expense_code;
	}
	public String getExpense_type() {
		return expense_type;
	}
	public void setExpense_type(String expense_type) {
		this.expense_type = expense_type;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getCgst() {
		return cgst;
	}
	public void setCgst(double cgst) {
		this.cgst = cgst;
	}
	public double getSgst() {
		return sgst;
	}
	public void setSgst(double sgst) {
		this.sgst = sgst;
	}
	public double getIgst() {
		return igst;
	}
	public void setIgst(double igst) {
		this.igst = igst;
	}
	public String getUid_startno() {
		return uid_startno;
	}
	public void setUid_startno(String uid_startno) {
		this.uid_startno = uid_startno;
	}
	public int getUid_endno() {
		return uid_endno;
	}
	public void setUid_endno(int uid_endno) {
		this.uid_endno = uid_endno;
	}
	public String getAutogenerateUID() {
		return autogenerateUID;
	}
	public void setAutogenerateUID(String autogenerateUID) {
		this.autogenerateUID = autogenerateUID;
	}
	public String getExpenses() {
		return expenses;
	}
	public void setExpenses(String expenses) {
		this.expenses = expenses;
	}
	
}
