package com.onesoft.courier.booking.controller;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.controlsfx.control.textfield.TextFields;

import com.DB.DBconnection;
import com.onesoft.courier.booking.bean.AwbInwardTableBean;
import com.onesoft.courier.booking.bean.InwardFormBean;
import com.onesoft.courier.booking.bean.SubAwbInwardTableBean;
import com.onesoft.courier.common.LoadCity;
import com.onesoft.courier.common.LoadClientBean;
import com.onesoft.courier.common.LoadClients;
import com.onesoft.courier.common.bean.LoadCityWithZipcodeBean;
import com.onesoft.courier.main.Main;
import com.onesoft.courier.stockdetail.bean.StockAllotmentBean;
import com.onesoft.courier.stockdetail.bean.StockAllotmentTableBean;
import com.onesoft.courier.stockdetail.bean.StockUnAllotmentTableBean;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TreeTableColumn.CellDataFeatures;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Callback;


public class InwardFormController implements Initializable{
	
	
	private ObservableList<AwbInwardTableBean> tabledata_Awb=FXCollections.observableArrayList();
	private ObservableList<SubAwbInwardTableBean> tabledata_SubAwb=FXCollections.observableArrayList();
	private ObservableList<String> listViewUnsavedSubAwbItems =FXCollections.observableArrayList ();
	
	TreeItem<SubAwbInwardTableBean> treeItem=new TreeItem<>();
	TreeItem<SubAwbInwardTableBean> treeItemroot=new TreeItem<>();
	
	
	private ObservableList<String> comboBoxClientItems=FXCollections.observableArrayList();
	
	private List<String> list_clients=new ArrayList<>();
	private List<InwardFormBean> list_inward=new ArrayList<>();
	private List<InwardFormBean> list_unsavedSubAwb=new ArrayList<>();
	
	private int submitCount=0;
	
	DecimalFormat df=new DecimalFormat(".##");
	DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
	DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	
	Pattern pattern = Pattern.compile("\\d+");			// ====== Pattern to check only Digits =====
	
// ==================================================
	
	@FXML
	private ImageView imgSearchIcon_AwbInward;
	
	@FXML
	private ImageView imgSearchIcon_SubAwbInward;
	
	@FXML
	private ListView<String> listView_UnsavedSubAwb;
	
	@FXML
	private Label lblUnsavedSubAwb;
	
	
// ===============================================
	
	@FXML
	private Pagination pagination_AwbInward;
	
	@FXML
	private Pagination pagination_SubAwbInward;	
	
	
// ==================================================	
	
	@FXML
	private TextField txtZipcode;
	
	@FXML
	private TextField txtDestination;
	
	@FXML
	private TextField txtPcs;
	
	@FXML
	private TextField txtWeight;
	
	@FXML
	private TextField txtAwbNo;
	
	@FXML
	private TextField txtSubAwbNo;
	
	@FXML
	private TextField txtClient;
	
	//@FXML
	//private ComboBox<String> comboBoxClient;
	
// ==================================================
	
	@FXML
	private Button btnReceive;

	@FXML
	private Button btnReset;

	@FXML
	private Button btnExit;
	
	@FXML
	private Button btnExportToExcel_Awb;
	
	@FXML
	private Button btnExportToExcel_SubAwb;
	
	
// ===============================================	
	
	@FXML
	private TableView<AwbInwardTableBean> tableAwbInward;
	
	@FXML
	private TableColumn<AwbInwardTableBean, Integer> awbInward_tableCol_Slno;
	
	@FXML
	private TableColumn<AwbInwardTableBean, String> awbInward_tableCol_AwbNo;
	
	@FXML
	private TableColumn<AwbInwardTableBean, String> awbInward_tableCol_BookingDate;
	
	@FXML
	private TableColumn<AwbInwardTableBean, String> awbInward_tableCol_Client;
	
	@FXML
	private TableColumn<AwbInwardTableBean, String> awbInward_tableCol_Destination;
	
	@FXML
	private TableColumn<AwbInwardTableBean, Integer> awbInward_tableCol_Zipcode;
	
	@FXML
	private TableColumn<AwbInwardTableBean, Integer> awbInward_tableCol_Pcs;
	
	@FXML
	private TableColumn<AwbInwardTableBean, Double> awbInward_tableCol_Weight;
	
	@FXML
	private TableColumn<AwbInwardTableBean, String> awbInward_tableCol_SubAwbStatus;
	
	
// ===============================================	
	

	@FXML
	private TreeTableView<SubAwbInwardTableBean> tableSubAwbInward;;
	
	@FXML
	private TreeTableColumn<SubAwbInwardTableBean, Number> subAwbInward_tableCol_Slno;
	
	@FXML
	private TreeTableColumn<SubAwbInwardTableBean, String> subAwbInward_tableCol_AwbNo;
	
	@FXML
	private TreeTableColumn<SubAwbInwardTableBean, String> subAwbInward_tableCol_SubAwbNo;
	
	@FXML
	private TreeTableColumn<SubAwbInwardTableBean, String> subAwbInward_tableCol_Destination;
	
	@FXML
	private TreeTableColumn<SubAwbInwardTableBean, Number> subAwbInward_tableCol_Zipcode;
	
	@FXML
	private TreeTableColumn<SubAwbInwardTableBean, Number> subAwbInward_tableCol_Pcs;
	
	@FXML
	private TreeTableColumn<SubAwbInwardTableBean, String> subAwbInward_tableCol_ReceiveDate;
	
	@FXML
	private TreeTableColumn<SubAwbInwardTableBean, Number> subAwbInward_tableCol_Weight;
	
	@FXML
	private TreeTableColumn<SubAwbInwardTableBean, String> subAwbInward_tableCol_SubAwbStatus;
	
	
	
// ===============================================		
		
	/*@FXML
	private TableView<SubAwbInwardTableBean> tableSubAwbInward;
	
	@FXML
	private TableColumn<SubAwbInwardTableBean, Integer> subAwbInward_tableCol_Slno;
	
	@FXML
	private TableColumn<SubAwbInwardTableBean, String> subAwbInward_tableCol_AwbNo;
	
	@FXML
	private TableColumn<SubAwbInwardTableBean, String> subAwbInward_tableCol_SubAwbNo;
	
	@FXML
	private TableColumn<SubAwbInwardTableBean, String> subAwbInward_tableCol_Destination;
	
	@FXML
	private TableColumn<SubAwbInwardTableBean, Integer> subAwbInward_tableCol_Zipcode;
	
	@FXML
	private TableColumn<SubAwbInwardTableBean, Integer> subAwbInward_tableCol_Pcs;
	
	@FXML
	private TableColumn<SubAwbInwardTableBean, String> subAwbInward_tableCol_ReceiveDate;

	*/
	
	
// =============================================================================================
	
	public void loadClients() throws SQLException
	{
		new LoadClients().loadClientWithName();
		
		for(LoadClientBean bean:LoadClients.SET_LOAD_CLIENTWITHNAME)
		{
		
			list_clients.add(bean.getClientName()+" | "+bean.getClientCode());
			
		}
		TextFields.bindAutoCompletion(txtClient, list_clients);
		
			
	}

	

	
// =============================================================================================	
	
	public void saveInwardData() throws SQLException
	{
		boolean status=false;
		Matcher matcher = pattern.matcher(txtSubAwbNo.getText());
		
		if(submitCount==0)				// Check Awb No if PCS is greater than 1
		{
			status=CheckAwbNoExistance(txtAwbNo.getText());
		}
		
		if(status==false)
		{
			submitCount++;				// read the submit button count
		
			String[] clientSplitValue=txtClient.getText().replaceAll("\\s+","").split("\\|");
			//String[] clientSplitValue=comboBoxClient.getValue().replaceAll("\\s+","").split("\\|");
		
			InwardFormBean inwardBean=new InwardFormBean();
			
			inwardBean.setClientCode(clientSplitValue[1]);
			inwardBean.setZipcode(Integer.valueOf(txtZipcode.getText()));
			inwardBean.setDestination(txtDestination.getText());
			inwardBean.setPcs(Integer.valueOf(txtPcs.getText()));
			inwardBean.setWeight(Double.valueOf(txtWeight.getText()));
			inwardBean.setAwbNo(txtAwbNo.getText());
			
			if(txtSubAwbNo.isDisable()==false)				// condition for: check Sub Awb textfield is disable or not
			{	
			
				inwardBean.setSubAwbnoStatus("Multiple");
				
				if(matcher.matches())		// check entered Sub Awb no is alphnumeric or not
				{
					inwardBean.setSubAwbNo(txtSubAwbNo.getText());
				}
				else
				{
					inwardBean.setSubAwbNo(txtSubAwbNo.getText());
					inwardBean.setSubAwb_Prefix(inwardBean.getSubAwbNo().replaceAll("[^A-Za-z]+", "").toUpperCase());
					inwardBean.setSubAwb_suffix(Long.parseLong(inwardBean.getSubAwbNo().replaceAll("\\D+","")));
				}
			}
			else
			{
				inwardBean.setSubAwbnoStatus("Single");
			}
			
			
			if(Integer.valueOf(txtPcs.getText()) >1 && submitCount>=1)		// if PCS greater than 1, then disable all the fields except sub awb field after first click on submit
			{	
				if(matcher.matches())
				{	
					txtSubAwbNo.setText(String.valueOf(Long.valueOf(inwardBean.getSubAwbNo())+1));
				}
				else
				{
					System.out.println("prefix: "+inwardBean.getSubAwb_Prefix());
					System.out.println("suffix: "+inwardBean.getSubAwb_suffix());
					txtSubAwbNo.setText(inwardBean.getSubAwb_Prefix()+(inwardBean.getSubAwb_suffix()+1));
				}
				
				txtClient.setDisable(true);
				//comboBoxClient.setDisable(true);
				txtAwbNo.setDisable(true);
				txtDestination.setDisable(true);
				txtPcs.setDisable(true);
				txtWeight.setDisable(true);
				txtZipcode.setDisable(true);
				txtSubAwbNo.requestFocus();
				
			}
			
			list_inward.add(inwardBean);
			
			
			
			if(Integer.valueOf(txtPcs.getText())==list_inward.size())
			{
				txtClient.setDisable(false);
				//comboBoxClient.setDisable(false);
				txtAwbNo.setDisable(false);
				txtDestination.setDisable(false);
				txtPcs.setDisable(false);
				txtSubAwbNo.setDisable(false);
				txtWeight.setDisable(false);
				txtZipcode.setDisable(false);
				txtSubAwbNo.setDisable(true);
				
				txtClient.requestFocus();
				//comboBoxClient.requestFocus();
				
				if(Integer.valueOf(txtPcs.getText())>1)
				{
					saveAwbNo();
					saveSubAwbnoData();
				}
				else
				{
					saveAwbNo();
				}
				//list_inward.clear();
			}
		}
	}
	
	
	
// ================== Method to check AWB No. existance =====================================		
	
	public boolean CheckAwbNoExistance(String awbno) throws SQLException
	{
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
	
		
		Statement st=null;
		ResultSet rs=null;
		
		boolean awbStatus=false;
		
		try
		{	
			st=con.createStatement();
			String sql="select * from dailybookingtransaction where air_way_bill_number='"+awbno+"'";
			rs=st.executeQuery(sql);
			System.out.println("Sql: " +sql);
			
			if(rs.next())
			{
				awbStatus=true;
				System.out.println("Awbno: exist: "+ txtAwbNo.getText());
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Awb No. Alert");
				alert.setHeaderText(null);
				alert.setContentText("Entered Awb No. is already exist");
				alert.showAndWait();
				txtSubAwbNo.clear();
				txtAwbNo.requestFocus();
			}
			else
			{
				if(txtSubAwbNo.isDisable()==false)
				{
				txtSubAwbNo.setText(txtAwbNo.getText());
				}
				System.out.println("Awbno: "+ txtAwbNo.getText());
			}
			
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
		return awbStatus;
		
	}
	
// =========== Method to Save Awb No. single entry in daily Booking Transaction table ===================		
	
	public void saveAwbNo() throws SQLException
	{
		
		boolean awbStatus=false;
		
		awbStatus=CheckAwbNoExistance(txtAwbNo.getText());
		
		System.out.println("Status from save: "+awbStatus);
		
		if(awbStatus==false)
		{
			DBconnection dbcon=new DBconnection();
			Connection con=dbcon.ConnectDB();
				
			
			PreparedStatement preparedStmt=null;
			ResultSet rs=null;
			String query=null;
			 		 
			try
			{	
				for(InwardFormBean bean: list_inward)
				{
					query = "insert into dailybookingtransaction(booking_date,air_way_bill_number,dailybookingtransaction2client,dailybookingtransaction2city,"
							+ "packets,billing_weight,sub_awbno_status,zipcode,create_date,"
								+ "lastmodifieddate,user_name) values(CURRENT_DATE,?,?,?,?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP,?)";
					preparedStmt = con.prepareStatement(query);
					
					
					preparedStmt.setString(1, bean.getAwbNo());
					preparedStmt.setString(2, bean.getClientCode().trim());
					preparedStmt.setString(3, bean.getDestination());
					preparedStmt.setInt(4, bean.getPcs());
					preparedStmt.setDouble(5, bean.getWeight());
					preparedStmt.setString(6, bean.getSubAwbnoStatus());
					preparedStmt.setLong(7, bean.getZipcode());
					preparedStmt.setString(8, bean.getLoggedInUser());
					
					int i=	preparedStmt.executeUpdate();
					break;
					
				}
				
				loadAwbInwardTable();
				
				if(list_inward.size()==1)
				{
					reset();
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setHeaderText(null);
							
						alert.setTitle("Submit Alert");
						alert.setContentText("Inward Received Successfully");
						alert.showAndWait();
					
				}
				
				
				
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				dbcon.disconnect(preparedStmt, null, rs, con);
				if(list_inward.size()==1)
				{
					
					list_inward.clear();
				}
				submitCount=0;
				
				
				/*if(Integer.valueOf(txtPcs.getText())==1)
				{
					reset();
				}*/
				
			}
		}
	}
	
	
// ============= Method to check existance of Sub Awb no. ===========================================			
	
	public boolean checkSubAwbno(String subAwbNo) throws SQLException
	{
		boolean checkSubAwbStatus=true;
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		
		Statement st=null;
		ResultSet rs=null;
		
		 		 
		try
		{	
			st=con.createStatement();
			String sql="select * from sub_awbno where sub_awbno='"+subAwbNo+"'";
			rs=st.executeQuery(sql);
			
			if(!rs.next())
			{
				checkSubAwbStatus=true;
			}
			else
			{
				checkSubAwbStatus=false;
				
				InwardFormBean inBean=new InwardFormBean();
				inBean.setUnSavedSubAwbNo(subAwbNo);
				list_unsavedSubAwb.add(inBean);
				
		
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
		return checkSubAwbStatus;
		
	}
	
	
// =============== Method to Save Sub Awb No. entries ===================================		
	
	public void saveSubAwbnoData() throws SQLException
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
		PreparedStatement preparedStmt=null;
		String query=null;
		
		boolean subAwbStatus=true;

	
		try
		{	
			for(InwardFormBean bean: list_inward)
			{
				subAwbStatus=checkSubAwbno(bean.getSubAwbNo());
				
				if(subAwbStatus==true)
				{
					query = "insert into sub_awbno(sub_awbno,awbno,zipcode,destination,pcs,create_date,"
							+ "lastmodifieddate) values(?,?,?,?,?,CURRENT_DATE,CURRENT_TIMESTAMP)";
					preparedStmt = con.prepareStatement(query);
					
					preparedStmt.setString(1, bean.getSubAwbNo());
					preparedStmt.setString(2, bean.getAwbNo());
					preparedStmt.setLong(3, bean.getZipcode());
					preparedStmt.setString(4, bean.getDestination());
					preparedStmt.setInt(5, 1);
			
					preparedStmt.executeUpdate();
				}
			}
			
			//loadSubAwbInwardTable();
			reset();
			
			if(list_unsavedSubAwb.size()==0)
			{
				//lblUnsavedSubAwb.setVisible(false);
				//listView_UnsavedSubAwb.setVisible(false);
				
				alert.setTitle("Submit Alert");
				alert.setContentText("All Sub Awb Inwards Successfully saved...");
				alert.showAndWait();
			}
			else
			{
				
				alert.setTitle("Submit Alert");
				alert.setContentText("Some Sub Awb no's are already exist...!!");
				alert.showAndWait();
				
				lblUnsavedSubAwb.setVisible(true);
				listView_UnsavedSubAwb.setVisible(true);
				
				for(InwardFormBean bean: list_unsavedSubAwb)
				{
					listViewUnsavedSubAwbItems.add(bean.getUnSavedSubAwbNo());
				}
				
				listView_UnsavedSubAwb.setItems(listViewUnsavedSubAwbItems);
				list_unsavedSubAwb.clear();
				
			}
		
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}	
		finally
		{	

			dbcon.disconnect(preparedStmt, null, null, con);
			if(list_inward.size()>1)
			{
				System.out.println("List of Unsaved Sub Awb Numbers: ");
				for(InwardFormBean bean: list_unsavedSubAwb)
				{
					System.out.println("Sub Awb No: "+bean.getUnSavedSubAwbNo());
				}
				
				list_inward.clear();
			}
		}
		
	}
	
	
	
// =============================================================================================


	public void reset()
	{
		//comboBoxClient.getSelectionModel().clearSelection();
		txtClient.clear();
		txtAwbNo.clear();
		txtDestination.clear();
		txtPcs.clear();
		txtSubAwbNo.clear();
		txtWeight.clear();
		txtZipcode.clear();
	}


//=============================================================================================
	
	public void getDestinationFromZipcode()
	{
		
		if(!txtZipcode.getText().equals(""))
		{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		
		boolean checkZipcodeStatus=false;
		LoadCityWithZipcodeBean destinationBean=new LoadCityWithZipcodeBean();
		
		for(LoadCityWithZipcodeBean bean:LoadCity.LIST_LOAD_CITY_WITH_ZIPCODE)
		{
			if(Long.valueOf(txtZipcode.getText())==bean.getZipcode())
			{
				checkZipcodeStatus=true;
				destinationBean.setCityCode(bean.getCityCode());
				break;
			}
			else
			{
				checkZipcodeStatus=false;
			}
		}
		
		
		if(checkZipcodeStatus==true)
		{
			txtDestination.setText(destinationBean.getCityCode());
			txtDestination.requestFocus();
		}
		else
		{
			txtDestination.clear();
			alert.setTitle("Zipcode City Alert");
			alert.setContentText("Please enter correct zipcode");
			alert.showAndWait();
			txtZipcode.requestFocus();
		}
		}
		else
		{
			txtDestination.requestFocus();
		}
	}
	
	
//=============================================================================================

	@FXML
	public void checkPcsCount()
	{
		if(Integer.valueOf(txtPcs.getText())>1)
		{
			txtSubAwbNo.setDisable(false);
		}
		else
		{
			txtSubAwbNo.setDisable(true);
		}
	}	

// ============== Method the move Cursor using Entry Key =================

	@FXML
	public void useEnterAsTabKey(KeyEvent e) throws SQLException
	{
		Node n=(Node) e.getSource();
		
	
		if(n.getId().equals("txtClient"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtZipcode.requestFocus();
			}	
		}	
		else if(n.getId().equals("txtZipcode"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				getDestinationFromZipcode();
				//txtDestination.requestFocus();
			}	
		}
		else if(n.getId().equals("txtDestination"))						
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtPcs.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtPcs"))						
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(!txtPcs.getText().equals(""))
				{
				checkPcsCount();
				}
				txtWeight.requestFocus();
				
			}
		}
		
		else if(n.getId().equals("txtWeight"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				txtAwbNo.requestFocus();
			}
		}
		
		else if(n.getId().equals("txtAwbNo"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				if(txtSubAwbNo.isDisable()==true)
				{
					btnReceive.requestFocus();
					CheckAwbNoExistance(txtAwbNo.getText());
				}
				else
				{
					//txtSubAwbNo.setText(txtAwbNo.getText());
					txtSubAwbNo.requestFocus();
					CheckAwbNoExistance(txtAwbNo.getText());
				}
			}
		}
			
		else if(n.getId().equals("txtSubAwbNo"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				btnReceive.requestFocus();
			}
		}
		
		else if(n.getId().equals("btnReceive"))
		{
			if(e.getCode().equals(KeyCode.ENTER))
			{
				setValidation();
			}
		}
	}	
	
	
// =============================================================================================	
	
	public void setValidation() throws SQLException
	{
			
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		
				
				
		/*if(comboBoxClient.getValue()==null || comboBoxClient.getValue().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Client field is Empty!");
			alert.showAndWait();
			comboBoxClient.requestFocus();
			
		} */
		
		
		if(txtClient.getText()==null || txtClient.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Client field is Empty!");
			alert.showAndWait();
			txtClient.requestFocus();
			
		} 
		
		else if (txtZipcode.getText() == null || txtZipcode.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Zipcode field is Empty!");
			alert.showAndWait();
			txtZipcode.requestFocus();
			
		}

		else if (txtDestination.getText() == null || txtDestination.getText().isEmpty())
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Destination field is Empty!");
			alert.showAndWait();
			txtDestination.requestFocus();
		
		}

		else if (txtPcs.getText() != null && txtPcs.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("PCS field is Empty!");
			alert.showAndWait();
			txtPcs.requestFocus();
		}
		
		else if (txtWeight.getText() != null && txtWeight.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Weight field is Empty!");
			alert.showAndWait();
			txtWeight.requestFocus();
		}

		
		else if (txtAwbNo.getText() != null && txtAwbNo.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Awb No. field is Empty!");
			alert.showAndWait();
			txtAwbNo.requestFocus();
		}
		
		
		else if (txtSubAwbNo.isDisable()==false && txtSubAwbNo.getText() != null && txtSubAwbNo.getText().isEmpty()) 
		{
			alert.setTitle("Empty Field Validation");
			alert.setContentText("Sub Awb No. field is Empty!");
			alert.showAndWait();
			txtSubAwbNo.requestFocus();
		}

		else if(txtSubAwbNo.isDisable()==false && txtAwbNo.isDisable()==false)
		{
			if(!txtAwbNo.getText().equals(txtSubAwbNo.getText()))
			{
				alert.setTitle("Miss match Validation");
				alert.setContentText("Awb No. and Sub Awb No. should be same for first time...!!");
				alert.showAndWait();
				txtSubAwbNo.setText(txtAwbNo.getText());
				txtSubAwbNo.requestFocus();
			}
			else
			{
				saveInwardData();
			}
			
		}
		else 
		{
			saveInwardData();
		}
	}		
	
	
// ================= Method to Load Un-allocated table data ==================		
	
	public void loadAwbInwardTable() throws SQLException
	{
		tabledata_Awb.clear();
		
		ResultSet rs=null;
		Statement stmt=null;
		String sql=null;
		DBconnection dbcon=new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		InwardFormBean inBean=new InwardFormBean();
		
		int slno=1;
			try
			{
	
				stmt=con.createStatement();
				sql="select dailybookingtransactionid, air_way_bill_number,booking_date,dailybookingtransaction2client,"
						+ "dailybookingtransaction2city,zipcode,packets,billing_weight,sub_awbno_status from dailybookingtransaction "
							+ "where sub_awbno_status='Single' OR sub_awbno_status='Multiple' order by dailybookingtransactionid DESC";
				rs=stmt.executeQuery(sql);
				
				while(rs.next())
				{
					inBean.setSlno(slno);
					inBean.setAwbNo(rs.getString("air_way_bill_number"));
					inBean.setBookingDate(date.format(rs.getDate("booking_date")));
					inBean.setClientCode(rs.getString("dailybookingtransaction2client"));
					inBean.setDestination(rs.getString("dailybookingtransaction2city"));
					inBean.setZipcode(rs.getInt("zipcode"));
					inBean.setPcs(rs.getInt("packets"));
					inBean.setWeight(rs.getDouble("billing_weight"));
					inBean.setSubAwbnoStatus(rs.getString("sub_awbno_status"));
					
					tabledata_Awb.add(new AwbInwardTableBean(inBean.getSlno(), inBean.getClientCode(), inBean.getDestination(),
							inBean.getAwbNo(), inBean.getBookingDate(), inBean.getSubAwbnoStatus(), inBean.getZipcode(),
							inBean.getPcs(), inBean.getWeight()));
						
					slno++;
				}
			
				tableAwbInward.setItems(tabledata_Awb);
				
				
				pagination_AwbInward.setPageCount((tabledata_Awb.size() / rowsPerPage() + 1));
				pagination_AwbInward.setCurrentPageIndex(0);
				pagination_AwbInward.setPageFactory((Integer pageIndex) -> createPage_AwbInward(pageIndex));
				
				
			}
		
			catch (Exception e)
			{
				System.out.println(e);
			}
	
			finally
			{
				dbcon.disconnect(null, stmt, rs, con);
			}
	}		
	
	
	
// ================= Method to Load Un-allocated table data ==================		
	
	
	
	/*public void loadSubAwbInwardTable() throws SQLException
	{
		tabledata_SubAwb.clear();
		
		ResultSet rs=null;
		Statement stmt=null;
		String sql=null;
		DBconnection dbcon=new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		
		
		
		int slno=1;

		try
		{
			stmt=con.createStatement();
			sql="select dbt.air_way_bill_number as awbno,sub.sub_awbno as sub_awbno,dbt.booking_date as booking_date,dbt.dailybookingtransaction2client as client,"
					+ "dbt.dailybookingtransaction2city as destination,dbt.zipcode as zipcode,sub.pcs as sub_Awb_pcs,"
					+ "dbt.billing_weight as weight,dbt.sub_awbno_status as status from dailybookingtransaction as dbt,"
					+ "sub_awbno as sub where dbt.air_way_bill_number= sub.awbno order by dbt.dailybookingtransactionid DESC";
			rs=stmt.executeQuery(sql);
			System.out.println("SQL: "+sql);
			
			while(rs.next())
			{
				InwardFormBean inBean=new InwardFormBean();
				inBean.setSlno(slno);
				inBean.setAwbNo(rs.getString("awbno"));
				inBean.setSubAwbNo(rs.getString("sub_awbno"));
				inBean.setBookingDate(date.format(rs.getDate("booking_date")));
				inBean.setClientCode(rs.getString("client"));
				inBean.setDestination(rs.getString("destination"));
				inBean.setZipcode(rs.getInt("zipcode"));
				inBean.setPcs(rs.getInt("sub_Awb_pcs"));
				inBean.setWeight(rs.getDouble("weight"));
				inBean.setSubAwbnoStatus(rs.getString("status"));
				
				
				
				treeItemroot.setValue(new SubAwbInwardTableBean(inBean.getSlno(), inBean.getClientCode(), inBean.getDestination(), inBean.getAwbNo(),inBean.getSubAwbNo(), inBean.getBookingDate(), inBean.getSubAwbnoStatus(), inBean.getZipcode(), inBean.getPcs(), inBean.getWeight()));
				treeItem.setValue(new SubAwbInwardTableBean(inBean.getSlno(), inBean.getClientCode(), inBean.getDestination(), inBean.getAwbNo(),inBean.getSubAwbNo(), inBean.getBookingDate(), inBean.getSubAwbnoStatus(), inBean.getZipcode(), inBean.getPcs(), inBean.getWeight()));
				System.out.println("Tree Item: "+ treeItem.getValue().getAwbNo());
				
				
			}
			
			//treeItemroot.getChildren().setAll(treeItem);
			tableSubAwbInward.setRoot(treeItemroot);
			//
			//tableSubAwbInward.setRoot(treeItemroot);
			tableSubAwbInward.setShowRoot(false);
		
			//tableSubAwbInward.setItems(tabledata_SubAwb);
			
			
			pagination_SubAwbInward.setPageCount((tabledata_SubAwb.size() / rowsPerPage() + 1));
			pagination_SubAwbInward.setCurrentPageIndex(0);
			pagination_SubAwbInward.setPageFactory((Integer pageIndex) -> createPage_SubAwbInward(pageIndex));
			
			
		}
	
		catch (Exception e)
		{
			System.out.println(e);
		}
		finally
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
	}	*/
	
	
	
	
	/*public void loadSubAwbInwardTable() throws SQLException
	{
		tabledata_SubAwb.clear();
		
		ResultSet rs=null;
		Statement stmt=null;
		String sql=null;
		DBconnection dbcon=new DBconnection();
		Connection con = dbcon.ConnectDB();
		
		InwardFormBean inBean=new InwardFormBean();
		
		int slno=1;

		try
		{
			stmt=con.createStatement();
			sql="select sub_awbno,awbno,zipcode,destination,pcs,create_date from sub_awbno order by slno DESC";
			rs=stmt.executeQuery(sql);
			
			while(rs.next())
			{
				inBean.setSlno(slno);
				inBean.setAwbNo(rs.getString("awbno"));
				inBean.setSubAwbNo(rs.getString("sub_awbno"));
				inBean.setDestination(rs.getString("destination"));
				inBean.setPcs(rs.getInt("pcs"));
				inBean.setZipcode(rs.getInt("zipcode"));
				inBean.setReceiveDate(date.format(rs.getDate("create_date")));
				
				tabledata_SubAwb.add(new SubAwbInwardTableBean(inBean.getSlno(), inBean.getSubAwbNo(), inBean.getDestination(),
						inBean.getAwbNo(), inBean.getReceiveDate(), inBean.getZipcode(), inBean.getPcs()));
					
				slno++;
			}
		
			tableSubAwbInward.setItems(tabledata_SubAwb);
			
			
			pagination_SubAwbInward.setPageCount((tabledata_SubAwb.size() / rowsPerPage() + 1));
			pagination_SubAwbInward.setCurrentPageIndex(0);
			pagination_SubAwbInward.setPageFactory((Integer pageIndex) -> createPage_SubAwbInward(pageIndex));
			
			
		}
	
		catch (Exception e)
		{
			System.out.println(e);
		}
		finally
		{
			dbcon.disconnect(null, stmt, rs, con);
		}
	}	
		*/
	
// ============ Code for paginatation =====================================================
		
	public int itemsPerPage()
	{
		return 1;
	}

	public int rowsPerPage() 
	{
		return 10;
	}

	public GridPane createPage_AwbInward(int pageIndex)
	{
		int lastIndex = 0;
		
		GridPane pane=new GridPane();
		int displace = tabledata_Awb.size() % rowsPerPage();
            
			if (displace >= 0)
			{
				lastIndex = tabledata_Awb.size() / rowsPerPage();
			}
	        
			int page = pageIndex * itemsPerPage();
	        for (int i = page; i < page + itemsPerPage(); i++)
	        {
	            if (lastIndex == pageIndex)
	            {
	            	tableAwbInward.setItems(FXCollections.observableArrayList(tabledata_Awb.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
	            }
	            else
	            {
	            	tableAwbInward.setItems(FXCollections.observableArrayList(tabledata_Awb.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
	            }
	        }
	       return pane;
	      
	    }	
	
	
/*	public GridPane createPage_SubAwbInward(int pageIndex)
	{
		int lastIndex = 0;
		
		GridPane pane=new GridPane();
		int displace = tabledata_SubAwb.size() % rowsPerPage();
            
			if (displace >= 0)
			{
				lastIndex = tabledata_SubAwb.size() / rowsPerPage();
			}
	        
			int page = pageIndex * itemsPerPage();
	        for (int i = page; i < page + itemsPerPage(); i++)
	        {
	            if (lastIndex == pageIndex)
	            {
	            	tableSubAwbInward.setItems(FXCollections.observableArrayList(tabledata_SubAwb.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + displace)));
	            }
	            else
	            {
	            	tableSubAwbInward.setItems(FXCollections.observableArrayList(tabledata_SubAwb.subList(pageIndex * rowsPerPage(), pageIndex * rowsPerPage() + rowsPerPage())));
	            }
	        }
	       return pane;
	      
	    }*/
	
	
	
// ========================================================================================		
	
	@FXML
	public void exportToExcel_AwbInward() throws SQLException, IOException
	{
			
		HSSFWorkbook workbook=new HSSFWorkbook();
		HSSFSheet sheet=workbook.createSheet("Awb Inwards");
		HSSFRow rowhead=sheet.createRow(0);
		
		rowhead.createCell(0).setCellValue("Serial No");
		rowhead.createCell(1).setCellValue("Awb No.");
		rowhead.createCell(2).setCellValue("Booking Date");
		
		rowhead.createCell(3).setCellValue("Client");
		rowhead.createCell(4).setCellValue("Destination");
		rowhead.createCell(5).setCellValue("Zipcode");

		rowhead.createCell(6).setCellValue("PCS");
		rowhead.createCell(7).setCellValue("Weight");
		rowhead.createCell(8).setCellValue("Sub Awb Status");
			
		int i=2;
			
		HSSFRow row;
			
		for(AwbInwardTableBean awbBean: tabledata_Awb)
		{
			row = sheet.createRow((short) i);
		
			row.createCell(0).setCellValue(awbBean.getSlno());
			row.createCell(1).setCellValue(awbBean.getAwbNo());
			row.createCell(2).setCellValue(awbBean.getBookingDate());
			
			row.createCell(3).setCellValue(awbBean.getClientCode());
			row.createCell(4).setCellValue(awbBean.getDestination());
			row.createCell(5).setCellValue(awbBean.getZipcode());
			
			row.createCell(6).setCellValue(awbBean.getPcs());
			row.createCell(7).setCellValue(awbBean.getWeight());
			row.createCell(8).setCellValue(awbBean.getSubAwbnoStatus());
			
			i++;
		}
		
		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files","*.xls"));
		File file = fc.showSaveDialog(null);
		FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
		//System.out.println("file chooser: "+file.getAbsoluteFile());
		
		workbook.write(fileOut);
		fileOut.close();
				
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Export to excel alert");
		alert.setHeaderText(null);
		alert.setContentText("File "+file.getName()+" successfully saved");
		alert.showAndWait();
		
		   
	}	
	
		
		
// ========================================================================================	
				
	@FXML
	public void exportToExcel_SubAwbInward() throws SQLException, IOException
	{
			
		/*HSSFWorkbook workbook=new HSSFWorkbook();
		HSSFSheet sheet=workbook.createSheet("Sub Awb Inwards");
		HSSFRow rowhead=sheet.createRow(0);
		
		rowhead.createCell(0).setCellValue("Serial No");
		rowhead.createCell(1).setCellValue("Awb No.");
		rowhead.createCell(2).setCellValue("Sub Awb No.");
		
		rowhead.createCell(3).setCellValue("Zipcode");
		rowhead.createCell(4).setCellValue("Destination");
		rowhead.createCell(5).setCellValue("PCS");
		rowhead.createCell(6).setCellValue("Received Date");
	
		int i=2;
				
		HSSFRow row;
			
		for(SubAwbInwardTableBean subAwbBean: tabledata_SubAwb)
		{
			row = sheet.createRow((short) i);
		
			row.createCell(0).setCellValue(subAwbBean.getSlno());
			row.createCell(1).setCellValue(subAwbBean.getAwbNo());
			row.createCell(2).setCellValue(subAwbBean.getSubAwbNo());
			row.createCell(3).setCellValue(subAwbBean.getZipcode());
			
			row.createCell(4).setCellValue(subAwbBean.getDestination());
			row.createCell(5).setCellValue(subAwbBean.getPcs());
			row.createCell(6).setCellValue(subAwbBean.getReceiveDate());
			
			
			i++;
		}
		
		
		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("Excel Files","*.xls"));
		File file = fc.showSaveDialog(null);
		FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
		
		workbook.write(fileOut);
		fileOut.close();
			
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Export to excel alert");
		alert.setHeaderText(null);
		alert.setContentText("File "+file.getName()+" successfully saved");
		alert.showAndWait();
		*/
		   
	}		


// =============================================================================================	

	public void exit() throws IOException, SQLException
	{
		Main m=new Main();
		m.homePage();
	}
	
// =============================================================================================	

		
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		txtSubAwbNo.setDisable(true);
		//checkPcsCount();
		
		LoadCity loadCity=new  LoadCity();
		
		lblUnsavedSubAwb.setVisible(false);
		listView_UnsavedSubAwb.setVisible(false);
		
		//txtDestination.setStyle("-fx-faint-focus-color: red;");
		//txtDestination.setStyle("");
		
		
		imgSearchIcon_AwbInward.setImage(new Image("/com/onesoft/courier/icon/searchicon_blue.png"));
		imgSearchIcon_SubAwbInward.setImage(new Image("/com/onesoft/courier/icon/searchicon_blue.png"));
		
		
		subAwbInward_tableCol_AwbNo.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<SubAwbInwardTableBean,String>, ObservableValue<String>>() {
			
			@Override
			public ObservableValue<String> call(CellDataFeatures<SubAwbInwardTableBean, String> param) {
				// TODO Auto-generated method stub
				return param.getValue().getValue().getAwbNo();
			}
		});

		subAwbInward_tableCol_Slno.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<SubAwbInwardTableBean,Number>, ObservableValue<Number>>() {
			
			@Override
			public ObservableValue<Number> call(CellDataFeatures<SubAwbInwardTableBean, Number> param) {
				// TODO Auto-generated method stub
				return param.getValue().getValue().getSlno();
			}
		});
		

	
	subAwbInward_tableCol_SubAwbNo.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<SubAwbInwardTableBean,String>, ObservableValue<String>>() {
		
		@Override
		public ObservableValue<String> call(CellDataFeatures<SubAwbInwardTableBean, String> param) {
			// TODO Auto-generated method stub
			return param.getValue().getValue().getSubAwbNo();
		}
	});
	
	subAwbInward_tableCol_ReceiveDate.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<SubAwbInwardTableBean,String>, ObservableValue<String>>() {
		
		@Override
		public ObservableValue<String> call(CellDataFeatures<SubAwbInwardTableBean, String> param) {
			// TODO Auto-generated method stub
			return param.getValue().getValue().getBookingDate();
					
		}
	});
	
	subAwbInward_tableCol_Destination.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<SubAwbInwardTableBean,String>, ObservableValue<String>>() {
		
		@Override
		public ObservableValue<String> call(CellDataFeatures<SubAwbInwardTableBean, String> param) {
			// TODO Auto-generated method stub
			return param.getValue().getValue().getDestination();
		}
	});
	
	
subAwbInward_tableCol_SubAwbStatus.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<SubAwbInwardTableBean,String>, ObservableValue<String>>() {
		
		@Override
		public ObservableValue<String> call(CellDataFeatures<SubAwbInwardTableBean, String> param) {
			// TODO Auto-generated method stub
			return param.getValue().getValue().getSubAwbnoStatus();
		}
	});
	
subAwbInward_tableCol_Pcs.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<SubAwbInwardTableBean,Number>, ObservableValue<Number>>() {
	
	@Override
	public ObservableValue<Number> call(CellDataFeatures<SubAwbInwardTableBean, Number> param) {
		// TODO Auto-generated method stub
		return param.getValue().getValue().getPcs();
	}
});

subAwbInward_tableCol_Zipcode.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<SubAwbInwardTableBean,Number>, ObservableValue<Number>>() {
	
	@Override
	public ObservableValue<Number> call(CellDataFeatures<SubAwbInwardTableBean, Number> param) {
		// TODO Auto-generated method stub
		return param.getValue().getValue().getZipcode();
	}
});
	
subAwbInward_tableCol_Weight.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<SubAwbInwardTableBean,Number>, ObservableValue<Number>>() {
	
	@Override
	public ObservableValue<Number> call(CellDataFeatures<SubAwbInwardTableBean, Number> param) {
		// TODO Auto-generated method stub
		return param.getValue().getValue().getWeight();
	}
});
		
		
		/*subAwbInward_tableCol_Slno.setCellValueFactory(new PropertyValueFactory<SubAwbInwardTableBean,Integer>("slno"));
		subAwbInward_tableCol_AwbNo.setCellValueFactory(new PropertyValueFactory<SubAwbInwardTableBean,String>("awbNo"));
		subAwbInward_tableCol_SubAwbNo.setCellValueFactory(new PropertyValueFactory<SubAwbInwardTableBean,String>("subAwbNo"));
		subAwbInward_tableCol_Destination.setCellValueFactory(new PropertyValueFactory<SubAwbInwardTableBean,String>("destination"));
		subAwbInward_tableCol_Zipcode.setCellValueFactory(new PropertyValueFactory<SubAwbInwardTableBean,Integer>("zipcode"));
		subAwbInward_tableCol_Pcs.setCellValueFactory(new PropertyValueFactory<SubAwbInwardTableBean,Integer>("pcs"));
		subAwbInward_tableCol_ReceiveDate.setCellValueFactory(new PropertyValueFactory<SubAwbInwardTableBean,String>("receiveDate"));*/
		
		
		awbInward_tableCol_Slno.setCellValueFactory(new PropertyValueFactory<AwbInwardTableBean,Integer>("slno"));
		awbInward_tableCol_AwbNo.setCellValueFactory(new PropertyValueFactory<AwbInwardTableBean,String>("awbNo"));
		awbInward_tableCol_BookingDate.setCellValueFactory(new PropertyValueFactory<AwbInwardTableBean,String>("bookingDate"));
		awbInward_tableCol_Client.setCellValueFactory(new PropertyValueFactory<AwbInwardTableBean,String>("clientCode"));
		awbInward_tableCol_Destination.setCellValueFactory(new PropertyValueFactory<AwbInwardTableBean,String>("destination"));
		awbInward_tableCol_Zipcode.setCellValueFactory(new PropertyValueFactory<AwbInwardTableBean,Integer>("zipcode"));
		awbInward_tableCol_Pcs.setCellValueFactory(new PropertyValueFactory<AwbInwardTableBean,Integer>("pcs"));
		awbInward_tableCol_Weight.setCellValueFactory(new PropertyValueFactory<AwbInwardTableBean,Double>("weight"));
		awbInward_tableCol_SubAwbStatus.setCellValueFactory(new PropertyValueFactory<AwbInwardTableBean,String>("subAwbnoStatus"));
		
		
	
		try
		{
			loadCity.loadZipcodeCityDetails();
			
			loadClients();
			loadAwbInwardTable();
		//	loadSubAwbInwardTable();
			
			
			
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	
	

}
