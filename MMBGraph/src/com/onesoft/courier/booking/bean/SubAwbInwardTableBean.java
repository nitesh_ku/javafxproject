package com.onesoft.courier.booking.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public class SubAwbInwardTableBean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty clientName;
	private SimpleStringProperty clientCode;
	private SimpleIntegerProperty zipcode;
	private SimpleStringProperty destination;
	private SimpleIntegerProperty pcs;
	private SimpleDoubleProperty weight;
	private SimpleStringProperty awbNo;
	private SimpleStringProperty subAwbNo;
	
	private SimpleStringProperty receiveDate;
	private SimpleStringProperty bookingDate;
	
	
	private SimpleStringProperty subAwb_Prefix;
	private SimpleLongProperty subAwb_suffix;
	private SimpleStringProperty subAwbnoStatus;
	private SimpleStringProperty loggedInUser;
	private SimpleStringProperty unSavedSubAwbNo;
	
	
	
	public SubAwbInwardTableBean(int slno,String client,String destination,String awbno,String sub_Awbno, String bookingDate, String sub_awb_status,
			int zipcode,int pcs, double weight)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.awbNo=new SimpleStringProperty(awbno);
		this.subAwbNo=new SimpleStringProperty(sub_Awbno);
		this.bookingDate=new SimpleStringProperty(bookingDate);
		this.subAwbnoStatus=new SimpleStringProperty(sub_awb_status);
		this.clientCode=new SimpleStringProperty(client);
		this.zipcode=new SimpleIntegerProperty(zipcode);
		this.destination=new SimpleStringProperty(destination);
		this.pcs=new SimpleIntegerProperty(pcs);
		this.weight=new SimpleDoubleProperty(weight);
	}
	
	
/*	public SubAwbInwardTableBean(int slno,String subAwbNo,String destination,String awbno, String receiveDate,int zipcode,int pcs)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.awbNo=new SimpleStringProperty(awbno);
		this.receiveDate=new SimpleStringProperty(receiveDate);
		this.subAwbNo=new SimpleStringProperty(subAwbNo);
		this.zipcode=new SimpleIntegerProperty(zipcode);
		this.destination=new SimpleStringProperty(destination);
		this.pcs=new SimpleIntegerProperty(pcs);
		
	}*/
	
	public SimpleStringProperty getReceiveDate() {
		return receiveDate;
	}
	public void setReceiveDate(SimpleStringProperty receiveDate) {
		this.receiveDate = receiveDate;
	}
	
	public SimpleStringProperty getClientName() {
		return clientName;
	}
	public void setClientName(SimpleStringProperty clientName) {
		this.clientName = clientName;
	}
	public SimpleStringProperty getClientCode() {
		return clientCode;
	}
	public void setClientCode(SimpleStringProperty clientCode) {
		this.clientCode = clientCode;
	}
	public SimpleIntegerProperty getZipcode() {
		return zipcode;
	}
	public void setZipcode(SimpleIntegerProperty zipcode) {
		this.zipcode = zipcode;
	}
	public SimpleStringProperty getDestination() {
		return destination;
	}
	public void setDestination(SimpleStringProperty destination) {
		this.destination = destination;
	}
	public SimpleIntegerProperty getPcs() {
		return pcs;
	}
	public void setPcs(SimpleIntegerProperty pcs) {
		this.pcs = pcs;
	}
	public SimpleDoubleProperty getWeight() {
		return weight;
	}
	public void setWeight(SimpleDoubleProperty weight) {
		this.weight = weight;
	}
	public SimpleStringProperty getAwbNo() {
		return awbNo;
	}
	public void setAwbNo(SimpleStringProperty awbNo) {
		this.awbNo = awbNo;
	}
	public SimpleStringProperty getSubAwbNo() {
		return subAwbNo;
	}
	public void setSubAwbNo(SimpleStringProperty subAwbNo) {
		this.subAwbNo = subAwbNo;
	}
	public String getSubAwb_Prefix() {
		return subAwb_Prefix.get();
	}
	public void setSubAwb_Prefix(SimpleStringProperty subAwb_Prefix) {
		this.subAwb_Prefix = subAwb_Prefix;
	}
	public SimpleLongProperty getSubAwb_suffix() {
		return subAwb_suffix;
	}
	public void setSubAwb_suffix(SimpleLongProperty subAwb_suffix) {
		this.subAwb_suffix = subAwb_suffix;
	}
	public SimpleStringProperty getSubAwbnoStatus() {
		return subAwbnoStatus;
	}
	public void setSubAwbnoStatus(SimpleStringProperty subAwbnoStatus) {
		this.subAwbnoStatus = subAwbnoStatus;
	}
	public String getLoggedInUser() {
		return loggedInUser.get();
	}
	public void setLoggedInUser(SimpleStringProperty loggedInUser) {
		this.loggedInUser = loggedInUser;
	}
	public String getUnSavedSubAwbNo() {
		return unSavedSubAwbNo.get();
	}
	public void setUnSavedSubAwbNo(SimpleStringProperty unSavedSubAwbNo) {
		this.unSavedSubAwbNo = unSavedSubAwbNo;
	}
	public SimpleIntegerProperty getSlno() {
		return slno;
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}


	public SimpleStringProperty getBookingDate() {
		return bookingDate;
	}


	public void setBookingDate(SimpleStringProperty bookingDate) {
		this.bookingDate = bookingDate;
	}
	
	
	

}
