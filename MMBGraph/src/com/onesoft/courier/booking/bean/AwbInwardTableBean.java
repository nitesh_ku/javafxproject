package com.onesoft.courier.booking.bean;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public class AwbInwardTableBean {
	
	private SimpleIntegerProperty slno;
	private SimpleStringProperty clientName;
	private SimpleStringProperty clientCode;
	private SimpleIntegerProperty zipcode;
	private SimpleStringProperty destination;
	private SimpleIntegerProperty pcs;
	private SimpleDoubleProperty weight;
	private SimpleStringProperty awbNo;
	private SimpleStringProperty subAwbNo;
	private SimpleStringProperty bookingDate;
	private SimpleStringProperty subAwb_Prefix;
	private SimpleLongProperty subAwb_suffix;
	private SimpleStringProperty subAwbnoStatus;
	private SimpleStringProperty loggedInUser;
	private SimpleStringProperty unSavedSubAwbNo;
	
	
	
	
	public AwbInwardTableBean(int slno,String client,String destination,String awbno, String bookingDate, String sub_awb_status,
			int zipcode,int pcs, double weight)
	{
		this.slno=new SimpleIntegerProperty(slno);
		this.awbNo=new SimpleStringProperty(awbno);
		this.bookingDate=new SimpleStringProperty(bookingDate);
		this.subAwbnoStatus=new SimpleStringProperty(sub_awb_status);
		this.clientCode=new SimpleStringProperty(client);
		this.zipcode=new SimpleIntegerProperty(zipcode);
		this.destination=new SimpleStringProperty(destination);
		this.pcs=new SimpleIntegerProperty(pcs);
		this.weight=new SimpleDoubleProperty(weight);
	}
	
	
	public String getBookingDate() {
		return bookingDate.get();
	}
	public void setBookingDate(SimpleStringProperty bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getClientName() {
		return clientName.get();
	}
	public void setClientName(SimpleStringProperty clientName) {
		this.clientName = clientName;
	}
	public String getClientCode() {
		return clientCode.get();
	}
	public void setClientCode(SimpleStringProperty clientCode) {
		this.clientCode = clientCode;
	}
	public int getZipcode() {
		return zipcode.get();
	}
	public void setZipcode(SimpleIntegerProperty zipcode) {
		this.zipcode = zipcode;
	}
	public String getDestination() {
		return destination.get();
	}
	public void setDestination(SimpleStringProperty destination) {
		this.destination = destination;
	}
	public int getPcs() {
		return pcs.get();
	}
	public void setPcs(SimpleIntegerProperty pcs) {
		this.pcs = pcs;
	}
	public double getWeight() {
		return weight.get();
	}
	public void setWeight(SimpleDoubleProperty weight) {
		this.weight = weight;
	}
	public String getAwbNo() {
		return awbNo.get();
	}
	public void setAwbNo(SimpleStringProperty awbNo) {
		this.awbNo = awbNo;
	}
	public String getSubAwbNo() {
		return subAwbNo.get();
	}
	public void setSubAwbNo(SimpleStringProperty subAwbNo) {
		this.subAwbNo = subAwbNo;
	}
	public String getSubAwb_Prefix() {
		return subAwb_Prefix.get();
	}
	public void setSubAwb_Prefix(SimpleStringProperty subAwb_Prefix) {
		this.subAwb_Prefix = subAwb_Prefix;
	}
	public SimpleLongProperty getSubAwb_suffix() {
		return subAwb_suffix;
	}
	public void setSubAwb_suffix(SimpleLongProperty subAwb_suffix) {
		this.subAwb_suffix = subAwb_suffix;
	}
	public String getSubAwbnoStatus() {
		return subAwbnoStatus.get();
	}
	public void setSubAwbnoStatus(SimpleStringProperty subAwbnoStatus) {
		this.subAwbnoStatus = subAwbnoStatus;
	}
	public String getLoggedInUser() {
		return loggedInUser.get();
	}
	public void setLoggedInUser(SimpleStringProperty loggedInUser) {
		this.loggedInUser = loggedInUser;
	}
	public String getUnSavedSubAwbNo() {
		return unSavedSubAwbNo.get();
	}
	public void setUnSavedSubAwbNo(SimpleStringProperty unSavedSubAwbNo) {
		this.unSavedSubAwbNo = unSavedSubAwbNo;
	}
	public int getSlno() {
		return slno.get();
	}
	public void setSlno(SimpleIntegerProperty slno) {
		this.slno = slno;
	}
	
	
	
	

}
