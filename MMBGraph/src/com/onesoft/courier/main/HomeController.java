package com.onesoft.courier.main;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.onesoft.courier.alert.AlertBoxController;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;

public class HomeController implements Initializable {
		
	
	
	@FXML
	private MenuItem mItemAlertBox;
	
	@FXML
	private MenuItem mItemSalesReport;

	@FXML
	private MenuItem mItemZoneReport;
	
	@FXML
	private MenuItem mItemNetworkReport;
	
	@FXML
	private MenuItem mItemTop10CityReport;

	@FXML
	private MenuItem mItemServiceGroupReport;
	
	@FXML
	private MenuItem mItemSalesYearlyComparison;
	
	@FXML
	private MenuItem mItemZoneYearlyComparison;
	
	@FXML
	private MenuItem mItemNetworkYearlyComparison;

	@FXML
	private MenuItem mItemTop10CityYearlyComparison;
	
	@FXML
	private MenuItem mItemServiceGroupYearlyComparison;
	
	@FXML
	private MenuItem mGenerateInvoice;
	
	@FXML
	private MenuItem mPaymentReceiving;
	
	@FXML
	private MenuItem mComplaintRegister;
	
	@FXML
	private MenuItem mPickUpForm;
	
	@FXML
	private MenuItem mItemExpenses;
	
	@FXML
	private MenuItem mItemLedger;
	
	@FXML
	private MenuItem mItemBalanceSheet;
	
	@FXML
	private MenuItem mItemIncomingStock;
	
	@FXML
	private MenuItem mItemStockAllotment;
	
	@FXML
	private MenuItem mItemReIssueStock;
	
	@FXML
	private MenuItem mItemInwardForm;
	
	
	//-------------------- Normal Reports --------------------
	
	Main m=new Main();
	
	@FXML
	private void goAlertBox() throws IOException
	{
		m.alertbox();
	}
	
	
	@FXML
	private void goSalesReport() throws IOException
	{
		m.showSalesReport();
	}
	
	@FXML
	private void goZoneReport() throws IOException
	{
		m.showZoneReport();
	}
	
	@FXML
	private void goNetworkReport() throws IOException
	{
		m.showNetworkReport();
	}
	
	@FXML
	private void goTop10CityReport() throws IOException
	{
		m.showTopCityReport();
	}
	
	@FXML
	private void goServiceGroupReport() throws IOException
	{
		m.showServiceGroupReport();
	}
	
	//-------------------- Comparison Reports --------------------

	@FXML
	private void goCompareSales() throws IOException
	{
		m.compareSales();
	}
	
	@FXML
	private void goCompareZones() throws IOException
	{
		m.compareZone();
	}
	
	@FXML
	private void goCompareNetworks() throws IOException
	{
		m.compareNetwork();
	}
	
	@FXML
	private void goCompareTopCity() throws IOException
	{
		m.compareTopCity();
	}
	
	@FXML
	private void goCompareServiceGroups() throws IOException
	{
		m.compareServiceGroup();
	}
	
	@FXML
	private void goInvoice() throws IOException
	{
		
		m.showInvoice();
	}
	
	@FXML
	private void goPaymentReceiving() throws IOException
	{
		
		m.showPaymentReceiving();
	}
	
	@FXML
	private void goComplaintRegister() throws IOException
	{
		
		m.showComplaintRegister();
	}
	
	@FXML
	private void goPickupForm() throws IOException
	{
		
		m.showPickupForm();
	}
	
	@FXML
	private void goExpensesForm() throws IOException
	{
		
		m.showExpensesForm();
	}
	
	@FXML
	private void goLedgerForm() throws IOException
	{
		
		m.showLedgerForm();
	}
	
	@FXML
	private void goBalanceSheetForm() throws IOException
	{
		m.showBalanceSheetForm();
	}
	
	@FXML
	private void goIncomingStockForm() throws IOException
	{
		
		m.showIncomingStockForm();
	}
	
	@FXML
	private void goStockAllotmentForm() throws IOException
	{
		m.showStockAllotmentForm();
	}
	
	@FXML
	private void goReIssueStockForm() throws IOException
	{
		m.showReIssueStockForm();
	}
	
	@FXML
	private void goInwardForm() throws IOException
	{
		m.showInwardForm();
	}
	
	
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
	
		
		
	}

}
