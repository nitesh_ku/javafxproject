package com.onesoft.courier.main;
	
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.imageio.ImageIO;

import com.DB.DBconnection;
import com.onesoft.courier.alert.AlertBoxBean;
import com.onesoft.courier.alert.AlertBoxController;
import com.onesoft.courier.alert.AlertBoxTableBean;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
	
	public static Stage primaryStage;
	private static BorderPane mainLayout;
	Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
	int count=0;
	//AlertBox albox=new AlertBox();
	
	@Override
	public void start(Stage primaryStage) throws IOException, SQLException {
		
		this.primaryStage =primaryStage;
		this.primaryStage.setTitle("MMB Graphs");
		homePage();
		
		/*try {
			BorderPane root = new BorderPane();
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}*/
	}
	
public void homePage() throws IOException, SQLException {
		
		// TODO Auto-generated method stub
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("Home.fxml"));
		mainLayout = loader.load();
		Scene scene = new Scene(mainLayout);
		
		// mainLayout.setStyle("-fx-background-color: cornsilk;");
		primaryStage.setMaximized(true);
		//primaryStage.setMaximized(true);
		primaryStage.setScene(scene);
		primaryStage.setOnCloseRequest(e -> Platform.exit());
		//primaryStage.setResizable(false);
		primaryStage.show();
		dashBoard();
		loadUnpaidInvoiceDate();
		/*if(count>0 && AlertBoxController.checkAlert==true)
		{
		alertbox();
		}*/
		
	}

	public void alertbox() throws IOException
	{
		
		primaryStage=new Stage();
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/alert/AlertBox.fxml"));
		BorderPane bpZoneReport=loader.load();
		//Stage stage=new Stage();
		
		Scene sc=new Scene(bpZoneReport);
		

		/*stage.initOwner(primaryStage);
		stage.setTitle("Pending Invoice Payment Alert");
		stage.setX(primaryScreenBounds.getMaxX()-565);
		stage.setY(primaryScreenBounds.getMaxY()-285);
		stage.setResizable(false);
	//	stage.initModality(Modality.APPLICATION_MODAL);
		stage.setScene(sc);
		stage.showAndWait();
		primaryStage.focusedProperty();*/
		
		
		
		primaryStage.initModality(Modality.APPLICATION_MODAL);
		//primaryStage.alwaysOnTopProperty();
		
		primaryStage.setScene(sc);
		primaryStage.setTitle("Pending Invoice Payment Alert");
		primaryStage.setX(primaryScreenBounds.getMaxX()-565);
		primaryStage.setY(primaryScreenBounds.getMaxY()-285);
		primaryStage.setResizable(false);
		primaryStage.show();
		
	}

	
	public void showLogin() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/login/controller/Login.fxml"));
		BorderPane bpZoneReport=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpZoneReport);
		
	}

	public void dashBoard() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/graphs/controller/DashBoard.fxml"));
		BorderPane bpZoneReport=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpZoneReport);
		//alertbox();
		
	}

	public void showSalesReport() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/graphs/controller/SalesReport.fxml"));
		BorderPane bpSalesReport=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpSalesReport);
		
	}

	public void showZoneReport() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/graphs/controller/ZoneReport.fxml"));
		BorderPane bpZoneReport=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpZoneReport);
		
	}

	public void showNetworkReport() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/graphs/controller/NetworkReport.fxml"));
		BorderPane bpNetworkReport=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpNetworkReport);
		
	}

	public void showTopCityReport() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/graphs/controller/Top10CityReport.fxml"));
		BorderPane bpTopcityReport=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpTopcityReport);
		
	}

	public void showServiceGroupReport() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/graphs/controller/ServiceGroupReport.fxml"));
		BorderPane bpServiceGroupReport=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpServiceGroupReport);
	}

	public void compareSales() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/graphs/controller/SalesYearlyCompare.fxml"));
		BorderPane bpCompareSalesReport=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpCompareSalesReport);
	}

	public void compareZone() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/graphs/controller/ZoneYearlyCompare.fxml"));
		BorderPane bpComparezoneReport=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpComparezoneReport);
	}

	public void compareNetwork() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/graphs/controller/NetworkYearlyCompare.fxml"));
		BorderPane bpCompareNetworkReport=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpCompareNetworkReport);
	}

	public void compareTopCity() throws IOException
	{	
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/graphs/controller/Top10CityYearlyCompare.fxml"));
		BorderPane bpCompareTopCityReport=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpCompareTopCityReport);
	}

	public void compareServiceGroup() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/graphs/controller/ServiceGroupYearlyCompare.fxml"));
		BorderPane bpCompareServiceGroupReport=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpCompareServiceGroupReport);
	}

	public void showInvoice() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/invoice/controller/Invoice.fxml"));
		BorderPane bpInvoice=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpInvoice);	
	}

	public void showPaymentReceiving() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/payment/controller/PaymentReceived.fxml"));
		BorderPane bpInvoice=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpInvoice);
	}

	public void showComplaintRegister() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/complaint/controller/ComplaintRegister.fxml"));
		BorderPane bpInvoice=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpInvoice);
	}
	
	public void showPickupForm() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/pickup/controller/PickUp.fxml"));
		BorderPane bpInvoice=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpInvoice);
	}
	
	public void showExpensesForm() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/expenses/controller/ExpensesEntry.fxml"));
		BorderPane bpInvoice=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpInvoice);
	}
	
	
	public void showLedgerForm() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/account/controller/Ledger.fxml"));
		BorderPane bpInvoice=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpInvoice);
	}
	
	
	public void showBalanceSheetForm() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/account/controller/BalanceSheet.fxml"));
		BorderPane bpInvoice=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpInvoice);
	}
	
	
	public void showIncomingStockForm() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/stockdetail/controller/IncomingStock.fxml"));
		BorderPane bpInvoice=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpInvoice);
	}
	
	
	public void showStockAllotmentForm() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/stockdetail/controller/StockAllotment.fxml"));
		BorderPane bpInvoice=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpInvoice);
	}
	
	
	public void showReIssueStockForm() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/stockdetail/controller/ReIssueStock.fxml"));
		BorderPane bpInvoice=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpInvoice);
	}
	
	public void showInwardForm() throws IOException
	{
		FXMLLoader loader=new FXMLLoader();
		loader.setLocation(Main.class.getResource("/com/onesoft/courier/booking/controller/InwardFrom.fxml"));
		BorderPane bpInvoice=loader.load();
		//primaryStage.setMaximized(true);
		mainLayout.setCenter(bpInvoice);
	}
	
	public void loadUnpaidInvoiceDate() throws SQLException
	{
	
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();
			
		ResultSet rs=null;
		Statement st=null;
		String finalsql=null;
		
		try
		{
			st=con.createStatement();
			finalsql="select invoice_number,invoice_date,clientname,grandtotal,due,amtreceived from invoice where status !='P' order by invoice_number";
			//System.out.println(finalsql);
			rs=st.executeQuery(finalsql);
		
			if(!rs.next())
			{
				
			}
			else
			{
				do
				{
					count++;
					
				}while(rs.next());
			}
			
			System.out.println("Count is: "+count);
			
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		finally
		{	
			dbcon.disconnect(null, st, rs, con);
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
