package com.onesoft.courier.main;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;

import javafx.geometry.Rectangle2D;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Screen;
import javafx.stage.Window;

public class AlertBox {
	
	
	public void openAlertBox()
	{
		 Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
		 
		 System.out.println("Max X: "+primaryScreenBounds.getMaxX());
		 System.out.println("Max Y: "+primaryScreenBounds.getMaxY());
		 System.out.println("Min X: "+primaryScreenBounds.getMinX());
		 System.out.println("Min Y: "+primaryScreenBounds.getMinY());
		 System.out.println("Width: "+primaryScreenBounds.getWidth());
		 System.out.println("Height: "+primaryScreenBounds.getHeight());
	
		 
		 Alert alert = new Alert(AlertType.ERROR);
		 alert.setTitle("Exception Dialog");
		 alert.setHeaderText("Look, an Exception Dialog");
		 alert.setContentText("Could not find file blabla.txt!");

		 Exception ex = new FileNotFoundException("Could not find file blabla.txt");

		 
		 
		 // Create expandable Exception.
		 StringWriter sw = new StringWriter();
		 PrintWriter pw = new PrintWriter(sw);
		 ex.printStackTrace(pw);
		 String exceptionText = sw.toString();

		 Label label = new Label("The exception stacktrace was:");

		 TextArea textArea = new TextArea(exceptionText);
		 textArea.setEditable(false);
		 textArea.setWrapText(true);

		 textArea.setMaxWidth(280);
		 textArea.setMaxHeight(60);
		 GridPane.setVgrow(textArea, Priority.ALWAYS);
		 GridPane.setHgrow(textArea, Priority.ALWAYS);

		 GridPane expContent = new GridPane();
		 expContent.setMaxWidth(Double.MAX_VALUE);
		 expContent.add(label, 0, 0);
		 expContent.add(textArea, 0, 1);

		 // Set expandable Exception into the dialog pane.
		 alert.getDialogPane().setExpandableContent(expContent);
		 alert.setX(primaryScreenBounds.getMaxX()-380);
			alert.setY(primaryScreenBounds.getMaxY()-250);
		 alert.showAndWait();
		 
		 
		/*Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Dialog");
		alert.setHeaderText("Look, a Confirmation Dialog");
		alert.setContentText("Are you ok with this?");
		alert.setX(primaryScreenBounds.getMaxX()-380);
		alert.setY(primaryScreenBounds.getMaxY()-180);
		
		
		
		//alert.

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
		    // ... user chose OK
		} else {
		    // ... user chose CANCEL or closed the dialog
		}
	}*/
	}
	

}
