package test.stackBarChart;

public class ClientVsZoneBean
{

	private int serialno;
	private String zoneCode;
	private String clientCode;
	private String serviceCode;
	private int pcs;
	private double billingWeight;
	private double basicAmount;
	private double insuranceAmount;
	private double docketCharge;
	private double fuelAmount;
	private double gst_amount;
	private double vas_total;
	private double total;

	
	public String getZoneCode() {
		return zoneCode;
	}
	public void setZoneCode(String zoneCode) {
		this.zoneCode = zoneCode;
	}
	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public double getBasicAmount() {
		return basicAmount;
	}
	public void setBasicAmount(double basicAmount) {
		this.basicAmount = basicAmount;
	}
	
	public double getDocketCharge() {
		return docketCharge;
	}
	public void setDocketCharge(double docketCharge) {
		this.docketCharge = docketCharge;
	}
	public double getFuelAmount() {
		return fuelAmount;
	}
	public void setFuelAmount(double fuelAmount) {
		this.fuelAmount = fuelAmount;
	}
	public double getGst_amount() {
		return gst_amount;
	}
	public void setGst_amount(double gst_amount) {
		this.gst_amount = gst_amount;
	}
	public double getVas_total() {
		return vas_total;
	}
	public void setVas_total(double vas_total) {
		this.vas_total = vas_total;
	}
	public double getBillingWeight() {
		return billingWeight;
	}
	public void setBillingWeight(double billingWeight) {
		this.billingWeight = billingWeight;
	}
	public int getPcs() {
		return pcs;
	}
	public void setPcs(int pcs) {
		this.pcs = pcs;
	}
	public double getInsuranceAmount() {
		return insuranceAmount;
	}
	public void setInsuranceAmount(double insuranceAmount) {
		this.insuranceAmount = insuranceAmount;
	}
	public int getSerialno() {
		return serialno;
	}
	public void setSerialno(int serialno) {
		this.serialno = serialno;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	
	
}
