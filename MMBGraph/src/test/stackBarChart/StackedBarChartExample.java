package test.stackBarChart;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import java.util.Arrays;
import java.util.Date;

import com.DB.DBconnection;

import javafx.application.Application; 
import javafx.collections.FXCollections; 
import javafx.scene.Group; 
import javafx.scene.Scene; 
import javafx.scene.chart.CategoryAxis; 
import javafx.stage.Stage; 
import javafx.scene.chart.NumberAxis; 
import javafx.scene.chart.StackedBarChart; 
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType; 
         
public class StackedBarChartExample extends Application { 
   @Override 
   public void start(Stage stage) {     
	   
	   Date date=new Date();    
	   System.out.println("Time >>>>>>> "+new SimpleDateFormat("HH:mm:ss").format(date));
	   
	 //  System.out.println("Time >>>>>>>>> "+new SimpleDateFormat("HH:mm").format(date));
	   
      //Defining the axes               
      CategoryAxis xAxis = new CategoryAxis();    
      xAxis.setCategories(FXCollections.<String>observableArrayList(Arrays.asList
         ("Africa", "America", "Asia", "Europe", "Oceania"))); 
      
      xAxis.setLabel("category");
      NumberAxis yAxis = new NumberAxis(); 
      yAxis.setLabel("Population (In millions)");    
         
      //Creating the Bar chart 
      StackedBarChart<String, Number> stackedBarChart = 
         new StackedBarChart<>(xAxis, yAxis);         
      stackedBarChart.setTitle("Historic World Population by Region"); 
         
      //Prepare XYChart.Series objects by setting data 
      XYChart.Series<String, Number> series1 = new XYChart.Series<>();  
      series1.setName("1800"); 
      series1.getData().add(new XYChart.Data<>("Africa", 107)); 
      series1.getData().add(new XYChart.Data<>("America", 31));  
      series1.getData().add(new XYChart.Data<>("Asia", 635)); 
      series1.getData().add(new XYChart.Data<>("Europe", 203)); 
      series1.getData().add(new XYChart.Data<>("Oceania", 2)); 
         
      XYChart.Series<String, Number> series2 = new XYChart.Series<>(); 
      series2.setName("1900"); 
      series2.getData().add(new XYChart.Data<>("Africa", 133)); 
      series2.getData().add(new XYChart.Data<>("America", 156)); 
      series2.getData().add(new XYChart.Data<>("Asia", 947)); 
      series2.getData().add(new XYChart.Data<>("Europe", 408)); 
      series1.getData().add(new XYChart.Data<>("Oceania", 6));  
     
      XYChart.Series<String, Number> series3 = new XYChart.Series<>(); 
      series3.setName("2008"); 
      series3.getData().add(new XYChart.Data<>("Africa", 973)); 
      series3.getData().add(new XYChart.Data<>("America", 914)); 
      series3.getData().add(new XYChart.Data<>("Asia", 4054)); 
      series3.getData().add(new XYChart.Data<>("Europe", 732)); 
      series1.getData().add(new XYChart.Data<>("Oceania", 34)); 
         
      //Setting the data to bar chart
      stackedBarChart.getData().addAll(series1, series2, series3); 
         
      //Creating a Group object  
      Group root = new Group(stackedBarChart); 
         
      //Creating a scene object 
      Scene scene = new Scene(root, 600, 400);  
      
      //Setting title to the Stage 
      stage.setTitle("stackedBarChart"); 
         
      //Adding scene to the stage 
      stage.setScene(scene); 
         
      //Displaying the contents of the stage 
      stage.show();         
   } 
   public static void main(String args[]){ 
      launch(args); 
   } 
   
   
   
// ------------------------------------------------ Method for Bar Chart data ---------------------------------------
	
	public void showGraph(String sql) throws SQLException
	{
		XYChart.Series series1 = new XYChart.Series();
		
		DBconnection dbcon=new DBconnection();
		Connection con=dbcon.ConnectDB();

		Statement st=null;
		ResultSet rs=null;
		//	ZoneBean netbean=new ZoneBean();
		ClientVsZoneBean czBean=new ClientVsZoneBean();
		
		String currentZone=null;

		String finalsql=null;

		try
		{
			//barChar.setTitle("Networks report between" +stdate+" to "+edate);
			st=con.createStatement();

			finalsql="select zone_code,dailybookingtransaction2client, sum(total) from dailybookingtransaction where total is not null and zone_code is not null group by zone_code,dailybookingtransaction2client order by zone_code";
			
		/*	if(comboBoxClient.getValue().equals(GraphFilterUtils.ALL_CLIENT))
			{
				finalsql="select SUM(total) as total, dailybookingtransaction2client from dailybookingtransaction where "+sql+" group by dailybookingtransaction2client";
			}
			else
			{
				finalsql="select SUM(total) as total, zone_code from dailybookingtransaction where "+sql+" group by zone_code";
			}*/
			//finalsql="select SUM(total) as total, zone_code from dailybookingtransaction where dailybookingtransaction2client='55555' group by zone_code,total";

			/*String finalsql="select vnd.code,sum(dbt.total) as total from dailybookingtransaction as dbt, vendormaster as vnd, clientmaster as cm where dbt.dailybookingtransaction2network=vnd.code and dbt.dailybookingtransaction2client=cm.code "
						+ "and vnd.vendor_type='N' "+sql+" Group by vnd.code";	*/


			System.out.println(finalsql);
			rs=st.executeQuery(finalsql);

			if(!rs.next())
			{
				
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Database Alert");
				alert.setHeaderText(null);
				alert.setContentText("Data not found");
				alert.showAndWait();

			}
			else
			{	
				do
				{	

					
				}
				while(rs.next());

			
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}

		finally
		{
			dbcon.disconnect(null, st, rs, con);
		}
	}
}
